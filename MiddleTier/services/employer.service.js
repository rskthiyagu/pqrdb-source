var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


/* static product service class */
class EmployerService {
    static create(data) {
        if (data) {
            var name = '';
            if (data['name'])
                name = data['name'];
            var query = "Insert into employers (Name) values ('" + name + "')";
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{"status": "success", "id": result['insertId']}]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(employerId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (employerId) {
                query = `select distinct s.Id as id, s.Name as name from employers s where s.Id = ` + employerId;
            } else {
                query = `select distinct s.Id as id, s.Name as name from employers s`;
            }

            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }


    static update(uid, data) {
        if (data && uid) {
            var name = data['name'];
            var query = 'Update employers set Name="' + name + '" where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from employers where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = EmployerService;