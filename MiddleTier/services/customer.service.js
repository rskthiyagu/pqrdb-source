var MongoClient = require('mongoose');


var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

// const CustomerModel = require("../models/model.customer");
const customerModal = require("../models/customer.model");
// let Validator = require('fastest-validator');


let customers = {};
let counter = 0;

/* create an instance of the validator */
// let customerValidator = new Validator()

/* static customer service class */
class CustomerService
{
	static create(data)
	{
		let count;

		// let customer = new CustomerModel(data.first_name, data.last_name, data.email, data.zipcode, data.password);

		// customer.uid = 'c' + counter++;

		// customers[customer.uid] = customer;
		// Connect to the db
		// MongoClient.connect("mongodb://localhost/shopping-project",{ promiseLibrary: require('bluebird'), useNewUrlParser: true }).then(
		// 	() =>  {
		// 		console.log('connection successful');
		// 		customerModal.create(data, function (err, post) {
		// 			if (err) return next(err);
		// 			console.log("created");
		// 			console.log(post);
		// 			return post;
		// 		  });
		// 	}
		// )
		// .catch((err) => console.error(err));
		
		// Create connection to database
		var config = {
		  server: 'localhost',
		  authentication: {
			  type: 'default',
			  options: {
				  userName: 'sa', // update me
				  password: 'sa123' // update me
			  }
		  },
    	  driver: 'tedious',
		  options: {
			database: 'TestDB',
			instanceName: 'SQLSERVER2016'
		  }
		};

		console.log("&&&&     ", config);
		
		var connection = new Connection(config);
		// Attempt to connect and execute queries if connection goes through
		return new Promise((resolve, reject) => {
		connection.on('connect', function(err) {
			console.log("((((((*****     entered")
			if (err) {
				  console.log("**   ", err);
				  return reject(err);
			} else {
				  console.log('Connected');
				  var request = new Request(
					'SELECT * FROM customer;',
					function(err, rowCount, rows, row) {
					if (err) {
						// callback(err);
						return reject(err);
					} else {
						console.log(rowCount + ' row(s) returned');
						
						// callback(null);
						console.log("^^^^&&     ", row);
						// return resolve(rows);
					}
					});
				
					// Print the rows read
					var result = "";
					var resultArray = [];
					request.on('row', function(columns) {
						columns.forEach(function(column) {
							if (column.value === null) {
								console.log('NULL');
							} else {
								result += column.value + " ";
								var colName = column.metadata.colName;
								var colValue = column.value;
								resultArray.push({colName: colValue});
							}
						});
						console.log("***    ", columns);
						return resolve(resultArray);
						// return result;
						result = "";
					});
				  // Execute SQL statement
				  connection.execSql(request);
			}
		});
	});
		// console.log("*****       ",connection);		


	}

	static retrieve(uid)
	{
		console.log("enter");
		MongoClient.connect("mongodb://localhost/shopping-project",{ promiseLibrary: require('bluebird'), useNewUrlParser: true }).then(
			() =>  {
				console.log('connection successful');
				customerModal.find(function (err, customers) {
					if (err) return next(err);
					console.log("getting");
					console.log(customers);
					const customerData = new Promise(function(resolve, reject) {
						// Do async job
						resolve(customers);
					})
					return customerData;
				  });
			}
		)
		.catch((err) => console.error(err));

		// if(customers[uid] != null)
		// {
		// 	return customers[uid];
		// }
		// else
		// {
		// 	throw new Error('Unable to retrieve a customer by (uid:'+ uid +')');
		// }
	}

	// static update(uid, data)
	// {
	// 	if(customers[uid] != null)
	// 	{
	// 		const customer = customers[uid];
			
	// 		Object.assign(customer, data);
	// 	}
	// 	else
	// 	{
	// 		throw new Error('Unable to retrieve a customer by (uid:'+ cuid +')');
	// 	}
	// }

	// static delete(uid)
	// {
	// 	if(customers[uid] != null)
	// 	{
	// 		delete customers[uid];
	// 	}
	// 	else
	// 	{
	// 		throw new Error('Unable to retrieve a customer by (uid:'+ cuid +')');
	// 	}
	// }
}

module.exports = CustomerService;