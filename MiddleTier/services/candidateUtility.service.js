var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');
var moment = require('moment');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var skillService = require('./skills.service');
var parserService = require('./parser.service');


/* static product service class */
class CandidateUtilityService {
    // archestrate the candidate stuffs
    static organize(data) {
        var candidateList = [];
        var candidateDetailList = data[0];
        var answerList = {'yesOrNoType': [{'id': 0, 'name': 'No'}, {'id': 1, 'name': 'Yes'}]};
        if (candidateDetailList && candidateDetailList.length !== 0) {
            for (var i = 0; i < candidateDetailList.length; i++) {
                var obj = {};
                obj = candidateDetailList[i];
                obj['skills'] = this.getCandidateSkills(candidateDetailList[i]['id'], data[1], data[2]);
                obj['authorizations'] = this.getCandidateAuthorizations(candidateDetailList[i]['id'], data[3], data[4]);
                obj['qualifications'] = this.getCandidateQualifications(candidateDetailList[i]['id'], data[5], data[6]);
                obj['employers'] = this.getCandidateEmployers(candidateDetailList[i]['id'], data[7], data[8]);
                obj['domains'] = this.getCandidateDomains(candidateDetailList[i]['id'], data[9], data[10]);
                obj['certifications'] = this.getCandidateCertifications(candidateDetailList[i]['id'], data[11], data[12]);
                obj['cities'] = this.getLocationData(candidateDetailList[i]['cityId'], data[13], 'cities');
                obj['states'] = this.getLocationData(candidateDetailList[i]['stateId'], data[14], 'states');
                obj['countries'] = this.getLocationData(candidateDetailList[i]['countryId'], data[15], 'countries');
                obj['vendors'] = this.getLocationData(candidateDetailList[i]['sourcedFrom'], data[16], 'vendors');
                obj['taxTerms'] = this.getLocationData(candidateDetailList[i]['taxTerms'], data[17], 'taxTerms');
                obj['duration'] = this.getLocationData(candidateDetailList[i]['duration'], data[18], 'duration');
                // yes / no type
                obj['gender'] = this.getLocationData(candidateDetailList[i]['gender'], answerList, 'yesOrNoType');
                obj['relocation'] = this.getLocationData(candidateDetailList[i]['relocation'], answerList, 'yesOrNoType');
                candidateList.push(obj);
            }
        }
        return candidateList;
    }

    static getCandidateSkills(candidateId, skillMappedList, skillList) {
        var candidateSkillMappedList = [];
        if (skillMappedList && skillMappedList.length !== 0) {
            for (var i = 0; i < skillMappedList.length; i++) {
                if (skillMappedList[i]['candidateId'] == candidateId) {
                    for (var j = 0; j < skillList.length; j++) {
                        if (skillMappedList[i]['skillId'] == skillList[j]['id']) {
                            candidateSkillMappedList.push(skillList[j]);
                        }
                    }
                }
            }
        }
        return candidateSkillMappedList;
    }

    static getCandidateAuthorizations(candidateId, authorizationMappedList, authorizationList) {
        var candidateAuthorizationMappedList = [];
        if (authorizationMappedList && authorizationMappedList.length !== 0) {
            for (var i = 0; i < authorizationMappedList.length; i++) {
                if (authorizationMappedList[i]['candidateId'] == candidateId) {
                    for (var j = 0; j < authorizationList.length; j++) {
                        if (authorizationMappedList[i]['authorizationId'] == authorizationList[j]['id']) {
                            candidateAuthorizationMappedList.push(authorizationList[j]);
                        }
                    }
                }
            }
        }
        return candidateAuthorizationMappedList;
    }

    static getCandidateQualifications(candidateId, qualificationMappedList, qualificationList) {
        var candidateQualificationMappedList = [];
        if (qualificationMappedList && qualificationMappedList.length !== 0) {
            for (var i = 0; i < qualificationMappedList.length; i++) {
                if (qualificationMappedList[i]['candidateId'] == candidateId) {
                    for (var j = 0; j < qualificationList.length; j++) {
                        if (qualificationMappedList[i]['qualificationId'] == qualificationList[j]['id']) {
                            candidateQualificationMappedList.push(qualificationList[j]);
                        }
                    }
                }
            }
        }
        return candidateQualificationMappedList;
    }

    static getCandidateEmployers(candidateId, employerMappedList, employerList) {
        var candidateEmployerMappedList = [];
        if (employerMappedList && employerMappedList.length !== 0) {
            for (var i = 0; i < employerMappedList.length; i++) {
                if (employerMappedList[i]['candidateId'] == candidateId) {
                    for (var j = 0; j < employerList.length; j++) {
                        if (employerMappedList[i]['employerId'] == employerList[j]['id']) {
                            candidateEmployerMappedList.push(employerList[j]);
                        }
                    }
                }
            }
        }
        return candidateEmployerMappedList;
    }

    static getCandidateDomains(candidateId, domainMappedList, domainList) {
        var candidateDomainMappedList = [];
        if (domainMappedList && domainMappedList.length !== 0) {
            for (var i = 0; i < domainMappedList.length; i++) {
                if (domainMappedList[i]['candidateId'] == candidateId) {
                    for (var j = 0; j < domainList.length; j++) {
                        if (domainMappedList[i]['domainId'] == domainList[j]['id']) {
                            candidateDomainMappedList.push(domainList[j]);
                        }
                    }
                }
            }
        }
        return candidateDomainMappedList;
    }

    static getCandidateCertifications(candidateId, certificationMappedList, certificationList) {
        var candidateCertificationMappedList = [];
        if (certificationMappedList && certificationMappedList.length !== 0) {
            for (var i = 0; i < certificationMappedList.length; i++) {
                if (certificationMappedList[i]['candidateId'] == candidateId) {
                    for (var j = 0; j < certificationList.length; j++) {
                        if (certificationMappedList[i]['certificationId'] == certificationList[j]['id']) {
                            candidateCertificationMappedList.push(certificationList[j]);
                        }
                    }
                }
            }
        }
        return candidateCertificationMappedList;
    }

    static getLocationData(id, list, key) {
        var listArray
        if (key === 'yesOrNoType') {
            listArray = list[key];
        } else {
            listArray = list;
        }
        for (var i = 0; i < listArray.length; i++) {
            if (id === listArray[i]['id']) {
                return [listArray[i]];
            }
        }
        return [];
    }

    // set search history
    static setSearchData(data, createdBy) {
        if (data) {
            var searchString = '';
            if (data.length !== 0) {
                for (var i = 0; i < data.length; i++) {
                    searchString += data[i];
                    if (i !== data.length - 1) {
                        searchString += ', ';
                    }
                }
            }
            // created date / updated date updation
            var createdOn = moment.utc().format('YYYY-MM-DD HH:mm');

            var query = "insert into searchlog (SearchData, CreatedOn, CreatedBy) values('" + searchString + "', '" + createdOn + "', " + createdBy + ")";

            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // get skills acquired
    static getAcquiredSkills(resumeHtml) {
        return new Promise((resolve, reject) => {
            const skillList = skillService.retrieve();
            Promise.all([skillList]).then(function (data) {
                if (data) {
                    var skillsList = data[0];
                    try {
                        var acquiredSkillList = parserService.validateSkillAvailability(resumeHtml, skillsList, true);
                        return resolve(acquiredSkillList);
                    }
                    catch (err) {
                        // unexpected error
                        return next(err);
                    }
                }
            });
        });
    }

    // get suggested skills
    static getSuggestedSkills(data) {
        try {
            var validationString = [
                "Core areas of expertise",
                "certification",
                "skills",
                "professional experience",
                "education",
                "training",
                "academic qualification",
                "qualification",
                "experience",
                "technical expertise",
                "work experiences",
                "highlighted skills",
                "work as",
                "summary",
                "professional summary",
                "profile summary",
                "key strengths",
                "objective",
                "android skills",
                "employment history",
                "technical skills",
                "areas of expertise",
				"environment"
                ];
            var splittedValue = data.split("\n");
            var suggestedSkillSplitList = [];
            for (var j = 0; j < validationString.length; j++) {
            var skillOccurence = false;
            var length = splittedValue.length;
            var offset;
            for (var i = 0; i < length; i++) {
                if (((splittedValue[i]).toLowerCase().includes(validationString[j])) && !skillOccurence) {
                    skillOccurence = true;
                    offset = i;
                    break;
                }
            }
            if (skillOccurence) {
                for (var i = offset; i < offset + 20; i++) {
                    if (splittedValue[i]) {
                        var tabSplitValue = (splittedValue[i]).split("\t");
                        var collanSplitValue = (splittedValue[i]).split(":");
                        var commaSplitValue = (splittedValue[i]).split(",");
                        var pipeSplitValue = (splittedValue[i]).split("|");
                        var tabValidatorList = [':', ',', '|'];
                        var collanValidatorList = [',', '|'];
                        var commaValidatorList = ['\t', ':', '|'];
                        var pipeValidatorList = ['\t', ':', ','];

                        // validate the splitted value skills
                        suggestedSkillSplitList = parserService.validateSuggestedSkillList(tabSplitValue, tabValidatorList, suggestedSkillSplitList);
                        suggestedSkillSplitList = parserService.validateSuggestedSkillList(collanSplitValue, collanValidatorList, suggestedSkillSplitList);
                        suggestedSkillSplitList = parserService.validateSuggestedSkillList(commaSplitValue, commaValidatorList, suggestedSkillSplitList)
                        suggestedSkillSplitList = parserService.validateSuggestedSkillList(pipeSplitValue, pipeValidatorList, suggestedSkillSplitList);
                    }
                }
            }
        }
            return suggestedSkillSplitList;
        } catch (ex) {
            // unexpected error
            return next(err);
        }
    }
}

module.exports = CandidateUtilityService;
