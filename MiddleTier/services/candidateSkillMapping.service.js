var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


/* static product service class */
class CandidateSkillMappingService {

    static create(data, candidateId) {
        if (data) {
            var skillList = data;
            var query = "Insert into skillsmapping (SkillId, CandidateId) values ";
            for (var i = 0; i < skillList.length; i++) {
                query += "(" + skillList[i]['id'] + ", " + candidateId + ")";
                if (i !== skillList.length - 1) {
                    query += ",";
                }
            }
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(candidateId, candidateList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId) {
                query = `select distinct s.Id as id, s.SkillId as skillId, s.CandidateId as candidateId from skillsmapping s where s.CandidateId = ` + candidateId;
            } else if (candidateList && candidateList.length !== 0) {
                query = `select distinct s.Id as id, s.SkillId as skillId, s.CandidateId as candidateId from skillsmapping s`;
                if (candidateList && candidateList.length !== 0) {
                    query += " where (s.CandidateId in (";
                    for (var i = 0; i < candidateList.length; i++) {
                        query += candidateList[i]['id'];
                        if (i !== candidateList.length - 1) {
                            query += ", ";
                        }
                    }
                    query += "))";
                }
            } else {
                query = `select distinct s.Id as id, s.SkillId as skillId, s.CandidateId as candidateId from skillsmapping s`;
            }
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });;
        });
    }

    // dummy method
    static update(uid, data) {
        if (data && uid) {
            var query = 'Update Skills set Name="' + name + ' where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var request = new Request(query,
                    function (err, rowCount) {
                        if (err) {
                            // callback(err);
                            return reject(err);
                        } else {
                            return resolve("sucess");
                        }
                    });
                // Execute SQL statement
                global.connection.execSql(request);
            });
        } else {
            return "Id & data are required to do operation";
        }
    }
    // dummy method
    static delete(uid) {
        if (uid) {
            var query = 'Delete from skillsmapping where CandidateId = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = CandidateSkillMappingService;