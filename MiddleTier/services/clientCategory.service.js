var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var errorService = require('../services/error.service');


/* static product service class */
class ClientCategoryService {
    static create(data) {
        if (data) {
            var query = "Insert into clientcategory (Name) values ('" + data['name'] + "')";
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('clientCategory', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{"status": "success", "id": result['insertId']}]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(clientCategoryId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (clientCategoryId) {
                query = `select distinct s.Id as id, s.Name as name from clientcategory s where s.Id = ` + clientCategoryId;
            } else {
                query = `select distinct s.Id as id, s.Name as name from clientcategory s`;
            }

            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('clientCategory', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });;
        });
    }


    static update(uid, data) {
        if (data && uid) {
            var name = data['name'];
            var query = 'Update clientcategory set Name="' + name + '" where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('clientCategory', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from clientcategory where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('clientCategory', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = ClientCategoryService;