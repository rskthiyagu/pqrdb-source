var Request = require('tedious').Request;
const bcrypt = require("bcryptjs");
const CryptoJS = require('crypto-js');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');

// global encryption key
var encryptKey = '123456$#@$^@1ERF';


/* static category service class */
class UserService {
    static create(data) {
        var userName = '';
        var password = '';
        var firstName = '';
        var lastName = '';
        var aliasName = '';
        var fileName = '';
        var fbLink = '';
        var twitterLink = '';
        var linkedinLink = '';
        var designation = '';
        var role = null;
        var coverImage = '';
        var mobileNumber = null;
        var email = '';
        var isActive;
        var isSubmited;
        
        if (data) {
            // null validations
            if (data.userName) {
                userName = data.userName;
            }
            if (data.password) {
                password = data.password;
            }
            if (data.firstName) {
                firstName = data.firstName;
                aliasName = data.firstName;
            }
            if (data.userName) {
                lastName = data.lastName;
                aliasName += ' ' + data.lastName;
            }
            if (data.fileName) {
                fileName = data.fileName;
            }
            if (data.fbLink) {
                fbLink = data.fbLink;
            }
            if (data.twitterLink) {
                twitterLink = data.twitterLink;
            }
            if (data.linkedinLink) {
                linkedinLink = data.linkedinLink;
            }
            if (data.designation) {
                designation = data.designation;
            }
            if (data.role) {
                role = data.role;
            }
            if (data.coverImage) {
                coverImage = data.coverImage;
            }
            if (data.mobileNumber) {
                mobileNumber = data.mobileNumber;
            }
            if (data.email) {
                email = data.email;
            }
            if (data.isSubmited) {
                isSubmited = data.isSubmited;
            } else {
                isSubmited = 1;
            }
            if (data.isActive) {
                isActive = data.isActive;
            } else {
                isActive = 0;
            }
            var query = "Insert into users (Username, Password, FirstName, LastName, FileName, Alias, FbLink, TwitterLink, LinkedinLink, Designation, CoverImage, Email, MobileNumber, IsActive, IsSecurity, IsCrud, IsCandidateReport, IsUserReport, IsUserAccess, IsSubmitted, IsUserNameVisible, IsMobileNumberVisible, IsEmailVisible, IsFbLinkVisible, IsTwitterLinkVisible, IsLinkedinLinkVisible, IsRecentCandidateTileVisible, IsPendingCandidateTileVisible, IsRecentSearchTileVisible, IsProgressReportTileVisible, IsTalentManagerTileVisible, IsCoverImageVisible, IsProfileImageVisible) values ('" + userName + "', '" + password + "', '" + firstName + "', '" + lastName + "', '" + fileName + "', '" + aliasName + "', '" + fbLink + "', '" + twitterLink + "', '" + linkedinLink + "', '" + designation + "', '" + coverImage + "', '" + email + "', " + mobileNumber + ", " + isActive + ", " + 1 + ", " + 0 + ", " + 0 + ", " + 0 + ", " + 0 + ", " + isSubmited + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ", " + 1 + ")";
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(uid, flag) {
        // Attempt to connect and execute queries if connection goes through
        var query;
        // if (flag) {
        //    // query = 'select top 3 ';
        // } else {
            query = 'select ';
        // }
        if (uid) {
            query += `u.Id as id, u.Username as userName, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.Designation as designation, u.FbLink as fbLink, u.TwitterLink as twitterLink, u.LinkedinLInk as linkedinLink, u.Role as role, u.CoverImage as coverImage, u.FileName as profileImage,
            u.MobileNumber as mobileNumber, u.Email as email, u.Gender as gender, u.IsActive as isActive, u.IsSecurity as isSecurity, u.IsCrud as isCrud, u.IsCandidateReport as isCandidateReport, u.IsUserReport as isUserReport, u.IsUserAccess as isUserAccess, u.IsAdminUser as isAdminUser,
            IsUserNameVisible, IsMobileNumberVisible, IsEmailVisible, IsFbLinkVisible, IsTwitterLinkVisible, IsLinkedinLinkVisible, IsRecentCandidateTileVisible, IsPendingCandidateTileVisible, IsRecentSearchTileVisible, IsProgressReportTileVisible, IsTalentManagerTileVisible, IsCoverImageVisible, IsProfileImageVisible,
            (select count(Id) from candidatedetails c where c.SourcedBy = u.Id and IsPending = 0) as userCompletedTotal,
            (select count(Id) from candidatedetails c where c.SourcedBy = u.Id and IsPending = 1) as userPendingTotal,
            (select count(Id) from candidatedetails c) as total from users u where u.Id =` + uid + ` and u.IsActive = 1 order by userCompletedTotal desc`;
        } else {
            query += `u.Id as id, u.Username as userName, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.Designation as designation, u.FbLink as fbLink, u.TwitterLink as twitterLink, u.LinkedinLInk as linkedinLink, u.Role as role, u.CoverImage as coverImage, u.FileName as profileImage,
                    u.MobileNumber as mobileNumber, u.Email as email, u.Gender as gender, u.IsActive as isActive, u.IsSecurity as isSecurity, u.IsCrud as isCrud, u.IsCandidateReport as isCandidateReport, u.IsUserReport as isUserReport, u.IsUserAccess as isUserAccess, u.IsAdminUser as isAdminUser,
                    IsUserNameVisible, IsMobileNumberVisible, IsEmailVisible, IsFbLinkVisible, IsTwitterLinkVisible, IsLinkedinLinkVisible, IsRecentCandidateTileVisible, IsPendingCandidateTileVisible, IsRecentSearchTileVisible, IsProgressReportTileVisible, IsTalentManagerTileVisible, IsCoverImageVisible, IsProfileImageVisible,
                    (select count(Id) from candidatedetails c where c.SourcedBy = u.Id and IsPending = 0) as userCompletedTotal,
                    (select count(Id) from candidatedetails c where c.SourcedBy = u.Id and IsPending = 1) as userPendingTotal,
                    (select count(Id) from candidatedetails c) as total from users u where u.IsActive = 1 order by userCompletedTotal desc`;
        }

        if (flag) {
            query += ' LIMIT 3';
        }

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    static retrieveAccountManager(uid, flag) {
        // Attempt to connect and execute queries if connection goes through
        var query;
        // if (flag) {
        //    // query = 'select top 3 ';
        // } else {
            query = 'select ';
        // }
        if (uid) {
            query += `u.Id as id, u.Username as userName, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.Designation as designation, u.FbLink as fbLink, u.TwitterLink as twitterLink, u.LinkedinLInk as linkedinLink, u.Role as role, u.CoverImage as coverImage, u.FileName as profileImage,
            u.MobileNumber as mobileNumber, u.Email as email, u.Gender as gender, u.IsActive as isActive, u.IsSecurity as isSecurity, u.IsCrud as isCrud, u.IsCandidateReport as isCandidateReport, u.IsUserReport as isUserReport, u.IsUserAccess as isUserAccess, u.IsAdminUser as isAdminUser,
            IsUserNameVisible, IsMobileNumberVisible, IsEmailVisible, IsFbLinkVisible, IsTwitterLinkVisible, IsLinkedinLinkVisible, IsRecentCandidateTileVisible, IsPendingCandidateTileVisible, IsRecentSearchTileVisible, IsProgressReportTileVisible, IsTalentManagerTileVisible, IsCoverImageVisible, IsProfileImageVisible,
            (select count(Id) from candidatedetails c where c.SourcedBy = u.Id and IsPending = 0) as userCompletedTotal,
            (select count(Id) from candidatedetails c where c.SourcedBy = u.Id and IsPending = 1) as userPendingTotal,
            (select count(Id) from candidatedetails c) as total from users u where u.Id =` + uid + ` and u.IsActive = 1 order by userCompletedTotal desc`;
        } else {
            query += `u.Id as id, u.Alias as name from users u where u.IsActive = 1`;
        }

        if (flag) {
            query += ' LIMIT 3';
        }
console.log(query);
        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    // retrieve submitted records for approval
    static retrievePendingUsers(uid) {
        // Attempt to connect and execute queries if connection goes through
        var query = 'select ';
        if (uid) {
            query += `u.Id as id, u.Username as userName, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.Designation as designation, u.FbLink as fbLink, u.TwitterLink as twitterLink, u.LinkedinLInk as linkedinLink, u.Role as role, u.CoverImage as coverImage, u.FileName as profileImage,
            u.MobileNumber as mobileNumber, u.Email as email, u.Gender as gender, u.IsActive as isActive, u.IsSecurity as isSecurity, u.IsCrud as isCrud, u.IsCandidateReport as isCandidateReport, u.IsUserReport as isUserReport
            from users u where u.Id =` + uid + ` and u.IsActive = 0 and u.IsSubmitted = 1`;
        } else {
            query += `u.Id as id, u.Username as userName, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.Designation as designation, u.FbLink as fbLink, u.TwitterLink as twitterLink, u.LinkedinLInk as linkedinLink, u.Role as role, u.CoverImage as coverImage, u.FileName as profileImage,
                    u.MobileNumber as mobileNumber, u.Email as email, u.Gender as gender, u.IsActive as isActive, u.IsSecurity as isSecurity, u.IsCrud as isCrud, u.IsCandidateReport as isCandidateReport, u.IsUserReport as isUserReport
                    from users u where u.IsActive = 0  and u.IsSubmitted = 1`;
        }

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    // retrieve rejected records
    static retrieveRejectedUsers(uid) {
        // Attempt to connect and execute queries if connection goes through
        var query = 'select ';
        if (uid) {
            query += `u.Id as id, u.Username as userName, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.Designation as designation, u.FbLink as fbLink, u.TwitterLink as twitterLink, u.LinkedinLInk as linkedinLink, u.Role as role, u.CoverImage as coverImage, u.FileName as profileImage,
            u.MobileNumber as mobileNumber, u.Email as email, u.Gender as gender, u.IsActive as isActive, u.IsSecurity as isSecurity, u.IsCrud as isCrud, u.IsCandidateReport as isCandidateReport, u.IsUserReport as isUserReport
            from users u where u.Id =` + uid + ` and u.IsActive = 0 and u.IsSubmitted = 0`;
        } else {
            query += `u.Id as id, u.Username as userName, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.Designation as designation, u.FbLink as fbLink, u.TwitterLink as twitterLink, u.LinkedinLInk as linkedinLink, u.Role as role, u.CoverImage as coverImage, u.FileName as profileImage,
                    u.MobileNumber as mobileNumber, u.Email as email, u.Gender as gender, u.IsActive as isActive, u.IsSecurity as isSecurity, u.IsCrud as isCrud, u.IsCandidateReport as isCandidateReport, u.IsUserReport as isUserReport
                    from users u where u.IsActive = 0  and u.IsSubmitted = 0`;
        }        

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    static update(uid, data) {
        var userName = '';
        var firstName = '';
        var lastName = '';
        var aliasName = '';
        var fbLink = '';
        var twitterLink = '';
        var linkedinLink = '';
        var designation = '';
        var mobileNumber = null;
        var email = '';
        if (data) {
            // null validations
            if (data.userName) {
                userName = data.userName;
            }
            if (data.firstName) {
                firstName = data.firstName;
                aliasName = data.firstName;
            }
            if (data.userName) {
                lastName = data.lastName;
                aliasName += ' ' + data.lastName;
            }
            if (data.fbLink) {
                fbLink = data.fbLink;
            }
            if (data.twitterLink) {
                twitterLink = data.twitterLink;
            }
            if (data.linkedinLink) {
                linkedinLink = data.linkedinLink;
            }
            if (data.designation) {
                designation = data.designation;
            }
            if (data.mobileNumber) {
                mobileNumber = data.mobileNumber;
            }
            if (data.email) {
                email = data.email;
            }
            var query = "Update users set Username = '" + userName + "', FirstName = '" + firstName + "', LastName = '" + lastName + "', Alias = '" + aliasName + "', FbLink = '" + fbLink + "', TwitterLink = '" + twitterLink + "', LinkedinLink = '" + linkedinLink + "', Designation = '" + designation + "', Email = '" + email + "', MobileNumber = " + mobileNumber + " where Id = " + uid;

            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // update the cover image of user
    static updateCoverImage(uid, data) {
        var coverImage = '';
        if (data) {
            // null validations
            if (data.coverImage) {
                coverImage = data.coverImage;
            }
            var query = "Update users set CoverImage = '" + coverImage + "' where Id = " + uid;
            
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }
    
    // update the cover image of user
    static userCandidateCreatedOnTransfer(uid, updatedUserId) {
        if (uid, updatedUserId) {
            var query = "Update candidatedetails set CreatedBy = " + updatedUserId + " where CreatedBy = " + id;
            
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // update the cover image of user
    static userCandidateUpdatedOnTransfer(uid, updatedUserId) {
        if (uid, updatedUserId) {
            var query = "Update candidatedetails set UpdatedBy = " + updatedUserId + " where UpdatedBy = " + id;
            
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // update the profile image of user
    static updateProfileImage(uid, data) {
        var profileImage = '';
        if (data) {
            // null validations
            if (data.profileImage) {
                profileImage = data.profileImage;
            }
            var query = "Update users set FileName = '" + profileImage + "' where Id = " + uid;

            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // update the profile image of user
    static updatePassword(uid, data) {
        var password = '';
        var oldPassword = '';
        if (data) {
            // null validations
            if (data.encryptNewPassword) {
                password = data.encryptNewPassword;
            }
            
            var query = "Update users set Password = '" + password + "' where Id = " + uid;
            
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // update setting widget access
    static updateUserAccess(data) {
        if (data) {
            const id = data['id'];
            const key = data['key'];
            const value = data['value'];
			
			let actualValue;
            if (value === true) {
                actualValue = 1;
            } else if (value === false) {
                actualValue = 0;
            } else {
                actualValue = value;
            }
            // null validations
            var query = "Update users set " + key + " = ";
            if (actualValue === 0 || actualValue === 1) {
                query += actualValue;
            } else {
                query += "'" + actualValue + "'";
            }
            query += " where Id = " + id;
            
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do operation";
        }
    }

    // Activate the user
    static activateUser(uid) {
        if (uid) {
            // null validations
            var query = "Update users set IsActive = " + 1 + ", IsSubmitted = " + 0 + " where Id = " + uid;
            
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do operation";
        }
    }

    // Activate the user
    static deactivateUser(uid) {
        if (uid) {
            // null validations
            var query = "Update users set IsActive = " + 0 + ", IsSubmitted = " + 0 + " where Id = " + uid;
            
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do operation";
        }
    }

    // Activate the user
    static updateResetKey(uid, resetKey) {
        if (uid) {
            // null validations
            var query = "Update users set resetKey = " + resetKey + " where Id = " + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve({status: "success"});
                        });
                    }
                });
            });
        } else {
            return "Id is required to do operation";
        }
    }

    // validate reset key
    static validateResetKey(data) {
        const uid = data['id'];
        const resetKey = data['token'];
        if (uid && resetKey) {
            var query = 'select * from Users u where u.Id = ' + uid + ' and u.ResetKey = ' + resetKey;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([result]);
                        });
                    }
                });
            });
        }
    }

    // reset password flow
    static resetPassword(data) {
        const password = data['password'];
        if (data['password'] && data['userName'] && data['email']) {
            // null validations
            var query = "Update users set Password = '" + password + "' where Username = '" + data['userName'] + "' and Email = '" + data['email'] + "'";
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            console.log(err);
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return null;
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from users where Id = ' + uid;

            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }

    // validate user details availability status
    static validateUserNameAvailability(param) {
        // Attempt to connect and execute queries if connection goes through

        var userName = param.userName;
        var query = `select u.Id as id,
                            u.Username as userName,
                            u.FirstName as firstName,
                            u.LastName as lastName,
                            u.Alias as name,
                            u.FileName as profileImage,
                            u.MobileNumber as mobileNumber,
                            u.IsActive as isActive
                            from users u where u.userName='` + userName + `'`;        

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    // validate user details availability status
    static validateUserDetailAvailability(param) {
        // Attempt to connect and execute queries if connection goes through

        var mobileNumber = param.mobileNumber;
        var email = param.email;
        var query = `select u.Id as id,
                            u.Username as userName,
                            u.FirstName as firstName,
                            u.LastName as lastName,
                            u.Alias as name,
                            u.FileName as profileImage,
                            u.MobileNumber as mobileNumber,
                            u.IsActive as isActive
                            from users u where u.MobileNumber='` + mobileNumber + `' and u.Email='` + email + `'`;

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    // validate user details availability status
    static fetchSearchLog(flag, id) {
        // Attempt to connect and execute queries if connection goes through
        var query = '';
        // if (flag) {
        //     query = 'select top 5';
        // } else {
            query = 'select';
        // }

        query += ` sl.Id as id,
                            sl.SearchData as title,
                            sl.CreatedBy as userId,
                            sl.CreatedOn as createdOn
                            from searchlog sl where sl.CreatedBy=` + id + ' order by sl.CreatedOn desc';

        if (flag) {
            query += ' limit 5';
        }

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }


    static validate(param) {
        // Attempt to connect and execute queries if connection goes through

        var userName = param.userName;
        var password = param.password;
        var query = "select u.Id as id, u.Username as userName, u.Password as password, u.IsActive as isActive, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.FileName as profileImage, u.MobileNumber as mobileNumber, u.IsActive as isActive, IsUserNameVisible, IsMobileNumberVisible, IsEmailVisible, IsFbLinkVisible, IsTwitterLinkVisible, IsLinkedinLinkVisible, IsRecentCandidateTileVisible, IsPendingCandidateTileVisible, IsRecentSearchTileVisible, IsProgressReportTileVisible, IsTalentManagerTileVisible, IsCoverImageVisible, IsProfileImageVisible from users u where u.Username='" + userName + "'";

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    // get the appropriate user for reset password
    static fetchUser(param) {
        // Attempt to connect and execute queries if connection goes through

        var userName = param.userName;
        var email = param.email;
        var query = "select u.Id as id, u.FirstName as firstName, u.LastName as lastName, u.Alias as name, u.IsActive as isActive, u.Email as email from users u where u.IsActive = 1 and u.Username='" + userName + "' and u.email='" + email + "'";

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    // validate the hash for password to conclude the logged user
    static validatePasswordHash(userList, passwordHash) {
        var password = userList[0]['password'];
        var updatedPassword = '';
        // password decrypt
        if (password) {
            var key = CryptoJS.enc.Utf8.parse(encryptKey);
            var iv = CryptoJS.enc.Utf8.parse(encryptKey);
            var decrypted = CryptoJS.AES.decrypt(password, key, {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

            updatedPassword =  decrypted.toString(CryptoJS.enc.Utf8);
        }
        return new Promise((resolve, reject) => {
        bcrypt.compare(updatedPassword, passwordHash, (err, isMatch) => {
            if (err) {
              return resolve([{status: "error"}]);
            } else if (!isMatch) {
              return resolve([{status: "fail"}]);
            } else {
              return resolve([{status: "pass"}]);
            }
          });
        });
    }

    static getCount() {
        var query = "select Id from users";
        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([result.length]);
                        });
                    }
                });
            });
    }
}

module.exports = UserService;