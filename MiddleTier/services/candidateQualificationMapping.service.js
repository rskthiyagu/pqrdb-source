var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


/* static product service class */
class CandidateQualificationMappingService {

    static create(data, candidateId) {
        if (data) {
            var qualificationList = data;
            var query = "Insert into qualificationsmapping(QualificationId, CandidateId) values ";
            for (var i = 0; i < qualificationList.length; i++) {
                query += "(" + qualificationList[i]['id'] + ", " + candidateId + ")";
                if (i !== qualificationList.length - 1) {
                    query += ",";
                }
            }
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(candidateId, candidateList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId) {
                query = `select distinct s.Id as id, s.QualificationId as qualificationId, s.CandidateId as candidateId from qualificationsmapping s where s.CandidateId = ` + candidateId;
            } else if (candidateList && candidateList.length !== 0) {
                query = `select distinct s.Id as id, s.QualificationId as qualificationId, s.CandidateId as candidateId from qualificationsmapping s`;
                if (candidateList && candidateList.length !== 0) {
                    query += " where (s.CandidateId in (";
                    for (var i = 0; i < candidateList.length; i++) {
                        query += candidateList[i]['id'];
                        if (i !== candidateList.length - 1) {
                            query += ", ";
                        }
                    }
                    query += "))";
                }
            } else {
                query = `select distinct s.Id as id, s.QualificationId as qualificationId, s.CandidateId as candidateId from qualificationsmapping s`;
            }

            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    // dummy method
    static update(uid, data) {
        if (data && uid) {
            var query = 'Update Skills set AuthorizationId="' + name + ' where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    // dummy method
    static delete(uid) {
        if (uid) {
            var query = 'Delete from qualificationsmapping where CandidateId = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = CandidateQualificationMappingService;