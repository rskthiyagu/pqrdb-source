var moment = require('moment');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


/* static product service class */
class ErrorService {
    static create(moduleName, error) {
        if (moduleName && error) {
            // created date / updated date updation
            var createdOn = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = 'Insert into errorlog (ModuleName, Error, CreatedOn) values ("' + moduleName + '", "' + error + '", "' + createdOn + '")';
            console.log("Errrr    ", query);
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{"status": "success"}]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(cityId, stateId, countryId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (cityId) {
                query = `select distinct s.Id as id, s.Name as name, s.StateId as stateId, (select Name from states where Id = s.StateId) as stateName, s.CountryId as countryId, (select Name from countries where Id = s.CountryId) as countryName from cities s where s.Id = ` + cityId;
            } else if (stateId) {
                query = `select distinct s.Id as id, s.Name as name, s.StateId as stateId, (select Name from states where Id = s.StateId) as stateName, s.CountryId as countryId, (select Name from countries where Id = s.CountryId) as countryName from cities s where s.StateId = ` + stateId;
            } else if (countryId) {
                query = `select distinct s.Id as id, s.Name as name, s.StateId as stateId, (select Name from states where Id = s.StateId) as stateName, s.CountryId as countryId, (select Name from countries where Id = s.CountryId) as countryName from cities s where s.CountryId = ` + countryId;
            } else {
                query = `select * from errorlog s`;
            }

            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }


    static update(uid, data) {
        if (data && uid) {
            var name = data['name'];
            var query = 'Update cities set Name="' + name + '" where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from cities where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = ErrorService;