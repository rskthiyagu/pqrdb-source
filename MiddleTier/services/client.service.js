var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');
var moment = require('moment');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


/* static product service class */
class ClientService {
    static create(data) {
        if (data) {
            // null check
            if (!data['name']) data['name'] = '';
            if (!data['website']) data['website'] = '';
            if (!data['zipCode']) data['zipCode'] = '';
            if (!data['createdBy']) data['createdBy'] = null;
            if (!data['updatedBy']) data['updatedBy'] = null;
            if (!data['createdOn']) data['createdOn'] = '';
            if (!data['updatedOn']) data['updatedOn'] = '';

            if (data['cities'] && data['cities'].length !== 0) {
                data['cityId'] = data['cities'][0]['id'];
            } else {
                data['cityId'] = null;
            }
            if (data['states'] && data['states'].length !== 0) {
                data['stateId'] = data['states'][0]['id'];
            } else {
                data['stateId'] = null;
            }
            if (data['countries'] && data['countries'].length !== 0) {
                data['countryId'] = data['countries'][0]['id'];
            } else {
                data['countryId'] = null;
            }
            if (data['clientCategory'] && data['clientCategory'].length !== 0) {
                data['clientCategory'] = data['clientCategory'][0]['id'];
            } else {
                data['clientCategory'] = null;
            }
            if (data['clientStatus'] && data['clientStatus'].length !== 0) {
                data['clientStatus'] = data['clientStatus'][0]['id'];
            } else {
                data['clientStatus'] = null;
            }
            if (data['accountManager'] && data['accountManager'].length !== 0) {
                data['accountManager'] = data['accountManager'][0]['id'];
            } else {
                data['accountManager'] = null;
            }


            // created date / updated date updation
            // data['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            // data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');

            var query = "Insert into clientdetails (Name, Website, CityId, StateId, CountryId, ZipCode, Category, Status, AccountManager, CreatedBy, UpdatedBy, CreatedOn, UpdatedOn) values ('" +
            data['name'] + "', '" + data['website'] + "', " + data['cityId'] + ", " + data['stateId'] + ", " + data['countryId'] + ", '" + data['zipCode'] + "', " + data['clientCategory'] + ", " + data['clientStatus'] + ", " + data['accountManager'] + ", " + data['createdBy'] + ", " + data['updatedBy'] + ", '" + data['createdOn'] + "', '" + data['updatedOn'] + "')";
            var connection = dbAccessService.createConnection();
            return new Promise((resolve, reject) => {
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('client', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // create resume only candidates
    static createPendingRecord(data) {
        if (data && data.length !== 0) {
            var query = 'Insert into candidatedetails (SourcedBy, CreatedOn, UpdatedOn, FileName, IsPending) values';
            for (var i = 0; i < data.length; i++) {
                // null check
                if (!data[i]['sourcedBy']) data[i]['sourcedBy'] = null;
                if (!data[i]['fileName']) data[i]['fileName'] = '';

                // created date / updated date updation
                // data[i]['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
                // data[i]['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
                query += " (" + data[i]['sourcedBy'] + ", '" + data[i]['createdOn'] + "', '" + data[i]['updatedOn'] + "', '" + data[i]['fileName'] + "', " + 1 + ")";
                if (i !== data.length - 1) {
                    query += ',';
                }
            }
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{ 'status': 'success' }]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(clientId, clientList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (clientId) {
                query = `select distinct Id as id, Name as name, Website as website, CityId as citiesId, StateId as statesId, CountryId as countriesId, ZipCode as zipCode, Category as clientCategoryId, Status as clientStatusId, AccountManager as accountManagerId, CreatedBy as createdBy, UpdatedBy as updatedBy, CreatedOn as createdOn, UpdatedOn as updatedOn,
                (select ct.Name as name from cities ct where ct.Id = c.CityId) as citiesName,
                (select st.Name as name from states st where st.Id = c.StateId) as statesName,
                (select cs.Name as name from countries cs where cs.Id = c.CountryId) as countriesName,
                (select cc.Name from clientcategory cc where cc.Id = c.Category) as clientCategoryName,
                (select cs.Name from clientstatus cs where cs.Id = c.Status),
                (select Alias from users u where u.Id = c.AccountManager && u.IsAccountManager = 1) as accountManagerName,
                (select Alias from users u where u.Id = c.CreatedBy) as userName,
                (select FileName from users u where u.Id = c.CreatedBy) as profileImage
                from clientdetails c where c.Id = ` + clientId + ' order by c.CreatedOn desc';
            } else if (clientList && clientList.length !== 0) {     // for filter & search purpose
                query = `select distinct Id as id, Name as name, CityId as cityId, StateId as stateId, CountryId as countryId, CreatedBy as createdBy, UpdatedBy as updatedBy, CreatedOn as createdOn, UpdatedOn as updatedOn from clientdetails c`;
                query += " where (c.Id in (";
                for (var i = 0; i < clientList.length; i++) {
                    query += clientList[i]['id'];
                    if (i !== clientList.length - 1) {
                        query += ", ";
                    }
                }
                query += ")) order by c.CreatedOn desc";
            } else {
                query = `select distinct Id as id, Name as name, Website as website, CityId as citiesId, StateId as statesId, CountryId as countriesId, ZipCode as zipCode, Category as clientCategoryId, Status as clientStatusId, AccountManager as accountManagerId, CreatedBy as createdBy, UpdatedBy as updatedBy, CreatedOn as createdOn, UpdatedOn as updatedOn,
                (select ct.Name as name from cities ct where ct.Id = c.CityId) as citiesName,
                (select st.Name as name from states st where st.Id = c.StateId) as statesName,
                (select cs.Name as name from countries cs where cs.Id = c.CountryId) as countriesName,
                (select cc.Name from clientcategory cc where cc.Id = c.Category) as clientCategoryName,
                (select cs.Name from clientstatus cs where cs.Id = c.Status),
                (select Alias from users u where u.Id = c.AccountManager && u.IsAccountManager = 1) as accountManagerName,
                (select Alias from users u where u.Id = c.CreatedBy) as userName,
                (select FileName from users u where u.Id = c.CreatedBy) as profileImage from clientdetails c order by c.CreatedOn desc`;
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('client', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // retrieve clients for dropdown
    static retrieveClientData() {
        return new Promise((resolve, reject) => {
            var query;
            query = `select distinct Id as id, Name as name, Category as categoryId from clientdetails c order by c.CreatedOn desc`;
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('client', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // fetch availability info of candidate
    static retrieveCandidateAvailability(candidateId, candidateList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId) {
                query = `select distinct c.Id as id, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                        c.IsInterviewSubmitted as isInterviewSubmitted, c.InterviewStatus as interviewStatus, c.InterviewRequirement as interviewRequirement, c.InterviewClient as interviewClient, c.InterviewComments as interviewComments,
                        (select Alias from users u where u.Id = c.UpdatedBy) as userName,
                        (select FileName from users u where u.Id = c.UpdatedBy) as profileImage from candidatedetails c where c.Id = ` + candidateId;
            } else if (candidateList && candidateList.length !== 0) {
                query = `select distinct c.Id as id, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                        c.IsInterviewSubmitted as isInterviewSubmitted, c.InterviewStatus as interviewStatus, c.InterviewRequirement as interviewRequirement, c.InterviewClient as interviewClient, c.InterviewComments as interviewComments,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName,
                        (select FileName from users u where u.Id = c.SourcedBy) as profileImage from candidatedetails c`;
                query += " where (c.Id in (";
                for (var i = 0; i < candidateList.length; i++) {
                    query += candidateList[i]['id'];
                    if (i !== candidateList.length - 1) {
                        query += ", ";
                    }
                }
                query += ")) and c.IsPending = 0 order by c.CreatedOn desc";
            } else {
                query = `select distinct c.Id as id, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                        c.IsInterviewSubmitted as isInterviewSubmitted, c.InterviewStatus as interviewStatus, c.InterviewRequirement as interviewRequirement, c.InterviewClient as interviewClient, c.InterviewComments as interviewComments,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName,
                        (select FileName from users u where u.Id = c.SourcedBy) as profileImage from candidatedetails c where c.IsPending = 0 order by c.CreatedOn desc`;
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static retrievePendingRecord(id, flag) {
        return new Promise((resolve, reject) => {
            var query;
            // if (flag) {
            //     query = 'select top 4 ';
            // } else {
            query = 'select ';
            // }
            query += `c.Id as id, c.FileName as fileName, c.CreatedOn as createdOn, (select count(Id) from candidatedetails cd where cd.IsPending = 1`
            if (typeof (id) === 'string') {
                query += ` and cd.SourcedBy=` + id;
            } else {
                if (id && id.length !== 0) {
                    query += ' and cd.SourcedBy in (';
                    for (var i = 0; i < id.length; i++) {
                        query += id[i];
                        if (i !== id.length - 1) {
                            query += ', ';
                        }
                    }
                    query += ')';
                }
            }
            query += `) as total,
            (select Alias from users u where u.Id = c.SourcedBy) as userName,
            (select FileName from users u where u.Id = c.SourcedBy) as profileImage from candidatedetails c where c.IsPending = 1`;
            if (typeof (id) === 'string') {
                query += ` and c.SourcedBy=` + id;
            } else if (id && id.length !== 0) {
                query += ' and c.SourcedBy in (';
                for (var i = 0; i < id.length; i++) {
                    query += id[i];
                    if (i !== id.length - 1) {
                        query += ', ';
                    }
                }
                query += ')';
            }
            query += ' order by c.CreatedOn desc';

            if (flag) {
                query += ' limit 4';
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        console.log(err);
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static updateDeleteRecord(data) {
        if (data) {
            var tableName = '';
            var key = '';
            var value = '';
            var id;

            // get values
            if (data['key'] === 'cities') {
                tableName = 'candidatedetails';
                key = 'CityId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'states') {
                tableName = 'candidatedetails';
                key = 'StateId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'countries') {
                tableName = 'candidatedetails';
                key = 'CountryId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'vendors') {
                tableName = 'candidatedetails';
                key = 'SourcedFrom';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'skills') {
                tableName = 'skillsmapping';
                key = 'SkillId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'authorizations') {
                tableName = 'authorizationsmapping';
                key = 'AuthorizationId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'employers') {
                tableName = 'employersmapping';
                key = 'EmployerId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'qualifications') {
                tableName = 'qualificationsmapping';
                key = 'QualificationId';
                value = data['value'];
                id = data['id'];
            }

            // created date / updated date updation
            data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update " + tableName + " set " + key + "=" + value + " where " + key + "=" + id;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // get statistics data of candidate count
    static getCandidateTotalStatistics(id) {
        return new Promise((resolve, reject) => {
            var query = `select count(Id) as candidateTotal, 
            (select count(Id) from candidatedetails where IsPending = 0 and SourcedBy = ` + id + `) as userCandidateTotal from candidatedetails where IsPending = 0;`
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // get statistics data of pending candidate total
    static getPendingCandidateTotalStatistics(id) {
        return new Promise((resolve, reject) => {
            var query = `select count(Id) as candidatePendingTotal,
            (select count(Id) from candidatedetails where IsPending = 1 and SourcedBy = ` + id +
                `) as userCandidatePendingTotal from candidatedetails where IsPending = 1;`

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // get statistics data of candidate today
    static getCandidateTotalTodayStatistics(id) {
        return new Promise((resolve, reject) => {
            var currentDate = moment.utc().format('YYYY-MM-DD');
            var query = `select count(Id) as candidateTodayTotal,
            (select count(Id) from candidatedetails where SourcedBy = ` + id + ` and CreatedOn > '` + currentDate + ` 00:00:00.000') as userCandidateTodayTotal from candidatedetails where CreatedOn > '` + currentDate + ` 00:00:00.000';`

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static retrieveNewlyCreated(flag) {
        return new Promise((resolve, reject) => {
            var query;
            // if (flag) {
            //     query = 'select top 12 ';
            // } else {
            query = 'select ';
            // }
            query += `c.Id as id, c.FirstName as firstName, c.LastName as lastName, c.Email as email, c.MobileNumber as mobileNumber, c.SourcedBy as sourcedBy, c.SourcedFrom as sourcedFrom, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn, c.IsAvailable as isAvailable, c.FileName as fileName, c.JobTitle as jobTitle, c.Experience as experience, c.Linkedin as linkedin, c.CityId as cityId, c.StateId as stateId, c.CountryId as countryId, (select count(Id) from candidatedetails cd where cd.IsPending = 0) as total,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName from candidatedetails c where IsPending = 0 order by c.CreatedOn desc`;

            if (flag) {
                query += ' limit 12';
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }


    static update(uid, data) {
        if (data) {
            // null check
            if (!data['firstName']) data['firstName'] = '';
            if (!data['lastName']) data['lastName'] = '';
            if (!data['mobileNumber']) data['mobileNumber'] = null;
            if (!data['email']) data['email'] = '';
            if (!data['linkedin']) data['linkedin'] = '';
            if (!data['experience']) data['experience'] = null;
            if (!data['sourcedBy']) data['sourcedBy'] = null;
            if (!data['vendors']) data['sourcedFrom'] = null;
            if (!data['fileName']) data['fileName'] = '';
            if (!data['jobTitle']) data['jobTitle'] = '';
            if (!data['cities']) data['cityId'] = null;
            if (!data['states']) data['stateId'] = null;
            if (!data['countries']) data['countryId'] = null;

            if (data['cities'] && data['cities'].length !== 0) {
                data['cityId'] = data['cities'][0]['id'];
            }
            if (data['states'] && data['states'].length !== 0) {
                data['stateId'] = data['states'][0]['id'];
            }
            if (data['countries'] && data['countries'].length !== 0) {
                data['countryId'] = data['countries'][0]['id'];
            }
            if (data['vendors'] && data['vendors'].length !== 0) {
                data['sourcedFrom'] = data['vendors'][0]['id'];
            }

            // created date / updated date updation
            // data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update candidatedetails set FirstName='" + data['firstName'] + "', LastName='" + data['lastName'] + "', Email='" + data['email'] + "', MobileNumber=" + data['mobileNumber'] + ", SourcedBy=" + data['sourcedBy'] + ", SourcedFrom=" + data['sourcedFrom'] + ", UpdatedOn='" + data['updatedOn'] + "', CityId=" + data['cityId'] + ", StateId=" + data['stateId'] + ", CountryId=" + data['countryId'] + ", Experience=" + data['experience'] + ", Linkedin='" + data['linkedin'] + "', fileName='" + data['fileName'] + "', JobTitle='" + data['jobTitle'] + "', IsPending=" + 0 + " where Id=" + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // update availability details of candidates
    static updateCandidateAvailability(uid, data) {
        if (data) {
            var communicationDetails = data['communicationDetails'];
            // null validation
            if (data['communicationDetails']['isCommunication'] === true) {
                data['communicationDetails']['isCommunication'] = 1;
            } else if (data['communicationDetails']['isCommunication'] === false) {
                data['communicationDetails']['isCommunication'] = 0;
            } else if (data['communicationDetails']['isCommunication'] === 0) {
                data['communicationDetails']['isCommunication'] = 0;
            } else if (data['communicationDetails']['isCommunication'] === 1) {
                data['communicationDetails']['isCommunication'] = 1;
            } else {
                data['communicationDetails']['isCommunication'] = null;
            }
            var interviewDetails = data['interviewDetails'];
            // null check
            if (!data['communicationDetails']['availabilityClient']) data['communicationDetails']['availabilityClient'] = '';
            if (!data['communicationDetails']['availabilityRequirement']) data['communicationDetails']['availabilityRequirement'] = '';
            if (!data['communicationDetails']['availabilityComments']) data['communicationDetails']['availabilityComments'] = '';

            // availability validation
            if (data['communicationDetails']['availability'] === 1) {
                data['communicationDetails']['isAvailable'] = 1;
            } else if (data['communicationDetails']['availability'] === 0) {
                data['communicationDetails']['isAvailable'] = 0;
            } else if (data['communicationDetails']['availability'] && data['communicationDetails']['availability'].length !== 0) {
                data['communicationDetails']['isAvailable'] = data['communicationDetails']['availability'][0]['id'];
            } else {
                data['communicationDetails']['isAvailable'] = null;
            }

            if (data['communicationDetails']['availabilityStatus'] && data['communicationDetails']['availabilityStatus'].length !== 0) {
                data['communicationDetails']['availabilityStatus'] = data['communicationDetails']['availabilityStatus'][0]['id'];
            } else {
                data['communicationDetails']['availabilityStatus'] = null;
            }

            // interview details
            if (!data['interviewDetails']['interviewClient']) data['interviewDetails']['interviewClient'] = '';
            if (!data['interviewDetails']['interviewRequirement']) data['interviewDetails']['interviewRequirement'] = '';
            if (!data['interviewDetails']['interviewComments']) data['interviewDetails']['interviewComments'] = '';

            if (data['interviewDetails']['isInterviewSubmitted'] === true) {
                data['interviewDetails']['isInterviewSubmitted'] = 1;
            } else if (data['interviewDetails']['isInterviewSubmitted'] === false) {
                data['interviewDetails']['isInterviewSubmitted'] = 0;
            } else if (data['interviewDetails']['isInterviewSubmitted'] === 0) {
                data['interviewDetails']['isInterviewSubmitted'] = 0;
            } else if (data['interviewDetails']['isInterviewSubmitted'] === 1) {
                data['interviewDetails']['isInterviewSubmitted'] = 1;
            } else {
                data['interviewDetails']['isInterviewSubmitted'] = null;
            }
            if (data['interviewDetails']['interviewStatus'] && data['interviewDetails']['interviewStatus'].length !== 0) {
                data['interviewDetails']['interviewStatus'] = data['interviewDetails']['interviewStatus'][0]['id'];
            } else {
                data['interviewDetails']['interviewStatus'] = null;
            }

            // created date / updated date updation
            // data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update candidatedetails set IsCommunicated=" +
                data['communicationDetails']['isCommunication'] +
                ", IsAvailable=" + data['communicationDetails']['isAvailable'] +
                ", AvailableStatus=" + data['communicationDetails']['availabilityStatus'] +
                ", AvailableComments='" + data['communicationDetails']['availabilityComments'] +
                "', AvailableClient='" + data['communicationDetails']['availabilityClient'] +
                "', AvailableRequirement='" + data['communicationDetails']['availabilityRequirement'] +
                "', IsInterviewSubmitted=" + data['interviewDetails']['isInterviewSubmitted'] +
                ", InterviewStatus=" + data['interviewDetails']['interviewStatus'] +
                ", InterviewClient='" + data['interviewDetails']['interviewClient'] +
                "', InterviewRequirement='" + data['interviewDetails']['interviewRequirement'] +
                "', InterviewComments='" + data['interviewDetails']['interviewComments'] +
                "', UpdatedBy=" + data['sourcedBy'] +
                ", UpdatedOn='" + data['updatedOn'] +
                "' where Id=" + uid;

            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            console.log("%%%%     ", err);
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from Skills where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }

    static getCount(id) {
        var query = "select max(Id) as count from candidatedetails";
        if (id) {
            query = 'select count(Id) as count from candidatedetails where IsPending = 1 and SourcedBy = ' + id;
        }
        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // validate user details availability status
    static validateCandidateDetailAvailability(param) {
        // Attempt to connect and execute queries if connection goes through

        var mobileNumber = param.mobileNumber;
        var email = param.email;
        var query = `select cd.Id as id,
                            cd.FirstName as firstName,
                            cd.LastName as lastName,
                            cd.MobileNumber as mobileNumber
                            from candidatedetails cd where cd.Email='` + email + `'`; //  cd.MobileNumber=` + mobileNumber + ` and

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static filter(skillsAnd, skillsOr, authorizations, qualificationsAnd, qualificationsOr, employers, cities, states, countries, users, vendors, domains, certifications, searchText) {
        var query = "select distinct c.Id as id from candidatedetails c";

        // join filter query formation
        if ((skillsAnd && skillsAnd.length !== 0) || (skillsOr && skillsOr.length !== 0) || (searchText && searchText.length !== 0)) {
            query += " left join skillsmapping sm on c.Id = sm.CandidateId left join skills s on s.Id = sm.SkillId";
        }
        if (authorizations && authorizations.length !== 0) {
            query += " left join authorizationsmapping am on c.Id = am.CandidateId left join authorizations a on a.Id = am.AuthorizationId";
        }
        if ((qualificationsAnd && qualificationsAnd.length !== 0) || (qualificationsOr && qualificationsOr.length !== 0)) {
            query += " left join qualificationsmapping qm on c.Id = qm.CandidateId left join qualifications q on q.Id = qm.QualificationId";
        }
        if (employers && employers.length !== 0) {
            query += " left join employersmapping em on c.Id = em.CandidateId left join employers e on e.Id = em.EmployerId";
        }
        if (domains && domains.length !== 0) {
            query += " left join domainsmapping dm on c.Id = dm.CandidateId left join domains d on d.Id = dm.DomainId";
        }
        if (certifications && certifications.length !== 0) {
            query += " left join certificationsmapping cm on c.Id = cm.CandidateId left join certifications cf on cf.Id = cm.CertificationId";
        }

        if ((skillsAnd && skillsAnd.length !== 0) ||
            (skillsOr && skillsOr.length !== 0) ||
            (authorizations && authorizations.length !== 0) ||
            (qualificationsAnd && qualificationsAnd.length !== 0) ||
            (qualificationsOr && qualificationsOr.length !== 0) ||
            (employers && employers.length !== 0) ||
            (cities && cities.length !== 0) ||
            (states && states.length !== 0) ||
            (countries && countries.length !== 0) ||
            (users && users.length !== 0) ||
            (vendors && vendors.length !== 0) ||
            (domains && domains.length !== 0) ||
            (certifications && certifications.length !== 0)) {
            query += " where (";
        }

        var intialFlag = true;
        // conditions applied
        if (skillsAnd && skillsAnd.length !== 0) {
            for (var i = 0; i < skillsAnd.length; i++) {
                query += "s.Id=" + skillsAnd[i]['id'];
                if (i !== skillsAnd.length - 1) {
                    query += " and ";
                }
            }
            intialFlag = false;
        }
        if (skillsOr && skillsOr.length !== 0) {
            if (!intialFlag) {
                query += " and ( 1 = 1 or ";
            }
            query += "s.Id in (";
            for (var i = 0; i < skillsOr.length; i++) {
                query += skillsOr[i]['id'];
                if (i !== skillsOr.length - 1) {
                    query += ", ";
                }
            }
            query += ")";
            if (!intialFlag) {
                query += ")";
            }
            intialFlag = false;
        }
        if (authorizations && authorizations.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "a.Id in (";
            for (var i = 0; i < authorizations.length; i++) {
                query += authorizations[i]['id'];
                if (i !== authorizations.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (qualificationsAnd && qualificationsAnd.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            for (var i = 0; i < qualificationsAnd.length; i++) {
                query += "q.Id=" + qualificationsAnd[i]['id'];
                if (i !== qualificationsAnd.length - 1) {
                    query += "and ";
                }
            }
            intialFlag = false;
        }
        if (qualificationsOr && qualificationsOr.length !== 0) {
            if (!intialFlag) {
                query += " and ( 1 = 1 or ";
            }
            query += "q.Id in (";
            for (var i = 0; i < qualificationsOr.length; i++) {
                query += qualificationsOr[i]['id'];
                if (i !== qualificationsOr.length - 1)
                    query += ", ";
            }
            query += ")";
            if (!intialFlag) {
                query += ")";
            }
            intialFlag = false;
        }
        if (employers && employers.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "e.Id in (";
            for (var i = 0; i < employers.length; i++) {
                query += employers[i]['id'];
                if (i !== employers.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (domains && domains.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "dm.Id in (";
            for (var i = 0; i < domains.length; i++) {
                query += domains[i]['id'];
                if (i !== domains.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (certifications && certifications.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "cf.Id in (";
            for (var i = 0; i < certifications.length; i++) {
                query += certifications[i]['id'];
                if (i !== certifications.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (cities && cities.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.CityId in (";
            for (var i = 0; i < cities.length; i++) {
                query += cities[i]['id'];
                if (i !== cities.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (states && states.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.StateId in (";
            for (var i = 0; i < states.length; i++) {
                query += states[i]['id'];
                if (i !== states.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (countries && countries.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.CountryId in (";
            for (var i = 0; i < countries.length; i++) {
                query += countries[i]['id'];
                if (i !== countries.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (users && users.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.SourcedBy in (";
            for (var i = 0; i < users.length; i++) {
                query += users[i]['id'];
                if (i !== users.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (vendors && vendors.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.SourcedFrom in (";
            for (var i = 0; i < vendors.length; i++) {
                query += vendors[i]['id'];
                if (i !== vendors.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }

        if ((skillsAnd && skillsAnd.length !== 0) ||
            (skillsOr && skillsOr.length !== 0) ||
            (authorizations && authorizations.length !== 0) ||
            (qualificationsAnd && qualificationsAnd.length !== 0) ||
            (qualificationsOr && qualificationsOr.length !== 0) ||
            (employers && employers.length !== 0) ||
            (cities && cities.length !== 0) ||
            (states && states.length !== 0) ||
            (countries && countries.length !== 0) ||
            (users && users.length !== 0) ||
            (vendors && vendors.length !== 0) ||
            (domains && domains.length !== 0) ||
            (certifications && certifications.length !== 0)) {
            query += ")";
        }

        if (searchText && searchText.length !== 0) {
            if (!intialFlag) {
                query += " and (";
            } else {
                query += " where ";
            }
            for (var i = 0; i < searchText.length; i++) {
                if (searchText[i]) {
                    query += "(Lower(s.Name) like '%" + (searchText[i]).toLowerCase() + "%' or Lower(c.JobTitle) like '%" + (searchText[i]).toLowerCase() + "%' or Lower(c.FirstName) like '%" + (searchText[i]).toLowerCase() + "%'  or Lower(c.LastName) like '%" + (searchText[i]).toLowerCase() + "%')";
                }
                if (i !== searchText.length - 1) {
                    query += " or ";
                }
            }
            if (!intialFlag) {
                query += ") and c.IsPending = 0";
            }
        }

        console.log(query);


        return new Promise((resolve, reject) => {

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }
}

module.exports = ClientService;