var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var errorService = require('../services/error.service');
var historyService = require('../services/history.service');


/* static product service class */
class submitalmappingService {

    static create(data) {
        if (data) {
            // validation
            if (!data['candidateId']) data['candidateId'] = null;
            if (!data['requirementId']) data['requirementId'] = null;
            if (!data['clientId']) data['clientId'] = null;
            if (!data['submittedTo']) data['submittedTo'] = null;
            if (!data['comments']) data['comments'] = "";
            if (!data['createdOn']) data['createdOn'] = "";
            if (!data['createdBy']) data['createdBy'] = null;
            if (!data['updatedBy']) data['updatedBy'] = null;
            if (!data['updatedOn']) data['updatedOn'] = "";
            // if (!data['isInterviewScheduled']) data['isInterviewScheduled'] = null;
            // if (!data['isCancelled']) data['isCancelled'] = null;
            if (!data['cancelReason']) data['cancelReason'] = null;
            var query = "Insert into submitalmapping (CandidateId, SubmittedTo, RequirementId, ClientId, Comments, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, IsInterviewScheduled, IsCancelled, CancelReason) values ";
            for (var i = 0; i < data['requirementList'].length; i++) {
                query += "(" + data['candidateId'] + ", " + data['submittedTo'] + ", " + data['requirementList'][i]['id'] + ", " + data['requirementList'][i]['clientId'] + ", '" + data['comments'] + "', '" + data['createdOn'] + "', " + data['createdBy'] + ", '" + data['updatedOn'] + "', " + data['updatedBy'] + ", " + data['requirementList'][i]['isInterviewScheduled'] + ", " + data['isCancelled'] + ", " + data['cancelReason'] + ")";
                if (i !== data['requirementList'].length - 1) {
                    query += ", ";
                }
            }
            // history geneartion
            // historyService.create(data);
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('submittal', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(id, candidateId, requirementId, clientId, submittedTo) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId) {
                query = `select distinct Id as id, CandidateId as candidateId, SubmittedTo as submittedTo, RequirementId as requirementId, ClientId as clientId, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = s.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = s.ClientId) as clientName,
                        (select r.title from requirementdetails r where r.Id = s.RequirementId) as requirementName,
                        (select c.Name from submittalData c where c.Id = s.SubmittedTo) as submittedToName,
                        (select Alias from users u where u.Id = s.CreatedBy) as userName,
                        (select FileName from users u where u.Id = s.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = s.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = s.UpdatedBy) as updateProfileImage from submitalmapping s where s.IsInterviewScheduled != 1 and s.IsCancelled != 1 and s.CandidateId = ` + candidateId;
            } else if (requirementId) {
                query = `select distinct Id as id, CandidateId as candidateId, SubmittedTo as submittedTo, RequirementId as requirementId, ClientId as clientId, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = s.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = s.ClientId) as clientName,
                        (select r.title from requirementdetails r where r.Id = s.RequirementId) as requirementName,
                        (select c.Name from submittalData c where c.Id = s.SubmittedTo) as submittedToName,
                        (select Alias from users u where u.Id = s.CreatedBy) as userName,
                        (select FileName from users u where u.Id = s.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = s.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = s.UpdatedBy) as updateProfileImage from submitalmapping s where s.IsInterviewScheduled != 1 and s.IsCancelled != 1 and s.RequirementId = ` + requirementId;
            } else if (clientId) {
                query = `select distinct Id as id, CandidateId as candidateId, SubmittedTo as submittedTo, RequirementId as requirementId, ClientId as clientId, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = s.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = s.ClientId) as clientName,
                        (select r.title from requirementdetails r where r.Id = s.RequirementId) as requirementName,
                        (select c.Name from submittalData c where c.Id = s.SubmittedTo) as submittedToName,
                        (select Alias from users u where u.Id = s.CreatedBy) as userName,
                        (select FileName from users u where u.Id = s.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = s.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = s.UpdatedBy) as updateProfileImage from submitalmapping s where s.IsInterviewScheduled != 1 and s.IsCancelled != 1 and s.ClientId = ` + clientId;
            } else if (submittedTo) {
                query = `select distinct Id as id, CandidateId as candidateId, SubmittedTo as submittedTo, RequirementId as requirementId, ClientId as clientId, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = s.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = s.ClientId) as clientName,
                        (select r.title from requirementdetails r where r.Id = s.RequirementId) as requirementName,
                        (select Alias from users u where u.Id = s.CreatedBy) as userName,
                        (select FileName from users u where u.Id = s.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = s.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = s.UpdatedBy) as updateProfileImage from submitalmapping s where s.IsInterviewScheduled != 1 and s.IsCancelled != 1 and s.SubmittedTo = ` + submittedTo;
            } else if (id) {
                query = `select distinct Id as id, CandidateId as candidateId, SubmittedTo as submittedTo, RequirementId as requirementId, ClientId as clientId, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = s.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = s.ClientId) as clientName,
                        (select r.title from requirementdetails r where r.Id = s.RequirementId) as requirementName,
                        (select c.Name from submittalData c where c.Id = s.SubmittedTo) as submittedToName,
                        (select Alias from users u where u.Id = s.CreatedBy) as userName,
                        (select FileName from users u where u.Id = s.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = s.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = s.UpdatedBy) as updateProfileImage from submitalmapping s where s.IsInterviewScheduled != 1 and s.IsCancelled != 1 and s.Id = ` + id;
            } else {
                query = `select distinct Id as id, CandidateId as candidateId, SubmittedTo as submittedTo, RequirementId as requirementId, ClientId as clientId, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = s.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = s.ClientId) as clientName,
                        (select r.title from requirementdetails r where r.Id = s.RequirementId) as requirementName,
                        (select c.Name from submittalData c where c.Id = s.SubmittedTo) as submittedToName,
                        (select Alias from users u where u.Id = s.CreatedBy) as userName,
                        (select FileName from users u where u.Id = s.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = s.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = s.UpdatedBy) as updateProfileImage from submitalmapping s where s.IsInterviewScheduled != 1 and s.IsCancelled != 1 order by s.UpdatedOn desc`;
            }

            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('submittal', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    // dummy method
    static update(uid, data) {
        if (data && uid) {
            // validation
            if (!data['candidateId']) data['candidateId'] = null;
            if (!data['requirementId']) data['requirementId'] = null;
            if (!data['clientId']) data['clientId'] = null;
            if (!data['submittedTo']) data['submittedTo'] = null;
            if (!data['comments']) data['comments'] = "";
            if (!data['updatedBy']) data['updatedBy'] = null;
            if (!data['updatedOn']) data['updatedOn'] = "";
            var query = 'Update submitalmapping set SubmittedTo=' + data['submittedTo'] + ', RequirementId=' + data['requirementId'] + ', ClientId=' + data['clientId'] + ', Comments=' + data['comments'] + ', UpdatedOn="' + data['updatedOn'] + '", UpdatedBy=' + data['updatedBy'] + ' where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('submittal', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static updateStatus(uid, data) {
        if (data && uid) {
            // validation
            if (!data['cancelReason']) data['cancelReason'] = null;
            if (!data['updatedBy']) data['updatedBy'] = null;
            if (!data['updatedOn']) data['updatedOn'] = "";
            var query = 'Update submitalmapping set IsInterviewScheduled=' + data['isInterviewScheduled'] + ', IsCancelled=' + data['isCancelled'] + ', CancelReason=' + data['cancelReason'] + ', UpdatedOn="' + data['updatedOn'] + '", UpdatedBy=' + data['updatedBy'] + ' where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('submittal', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }
    // dummy method
    static delete(uid) {
        if (uid) {
            var query = 'Delete from submitalmapping where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('submittal', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = submitalmappingService;