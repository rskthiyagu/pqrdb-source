var moment = require('moment');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var errorService = require('../services/error.service');


/* static product service class */
class CandidateService {
    static create(data) {
        if (data) {
            // null check
            if (!data['firstName']) data['firstName'] = '';
            if (!data['lastName']) data['lastName'] = '';
            if (!data['mobileNumber']) data['mobileNumber'] = null;
            if (!data['email']) data['email'] = '';
            if (!data['linkedin']) data['linkedin'] = '';
            if (!data['experience']) data['experience'] = null;
            if (!data['sourcedBy']) data['sourcedBy'] = null;
            if (!data['vendors']) data['sourcedFrom'] = null;
            if (!data['fileName']) data['fileName'] = '';
            if (!data['cities']) data['cityId'] = null;
            if (!data['states']) data['stateId'] = null;
            if (!data['countries']) data['countryId'] = null;
            if (!data['jobTitle']) data['jobTitle'] = '';

            if (data['gender'] && data['gender'].length !== 0) {
                data['gender'] = data['gender'][0]['id'];
            } else {
                data['gender'] = null;
            }
            if (data['cities'] && data['cities'].length !== 0) {
                data['cityId'] = data['cities'][0]['id'];
            } else {
                data['cityId'] = null;
            }
            if (data['states'] && data['states'].length !== 0) {
                data['stateId'] = data['states'][0]['id'];
            } else {
                data['stateId'] = null;
            }
            if (data['countries'] && data['countries'].length !== 0) {
                data['countryId'] = data['countries'][0]['id'];
            } else {
                data['countryId'] = null;
            }
            if (data['vendors'] && data['vendors'].length !== 0) {
                data['sourcedFrom'] = data['vendors'][0]['id'];
            } else {
                data['sourcedFrom'] = null;
            }

            // adding new fields
            if (!data['hourlyRate']) data['hourlyRate'] = ''; 
            if (!data['expectedSalary']) data['expectedSalary'] = '';
            if (!data['currentCTC']) data['currentCTC'] = '';
            if (data['expectedCTC']) data['expectedCTC'] = '';
            if (data['noticePeriod']) data['noticePeriod'] = '';
            if (data['relocation'] && data['relocation'].length !== 0) {
                data['relocation'] = data['relocation'][0]['id'];
            } else {
                data['relocation'] = null;
            }
            if (data['knownCandidate'] && data['knownCandidate'].length !== 0) {
                data['knownCandidate'] = data['knownCandidate'][0]['id'];
            } else {
                data['knownCandidate'] = null;
            }
            if (data['knownCandidateSubmitted'] && data['knownCandidateSubmitted'].length !== 0) {
                data['knownCandidateSubmitted'] = data['knownCandidateSubmitted'][0]['id'];
            } else {
                data['knownCandidateSubmitted'] = null;
            }
            if (data['knownCandidateInterviewed'] && data['knownCandidateInterviewed'].length !== 0) {
                data['knownCandidateInterviewed'] = data['knownCandidateInterviewed'][0]['id'];
            } else {
                data['knownCandidateInterviewed'] = null;
            }
            if (data['knownCandidateInterviewRound'] && data['knownCandidateInterviewRound'].length !== 0) {
                data['knownCandidateInterviewRound'] = data['knownCandidateInterviewRound'][0]['id'];
            } else {
                data['knownCandidateInterviewRound'] = null;
            }
            if (data['knownCandidateInterviewResult'] && data['knownCandidateInterviewResult'].length !== 0) {
                data['knownCandidateInterviewResult'] = data['knownCandidateInterviewResult'][0]['id'];
            } else {
                data['knownCandidateInterviewResult'] = null;
            }
            if (!data['comments']) data['comments'] = ''; 
            if (!data['communicationSkill']) data['communicationSkill'] = null;


            // created date / updated date updation
            // data['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            // data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');

            var query = "Insert into candidatedetails (FirstName, LastName, Email, MobileNumber, SourcedBy, SourcedFrom, CreatedOn, UpdatedOn, CityId, StateId, CountryId, Experience, Linkedin, FileName, JobTitle, IsPending, HourlyRate, ExpectedSalary, CurrentCTC, ExpectedCTC, NoticePeriod, IsRelocation,  KnownCandidate, KnownCandidateSubmitted, KnownCandidateInterviewed, KnownCandidateInterviewRound, KnownCandidateInterviewResult, Comments, CommunicationSkill, Gender) values ('"
            + data['firstName'] + "', '" + data['lastName'] + "', '" + data['email'] + "', " + data['mobileNumber'] + ", " + data['sourcedBy'] + ", " + data['sourcedFrom'] + ", '" + data['createdOn'] + "', '" + data['updatedOn'] + "', " + data['cityId'] + ", " + data['stateId'] + ", " + data['countryId'] + ", " + data['experience'] + ", '" + data['linkedin'] + "', '" + data['fileName'] + "', '" + data['jobTitle'] + "', " + 0 + ", '" + data['hourlyRate'] + "', '" + data['expectedSalary'] + "', '" + data['currentCTC'] + "', '" + data['expectedCTC'] + "', '" + data['noticePeriod'] + "', " + data['relocation'] + ", " + data['knownCandidate'] + ", " + data['knownCandidateSubmitted'] + ", " + data['knownCandidateInterviewed'] + ", " + data['knownCandidateInterviewRound'] + ", " + data['knownCandidateInterviewResult'] + ", '" + data['comments'] + "', " + data['communicationSkill'] + ", " + data['gender'] + ")";
            console.log(query);
            var connection = dbAccessService.createConnection();
            return new Promise((resolve, reject) => {
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('candidate', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            // need to pass id to front end
                            return resolve({'id': result.insertId});
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // create resume only candidates
    static createPendingRecord(data) {
        if (data && data.length !== 0) {
            var query = 'Insert into candidatedetails (SourcedBy, CreatedOn, UpdatedOn, FileName, IsPending) values';
            for (var i = 0; i < data.length; i++) {
                // null check
                if (!data[i]['sourcedBy']) data[i]['sourcedBy'] = null;
                if (!data[i]['fileName']) data[i]['fileName'] = '';

                // created date / updated date updation
                // data[i]['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
                // data[i]['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
                query += " (" + data[i]['sourcedBy'] + ", '" + data[i]['createdOn'] + "', '" + data[i]['updatedOn'] + "', '" + data[i]['fileName'] + "', " + 1 + ")";
                if (i !== data.length - 1) {
                    query += ',';
                }
            }
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('candidate', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{ 'status': 'success' }]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(candidateId, candidateList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId) {
                query = `select distinct c.Id as id, c.FirstName as firstName, c.LastName as lastName, c.Email as email, c.MobileNumber as mobileNumber, c.SourcedBy as sourcedBy, c.SourcedFrom as sourcedFrom, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn, c.IsAvailable as isAvailable, c.IsInterviewSubmitted as isInterviewSubmitted, c.FileName as fileName, c.JobTitle as jobTitle, c.Experience as experience, c.Linkedin as linkedin, c.CityId as cityId, c.StateId as stateId, c.CountryId as countryId, (select count(Id) from candidatedetails cd where cd.IsPending = 0) as total, HourlyRate as hourlyRate, ExpectedSalary as expectedSalary, CurrentCTC as currentCTC, ExpectedCTC as expectedCTC, NoticePeriod as noticePeriod, IsRelocation as relocation, c.Gender as gender,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName,
                        (select FileName from users u where u.Id = c.SourcedBy) as profileImage,
                        (select Alias from users u where u.Id = c.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = c.UpdatedBy) as updateProfileImage from candidatedetails c where c.Id = ` + candidateId + ' order by c.UpdatedOn desc';
            } else if (candidateList && candidateList.length !== 0) {
                query = `select distinct c.Id as id, c.FirstName as firstName, c.LastName as lastName, c.Email as email, c.MobileNumber as mobileNumber, c.SourcedBy as sourcedBy, c.SourcedFrom as sourcedFrom, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn, c.IsAvailable as isAvailable, c.IsInterviewSubmitted as isInterviewSubmitted, c.FileName as fileName, c.JobTitle as jobTitle, c.Experience as experience, c.CityId as cityId, c.StateId as stateId, c.CountryId as countryId, (select count(Id) from candidatedetails cd where cd.IsPending = 0) as total, IsRelocation as relocation, c.Gender as gender,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName,
                        (select FileName from users u where u.Id = c.SourcedBy) as profileImage,
                        (select Alias from users u where u.Id = c.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = c.UpdatedBy) as updateProfileImage from candidatedetails c`;
                query += " where (c.Id in (";
                for (var i = 0; i < candidateList.length; i++) {
                    query += candidateList[i]['id'];
                    if (i !== candidateList.length - 1) {
                        query += ", ";
                    }
                }
                query += ")) and c.IsPending = 0 order by c.UpdatedOn desc";
            } else {
                query = `select distinct c.Id as id, c.FirstName as firstName, c.LastName as lastName, c.Email as email, c.MobileNumber as mobileNumber, c.SourcedBy as sourcedBy, c.SourcedFrom as sourcedFrom, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn, c.IsAvailable as isAvailable, c.IsInterviewSubmitted as isInterviewSubmitted, c.FileName as fileName, c.JobTitle as jobTitle, c.Experience as experience, c.CityId as cityId, c.StateId as stateId, c.CountryId as countryId, (select count(Id) from candidatedetails cd where cd.IsPending = 0) as total, IsRelocation as relocation, c.Gender as gender,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName,
                        (select FileName from users u where u.Id = c.SourcedBy) as profileImage,
                        (select Alias from users u where u.Id = c.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = c.UpdatedBy) as updateProfileImage from candidatedetails c where c.IsPending = 0  order by c.UpdatedOn desc`;
            }

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // fetch past history
    static retrievePastHistory(candidateId) {
        var query = 'select KnownCandidate as knownCandidate, KnownCandidateSubmitted as knownCandidateSubmitted, KnownCandidateInterviewed as knownCandidateInterviewed, KnownCandidateInterviewRound as knownCandidateInterviewRound, KnownCandidateInterviewResult as knownCandidateInterviewResult, Comments as comments, CommunicationSkill as communicationSkill from candidatedetails where Id=' + candidateId;
        return new Promise((resolve, reject) => {
        var connection = dbAccessService.createConnection();
        connection.connect(function (err) {
            if (err) {
                return reject(err);
            } else {
                connection.query(query, function (err, result) {
                    if (err) {
                        errorService.create('candidate', err);
                        return reject(err);
                    }
                    connection.end(function (err) {
                        if (err) throw reject(err);
                    });
                    return resolve(result);
                });
            }
        });
    });
    }

    // fetch availability info of candidate
    static retrieveCandidateAvailability(candidateId, candidateList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId) {
                query = `select distinct c.Id as id, c.CountryId as countryId, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                c.HourlyRate as hourlyRate, c.ExpectedSalary as expectedSalary, c.CurrentCTC as currentCTC, c.ExpectedCTC as expectedCTC, c.NoticePeriod as noticePeriod, c.TaxTerms as taxTerms, c.Duration as duration, c.AvailabilityPeriod as candidateAvailabilityPeriod, c.ExpectedAvailableMonth as availableMonth,
                (select name from countries where Id = c.CountryId) as countryName 
                from candidatedetails c where c.IsPending = 0 and c.Id = ` + candidateId;
            } else if (candidateList && candidateList.length !== 0) {
                query = `select distinct c.Id as id, c.CountryId as countryId, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                c.HourlyRate as hourlyRate, c.ExpectedSalary as expectedSalary, c.CurrentCTC as currentCTC, c.ExpectedCTC as expectedCTC, c.NoticePeriod as noticePeriod, c.TaxTerms as taxTerms, c.Duration as duration, c.AvailabilityPeriod as candidateAvailabilityPeriod, c.ExpectedAvailableMonth as availableMonth,
                (select name from countries where Id = c.CountryId) as countryName 
                from candidatedetails c`;
                query += " where (c.Id in (";
                for (var i = 0; i < candidateList.length; i++) {
                    query += candidateList[i]['id'];
                    if (i !== candidateList.length - 1) {
                        query += ", ";
                    }
                }
                query += ")) and c.IsPending = 0 order by c.CreatedOn desc";
            } else {
                query = `select distinct c.Id as id, c.CountryId as countryId, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                        c.HourlyRate as hourlyRate, c.ExpectedSalary as expectedSalary, c.CurrentCTC as currentCTC, c.ExpectedCTC as expectedCTC, c.NoticePeriod as noticePeriod, c.TaxTerms as taxTerms, c.Duration as duration, c.AvailabilityPeriod as candidateAvailabilityPeriod, c.ExpectedAvailableMonth as availableMonth,
                        (select name from countries where Id = c.CountryId) as countryName 
                        from candidatedetails c where c.IsPending = 0 order by c.CreatedOn desc`;
            }
            
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static retrievePendingRecord(id, flag) {
        return new Promise((resolve, reject) => {
            var query;
            // if (flag) {
            //     query = 'select top 4 ';
            // } else {
            query = 'select ';
            // }
            query += `c.Id as id, c.FileName as fileName, c.IsPending as isPending, c.CreatedOn as createdOn, (select count(Id) from candidatedetails cd where cd.IsPending = 1`
            if (typeof (id) === 'string') {
                query += ` and cd.SourcedBy=` + id;
            } else {
                if (id && id.length !== 0) {
                    query += ' and cd.SourcedBy in (';
                    for (var i = 0; i < id.length; i++) {
                        query += id[i];
                        if (i !== id.length - 1) {
                            query += ', ';
                        }
                    }
                    query += ')';
                }
            }
            query += `) as total,
            (select Alias from users u where u.Id = c.SourcedBy) as userName,
            (select FileName from users u where u.Id = c.SourcedBy) as profileImage from candidatedetails c where c.IsPending = 1`;
            if (typeof (id) === 'string') {
                query += ` and c.SourcedBy=` + id;
            } else if (id && id.length !== 0) {
                query += ' and c.SourcedBy in (';
                for (var i = 0; i < id.length; i++) {
                    query += id[i];
                    if (i !== id.length - 1) {
                        query += ', ';
                    }
                }
                query += ')';
            }
            query += ' order by c.CreatedOn desc';

            if (flag) {
                query += ' limit 4';
            }

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static updateDeleteRecord(data) {
        if (data) {
            var tableName = '';
            var key = '';
            var value = '';
            var id;

            // get values
            if (data['key'] === 'cities') {
                tableName = 'candidatedetails';
                key = 'CityId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'states') {
                tableName = 'candidatedetails';
                key = 'StateId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'countries') {
                tableName = 'candidatedetails';
                key = 'CountryId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'vendors') {
                tableName = 'candidatedetails';
                key = 'SourcedFrom';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'skills') {
                tableName = 'skillsmapping';
                key = 'SkillId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'authorizations') {
                tableName = 'authorizationsmapping';
                key = 'AuthorizationId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'employers') {
                tableName = 'employersmapping';
                key = 'EmployerId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'qualifications') {
                tableName = 'qualificationsmapping';
                key = 'QualificationId';
                value = data['value'];
                id = data['id'];
            }

            // created date / updated date updation
            data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update " + tableName + " set " + key + "=" + value + " where " + key + "=" + id;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('candidate', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // get statistics data of candidate count
    static getCandidateTotalStatistics(id) {
        return new Promise((resolve, reject) => {
            var query = `select count(Id) as candidateTotal, 
            (select count(Id) from candidatedetails where IsPending = 0 and SourcedBy = ` + id + `) as userCandidateTotal from candidatedetails where IsPending = 0;`
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // get statistics data of pending candidate total
    static getPendingCandidateTotalStatistics(id) {
        return new Promise((resolve, reject) => {
            var query = `select count(Id) as candidatePendingTotal,
            (select count(Id) from candidatedetails where IsPending = 1 and SourcedBy = ` + id +
                `) as userCandidatePendingTotal from candidatedetails where IsPending = 1;`

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // get statistics data of candidate today
    static getCandidateTotalTodayStatistics(id) {
        return new Promise((resolve, reject) => {
            var currentDate = moment.utc().format('YYYY-MM-DD');
            var query = `select count(Id) as candidateTodayTotal,
            (select count(Id) from candidatedetails where SourcedBy = ` + id + ` and CreatedOn > '` + currentDate + ` 00:00:00.000') as userCandidateTodayTotal from candidatedetails where CreatedOn > '` + currentDate + ` 00:00:00.000';`

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static retrieveNewlyCreated(flag) {
        return new Promise((resolve, reject) => {
            var query;
            // if (flag) {
            //     query = 'select top 12 ';
            // } else {
            query = 'select ';
            // }
            query += `c.Id as id, c.FirstName as firstName, c.LastName as lastName, c.Email as email, c.MobileNumber as mobileNumber, c.SourcedBy as sourcedBy, c.SourcedFrom as sourcedFrom, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn, c.IsAvailable as isAvailable, c.FileName as fileName, c.JobTitle as jobTitle, c.Experience as experience, c.Linkedin as linkedin, c.CityId as cityId, c.StateId as stateId, c.CountryId as countryId, c.Gender as genderId, (select count(Id) from candidatedetails cd where cd.IsPending = 0) as total,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName from candidatedetails c where IsPending = 0 order by c.CreatedOn desc`;

            if (flag) {
                query += ' limit 12';
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }


    static update(uid, data) {
        if (data) {
            // null check
            if (!data['firstName']) data['firstName'] = '';
            if (!data['lastName']) data['lastName'] = '';
            if (!data['mobileNumber']) data['mobileNumber'] = null;
            if (!data['email']) data['email'] = '';
            if (!data['linkedin']) data['linkedin'] = '';
            if (!data['experience']) data['experience'] = null;
            if (!data['sourcedBy']) data['sourcedBy'] = null;
            if (!data['vendors']) data['sourcedFrom'] = null;
            if (!data['fileName']) data['fileName'] = '';
            if (!data['jobTitle']) data['jobTitle'] = '';
            if (!data['cities']) data['cityId'] = null;
            if (!data['states']) data['stateId'] = null;
            if (!data['countries']) data['countryId'] = null;

            if (data['cities'] && data['cities'].length !== 0) {
                data['cityId'] = data['cities'][0]['id'];
            }
            if (data['states'] && data['states'].length !== 0) {
                data['stateId'] = data['states'][0]['id'];
            }
            if (data['countries'] && data['countries'].length !== 0) {
                data['countryId'] = data['countries'][0]['id'];
            }
            if (data['vendors'] && data['vendors'].length !== 0) {
                data['sourcedFrom'] = data['vendors'][0]['id'];
            }

            // created date / updated date updation
            // data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update candidatedetails set FirstName='" + data['firstName'] + "', LastName='" + data['lastName'] + "', Email='" + data['email'] + "', MobileNumber=" + data['mobileNumber'] + ", UpdatedBy=" + data['sourcedBy'] + ", SourcedFrom=" + data['sourcedFrom'] + ", UpdatedOn='" + data['updatedOn'] + "', CityId=" + data['cityId'] + ", StateId=" + data['stateId'] + ", CountryId=" + data['countryId'] + ", Experience=" + data['experience'] + ", Linkedin='" + data['linkedin'] + "', fileName='" + data['fileName'] + "', JobTitle='" + data['jobTitle'] + "', IsPending=" + 0 + " where Id=" + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('candidate', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // update availability details of candidates
    static updateCandidateAvailability(uid, data) {
        if (data) {
            if (!data['availabilityComments']) data['availabilityComments'] = '';
            if (!data['isCommunication']) data['isCommunication'] = null;
            if (!data['hourlyRate']) data['hourlyRate'] = '';
            if (!data['salary']) data['salary'] = '';
            if (!data['taxTerms']) data['taxTerms'] = null;
            if (!data['duration']) data['duration'] = null;
            if (!data['currentCTC']) data['currentCTC'] = '';
            if (!data['expectedCTC']) data['expectedCTC'] = '';
            if (!data['noticePeriod']) data['noticePeriod'] = '';
            if (!data['candidateAvailabilityPeriod']) data['candidateAvailabilityPeriod'] = null;
            if (!data['availableMonth']) data['availableMonth'] = '';


            // created date / updated date updation
            // data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update candidatedetails set IsCommunicated=" +
                data['isCommunication'] +
                ", IsAvailable=" + data['availability'] +
                ", AvailableComments='" + data['availabilityComments'] +
                "', HourlyRate='" + data['hourlyRate'] +
                "', ExpectedSalary='" + data['salary'] +
                "', TaxTerms=" + data['taxTerms'] +
                ", Duration=" + data['duration'] +
                ", CurrentCTC='" + data['currentCTC'] +
                "', ExpectedCTC='" + data['expectedCTC'] +
                "', NoticePeriod='" + data['noticePeriod'] +
                "', AvailabilityPeriod=" + data['candidateAvailabilityPeriod'] +
                ", ExpectedAvailableMonth='" + data['availableMonth'] +
                "', UpdatedBy=" + data['updatedBy'] +
                ", UpdatedOn='" + data['updatedOn'] +
                "' where Id=" + uid;

                console.log(query);

            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('candidate', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from candidatedetails where Id = ' + uid;

            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('candidate', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }

    static getCount(id) {
        var query = "select max(Id) as count from candidatedetails";
        if (id) {
            query = 'select count(Id) as count from candidatedetails where IsPending = 1 and SourcedBy = ' + id;
        }
        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // validate user details availability status
    static validateCandidateDetailAvailability(param) {
        // Attempt to connect and execute queries if connection goes through

        var mobileNumber = param.mobileNumber;
        var email = param.email;
        var query = `select cd.Id as id,
                            cd.FirstName as firstName,
                            cd.LastName as lastName,
                            cd.MobileNumber as mobileNumber
                            from candidatedetails cd where cd.Email='` + email + `'`; //  cd.MobileNumber=` + mobileNumber + ` and

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static filter(skillsAnd, skillsOr, authorizations, qualificationsAnd, qualificationsOr, employers, cities, states, countries, users, vendors, domains, certifications, searchText) {
        var query = "select distinct c.Id as id from candidatedetails c";

        // join filter query formation
        if ((skillsAnd && skillsAnd.length !== 0) || (skillsOr && skillsOr.length !== 0) || (searchText && searchText.length !== 0)) {
            query += " left join skillsmapping sm on c.Id = sm.CandidateId left join skills s on s.Id = sm.SkillId";
        }
        if (authorizations && authorizations.length !== 0) {
            query += " left join authorizationsmapping am on c.Id = am.CandidateId left join authorizations a on a.Id = am.AuthorizationId";
        }
        if ((qualificationsAnd && qualificationsAnd.length !== 0) || (qualificationsOr && qualificationsOr.length !== 0)) {
            query += " left join qualificationsmapping qm on c.Id = qm.CandidateId left join qualifications q on q.Id = qm.QualificationId";
        }
        if (employers && employers.length !== 0) {
            query += " left join employersmapping em on c.Id = em.CandidateId left join employers e on e.Id = em.EmployerId";
        }
        if (domains && domains.length !== 0) {
            query += " left join domainsmapping dm on c.Id = dm.CandidateId left join domains d on d.Id = dm.DomainId";
        }
        if (certifications && certifications.length !== 0) {
            query += " left join certificationsmapping cm on c.Id = cm.CandidateId left join certifications cf on cf.Id = cm.CertificationId";
        }

        if ((skillsAnd && skillsAnd.length !== 0) ||
            (skillsOr && skillsOr.length !== 0) ||
            (authorizations && authorizations.length !== 0) ||
            (qualificationsAnd && qualificationsAnd.length !== 0) ||
            (qualificationsOr && qualificationsOr.length !== 0) ||
            (employers && employers.length !== 0) ||
            (cities && cities.length !== 0) ||
            (states && states.length !== 0) ||
            (countries && countries.length !== 0) ||
            (users && users.length !== 0) ||
            (vendors && vendors.length !== 0) ||
            (domains && domains.length !== 0) ||
            (certifications && certifications.length !== 0)) {
            query += " where (";
        }

        var intialFlag = true;
        // conditions applied
        if (skillsAnd && skillsAnd.length !== 0) {
            for (var i = 0; i < skillsAnd.length; i++) {
                query += "s.Id=" + skillsAnd[i]['id'];
                if (i !== skillsAnd.length - 1) {
                    query += " and ";
                }
            }
            intialFlag = false;
        }
        if (skillsOr && skillsOr.length !== 0) {
            if (!intialFlag) {
                query += " and ( 1 = 1 or ";
            }
            query += "s.Id in (";
            for (var i = 0; i < skillsOr.length; i++) {
                query += skillsOr[i]['id'];
                if (i !== skillsOr.length - 1) {
                    query += ", ";
                }
            }
            query += ")";
            if (!intialFlag) {
                query += ")";
            }
            intialFlag = false;
        }
        if (authorizations && authorizations.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "a.Id in (";
            for (var i = 0; i < authorizations.length; i++) {
                query += authorizations[i]['id'];
                if (i !== authorizations.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (qualificationsAnd && qualificationsAnd.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            for (var i = 0; i < qualificationsAnd.length; i++) {
                query += "q.Id=" + qualificationsAnd[i]['id'];
                if (i !== qualificationsAnd.length - 1) {
                    query += "and ";
                }
            }
            intialFlag = false;
        }
        if (qualificationsOr && qualificationsOr.length !== 0) {
            if (!intialFlag) {
                query += " and ( 1 = 1 or ";
            }
            query += "q.Id in (";
            for (var i = 0; i < qualificationsOr.length; i++) {
                query += qualificationsOr[i]['id'];
                if (i !== qualificationsOr.length - 1)
                    query += ", ";
            }
            query += ")";
            if (!intialFlag) {
                query += ")";
            }
            intialFlag = false;
        }
        if (employers && employers.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "e.Id in (";
            for (var i = 0; i < employers.length; i++) {
                query += employers[i]['id'];
                if (i !== employers.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (domains && domains.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "dm.Id in (";
            for (var i = 0; i < domains.length; i++) {
                query += domains[i]['id'];
                if (i !== domains.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (certifications && certifications.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "cf.Id in (";
            for (var i = 0; i < certifications.length; i++) {
                query += certifications[i]['id'];
                if (i !== certifications.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (cities && cities.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.CityId in (";
            for (var i = 0; i < cities.length; i++) {
                query += cities[i]['id'];
                if (i !== cities.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (states && states.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.StateId in (";
            for (var i = 0; i < states.length; i++) {
                query += states[i]['id'];
                if (i !== states.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (countries && countries.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.CountryId in (";
            for (var i = 0; i < countries.length; i++) {
                query += countries[i]['id'];
                if (i !== countries.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (users && users.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.SourcedBy in (";
            for (var i = 0; i < users.length; i++) {
                query += users[i]['id'];
                if (i !== users.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (vendors && vendors.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.SourcedFrom in (";
            for (var i = 0; i < vendors.length; i++) {
                query += vendors[i]['id'];
                if (i !== vendors.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }

        if ((skillsAnd && skillsAnd.length !== 0) ||
            (skillsOr && skillsOr.length !== 0) ||
            (authorizations && authorizations.length !== 0) ||
            (qualificationsAnd && qualificationsAnd.length !== 0) ||
            (qualificationsOr && qualificationsOr.length !== 0) ||
            (employers && employers.length !== 0) ||
            (cities && cities.length !== 0) ||
            (states && states.length !== 0) ||
            (countries && countries.length !== 0) ||
            (users && users.length !== 0) ||
            (vendors && vendors.length !== 0) ||
            (domains && domains.length !== 0) ||
            (certifications && certifications.length !== 0)) {
            query += ")";
        }

        if (searchText && searchText.length !== 0) {
            if (!intialFlag) {
                query += " and (";
            } else {
                query += " where ";
            }
            for (var i = 0; i < searchText.length; i++) {
                if (searchText[i]) {
                    query += "(Lower(s.Name) like '%" + (searchText[i]).toLowerCase() + "%' or Lower(c.JobTitle) like '%" + (searchText[i]).toLowerCase() + "%' or Lower(c.FirstName) like '%" + (searchText[i]).toLowerCase() + "%'  or Lower(c.LastName) like '%" + (searchText[i]).toLowerCase() + "%')";
                }
                if (i !== searchText.length - 1) {
                    query += " or ";
                }
            }
            if (!intialFlag) {
                query += ") and c.IsPending = 0";
            }
        }

        console.log(query);


        return new Promise((resolve, reject) => {

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('candidate', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }
}

module.exports = CandidateService;