
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


/* static product service class */
class ParserService {
    // validate skill availability
    static validateSkillAvailability(resumeHtml, skillsList, flag) {
        var acquiredSkillList = [];
        var occurenceOffset;
        // looping for validate teh occurence of skills in resume
        for (var i = 0; i < skillsList.length; i++) {
            occurenceOffset = 0;
            var skill;
            if (flag)
                skill = skillsList[i]['name'];
            else
                skill = skillsList[i];
            // need to remove the / before proceed with reg exp skill
            for (var x = 0; x < skill.length; x++) {
                // if (skill[x].match('*\*'))
            }
            var skillPattern;
            try {
                skillPattern = new RegExp(skill, 'gi');
            } catch (ex) {
                return acquiredSkillList;
            }
            var occurence = (resumeHtml.match(skillPattern));
            if (occurence) {
                occurenceOffset = occurence.length;
            }
            if (occurenceOffset > 1) {
                acquiredSkillList.push(skillsList[i]);
            }
        }
        return acquiredSkillList;
    }

    // validate splitted value & sort out the suggested skills
    static validateSuggestedSkillList(splitValue, validatorList, suggestedSkillSplitList) {
        for (var j = 0; j < splitValue.length; j++) {
            if (splitValue[j] && splitValue[j] != '') {
                var internalSplitFlag = false;
                for (var k = 0; k < validatorList.length; k++) {
                    var splitArray = splitValue[j].split(validatorList[k]);
                    if (splitArray && splitArray.length > 1) {
                        internalSplitFlag = true;
                        for (var x = 0; x < splitArray.length; x++) {
                            if (validatorList[k] === ':') {
                                splitArray[0] = '';
                            }
                            if (splitArray[x] && splitArray[x] !== '') {
                                suggestedSkillSplitList.push(splitArray[x]);
                            }
                        }
                    }
                }
                if (!internalSplitFlag) {
                    suggestedSkillSplitList.push(splitValue[j]);
                }
            }
        }
        return suggestedSkillSplitList;
    }
}

module.exports = ParserService;