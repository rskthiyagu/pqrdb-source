
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


let customers = {};
let counter = 0;

/* static product service class */
class ProductService {
    static create(data) {
        var name;
        var desc;
        var imagePath;
        var createdBy;
        var createdDate;
        var updatedBy;
        var updatedDate;
        if (data) {
            // null validations
            if (data.Name) {
                name = data.Name;
            }
            if (data.Description) {
                desc = data.Description;
            }
            if (data.ImagePath) {
                imagePath = data.ImagePath;
            }
            if (data.CreatedBy) {
                createdBy = data.CreatedBy;
            } else {
                createdBy = '';
            }
            if (data.CreatedDate) {
                createdDate = data.CreatedDate;
            } else {
                createdDate = '';
            }
            if (data.UpdatedBy) {
                updatedBy = data.UpdatedBy;
            } else {
                updatedBy = '';
            }
            if (data.UpdatedDate) {
                updatedDate = data.UpdatedDate;
            } else {
                updatedDate = '';
            }
            var query = 'Insert into Categories values ("' + name + '", "' + desc + '", "' + imagePath + '", "' + createdBy + '", "' + createdDate + '", "' + updatedBy + '", "' + updatedDate + '")';
            return new Promise((resolve, reject) => {
                var request = new Request(query,
                    function (err, rowCount) {
                        if (err) {
                            // callback(err);
                            return reject(err);
                        } else {
                            console.log(rowCount + ' row(s) returned');
                            return resolve("sucess");
                        }
                    });
                // Execute SQL statement
                global.connection.execSql(request);
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(vendorId, categoryId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (vendorId && categoryId) {
                query = `select distinct p.Id as id, p.Name as name, p.ImagePath as imagePath, vp.VendorId, vp.Price as price, vp.Quantity as quantity from VendorProductDetails vp
                inner join Categories c on vp.CategoryId=c.Id
                left join Vendors v on vp.VendorId = v.Id
                inner join Products p on vp.ProductId = p.Id
                where vp.CategoryId = ` + categoryId + `and vp.VendorId = ` + vendorId;
            } else {
                query = `select distinct p.*, vp.*   from VendorProductDetails vp
                inner join Categories c on vp.CategoryId=c.Id
                left join Vendors v on vp.VendorId = v.Id
                inner join Products p on vp.ProductId = p.Id
                where vp.CategoryId = 5 and vp.VendorId = 1`;
            }
            var request = new Request(query,
                function (err, rowCount) {
                    if (err) {
                        // callback(err);
                        return reject(err);
                    } else {
                        console.log(rowCount + ' row(s) returned');
                    }
                });

            // Print the rows read
            var resultArray = [];
            var obj = {};
            request.on('row', function (columns) {
                columns.forEach(function (column) {
                    var colName = column.metadata.colName;
                    var colValue = column.value;
                    obj[colName] = colValue;
                });
                resultArray.push(obj);
                obj = {};
                return resolve(resultArray);
            });
            // Execute SQL statement
            global.connection.execSql(request);
        });
    }

    /* fetch best products */
    static retrieveBestProducts(vendorId, categoryId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (vendorId && categoryId) {
                query = `select distinct p.Id as id, p.Name as name, p.ImagePath as imagePath, vp.VendorId, vp.Price as price, vp.Quantity as quantity from VendorProductDetails vp
                inner join Categories c on vp.CategoryId=c.Id
                left join Vendors v on vp.VendorId = v.Id
                inner join Products p on vp.ProductId = p.Id
                where vp.CategoryId = ` + categoryId + `and vp.VendorId = ` + vendorId;
            } else {
                query = `select distinct p.Id as id, p.Name as name, p.ImagePath as imagePath, vp.VendorId, vp.Price as price, vp.Quantity as quantity from VendorProductDetails vp
                inner join Categories c on vp.CategoryId=c.Id
                left join Vendors v on vp.VendorId = v.Id
                inner join Products p on vp.ProductId = p.Id
                where vp.CategoryId = 5 and vp.VendorId = 0`;
            }
            var request = new Request(query,
                function (err, rowCount) {
                    if (err) {
                        // callback(err);
                        return reject(err);
                    } else {
                        console.log(rowCount + ' row(s) returned');
                    }
                });

            // Print the rows read
            var resultArray = [];
            var obj = {};
            request.on('row', function (columns) {
                columns.forEach(function (column) {
                    var colName = column.metadata.colName;
                    var colValue = column.value;
                    obj[colName] = colValue;
                });
                resultArray.push(obj);
                obj = {};
                return resolve(resultArray);
            });
            // Execute SQL statement
            global.connection.execSql(request);
        });
    }

    static update(uid, data) {
        var name;
        var desc;
        var imagePath;
        var createdBy;
        var createdDate;
        var updatedBy;
        var updatedDate;
        if (data && uid) {
            // null validations
            if (data.Name) {
                name = data.Name;
            }
            if (data.Description) {
                desc = data.Description;
            }
            if (data.ImagePath) {
                imagePath = data.ImagePath;
            }
            if (data.CreatedBy) {
                createdBy = data.CreatedBy;
            } else {
                createdBy = '';
            }
            if (data.CreatedDate) {
                createdDate = data.CreatedDate;
            } else {
                createdDate = '';
            }
            if (data.UpdatedBy) {
                updatedBy = data.UpdatedBy;
            } else {
                updatedBy = '';
            }
            if (data.UpdatedDate) {
                updatedDate = data.UpdatedDate;
            } else {
                updatedDate = '';
            }
            var query = 'Update Categories set Name="' + name + '", Description="' + desc + '", ImagePath="' + imagePath + '", CreatedBy="' + createdBy + '", CreatedDate="' + createdDate + '", UpdatedBy="' + updatedBy + '", UpdatedDate="' + updatedDate + '") where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var request = new Request(query,
                    function (err, rowCount) {
                        if (err) {
                            // callback(err);
                            return reject(err);
                        } else {
                            return resolve("sucess");
                        }
                    });
                // Execute SQL statement
                global.connection.execSql(request);
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from Categories where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var request = new Request(query,
                    function (err, rowCount) {
                        if (err) {
                            // callback(err);
                            return reject(err);
                        } else {
                            return resolve("sucess");
                        }
                    });
                // Execute SQL statement
                global.connection.execSql(request);
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = ProductService;