var Request = require('tedious').Request;

var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var errorService = require('../services/error.service');


/* static product service class */
class CandidateSkillMappingService {

    static create(data, requirementId) {
        if (data) {
            var skillList = data;
            var query = "Insert into requirementskillsmapping (SkillId, RequirementId) values ";
            for (var i = 0; i < skillList.length; i++) {
                query += "(" + skillList[i]['id'] + ", " + requirementId + ")";
                if (i !== skillList.length - 1) {
                    query += ",";
                }
            }
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('requirementSkills', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(requirementId, requirementList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (requirementId) {
                query = `select distinct s.Id as id, s.SkillId as skillsId, s.RequirementId as requirementId,
                        (select Name from skills sk where sk.Id = s.SkillId) as skillsName from requirementskillsmapping s where s.RequirementId = ` + requirementId;
            } else if (requirementList && requirementList.length !== 0) {
                query = `select distinct s.Id as id, s.SkillId as skillId, s.RequirementId as requirementId from requirementskillsmapping s`;
                if (requirementList && requirementList.length !== 0) {
                    query += " where (s.RequirementId in (";
                    for (var i = 0; i < requirementList.length; i++) {
                        query += requirementList[i]['id'];
                        if (i !== requirementList.length - 1) {
                            query += ", ";
                        }
                    }
                    query += "))";
                }
            } else {
                query = `select distinct s.Id as id, s.SkillId as skillId, s.RequirementId as requirementId from requirementskillsmapping s`;
            }
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('requirementSkills', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });;
        });
    }

    // dummy method
    static update(uid, data) {
        if (data && uid) {
            var query = 'Update Skills set Name="' + name + ' where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var request = new Request(query,
                    function (err, rowCount) {
                        if (err) {
                            // callback(err);
                            return reject(err);
                        } else {
                            return resolve("sucess");
                        }
                    });
                // Execute SQL statement
                global.connection.execSql(request);
            });
        } else {
            return "Id & data are required to do operation";
        }
    }
    // dummy method
    static delete(uid) {
        if (uid) {
            var query = 'Delete from requirementskillsmapping where RequirementId = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('requirementSkills', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = CandidateSkillMappingService;