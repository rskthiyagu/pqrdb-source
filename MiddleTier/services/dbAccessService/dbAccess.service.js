var MongoClient = require('mongoose');


var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');
var mysql = require('mysql');

class DbAccessService
{
    // connection establishment
    static createConnection() {
        // connection configuration
		var connection = mysql.createConnection({
            host: "localhost",
            user: "root",
            password: "sa@123",
            database: "pqrdb",
            timezone: 'utc'
          });

        return connection;
    }
}

module.exports = DbAccessService;