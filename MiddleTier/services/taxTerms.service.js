var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var errorService = require('../services/error.service');


/* static product service class */
class TaxTermsService {
    static create(data) {
        if (data) {
            var query = "Insert into taxterms (Name) values ('" + data['name'] + "')";
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('taxTerm', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{"status": "success", "id": result['insertId']}]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(taxTermId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (taxTermId) {
                query = `select distinct s.Id as id, s.Name as name from taxterms s where s.Id = ` + taxTermId;
            } else {
                query = `select distinct s.Id as id, s.Name as name from taxterms s`;
            }

            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('taxTerm', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }


    static update(uid, data) {
        if (data && uid) {
            var name = data['name'];
            var query = 'Update taxterms set Name="' + name + '" where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('taxTerm', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from taxterms where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('taxTerm', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = TaxTermsService;