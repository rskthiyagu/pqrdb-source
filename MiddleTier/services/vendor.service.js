var Request = require('tedious').Request;

var dbAccessService = require('../services/dbAccessService/dbAccess.service');

/* static category service class */
class VendorService {
    static create(data) {
        var name;
        if (data) {
            // null validations
            if (data.name) {
                name = data.name;
            }
            var query = "Insert into vendors (Name) values ('" + name + "')";
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{"status": "success", "id": result['insertId']}]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(uid) {
        // Attempt to connect and execute queries if connection goes through
        var query;
        if (uid) {
            query = 'SELECT * FROM vendors where Id = ' + uid;
        } else {
            query = `select v.Id as id, v.Name as name from vendors v`;
        }

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
        });
    }

    /* fetch best vendors  -- dummy method */
    static retrieveBestVendors() {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query = `select top 3 v.Id as id, v.Name as name ,v.ImagePath as imagePath, v.Description as description from vendors v`;
            var request = new Request(query,
                function (err, rowCount) {
                    if (err) {
                        return reject(err);
                    }
                });

            // Print the rows read
            var resultArray = [];
            var obj = {};
            request.on('row', function (columns) {
                columns.forEach(function (column) {
                    var colName = column.metadata.colName;
                    var colValue = column.value;
                    obj[colName] = colValue;
                });
                resultArray.push(obj);
                obj = {};
                return resolve(resultArray);
            });
            // Execute SQL statement
            global.connection.execSql(request);
        });
    }

    static update(uid, data) {
        var name;
        if (data && uid) {
            // null validations
            if (data.Name) {
                name = data.Name;
            }
            var query = 'Update vendors set Name="' + name + '" where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from vendors where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = VendorService;