var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var errorService = require('../services/error.service');
var historyService = require('../services/history.service');


/* static product service class */
class InterviewMappingService {

    static create(data) {
        if (data) {
            // validation
            if (!data['candidateId']) data['candidateId'] = null;
            if (!data['requirementId']) data['requirementId'] = null;
            if (!data['clientId']) data['clientId'] = null;
            // if (!data['interviewRound']) data['interviewRound'] = null;
            // if (!data['interviewStatus']) data['interviewStatus'] = null;
            if (!data['comments']) data['comments'] = "";
            if (!data['createdOn']) data['createdOn'] = "";
            if (!data['createdBy']) data['createdBy'] = null;
            if (!data['updatedBy']) data['updatedBy'] = null;
            if (!data['updatedOn']) data['updatedOn'] = "";
            var query = "Insert into interviewmapping (CandidateId, RequirementId, ClientId, InterviewRound, InterviewStatus, Comments, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy) values ";
            for (var i = 0; i < data['requirementList'].length; i++) {
                query += "(" + data['candidateId'] + ", " + data['requirementList'][i]['id'] + ", " + data['requirementList'][i]['clientId'] + ", " + data['interviewRound'] + ", " + data['interviewStatus'] + ", '" + data['comments'] + "', '" + data['createdOn'] + "', " + data['createdBy'] + ", '" + data['updatedOn'] + "', " + data['updatedBy'] + ")";
                if (i !== data['requirementList'].length - 1) {
                    query += ", ";
                }
            }
            // history
            // historyService.create(data);
            console.log(query);
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('interview', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieveBulk(id, candidateId, requirementId, clientId, interviewStatus, interviewRound) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId) {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                        (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                        (select Alias from users u where u.Id = i.CreatedBy) as userName,
                        (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i where i.CandidateId = ` + candidateId;
            } else if (requirementId) {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                        (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                        (select Alias from users u where u.Id = i.CreatedBy) as userName,
                        (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i where i.RequirementId = ` + requirementId;
            } else if (clientId) {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                        (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                        (select Alias from users u where u.Id = i.CreatedBy) as userName,
                        (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i where i.ClientId = ` + clientId;
            } else if (interviewStatus) {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                        (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                        (select Alias from users u where u.Id = i.CreatedBy) as userName,
                        (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i where i.InterviewStatus = ` + interviewStatus;
            } else if (interviewRound) {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                        (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                        (select Alias from users u where u.Id = i.CreatedBy) as userName,
                        (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i where i.InterviewRound = ` + interviewRound;
            } else if (id) {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                        (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                        (select Alias from users u where u.Id = i.CreatedBy) as userName,
                        (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i where i.Id = ` + id;
            } else {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                        (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                        (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                        (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                        (select Alias from users u where u.Id = i.CreatedBy) as userName,
                        (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                        (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                        (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i`;
            }

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('interview', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // fetch the interview combination
    static retrieve(candidateId, requirementId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId && requirementId) {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                (select isd.Name from interviewstatusdata isd where isd.Id = i.InterviewStatus) as interviewStatusName,
                (select r.Title from requirementdetails r where r.Id = i.RequirementId) as requirementName,
                (select Alias from users u where u.Id = i.CreatedBy) as userName,
                (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i where CandidateId =` + candidateId + ' and RequirementId = ' + requirementId;
            } else if (requirementId) {
                query = `select distinct Id as id, CandidateId as candidateId, RequirementId as requirementId, ClientId as clientId, InterviewRound as interviewRound, InterviewStatus as interviewStatus, Comments as comments, CreatedOn as createdOn, CreatedBy as createdBy, UpdatedOn as updatedOn, UpdatedBy as updatedBy,
                (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                (select cl.Name from clientdetails cl where cl.Id = i.ClientId) as clientName,
                (select ir.Name from interviewrounds ir where ir.Id = i.InterviewRound) as interviewRoundName,
                (select isd.Name from interviewstatusdata isd where isd.Id = i.InterviewStatus) as interviewStatusName,
                (select r.Title from requirementdetails r where r.Id = i.RequirementId) as requirementName,
                (select Alias from users u where u.Id = i.CreatedBy) as userName,
                (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage from interviewmapping i where RequirementId = ` + requirementId;
            } else {
                query = `select i.CandidateId as candidateId,
                i.RequirementId as requirementId, i.ClientId,
                (select c.FirstName from candidatedetails c where c.Id = i.CandidateId) as candidateName,
                (select r.Title from requirementdetails r where r.Id = i.RequirementId) as requirementName,
                (select cd.Name from clientdetails cd where cd.Id = i.ClientId) as clientName,
                (select Alias from users u where u.Id = i.CreatedBy) as userName,
                (select FileName from users u where u.Id = i.CreatedBy) as profileImage,
                (select Alias from users u where u.Id = i.UpdatedBy) as updateUserName,
                (select FileName from users u where u.Id = i.UpdatedBy) as updateProfileImage,
                (select count(Id) from interviewmapping i where i.InterviewRound = 6) as count
                 from interviewmapping i group by i.CandidateId, i.RequirementId, i.ClientId order by i.UpdatedOn desc`;
            }

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('interview', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // update the interview value
    static update(uid, data) {
        if (data && uid) {
            if (!data['requirementId']) data['requirementId'] = null;
            if (!data['clientId']) data['clientId'] = null;
            if (!data['interviewRound']) data['interviewRound'] = null;
            if (!data['interviewStatus']) data['interviewStatus'] = null;
            if (!data['comments']) data['comments'] = "";
            if (!data['updatedBy']) data['updatedBy'] = null;
            if (!data['updatedOn']) data['updatedOn'] = "";
            var query = 'Update interviewmapping set RequirementId=' + data['requirementId'] + ', ClientId=' + data['clientId'] + ', InterviewRound=' + data['interviewRound'] + ', InterviewStatus=' + data['interviewStatus'] + ', Comments="' + data['comments'] + ', UpdatedOn="' + data['updatedOn'] + ', UpdatedBy=' + data['updatedBy'] + ' where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('interview', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }
    // dummy method
    static delete(uid) {
        if (uid) {
            var query = 'Delete from interviewmapping where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('interview', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = InterviewMappingService;