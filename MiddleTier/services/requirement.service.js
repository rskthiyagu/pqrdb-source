var moment = require('moment');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var errorService = require('../services/error.service');


/* static product service class */
class RequirementService {
    static create(data) {
        if (data) {
            // null check
            if (!data['title']) data['title'] = '';
            if (!data['zipCode']) data['zipCode'] = '';
            if (!data['clientBillRate']) data['clientBillRate'] = '';
            if (!data['candidatePayRate']) data['candidatePayRate'] = '';
            if (!data['experience']) data['experience'] = null;
            if (!data['sourcedBy']) data['sourcedBy'] = null;
            if (!data['positionNumber']) data['positionNumber'] = null;
            if (!data['jobDescription']) data['jobDescription'] = '';
            if (!data['createdOn']) data['createdOn'] = '';
            if (!data['updatedOn']) data['updatedOn'] = '';
            if (!data['fileName']) data['fileName'] = '';
            if (!data['endClients']) data['endClients'] = '';

            if (data['cities'] && data['cities'].length !== 0) {
                data['cityId'] = data['cities'][0]['id'];
            } else {
                data['cityId'] = null;
            }
            if (data['states'] && data['states'].length !== 0) {
                data['stateId'] = data['states'][0]['id'];
            } else {
                data['stateId'] = null;
            }
            if (data['countries'] && data['countries'].length !== 0) {
                data['countryId'] = data['countries'][0]['id'];
            } else {
                data['countryId'] = null;
            }
            if (data['clients'] && data['clients'].length !== 0) {
                data['clients'] = data['clients'][0]['id'];
            } else {
                data['clients'] = null;
            }
            if (data['duration'] && data['duration'].length !== 0) {
                data['duration'] = data['duration'][0]['id'];
            } else {
                data['duration'] = null;
            }
            if (data['jobStatus'] && data['jobStatus'].length !== 0) {
                data['jobStatus'] = data['jobStatus'][0]['id'];
            } else {
                data['jobStatus'] = null;
            }
            if (data['accountManager'] && data['accountManager'].length !== 0) {
                data['accountManager'] = data['accountManager'][0]['id'];
            } else {
                data['accountManager'] = null;
            }

            var query = "Insert into requirementdetails (Title, ZipCode, ClientBillRate, CandidatePayRate, Experience, PositionNumber, JobDescription, CityId, StateId, CountryId, Clients, Duration, JobStatus, AccountManager, FileName, EndClients, CreatedOn, UpdatedOn, CreatedBy, updatedBy) values ('"
                         + data['title'] + "', '" + data['zipCode'] + "', '" + data['clientBillRate'] + "', '" + data['candidatePayRate'] + "', " + data['experience'] + ", " + data['positionNumber'] + ", '" + data['jobDescription'] + "', " + data['cityId'] + ", " + data['stateId'] + ", " + data['countryId'] + ", " + data['clients'] + ", " + data['duration'] + ", " + data['jobStatus'] + ", " + data['accountManager'] + ", '" + data['fileName'] + "', '" + data['endClients'] + "', '" + data['createdOn'] + "', '" + data['updatedOn'] + "', " + data['sourcedBy'] + ", " + data['sourcedBy'] + ")";
                        var connection = dbAccessService.createConnection();
            return new Promise((resolve, reject) => {
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('requirement', err);
                                return reject(err);
                            }
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(requirementId, requirementList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (requirementId) {
                query = `select distinct c.Id as id, c.Id as count, c.Title as title, c.ZipCode as zipCode, c.ClientBillRate as clientBillRate, c.CandidatePayRate as candidatePayRate, c.Experience as experience, c.PositionNumber as positionNumber, c.JobDescription as jobDescription, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn,
                c.CityId as citiesId, c.StateId as statesId, c.CountryId as countriesId, c.Clients as clientsId, c.Duration as durationId, c.JobStatus as jobStatusId, c.AccountManager as accountManager, c.EndClients as endClients, c.FileName as jobDescriptionFile,
                (select ct.Name as name from cities ct where ct.Id = c.CityId) as citiesName,
                (select st.Name as name from states st where st.Id = c.StateId) as statesName,
                (select cs.Name as name from countries cs where cs.Id = c.CountryId) as countriesName,
                (select cl.Name from clientdetails cl where cl.Id = c.Clients) as clientsName,
                (select d.Name as name from durations d where d.Id = c.Duration) as durationName,
                (select js.Name as name from requirementstatus js where js.Id = c.JobStatus) as jobStatusName,
                (select Alias from users u where u.Id = c.AccountManager) as accountManagerName,
                (select Alias from users u where u.Id = c.CreatedBy) as userName,
                (select FileName from users u where u.Id = c.CreatedBy) as profileImage from requirementdetails c where c.Id = ` + requirementId + ' order by c.UpdatedOn desc';
            } else if (requirementList && requirementList.length !== 0) {   // for filter & search purpose
                query = `select distinct c.Id as id, c.Title as title, c.PositionNumber as positionNumber, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn,
                (select Alias from users u where u.Id = c.CreatedBy) as userName,
                (select FileName from users u where u.Id = c.CreatedBy) as profileImage from requirementdetails c`;
                query += " where (c.Id in (";
                for (var i = 0; i < requirementList.length; i++) {
                    query += requirementList[i]['id'];
                    if (i !== requirementList.length - 1) {
                        query += ", ";
                    }
                }
                query += ")) order by c.UpdatedOn desc";
            } else { // need to change the approach as candidate
                query = `select distinct c.Id as id, c.Title as title, c.PositionNumber as positionNumber, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn, c.EndClients as endClients,
                (select ct.Name as name from cities ct where ct.Id = c.CityId) as cityName,
                (select st.Name as name from states st where st.Id = c.StateId) as stateName,
                (select cs.Name as name from countries cs where cs.Id = c.CountryId) as countryName,
                (select cl.Name from clientdetails cl where cl.Id = c.Clients) as clientName,
                (select d.Name as name from durations d where d.Id = c.Duration) as durationName,
                (select js.Name as name from requirementstatus js where js.Id = c.JobStatus) as jobStatusName,
                (select Alias from users u where u.Id = c.AccountManager && u.IsAccountManager = 1) as accountManagerName,
                (select Alias from users u where u.Id = c.CreatedBy) as userName,
                (select FileName from users u where u.Id = c.CreatedBy) as profileImage from requirementdetails c order by c.UpdatedOn desc`; //  where c.JobStatus = 0
            }

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('requirement', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // retrieve requirement submission
    static retrieveRequirementSubmission(requirementId) {
        return new Promise((resolve, reject) => {
            var query;
            if (requirementId) {
                query = `select distinct r.Id as id, r.CandidateId as candidateId, r.ClientId as clientId, r.SubmittedTo as submittedTo, r.Comments as comments, r.CreatedOn as createdOn, r.CreatedBy as createdBy, r.UpdatedBy as updatedBy, r.UpdatedOn as updatedOn,
                    (select c.FirstName from candidatedetails c where c.Id = r.CandidateId) as candidateFirstName,
                    (select c.LastName from candidatedetails c where c.Id = r.CandidateId) as candidateLastName,
                    (select c.Name from clientdetails c where c.Id = r.clientId) as clientName,
                    (select c.Name from submittalStatus c where c.Id = r.SubmittedTo) as submittedToName,
                    (select c.Name from users u where u.Id = r.UpdatedBy) as userName
                     from submitalmapping r order by r.UpdatedOn desc where r.IsInterviewScheduled = 0 and r.IsCancelled = 0 and r.RequirementId=` + requirementId;
            } else {
                query = `select distinct r.Id as id, r.title as name, r.Clients as clientId,
                    (select c.Name from clientdetails c where c.Id = r.Clients) as clientName from submitalmapping r order by r.CreatedOn desc`;
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('requirementSubmission', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // retrieve clients for dropdown
    static retrieveRequirementData(clientId) {
        return new Promise((resolve, reject) => {
            var query;
            if (clientId) {
                query = `select distinct r.Id as id, r.title as name, r.Clients as clientId,
                    (select c.Name from clientdetails c where c.Id = r.Clients) as clientName from requirementdetails r order by r.CreatedOn desc where r.clientId=` + clientId;
            } else {
                query = `select distinct r.Id as id, r.title as name, r.Clients as clientId,
                    (select c.Name from clientdetails c where c.Id = r.Clients) as clientName from requirementdetails r order by r.CreatedOn desc`;
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) {
                            errorService.create('requirement', err);
                            return reject(err);
                        }
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // fetch availability info of candidate
    static retrieveCandidateAvailability(candidateId, candidateList) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (candidateId) {
                query = `select distinct c.Id as id, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                        c.IsInterviewSubmitted as isInterviewSubmitted, c.InterviewStatus as interviewStatus, c.InterviewRequirement as interviewRequirement, c.InterviewClient as interviewClient, c.InterviewComments as interviewComments,
                        (select Alias from users u where u.Id = c.UpdatedBy) as userName,
                        (select FileName from users u where u.Id = c.UpdatedBy) as profileImage from candidatedetails c where c.Id = ` + candidateId;
            } else if (candidateList && candidateList.length !== 0) {
                query = `select distinct c.Id as id, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                        c.IsInterviewSubmitted as isInterviewSubmitted, c.InterviewStatus as interviewStatus, c.InterviewRequirement as interviewRequirement, c.InterviewClient as interviewClient, c.InterviewComments as interviewComments,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName,
                        (select FileName from users u where u.Id = c.SourcedBy) as profileImage from candidatedetails c`;
                query += " where (c.Id in (";
                for (var i = 0; i < candidateList.length; i++) {
                    query += candidateList[i]['id'];
                    if (i !== candidateList.length - 1) {
                        query += ", ";
                    }
                }
                query += ")) and c.IsPending = 0 order by c.CreatedOn desc";
            } else {
                query = `select distinct c.Id as id, c.IsAvailable as availability, c.AvailableStatus as availabilityStatus, c.AvailableComments as availabilityComments, c.AvailableRequirement as availabilityRequirement, c.AvailableClient as availabilityClient, c.IsCommunicated as isCommunication,
                        c.IsInterviewSubmitted as isInterviewSubmitted, c.InterviewStatus as interviewStatus, c.InterviewRequirement as interviewRequirement, c.InterviewClient as interviewClient, c.InterviewComments as interviewComments,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName,
                        (select FileName from users u where u.Id = c.SourcedBy) as profileImage from candidatedetails c where c.IsPending = 0 order by c.CreatedOn desc`;
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static updateDeleteRecord(data) {
        if (data) {
            var tableName = '';
            var key = '';
            var value = '';
            var id;

            // get values
            if (data['key'] === 'cities') {
                tableName = 'candidatedetails';
                key = 'CityId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'states') {
                tableName = 'candidatedetails';
                key = 'StateId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'countries') {
                tableName = 'candidatedetails';
                key = 'CountryId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'vendors') {
                tableName = 'candidatedetails';
                key = 'SourcedFrom';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'skills') {
                tableName = 'skillsmapping';
                key = 'SkillId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'authorizations') {
                tableName = 'authorizationsmapping';
                key = 'AuthorizationId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'employers') {
                tableName = 'employersmapping';
                key = 'EmployerId';
                value = data['value'];
                id = data['id'];
            } else if (data['key'] === 'qualifications') {
                tableName = 'qualificationsmapping';
                key = 'QualificationId';
                value = data['value'];
                id = data['id'];
            }

            // created date / updated date updation
            data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update " + tableName + " set " + key + "=" + value + " where " + key + "=" + id;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // get statistics data of candidate count
    static getCandidateTotalStatistics(id) {
        return new Promise((resolve, reject) => {
            var query = `select count(Id) as candidateTotal, 
            (select count(Id) from candidatedetails where IsPending = 0 and SourcedBy = ` + id + `) as userCandidateTotal from candidatedetails where IsPending = 0;`
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // get statistics data of pending candidate total
    static getPendingCandidateTotalStatistics(id) {
        return new Promise((resolve, reject) => {
            var query = `select count(Id) as candidatePendingTotal,
            (select count(Id) from candidatedetails where IsPending = 1 and SourcedBy = ` + id +
                `) as userCandidatePendingTotal from candidatedetails where IsPending = 1;`

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // get statistics data of candidate today
    static getCandidateTotalTodayStatistics(id) {
        return new Promise((resolve, reject) => {
            var currentDate = moment.utc().format('YYYY-MM-DD');
            var query = `select count(Id) as candidateTodayTotal,
            (select count(Id) from candidatedetails where SourcedBy = ` + id + ` and CreatedOn > '` + currentDate + ` 00:00:00.000') as userCandidateTodayTotal from candidatedetails where CreatedOn > '` + currentDate + ` 00:00:00.000';`

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static retrieveNewlyCreated(flag) {
        return new Promise((resolve, reject) => {
            var query;
            // if (flag) {
            //     query = 'select top 12 ';
            // } else {
            query = 'select ';
            // }
            query += `c.Id as id, c.FirstName as firstName, c.LastName as lastName, c.Email as email, c.MobileNumber as mobileNumber, c.SourcedBy as sourcedBy, c.SourcedFrom as sourcedFrom, c.CreatedOn as createdOn, c.UpdatedOn as updatedOn, c.IsAvailable as isAvailable, c.FileName as fileName, c.JobTitle as jobTitle, c.Experience as experience, c.Linkedin as linkedin, c.CityId as cityId, c.StateId as stateId, c.CountryId as countryId, (select count(Id) from candidatedetails cd where cd.IsPending = 0) as total,
                        (select Alias from users u where u.Id = c.SourcedBy) as userName from candidatedetails c where IsPending = 0 order by c.CreatedOn desc`;

            if (flag) {
                query += ' limit 12';
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }


    static update(uid, data) {
        if (data) {
            // null check
            // null check
            if (!data['title']) data['title'] = '';
            if (!data['zipCode']) data['zipCode'] = '';
            if (!data['clientBillRate']) data['clientBillRate'] = '';
            if (!data['candidatePayRate']) data['candidatePayRate'] = '';
            if (!data['experience']) data['experience'] = null;
            if (!data['sourcedBy']) data['sourcedBy'] = null;
            if (!data['positionNumber']) data['positionNumber'] = null;
            if (!data['jobDescription']) data['jobDescription'] = '';
            if (!data['createdOn']) data['createdOn'] = '';
            if (!data['updatedOn']) data['updatedOn'] = '';
            if (!data['fileName']) data['fileName'] = '';
            if (!data['endClients']) data['endClients'] = '';

            if (data['cities'] && data['cities'].length !== 0) {
                data['cityId'] = data['cities'][0]['id'];
            } else {
                data['cityId'] = null;
            }
            if (data['states'] && data['states'].length !== 0) {
                data['stateId'] = data['states'][0]['id'];
            } else {
                data['stateId'] = null;
            }
            if (data['countries'] && data['countries'].length !== 0) {
                data['countryId'] = data['countries'][0]['id'];
            } else {
                data['countryId'] = null;
            }
            if (data['clients'] && data['clients'].length !== 0) {
                data['clients'] = data['clients'][0]['id'];
            } else {
                data['clients'] = null;
            }
            if (data['duration'] && data['duration'].length !== 0) {
                data['duration'] = data['duration'][0]['id'];
            } else {
                data['duration'] = null;
            }
            if (data['jobStatus'] && data['jobStatus'].length !== 0) {
                data['jobStatus'] = data['jobStatus'][0]['id'];
            } else {
                data['jobStatus'] = null;
            }
            if (data['accountManager'] && data['accountManager'].length !== 0) {
                data['accountManager'] = data['accountManager'][0]['id'];
            } else {
                data['accountManager'] = null;
            }

            // created date / updated date updation
            // data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update requirementdetails set Title='" + data['title'] + "', ZipCode='" + data['zipCode'] + "', ClientBillRate='" + data['clientBillRate'] + "', CandidatePayRate='" + data['candidatePayRate'] + "', Experience=" + data['experience'] + ", PositionNumber=" + data['positionNumber'] + ", JobDescription='" + data['jobDescription'] + "', CityId=" + data['cityId'] + ", StateId=" + data['stateId'] + ", CountryId=" + data['countryId'] + ", Clients=" + data['clients'] + ", Duration=" + data['duration'] + ", JobStatus=" + data['jobStatus'] + ", AccountManager=" + data['accountManager'] + ", FileName='" + data['fileName'] + "', EndClients='" + data['endClients'] + "', UpdatedOn='" + data['updatedOn'] + "', UpdatedBy=" + data['sourcedBy'] + " where Id=" + uid;
            console.log(query);
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    // update availability details of candidates
    static updateCandidateAvailability(uid, data) {
        if (data) {
            var communicationDetails = data['communicationDetails'];
            // null validation
            if (data['communicationDetails']['isCommunication'] === true) {
                data['communicationDetails']['isCommunication'] = 1;
            } else if (data['communicationDetails']['isCommunication'] === false) {
                data['communicationDetails']['isCommunication'] = 0;
            } else if (data['communicationDetails']['isCommunication'] === 0) {
                data['communicationDetails']['isCommunication'] = 0;
            } else if (data['communicationDetails']['isCommunication'] === 1) {
                data['communicationDetails']['isCommunication'] = 1;
            } else {
                data['communicationDetails']['isCommunication'] = null;
            }
            var interviewDetails = data['interviewDetails'];
            // null check
            if (!data['communicationDetails']['availabilityClient']) data['communicationDetails']['availabilityClient'] = '';
            if (!data['communicationDetails']['availabilityRequirement']) data['communicationDetails']['availabilityRequirement'] = '';
            if (!data['communicationDetails']['availabilityComments']) data['communicationDetails']['availabilityComments'] = '';

            // availability validation
            if (data['communicationDetails']['availability'] === 1) {
                data['communicationDetails']['isAvailable'] = 1;
            } else if (data['communicationDetails']['availability'] === 0) {
                data['communicationDetails']['isAvailable'] = 0;
            } else if (data['communicationDetails']['availability'] && data['communicationDetails']['availability'].length !== 0) {
                data['communicationDetails']['isAvailable'] = data['communicationDetails']['availability'][0]['id'];
            } else {
                data['communicationDetails']['isAvailable'] = null;
            }

            if (data['communicationDetails']['availabilityStatus'] && data['communicationDetails']['availabilityStatus'].length !== 0) {
                data['communicationDetails']['availabilityStatus'] = data['communicationDetails']['availabilityStatus'][0]['id'];
            } else {
                data['communicationDetails']['availabilityStatus'] = null;
            }

            // interview details
            if (!data['interviewDetails']['interviewClient']) data['interviewDetails']['interviewClient'] = '';
            if (!data['interviewDetails']['interviewRequirement']) data['interviewDetails']['interviewRequirement'] = '';
            if (!data['interviewDetails']['interviewComments']) data['interviewDetails']['interviewComments'] = '';

            if (data['interviewDetails']['isInterviewSubmitted'] === true) {
                data['interviewDetails']['isInterviewSubmitted'] = 1;
            } else if (data['interviewDetails']['isInterviewSubmitted'] === false) {
                data['interviewDetails']['isInterviewSubmitted'] = 0;
            } else if (data['interviewDetails']['isInterviewSubmitted'] === 0) {
                data['interviewDetails']['isInterviewSubmitted'] = 0;
            } else if (data['interviewDetails']['isInterviewSubmitted'] === 1) {
                data['interviewDetails']['isInterviewSubmitted'] = 1;
            } else {
                data['interviewDetails']['isInterviewSubmitted'] = null;
            }
            if (data['interviewDetails']['interviewStatus'] && data['interviewDetails']['interviewStatus'].length !== 0) {
                data['interviewDetails']['interviewStatus'] = data['interviewDetails']['interviewStatus'][0]['id'];
            } else {
                data['interviewDetails']['interviewStatus'] = null;
            }

            // created date / updated date updation
            // data['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
            var query = "Update candidatedetails set IsCommunicated=" +
                data['communicationDetails']['isCommunication'] +
                ", IsAvailable=" + data['communicationDetails']['isAvailable'] +
                ", AvailableStatus=" + data['communicationDetails']['availabilityStatus'] +
                ", AvailableComments='" + data['communicationDetails']['availabilityComments'] +
                "', AvailableClient='" + data['communicationDetails']['availabilityClient'] +
                "', AvailableRequirement='" + data['communicationDetails']['availabilityRequirement'] +
                "', IsInterviewSubmitted=" + data['interviewDetails']['isInterviewSubmitted'] +
                ", InterviewStatus=" + data['interviewDetails']['interviewStatus'] +
                ", InterviewClient='" + data['interviewDetails']['interviewClient'] +
                "', InterviewRequirement='" + data['interviewDetails']['interviewRequirement'] +
                "', InterviewComments='" + data['interviewDetails']['interviewComments'] +
                "', UpdatedBy=" + data['sourcedBy'] +
                ", UpdatedOn='" + data['updatedOn'] +
                "' where Id=" + uid;

            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            console.log("%%%%     ", err);
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from Skills where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function (err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }

    static getCount(id) {
        var query = "select max(Id) as count from requirementdetails";
        if (id) {
            query = 'select count(Id) as count from requirementdetails where CreatedBy = ' + id;
        }
        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    // validate user details availability status
    static validateCandidateDetailAvailability(param) {
        // Attempt to connect and execute queries if connection goes through

        var mobileNumber = param.mobileNumber;
        var email = param.email;
        var query = `select cd.Id as id,
                            cd.FirstName as firstName,
                            cd.LastName as lastName,
                            cd.MobileNumber as mobileNumber
                            from candidatedetails cd where cd.MobileNumber=` + mobileNumber + ` and cd.Email='` + email;

        return new Promise((resolve, reject) => {
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }

    static filter(skillsAnd, skillsOr, authorizations, qualificationsAnd, qualificationsOr, employers, cities, states, countries, users, vendors, domains, certifications, searchText) {
        var query = "select distinct c.Id as id from candidatedetails c";

        // join filter query formation
        if ((skillsAnd && skillsAnd.length !== 0) || (skillsOr && skillsOr.length !== 0) || (searchText && searchText.length !== 0)) {
            query += " left join skillsmapping sm on c.Id = sm.CandidateId left join skills s on s.Id = sm.SkillId";
        }
        if (authorizations && authorizations.length !== 0) {
            query += " left join authorizationsmapping am on c.Id = am.CandidateId left join authorizations a on a.Id = am.AuthorizationId";
        }
        if ((qualificationsAnd && qualificationsAnd.length !== 0) || (qualificationsOr && qualificationsOr.length !== 0)) {
            query += " left join qualificationsmapping qm on c.Id = qm.CandidateId left join qualifications q on q.Id = qm.QualificationId";
        }
        if (employers && employers.length !== 0) {
            query += " left join employersmapping em on c.Id = em.CandidateId left join employers e on e.Id = em.EmployerId";
        }
        if (domains && domains.length !== 0) {
            query += " left join domainsmapping dm on c.Id = dm.CandidateId left join domains d on d.Id = dm.DomainId";
        }
        if (certifications && certifications.length !== 0) {
            query += " left join certificationsmapping cm on c.Id = cm.CandidateId left join certifications cf on cf.Id = cm.CertificationId";
        }

        if ((skillsAnd && skillsAnd.length !== 0) ||
            (skillsOr && skillsOr.length !== 0) ||
            (authorizations && authorizations.length !== 0) ||
            (qualificationsAnd && qualificationsAnd.length !== 0) ||
            (qualificationsOr && qualificationsOr.length !== 0) ||
            (employers && employers.length !== 0) ||
            (cities && cities.length !== 0) ||
            (states && states.length !== 0) ||
            (countries && countries.length !== 0) ||
            (users && users.length !== 0) ||
            (vendors && vendors.length !== 0) ||
            (domains && domains.length !== 0) ||
            (certifications && certifications.length !== 0)) {
            query += " where (";
        }

        var intialFlag = true;
        // conditions applied
        if (skillsAnd && skillsAnd.length !== 0) {
            for (var i = 0; i < skillsAnd.length; i++) {
                query += "s.Id=" + skillsAnd[i]['id'];
                if (i !== skillsAnd.length - 1) {
                    query += " and ";
                }
            }
            intialFlag = false;
        }
        if (skillsOr && skillsOr.length !== 0) {
            if (!intialFlag) {
                query += " and ( 1 = 1 or ";
            }
            query += "s.Id in (";
            for (var i = 0; i < skillsOr.length; i++) {
                query += skillsOr[i]['id'];
                if (i !== skillsOr.length - 1) {
                    query += ", ";
                }
            }
            query += ")";
            if (!intialFlag) {
                query += ")";
            }
            intialFlag = false;
        }
        if (authorizations && authorizations.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "a.Id in (";
            for (var i = 0; i < authorizations.length; i++) {
                query += authorizations[i]['id'];
                if (i !== authorizations.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (qualificationsAnd && qualificationsAnd.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            for (var i = 0; i < qualificationsAnd.length; i++) {
                query += "q.Id=" + qualificationsAnd[i]['id'];
                if (i !== qualificationsAnd.length - 1) {
                    query += "and ";
                }
            }
            intialFlag = false;
        }
        if (qualificationsOr && qualificationsOr.length !== 0) {
            if (!intialFlag) {
                query += " and ( 1 = 1 or ";
            }
            query += "q.Id in (";
            for (var i = 0; i < qualificationsOr.length; i++) {
                query += qualificationsOr[i]['id'];
                if (i !== qualificationsOr.length - 1)
                    query += ", ";
            }
            query += ")";
            if (!intialFlag) {
                query += ")";
            }
            intialFlag = false;
        }
        if (employers && employers.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "e.Id in (";
            for (var i = 0; i < employers.length; i++) {
                query += employers[i]['id'];
                if (i !== employers.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (domains && domains.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "dm.Id in (";
            for (var i = 0; i < domains.length; i++) {
                query += domains[i]['id'];
                if (i !== domains.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (certifications && certifications.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "cf.Id in (";
            for (var i = 0; i < certifications.length; i++) {
                query += certifications[i]['id'];
                if (i !== certifications.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (cities && cities.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.CityId in (";
            for (var i = 0; i < cities.length; i++) {
                query += cities[i]['id'];
                if (i !== cities.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (states && states.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.StateId in (";
            for (var i = 0; i < states.length; i++) {
                query += states[i]['id'];
                if (i !== states.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (countries && countries.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.CountryId in (";
            for (var i = 0; i < countries.length; i++) {
                query += countries[i]['id'];
                if (i !== countries.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (users && users.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.SourcedBy in (";
            for (var i = 0; i < users.length; i++) {
                query += users[i]['id'];
                if (i !== users.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }
        if (vendors && vendors.length !== 0) {
            if (!intialFlag) {
                query += " and ";
            }
            query += "c.SourcedFrom in (";
            for (var i = 0; i < vendors.length; i++) {
                query += vendors[i]['id'];
                if (i !== vendors.length - 1)
                    query += ", ";
            }
            query += ")";
            intialFlag = false;
        }

        if ((skillsAnd && skillsAnd.length !== 0) ||
            (skillsOr && skillsOr.length !== 0) ||
            (authorizations && authorizations.length !== 0) ||
            (qualificationsAnd && qualificationsAnd.length !== 0) ||
            (qualificationsOr && qualificationsOr.length !== 0) ||
            (employers && employers.length !== 0) ||
            (cities && cities.length !== 0) ||
            (states && states.length !== 0) ||
            (countries && countries.length !== 0) ||
            (users && users.length !== 0) ||
            (vendors && vendors.length !== 0) ||
            (domains && domains.length !== 0) ||
            (certifications && certifications.length !== 0)) {
            query += ")";
        }

        if (searchText && searchText.length !== 0) {
            if (!intialFlag) {
                query += " and (";
            } else {
                query += " where ";
            }
            for (var i = 0; i < searchText.length; i++) {
                if (searchText[i]) {
                    query += "(Lower(s.Name) like '%" + (searchText[i]).toLowerCase() + "%' or Lower(c.JobTitle) like '%" + (searchText[i]).toLowerCase() + "%' or Lower(c.FirstName) like '%" + (searchText[i]).toLowerCase() + "%'  or Lower(c.LastName) like '%" + (searchText[i]).toLowerCase() + "%')";
                }
                if (i !== searchText.length - 1) {
                    query += " or ";
                }
            }
            if (!intialFlag) {
                query += ") and c.IsPending = 0";
            }
        }


        return new Promise((resolve, reject) => {

            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function (err, result) {
                        if (err) throw reject(err);
                        connection.end(function (err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }
}

module.exports = RequirementService;