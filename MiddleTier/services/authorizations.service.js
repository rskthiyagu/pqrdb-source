
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


/* static product service class */
class AuthorizationService {
    static create(data) {
        if (data) {
            var name = '';
            if (data['name'])
                name = data['name'];
            var query = "Insert into authorizations (Name) values ('" + name + "')";
            var connection = dbAccessService.createConnection();
            return new Promise((resolve, reject) => {
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function(err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{"status": "success", "id": result['insertId']}]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(uid) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (uid) {
                query = `select distinct a.Id as id, a.Name as name from authorizations a where a.Id = ` + uid;
            } else {
                query = `select distinct a.Id as id, a.Name as name from authorizations a`;
            }
            var connection = dbAccessService.createConnection();
            connection.connect(function (err) {
                if (err) {
                    return reject(err);
                } else {
                    connection.query(query, function(err, result) {
                        if (err) throw reject(err);
                        connection.end(function(err) {
                            if (err) throw reject(err);
                        });
                        return resolve(result);
                    });
                }
            });
        });
    }


    static update(uid, data) {
        if (data && uid) {
            var name = data['name'];
            var query = 'Update authorizations set Name="' + name + ' where Id = ' + uid;
            var connection = dbAccessService.createConnection();
            return new Promise((resolve, reject) => {
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function(err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from authorizations where Id = ' + uid;
            var connection = dbAccessService.createConnection();
            return new Promise((resolve, reject) => {
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function(err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = AuthorizationService;