var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var async = require('async');

var dbAccessService = require('../services/dbAccessService/dbAccess.service');


/* static product service class */
class AvailabilityStatusService {
    static create(data) {
        if (data) {
            var query = "Insert into availabilitystatusdata (Name) values ('" + data['name'] + "')";
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(statusId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (statusId) {
                query = `select distinct s.Id as id, s.Name as name from availabilitystatusdata s where s.Id = ` + statusId;
            } else {
                query = `select distinct s.Id as id, s.Name as name from availabilitystatusdata s`;
            }

            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });;
        });
    }


    static update(uid, data) {
        if (data && uid) {
            var name = data['name'];
            var query = 'Update availabilitystatusdata set Name="' + name + '" where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }

    static delete(uid) {
        if (uid) {
            var query = 'Delete from availabilitystatusdata where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) throw reject(err);
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = AvailabilityStatusService;