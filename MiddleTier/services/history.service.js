var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var errorService = require('../services/error.service');


/* static product service class */
class HistoryService {
    static create(data) {
        if (data) {
            // null validation
            if (!data['candidateId']) data['candidateId'] = null;
            if (!data['availability']) data['availability'] = null;
            if (!data['isInterviewSubmitted']) data['isInterviewSubmitted'] = null;
            if (!data['submittedTo']) data['submittedTo'] = null;
            if (!data['isInterview']) data['isInterview'] = null;
            if (!data['interviewRound']) data['interviewRound'] = null;
            if (!data['interviewStatus']) data['interviewStatus'] = null;
            if (!data['requirementId']) data['requirementId'] = null;
            if (!data['clientId']) data['clientId'] = null;
            if (!data['createdOn']) data['createdOn'] = '';
            if (!data['createdBy']) data['createdBy'] = null;
            if (!data['updatedOn']) data['updatedOn'] = '';
            if (!data['updatedBy']) data['updatedBy'] = null;
            var query = "Insert into history (CandidateId, Availability, Submission, SubmittedTo, Interview, InterviewRound, InterviewStatus, RequirementId, ClientId, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy) values ";
            if (data['requirementList'] && data['requirementList'].length > 0) {
                for (var i = 0; i < data['requirementList'].length; i++) {
                    query += "(" + data['candidateId'] + ", " + data['availability'] + ", " + data['isInterviewSubmitted'] + ", " + data['submittedTo'] + ", " + data['isInterview'] + ", " + data['interviewRound'] + ", " + data['interviewStatus'] + ", " + data['requirementList'][i]['id'] + ", " + data['requirementList'][i]['clientId'] + ", '" + data['createdOn'] + "', " + data['createdBy'] + ", '" + data['updatedOn'] + "', " + data['updatedBy'] + ")";
                    if (i !== data['requirementList'].length - 1)
                        query += ', ';
                }
            } else {
                query += "(" + data['candidateId'] + ", " + data['availability'] + ", " + data['isInterviewSubmitted'] + ", " + data['submittedTo'] + ", " + data['isInterview'] + ", " + data['interviewRound'] + ", " + data['interviewStatus'] + ", " + data['requirementId'] + ", " + data['clientId'] + ", '" + data['createdOn'] + "', " + data['createdBy'] + ", '" + data['updatedOn'] + "', " + data['updatedBy'] + ")";
            }
                        console.log(query);
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('history', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve([{"status": "success", "id": result['insertId']}]);
                        });
                    }
                });
            });
        } else {
            return "data is required to do operation";
        }
    }

    static retrieve(id, candidateId, requirementId, clientId) {
        // Attempt to connect and execute queries if connection goes through
        return new Promise((resolve, reject) => {
            var query;
            if (id) {
                query = `select distinct h.Id as id, h.CandidateId as candidateId, h.Availability as availability, h.Submission as submission, h.SubmittedTo as submittedTo, h.Interview as interview, h.InterviewRound as interviewRound, h.InterviewStatus as interviewStatus, h.RequirementId as requirementId, h.ClientId as clientId, h.CreatedOn as createdOn, h.CreatedBy as createdBy, h.UpdatedOn as updatedOn, h.UpdatedBy as updatedBy from history h where h.Id = ` + id;
            } else if (candidateId) {
                query = `select distinct h.Id as id, h.CandidateId as candidateId, h.Availability as availability, h.Submission as submission, h.SubmittedTo as submittedTo, h.Interview as interview, h.InterviewRound as interviewRound, h.InterviewStatus as interviewStatus, h.RequirementId as requirementId, h.ClientId as clientId, h.CreatedOn as createdOn, h.CreatedBy as createdBy, h.UpdatedOn as updatedOn, h.UpdatedBy as updatedBy from history h where h.CandidateId = ` + candidateId;
            } else if (requirementId) {
                query = `select distinct h.Id as id, h.CandidateId as candidateId, h.Availability as availability, h.Submission as submission, h.SubmittedTo as submittedTo, h.Interview as interview, h.InterviewRound as interviewRound, h.InterviewStatus as interviewStatus, h.RequirementId as requirementId, h.ClientId as clientId, h.CreatedOn as createdOn, h.CreatedBy as createdBy, h.UpdatedOn as updatedOn, h.UpdatedBy as updatedBy from history h where h.RequirementId = ` + requirementId;
            } else if (clientId) {
                query = `select distinct distinct h.Id as id, h.CandidateId as candidateId, h.Availability as availability, h.Submission as submission, h.SubmittedTo as submittedTo, h.Interview as interview, h.InterviewRound as interviewRound, h.InterviewStatus as interviewStatus, h.RequirementId as requirementId, h.ClientId as clientId, h.CreatedOn as createdOn, h.CreatedBy as createdBy, h.UpdatedOn as updatedOn, h.UpdatedBy as updatedBy from history h where h.ClientId = ` + clientId;
            } else {
                query = `select distinct h.Id as id, h.CandidateId as candidateId, h.Availability as availability, h.Submission as submission, h.SubmittedTo as submittedTo, h.Interview as interview, h.InterviewRound as interviewRound, h.InterviewStatus as interviewStatus, h.RequirementId as requirementId, h.ClientId as clientId, h.CreatedOn as createdOn, h.CreatedBy as createdBy, h.UpdatedOn as updatedOn, h.UpdatedBy as updatedBy from history h`;
            }

            var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('history', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(result);
                        });
                    }
                });;
        });
    }

    // dummy for now
    static update(uid, data) {
        if (data && uid) {
            var name = data['name'];
            var query = 'Update history set Name="' + name + '" where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('history', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id & data are required to do operation";
        }
    }
    // dummy for now
    static delete(uid) {
        if (uid) {
            var query = 'Delete from history where Id = ' + uid;
            return new Promise((resolve, reject) => {
                var connection = dbAccessService.createConnection();
                connection.connect(function (err) {
                    if (err) {
                        return reject(err);
                    } else {
                        connection.query(query, function (err, result) {
                            if (err) {
                                errorService.create('history', err);
                                return reject(err);
                            }
                            connection.end(function(err) {
                                if (err) throw reject(err);
                            });
                            return resolve(["success"]);
                        });
                    }
                });
            });
        } else {
            return "Id is required to do this operation";
        }
    }
}

module.exports = HistoryService;