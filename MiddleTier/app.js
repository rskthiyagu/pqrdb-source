var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
const fileUpload = require('express-fileupload');
const Nexmo = require('nexmo');
// const nodemailer = require('nodemailer');
const mammoth = require("mammoth");
const resumeParser = require('resume-parser');
var reader = require('any-text');
const fs = require('fs');
const sgMail = require('@sendgrid/mail');
var glob = require("glob");
const sendmail = require('sendmail')();

var dbAccess = require('./routes/dbConnection');

// data fields
var users = require('./routes/users');
var skills = require('./routes/skills');
var authorizations = require('./routes/authorizations');
var qualifications = require('./routes/qualifications');
var vendors = require('./routes/vendor');
var cities = require('./routes/cities');
var states = require('./routes/states');
var countries = require('./routes/countries');
var employers = require('./routes/employers');
var candidates = require('./routes/candidate');
var availabilityStatus = require('./routes/availabilityStatus');
var interviewStatus = require('./routes/interviewStatus');
var domains = require('./routes/domains');
var certifications = require('./routes/certifications');
var taxTerms = require('./routes/taxTerms');
var durations = require('./routes/durations');
var interviewRounds = require('./routes/interviewRounds');
var clients = require('./routes/client');
var requirements = require('./routes/requirement');
var requirementStatus = require('./routes/requirementStatus');
var clientStatus = require('./routes/clientStatus');
var clientCategory = require('./routes/clientCategory');
var submittalStatus = require('./routes/submittalStatus');
var interviews = require('./routes/interview');
var submittal = require('./routes/submittal');
var submitReason = require('./routes/submitReason');

var userservice = require('./services/user.service');
var candidateUtilityService = require('./services/candidateUtility.service');
var parserService = require('./services/parser.service');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('public'));

// for static files
app.use('/profileImages', express.static(__dirname + '/userProfileImages'));
app.use('/coverImages', express.static(__dirname + '/userCoverImages'));
app.use('/resumes', express.static(__dirname + '/candidateResumes'));
app.use('/requirementDocuments', express.static(__dirname + '/requirementDocuments'));

const nexmo = new Nexmo({
  apiKey: 'cb39e340',
  apiSecret: 'ttUUnbuk3DwYxT14',
});

var originsWhitelist = [
  'http://localhost:4200',      //this is my front-end url for development
  'http://localhost:8082',
  'http://210.18.138.11:8082'
];
var corsOptions = {
  origin: function (origin, callback) {
    var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
    callback(null, isWhitelisted);
  },
  credentials: true
}
//here is the magic
app.use(cors(corsOptions));

app.use('/api/users', users);
app.use('/api/dbAccess', dbAccess);
app.use('/api/skills', skills);
app.use('/api/authorizations', authorizations);
app.use('/api/qualifications', qualifications);
app.use('/api/vendors', vendors);
app.use('/api/cities', cities);
app.use('/api/states', states);
app.use('/api/countries', countries);
app.use('/api/employers', employers);
app.use('/api/candidates', candidates);
app.use('/api/availabilityStatus', availabilityStatus);
app.use('/api/interviewStatus', interviewStatus);
app.use('/api/domains', domains);
app.use('/api/certifications', certifications);
app.use('/api/durations', durations);
app.use('/api/taxTerms', taxTerms);
app.use('/api/interviewRounds', interviewRounds);
app.use('/api/clients', clients);
app.use('/api/requirements', requirements);
app.use('/api/clients', clients);
app.use('/api/requirementStatus', requirementStatus);
app.use('/api/clientStatus', clientStatus);
app.use('/api/clientCategory', clientCategory);
app.use('/api/submittalStatus', submittalStatus);
app.use('/api/interviews', interviews);
app.use('/api/submittals', submittal);
app.use('/api/submitReason', submitReason);

app.use(fileUpload({
  useTempFiles: true,
  tempFileDir: '/tmp/',
  debug: true,
  uploadTimeout: 3000
}));

// upload the requirement document
app.post('/api/requirements/uploadFile', function (req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let fileName = 'requirementDocuments/' + req.files.files.name;
  let sampleFile = req.files.files;

  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv(fileName, function (err) {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }
    res.status(200).send({ data: 'File uploaded!' });
  });
});

app.post('/api/users/uploadUserProfileImage', function (req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let fileName = 'userProfileImages/' + req.files.files.name;
  let sampleFile = req.files.files;
  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv(fileName, function (err) {
    if (err) {
      return res.status(500).send(err);
    }

    res.status(200).send({ data: 'File uploaded!' });
  });
});

app.post('/api/users/uploadUserCoverImage', function (req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let fileName = 'userCoverImages/' + req.files.files.name;
  let sampleFile = req.files.files;

  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv(fileName, function (err) {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }
    res.status(200).send({ data: 'File uploaded!' });
  });
});

app.post('/api/candidates/uploadResume', function (req, res) {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  var fileNameSuffix = req.files.files.name;
  var duplicateFileNamePrefix = req.body.fileName;
  let fileName = 'candidateResumes/' + req.files.files.name;
  let sampleFile = req.files.files;

  // delete the duplicate files
    if (duplicateFileNamePrefix) {
      glob("candidateResumes/" + duplicateFileNamePrefix + "*", null, function (er, files) {
        for (const file of files) {
          // remove file
          fs.unlinkSync(file);
        }
        console.log(files);
        sampleFile.mv(fileName, function (err) {
          if (err) {
            console.log(err);
            return res.status(500).send(err);
          }
          res.status(200).send({ data: 'File uploaded!', fileName: fileNameSuffix });
        });
      });
    } else {
      sampleFile.mv(fileName, function (err) {
        if (err) {
          console.log(err);
          return res.status(500).send(err);
        }
        res.status(200).send({ data: 'File uploaded!', fileName: fileNameSuffix });
      });
    }

  // fs.unlink(fileName, (err) => {
  //   if (err) {
  //     sampleFile.mv(fileName, function (err) {
  //       if (err) {
  //         console.log(err);
  //         return res.status(500).send(err);
  //       }
  //       res.status(200).send({ data: 'File uploaded!', fileName: fileNameSuffix });
  //     });
  //   } else {
  //     sampleFile.mv(fileName, function (err) {
  //       if (err) {
  //         console.log(err);
  //         return res.status(500).send(err);
  //       }
  //       res.status(200).send({ data: 'File uploaded!', fileName: fileNameSuffix });
  //     });
  //   }
  // });
});

app.post('/api/candidates/suggestedSkills', function (req, res) {
  try {
    var fileName = 'candidateResumes/' + req.body['fileName'];
    reader.getText(fileName).then(function (resumeText) {
      var acquiredSkillList = [];
      var acquiredSkills = candidateUtilityService.getAcquiredSkills(resumeText);
      Promise.all([acquiredSkills]).then(function (skillData) {
        if (skillData && skillData.length !== 0)
          acquiredSkillList = skillData[0];

        // get suggested skills list
        var suggestedSkillSplitList = candidateUtilityService.getSuggestedSkills(resumeText);

        // remove duplicate with set method
        var uniqueSuggestedSplitList = [...new Set(suggestedSkillSplitList)];

        // remove white space with filter method
        uniqueSuggestedSplitList = uniqueSuggestedSplitList.filter(function (str) {
          return /\S/.test(str);
        });
        // eliminate the acquired skills in suggestions
        var concludedSuggestedSplitList = [];
        for (var i = 0; i < uniqueSuggestedSplitList.length; i++) {
          var existFlag = false;
          for (var j = 0; j < acquiredSkillList.length; j++) {
            if (((uniqueSuggestedSplitList[i]).trim()).toLowerCase() == (acquiredSkillList[j]['name']).toLowerCase()) {
              existFlag = true;
              break;
            }
          }
          if (!existFlag)
            concludedSuggestedSplitList.push((uniqueSuggestedSplitList[i]).trim());
        }
        var suggestedSkillList = parserService.validateSkillAvailability(resumeText, concludedSuggestedSplitList, false);
        suggestedSkillList = [...new Set(suggestedSkillList)];
        return res.status(200).json({ 'status': 'success', 'acquiredSkills': acquiredSkillList, 'suggestSkills': suggestedSkillList });
      });
    });
  }
  catch (err) {
    console.log("EEEE     ", err);
    // unexpected error
    return next(err);
  }
});

/* 2 step verification intialize */
app.post('/api/users/generateOTP', function (req, res) {
  const mobileNumber = req.body.mobileNumber;
  nexmo.verify.request({
    number: mobileNumber,
    brand: 'PQRDB',
    code_length: '4'
  }, (err, result) => {
    console.log(err ? err : result);
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).send({ 'message': 'OTP intiated sucessfully', 'data': result });
    }
  });
});

/* validate the otp */
app.post('/api/users/validateOTP', function (req, res) {
  console.log(req);
  const reqId = '' + req.body.requestId;
  const otpCode = '' + req.body.otpCode;
  nexmo.verify.check({
    request_id: reqId,
    code: otpCode
  }, (err, result) => {
    console.log(err ? err : result);
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).send({ 'message': 'OTP validated sucessfully', 'data': result });
    }
  });
});

// mail trigger post
app.post('/api/users/passwordResetMail', function (req, res, err) {

  let mailId = req.body.email;
  const id = req.body.id;
  const resetKey = Math.floor(100000 + Math.random() * 900000);
  const text = 'Your Password Reset Key is ' + resetKey;

  sgMail.setApiKey("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")
  const msg = {
    to: mailId, // Change to your recipient
    from: 'thiyaghu01@gmail.com', // Change to your verified sender    PQRDB2020
    subject: 'PQRDB: User Password Reset Mail',
    html: '<strong>' + text + '</strong>',
  }
  sgMail
    .send(msg)
    .then(() => {
      const updatedUser = userservice.updateResetKey(id, resetKey);
      Promise.all([updatedUser]).then(data => {
        if (data && data[0]['status'] === 'success') {
          console.log('Email sent successfully');
          setTimeout(function () {
            const resetToken = userservice.updateResetKey(id, null);
            Promise.all([resetToken]).then(data => {
            });
          }, 120000);
          return res.status(200).json({ 'status': 'success' });
        } else {
          return res.status(200).json({ 'status': 'fail' });
        }
      });
    })
    .catch((error) => {
      console.error(error)
    })
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

module.exports = app;
