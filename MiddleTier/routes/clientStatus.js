var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var clientStatusService = require('../services/clientStatus.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newClientStatus = await clientStatusService.create(body);

            Promise.all(newClientStatus).then(data => {
                return res.status(201).json({ "clientStatus": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const status = await clientStatusService.retrieve(req.params.id);

        Promise.all(status).then(data => {
            return res.status(200).json({ "clientStatus": data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const statusList = await clientStatusService.retrieve();

        Promise.all(statusList).then(data => {
            return res.status(200).json({ clientStatus: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedStatus = await clientStatusService.update(req.params.id, req.body);

        Promise.all(updatedStatus).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedStatus = await clientStatusService.delete(req.params.id);

        Promise.all(deletedStatus).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;