var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var authorizationService = require('../services/authorizations.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newAuthorization = await authorizationService.create(body);

            Promise.all(newAuthorization).then(data => {
                return res.status(201).json({ "authorizations": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const authorization = await authorizationService.retrieve(req.params.id);

        Promise.all(authorization).then(data => {
            return res.status(200).json({ authorization: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const authorizationList = await authorizationService.retrieve();

        Promise.all(authorizationList).then(data => {
            return res.status(200).json({ authorizations: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedAuthorization = await authorizationService.update(req.params.id, req.body);

        Promise.all(updatedAuthorization).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedAuthorization = await authorizationService.delete(req.params.id);

        Promise.all(deletedAuthorization).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;