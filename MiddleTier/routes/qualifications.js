var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var qualificationService = require('../services/qualification.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newQualification = await qualificationService.create(body);

            Promise.all(newQualification).then(data => {
                return res.status(201).json({ "qualifications": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const qualification = await qualificationService.retrieve(req.params.id);

        Promise.all(qualification).then(data => {
            return res.status(200).json({ qualifications: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const qualificationList = await qualificationService.retrieve();

        Promise.all(qualificationList).then(data => {
            return res.status(200).json({ qualifications: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedQualification = await qualificationService.update(req.params.id, req.body);

        Promise.all(updatedQualification).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedQualification = await qualificationService.delete(req.params.id);

        Promise.all(deletedQualification).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;