var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var taxTermService = require('../services/taxTerms.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newTaxTerm = await taxTermService.create(body);

            Promise.all(newTaxTerm).then(data => {
                return res.status(201).json({ "taxTerms": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const taxTerm = await taxTermService.retrieve(req.params.id);

        Promise.all(taxTerm).then(data => {
            return res.status(200).json({ "taxTerms": data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const taxTermList = await taxTermService.retrieve();

        Promise.all(taxTermList).then(data => {
            return res.status(200).json({ "taxTerms": data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedTaxTerm = await taxTermService.update(req.params.id, req.body);

        Promise.all(updatedTaxTerm).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedTaxTerm = await taxTermService.delete(req.params.id);

        Promise.all(deletedTaxTerm).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;