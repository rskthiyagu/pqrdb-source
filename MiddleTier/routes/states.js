var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var stateService = require('../services/state.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newState = await stateService.create(body);

            Promise.all(newState).then(data => {
                return res.status(201).json({ "states": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const state = await stateService.retrieve(req.params.id);

        Promise.all(state).then(data => {
            return res.status(200).json({ 'states': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a state by countryId */
router.get('/fetchRecordByCountryId/:id', cors(), async (req, res, next) => {
    try {
        const state = await stateService.retrieve(null, req.params.id);

        Promise.all(state).then(data => {
            return res.status(200).json({ 'states': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});


/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const stateList = await stateService.retrieve();

        Promise.all(stateList).then(data => {
            return res.status(200).json({ 'states': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedState = await stateService.update(req.params.id, req.body);

        Promise.all(updatedState).then(data => {
            return res.status(200).json({ 'states': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedState = await stateService.delete(req.params.id);

        Promise.all(deletedState).then(data => {
            return res.status(200).json({ 'success': true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;