var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var durationService = require('../services/durations.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newDuration = await durationService.create(body);

            Promise.all(newDuration).then(data => {
                return res.status(201).json({ "duration": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const duration = await durationService.retrieve(req.params.id);

        Promise.all(duration).then(data => {
            return res.status(200).json({ "duration": data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const durationList = await durationService.retrieve();

        Promise.all(durationList).then(data => {
            return res.status(200).json({ "duration": data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedDuration = await durationService.update(req.params.id, req.body);

        Promise.all(updatedDuration).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedDuration = await durationService.delete(req.params.id);

        Promise.all(deletedDuration).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;