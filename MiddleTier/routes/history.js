var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var historyService = require('../services/history.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newHistory = await historyService.create(body);

            Promise.all(newHistory).then(data => {
                return res.status(201).json({ "history": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const history = await historyService.retrieve(req.params.id);

        Promise.all(history).then(data => {
            return res.status(200).json({ history: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a history by candidateId */
router.get('/fetchRecordByCandidates/:id', cors(), async (req, res, next) => {
    try {
        const history = await historyService.retrieve(null, req.params.id);

        Promise.all(history).then(data => {
            return res.status(200).json({ history: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a history by requirementId */
router.get('/fetchRecordByRequirements/:id', cors(), async (req, res, next) => {
    try {
        const history = await historyService.retrieve(null, null, req.params.id);

        Promise.all(history).then(data => {
            return res.status(200).json({ history: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a history by clientId */
router.get('/fetchRecordByClients/:id', cors(), async (req, res, next) => {
    try {
        const history = await historyService.retrieve(null, null, null, req.params.id);

        Promise.all(history).then(data => {
            return res.status(200).json({ history: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const historyList = await historyService.retrieve();

        Promise.all(historyList).then(data => {
            return res.status(200).json({ history: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedHistory = await historyService.update(req.params.id, req.body);

        Promise.all(updatedHistory).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedHistory = await historyService.delete(req.params.id);

        Promise.all(deletedHistory).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;