var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var requirementStatusService = require('../services/requirementStatus.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newRequirementStatus = await requirementStatusService.create(body);

            Promise.all(newRequirementStatus).then(data => {
                return res.status(201).json({ "jobStatus": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const requirementStatus = await requirementStatusService.retrieve(req.params.id);

        Promise.all(requirementStatus).then(data => {
            return res.status(200).json({ jobStatus: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const requirementStatusList = await requirementStatusService.retrieve();

        Promise.all(requirementStatusList).then(data => {
            return res.status(200).json({ jobStatus: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedRequirementStatus = await requirementStatusService.update(req.params.id, req.body);

        Promise.all(updatedRequirementStatus).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedRequirementStatus = await requirementStatusService.delete(req.params.id);

        Promise.all(deletedRequirementStatus).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;