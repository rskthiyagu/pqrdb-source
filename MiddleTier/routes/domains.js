var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var domainService = require('../services/domains.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newDomain = await domainService.create(body);

            Promise.all(newDomain).then(data => {
                return res.status(201).json({ "domains": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const domain = await domainService.retrieve(req.params.id);

        Promise.all(domain).then(data => {
            return res.status(200).json({ domains: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const domainList = await domainService.retrieve();

        Promise.all(domainList).then(data => {
            return res.status(200).json({ domains: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedDomain = await domainService.update(req.params.id, req.body);

        Promise.all(updatedDomain).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedDomain = await domainService.delete(req.params.id);

        Promise.all(deletedDomain).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;