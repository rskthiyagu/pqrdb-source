var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var submitReasonService = require('../services/submitReason.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newSkill = await submitReasonService.create(body);

            Promise.all(newSkill).then(data => {
                return res.status(201).json({ "submitReason": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const skill = await submitReasonService.retrieve(req.params.id);

        Promise.all(skill).then(data => {
            return res.status(200).json({ submitReason: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const skillList = await submitReasonService.retrieve();

        Promise.all(skillList).then(data => {
            return res.status(200).json({ submitReason: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedSkill = await submitReasonService.update(req.params.id, req.body);

        Promise.all(updatedSkill).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedSkill = await submitReasonService.delete(req.params.id);

        Promise.all(deletedSkill).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;