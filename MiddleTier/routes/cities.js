var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var cityService = require('../services/city.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newCity = await cityService.create(body);

            Promise.all(newCity).then(data => {
                return res.status(201).json({ "cities": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const city = await cityService.retrieve(req.params.id);

        Promise.all(city).then(data => {
            return res.status(200).json({ 'cities': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a cities based on countryID */
router.get('/fetchRecordByCountryId/:id', cors(), async (req, res, next) => {
    try {
        const city = await cityService.retrieve(null, null, req.params.id);

        Promise.all(city).then(data => {
            return res.status(200).json({ 'cities': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a cities based on stateID */
router.get('/fetchRecordByStateId/:id', cors(), async (req, res, next) => {
    try {
        const city = await cityService.retrieve(null, req.params.id);

        Promise.all(city).then(data => {
            return res.status(200).json({ 'cities': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const cityList = await cityService.retrieve();

        Promise.all(cityList).then(data => {
            return res.status(200).json({ 'cities': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedCity = await cityService.update(req.params.id, req.body);

        Promise.all(updatedCity).then(data => {
            return res.status(200).json({ 'cities': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedCity = await cityService.delete(req.params.id);

        Promise.all(deletedCity).then(data => {
            return res.status(200).json({ 'success': true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;