var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var interviewMappingService = require('../services/interviewMapping.service');
var historyService = require('../services/history.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newinterview = await interviewMappingService.create(body);
            var historyCreated = historyService.create(body);

            Promise.all(newinterview).then(data => {
                return res.status(201).json({ "interviews": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a interview by interviewId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const interview = await interviewMappingService.retrieve(req.params.id);

        Promise.all(interview).then(data => {
            return res.status(200).json({ interviews: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a interview by interviewId */
router.post('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const candidateId = req.body.candidateId;
        const requirementId = req.body.requirementId;
        const interview = await interviewMappingService.retrieve(candidateId, requirementId);

        Promise.all(interview).then(data => {
            // if (data && data.length > 0) {
            //     var listArray = [];
            //     for (var i = 0; i < data.length; i++) {
            //         if (data[i]['count'] == 0) {
            //             listArray.push(data[i]);
            //         }
            //     }
            //     return res.status(200).json({ interviews: listArray });
            // } else {
            //     return res.status(200).json({ interviews: [] });
            // }

            return res.status(200).json({ interviews: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a interview by candidateId */
router.get('/fetchRecordByCandidates/:id', cors(), async (req, res, next) => {
    try {
        const interview = await interviewMappingService.retrieve(null, req.params.id);

        Promise.all(interview).then(data => {
            return res.status(200).json({ interviews: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a interview by requirementId */
router.get('/fetchRecordByRequirements/:id', cors(), async (req, res, next) => {
    try {
        const interview = await interviewMappingService.retrieve(null, req.params.id);

        Promise.all(interview).then(data => {
            return res.status(200).json({ interviews: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a interview by clientId */
router.get('/fetchRecordByClients/:id', cors(), async (req, res, next) => {
    try {
        const interview = await interviewMappingService.retrieve(null, null, null, req.params.id);

        Promise.all(interview).then(data => {
            return res.status(200).json({ interviews: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a interview by candidateId */
router.get('/fetchRecordByInterviewStatus/:id', cors(), async (req, res, next) => {
    try {
        const interview = await interviewMappingService.retrieve(null, null, null, null, req.params.id);

        Promise.all(interview).then(data => {
            return res.status(200).json({ interviews: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a interview by candidateId */
router.get('/fetchRecordByInterviewRound/:id', cors(), async (req, res, next) => {
    try {
        const interview = await interviewMappingService.retrieve(null, null, null, null, null, req.params.id);

        Promise.all(interview).then(data => {
            return res.status(200).json({ interviews: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const interviewList = await interviewMappingService.retrieve();

        Promise.all(interviewList).then(data => {
            // if (data && data.length > 0) {
            //     var listArray = [];
            //     for (var i = 0; i < data.length; i++) {
            //         if (data[i]['count'] == 0) {
            //             listArray.push(data[i]);
            //         }
            //     }
            //     return res.status(200).json({ interviews: listArray });
            // } else {
            //     return res.status(200).json({ interviews: [] });
            // }
            return res.status(200).json({ interviews: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedInterview = await interviewMappingService.update(req.params.id, req.body);

        Promise.all(updatedInterview).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedInterview = await interviewMappingService.delete(req.params.id);

        Promise.all(deletedInterview).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;