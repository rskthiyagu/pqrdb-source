var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var employerService = require('../services/employer.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newEmployer = await employerService.create(body);

            Promise.all(newEmployer).then(data => {
                return res.status(201).json({ "employers": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const employer = await employerService.retrieve(req.params.id);

        Promise.all(employer).then(data => {
            return res.status(200).json({ 'employers': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const employerList = await employerService.retrieve();

        Promise.all(employerList).then(data => {
            return res.status(200).json({ 'employers': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedEmployer = await employerService.update(req.params.id, req.body);

        Promise.all(updatedEmployer).then(data => {
            return res.status(200).json({ 'employers': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedEmployer = await employerService.delete(req.params.id);

        Promise.all(deletedEmployer).then(data => {
            return res.status(200).json({ 'success': true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;