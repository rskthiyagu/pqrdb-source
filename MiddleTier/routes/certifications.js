var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var certficationService = require('../services/certifications.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newCertification = await certficationService.create(body);

            Promise.all(newCertification).then(data => {
                return res.status(201).json({ "certifications": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const certification = await certficationService.retrieve(req.params.id);

        Promise.all(certification).then(data => {
            return res.status(200).json({ certifications: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const certificationList = await certficationService.retrieve();

        Promise.all(certificationList).then(data => {
            return res.status(200).json({ certifications: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedCertification = await certficationService.update(req.params.id, req.body);

        Promise.all(updatedCertification).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedCertification = await certficationService.delete(req.params.id);

        Promise.all(deletedCertification).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;