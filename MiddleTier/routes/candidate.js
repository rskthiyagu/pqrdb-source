var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var searchTextBackup = [];

var dbAccessService = require('../services/dbAccessService/dbAccess.service');
var candidateService = require('../services/candidate.service');
var skillMappingService = require('../services/candidateSkillMapping.service');
var authorizationMappingService = require('../services/candidateAuthorizationMapping.service');
var qualificationMappingService = require('../services/candidateQualificationMapping.service');
var employerMappingService = require('../services/candidateEmployerMapping.service');
var domainMappingService = require('../services/candidateDomainMapping.service');
var certificationMappingService = require('../services/candidateCertificationsMapping.service');

var skillService = require('../services/skills.service');
var authorizationService = require('../services/authorizations.service');
var qualificationService = require('../services/qualification.service');
var employerService = require('../services/employer.service');
var domainService = require('../services/domains.service');
var certificationService = require('../services/certifications.service');

var cityService = require('../services/city.service');
var stateService = require('../services/state.service');
var countryService = require('../services/countries.service');
var vendorService = require('../services/vendor.service');
var taxTermsService = require('../services/taxTerms.service');
var durationsService = require('../services/durations.service');

var candidateUtilityService = require('../services/candidateUtility.service');
var interviewMappingService = require('../services/interviewMapping.service');
var submittalMappingService = require('../services/submittalMapping.service');

var historyService = require('../services/history.service');


/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
        var candidateCount = await candidateService.getCount();

        Promise.all(candidateCount).then(async data => {
            var newCategory = await candidateService.create(body);
            var candidateId = parseInt(data[0]['count']) + 1;
            var skillList = body['skills'];
            var authorizationList = body['authorizations'];
            var qualificationList = body['qualifications'];
            var employerList = body['employers'];
            var domainList = body['domains'];
            var certificationList = body['certifications'];
            var newSkillMapping;
            var newAuthorizationMapping;
            var newQualificationMapping;
            var newEmployerMapping;
            var newDomainMapping;
            var newCertificateMapping;
            if (skillList && skillList.length !== 0)
                newSkillMapping = await skillMappingService.create(skillList, candidateId);
            if (authorizationList && authorizationList.length !== 0)
                newAuthorizationMapping = await authorizationMappingService.create(authorizationList, candidateId);
            if (qualificationList && qualificationList.length !== 0)
                newQualificationMapping = await qualificationMappingService.create(qualificationList, candidateId);
            if (employerList && employerList.length !== 0)
                newEmployerMapping = await employerMappingService.create(employerList, candidateId);
            if (domainList && domainList.length !== 0)
                newDomainMapping = await domainMappingService.create(domainList, candidateId);
            if (certificationList && certificationList.length !== 0)
                newCertificateMapping = await certificationMappingService.create(certificationList, candidateId);
            Promise.all([newCategory, newSkillMapping, newAuthorizationMapping, newQualificationMapping, newEmployerMapping, newDomainMapping, newCertificateMapping]).then(resData => {
                console.log("RERERER  ", resData);
                return res.status(201).json({ "success": true, "id": resData[0]['id'] });
            });
        });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* adds a new resume record to the list */
router.post('/createPendingRecord', cors(), async (req, res, next) => {
    const body = req.body;

    try {
        var pendingCandidate = await candidateService.createPendingRecord(body);

        Promise.all(pendingCandidate).then(data => {
            return res.status(201).json({ "success": true });
        });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const id = req.params.id;
        const category = await candidateService.retrieve(id);
        const candidateSkillMappingList = await skillMappingService.retrieve(id);
        const skillsList = await skillService.retrieve();
        const candidateAuthorizationMappingList = await authorizationMappingService.retrieve(id);
        const authorizationList = await authorizationService.retrieve();
        const candidateQualificationMappingList = await qualificationMappingService.retrieve(id);
        const qualificationList = await qualificationService.retrieve();
        const candidateEmployerMappingList = await employerMappingService.retrieve(id);
        const employerList = await employerService.retrieve();
        const domainList = await domainService.retrieve();
        const candidateDomainMappingList = await domainMappingService.retrieve(id);
        const certificationList = await certificationService.retrieve();
        const candidateCertificationMappingList = await certificationMappingService.retrieve(id);
        const cityList = await cityService.retrieve();
        const stateList = await stateService.retrieve();
        const countryList = await countryService.retrieve();
        const vendorList = await vendorService.retrieve();
        const taxTermsList = await taxTermsService.retrieve();
        const durationList = await durationsService.retrieve();

        Promise.all([category,
                     candidateSkillMappingList,
                     skillsList,
                     candidateAuthorizationMappingList,
                     authorizationList,
                     candidateQualificationMappingList,
                     qualificationList,
                     candidateEmployerMappingList,
                     employerList,
                     candidateDomainMappingList,
                     domainList,
                     candidateCertificationMappingList,
                     certificationList,
                     cityList,
                     stateList,
                     countryList,
                     vendorList,
                     taxTermsList,
                     durationList]).then(data => {
            const candidateResultSet = candidateUtilityService.organize(data);
            return res.status(200).json({ candidates: candidateResultSet });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* fetch recently created Record */
router.get('/fetchRecentRecord', cors(), async (req, res, next) => {
    try {
        const candidateList = await candidateService.retrieveNewlyCreated(true);
        Promise.all([candidateList]).then(data => {
            if (data && data.length !== 0) {
                return res.status(200).json({ recentCandidates: data[0] });
            } else {
                return res.status(200).json({ recentCandidates: data });
            }
        })
    } catch (err) {
        return next(err);
    }
});

// fetch past history
router.get('/retrievePastHistory/:id', cors(), async (req, res, next) => {
    try {
        const candidateList = await candidateService.retrievePastHistory(req.params.id);
        Promise.all([candidateList]).then(data => {
            if (data && data.length !== 0) {
                return res.status(200).json({ pastHistory: data[0] });
            } else {
                return res.status(200).json({ pastHistory: data });
            }
        })
    } catch (err) {
        return next(err);
    }
});

/* fetch recently created Record */
router.get('/fetchPendingRecord/:id', cors(), async (req, res, next) => {
    try {
        const id = req.params.id;
        const candidateList = await candidateService.retrievePendingRecord(id, false);
        Promise.all([candidateList]).then(data => {
            if (data && data.length !== 0) {
                return res.status(200).json({ pendingCandidates: data[0] });
            } else {
                return res.status(200).json({ pendingCandidates: data });
            }
        })
    } catch (err) {
        return next(err);
    }
})

/* fetch recently created Record */
router.get('/fetchRecentPendingRecord/:id', cors(), async (req, res, next) => {
    try {
        const id = req.params.id;
        const candidateList = await candidateService.retrievePendingRecord(id, true);
        Promise.all([candidateList]).then(data => {
            if (data && data.length !== 0) {
                return res.status(200).json({ pendingCandidates: data[0] });
            } else {
                return res.status(200).json({ pendingCandidates: data });
            }
        })
    } catch (err) {
        return next(err);
    }
})

/* fetch recently created Record for list of users */
router.post('/fetchPendingRecord', cors(), async (req, res, next) => {
    try {
        const idList = req.body;
        const candidateList = await candidateService.retrievePendingRecord(idList, false);
        Promise.all([candidateList]).then(data => {
            if (data && data.length !== 0) {
                return res.status(200).json({ pendingCandidates: data[0] });
            } else {
                return res.status(200).json({ pendingCandidates: data });
            }
        })
    } catch (err) {
        return next(err);
    }
})

/* retrieves a category by categoryId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const candidatesList = await candidateService.retrieve();
        const candidateSkillMappingList = await skillMappingService.retrieve();
        const skillsList = await skillService.retrieve();
        const candidateAuthorizationMappingList = await authorizationMappingService.retrieve();
        const authorizationList = await authorizationService.retrieve();
        const candidateQualificationMappingList = await qualificationMappingService.retrieve();
        const qualificationList = await qualificationService.retrieve();
        const candidateEmployerMappingList = await employerMappingService.retrieve();
        const employerList = await employerService.retrieve();
        const domainList = await domainService.retrieve();
        const candidateDomainMappingList = await domainMappingService.retrieve();
        const certificationList = await certificationService.retrieve();
        const candidateCertificationMappingList = await certificationMappingService.retrieve();
        const cityList = await cityService.retrieve();
        const stateList = await stateService.retrieve();
        const countryList = await countryService.retrieve();
        const vendorList = await vendorService.retrieve();
        const taxTermsList = await taxTermsService.retrieve();
        const durationList = await durationsService.retrieve();

        Promise.all([candidatesList,
                     candidateSkillMappingList,
                     skillsList,
                     candidateAuthorizationMappingList,
                     authorizationList,
                     candidateQualificationMappingList,
                     qualificationList,
                     candidateEmployerMappingList,
                     employerList,
                     candidateDomainMappingList,
                     domainList,
                     candidateCertificationMappingList,
                     certificationList,
                     cityList,
                     stateList,
                     countryList,
                     vendorList,
                    taxTermsList,
                    durationList
                ]).then(data => {
            const candidateResultSet = candidateUtilityService.organize(data);
            return res.status(200).json({ candidates: candidateResultSet });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

// fetch available candidates
router.get('/fetchAvailableCandidates/:id', cors(), async (req, res, next) => {
    try {
        var candidateId = req.params.id;
        const candidatesList = await candidateService.retrieveCandidateAvailability(candidateId);

        Promise.all([candidatesList]).then(data => {
            if (data && data.length !== 0) {
                return res.status(200).json({ candidates: data[0] });
            } else {
                return res.status(200).json({ candidates: data });
            }
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the customer by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        var candidateId = req.params.id;
        var body = req.body;
        var newCategory = await candidateService.update(candidateId, body);
        deletedSkillMapping = await skillMappingService.delete(candidateId);
        deletedAuthorizationMapping = await authorizationMappingService.delete(candidateId);
        deletedQualificationMapping = await qualificationMappingService.delete(candidateId);
        deletedEmployerMapping = await employerMappingService.delete(candidateId);


        Promise.all([newCategory, deletedSkillMapping, deletedAuthorizationMapping, deletedQualificationMapping, deletedEmployerMapping]).then(async data => {
            var candidateId = req.params.id;
            var skillList = body['skills'];
            var authorizationList = body['authorizations'];
            var qualificationList = body['qualifications'];
            var employerList = body['employers'];
            var domainList = body['domains'];
            var certificationList = body['certifications'];
            var newSkillMapping;
            var newAuthorizationMapping;
            var newQualificationMapping;
            var newEmployerMapping;
            var newDomainMapping;
            var newCertificateMapping;
            if (skillList && skillList.length !== 0)
                newSkillMapping = await skillMappingService.create(skillList, candidateId);
            if (authorizationList && authorizationList.length !== 0)
                newAuthorizationMapping = await authorizationMappingService.create(authorizationList, candidateId);
            if (qualificationList && qualificationList.length !== 0)
                newQualificationMapping = await qualificationMappingService.create(qualificationList, candidateId);
            if (employerList && employerList.length !== 0)
                newEmployerMapping = await employerMappingService.create(employerList, candidateId);
            if (domainList && domainList.length !== 0)
                newDomainMapping = await domainMappingService.create(domainList, candidateId);
            if (certificationList && certificationList.length !== 0)
                newCertificateMapping = await certificationMappingService.create(certificationList, candidateId);
            Promise.all([newSkillMapping, newAuthorizationMapping, newQualificationMapping, newEmployerMapping, newDomainMapping, newCertificateMapping]).then(data => {
                return res.status(200).json({ "success": true });
            });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the customer by uid */
router.put('/updateCandidateAvailability/:id', cors(), async (req, res, next) => {
    try {
        var candidateId = req.params.id;
        var body = req.body;
        var communicationDetail;
        var submissionDetail;
        var interviewDetail;
        const communicationObject = body['communicationDetails'];
        communicationDetail = await candidateService.updateCandidateAvailability(candidateId, communicationObject);
        body['communicationDetails']['candidateId'] = candidateId;
        historyService.create(body['communicationDetails']);
        if (body['submissionDetails'] && body['submissionDetails']['isInterviewSubmitted']) {
            submissionDetail = await submittalMappingService.create(body['submissionDetails']);
            historyService.create(body['submissionDetails']);
        }
        if (body['interviewDetails'] && body['interviewDetails']['isInterview']) {
            interviewDetail = await interviewMappingService.create(body['interviewDetails']);
            historyService.create(body['interviewDetails']);
        }

        Promise.all([communicationDetail, submissionDetail, interviewDetail]).then(async data => {
            return res.status(200).json({ "success": true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the customer by uid */
router.post('/updateDeleteRecord', cors(), async (req, res, next) => {
    try {
        var body = req.body;
        var newCategory = await candidateService.updateDeleteRecord(body);

        Promise.all([newCategory]).then(async data => {
            return res.status(200).json({ "success": true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* validate email & mobile number of candidate */
router.post('/validateUserDetails', cors(), async (req, res, next) => {
    try {
        var body = req.body;
        var newCategory = await candidateService.validateCandidateDetailAvailability(body);

        Promise.all([newCategory]).then(data => {
            if (data && data.length !== 0 && data[0] && data[0].length !== 0) {
                return res.status(200).json({ "data": data, "status": false });
            } else {
                return res.status(200).json({ "status": true });
            }
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const candidateId = req.params.id;

        const candidatesList = await candidateService.delete(candidateId);
        const candidateSkillMappingList = await skillMappingService.delete(candidateId);
        const candidateAuthorizationMappingList = await authorizationMappingService.delete(candidateId);
        const candidateQualificationMappingList = await qualificationMappingService.delete(candidateId);
        const candidateEmployerMappingList = await employerMappingService.delete(candidateId);
        const candidateDomainMappingList = await domainMappingService.delete(candidateId);
        const candidateCertificationMappingList = await certificationMappingService.delete(candidateId);
        Promise.all([candidatesList,
                     candidateSkillMappingList,
                     candidateAuthorizationMappingList,
                     candidateQualificationMappingList,
                     candidateEmployerMappingList,
                     candidateDomainMappingList,
                     candidateCertificationMappingList]).then(data => {
                        return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a count of pending candidates */
router.get('/getPendingCount/:id', cors(), async (req, res, next) => {
    try {
        const id = req.params.id;
        const candidateCount = await candidateService.getCount(id);

        Promise.all(candidateCount).then(data => {
            return res.status(200).json({ 'count': data[0]['count'] });
        });
    } catch (err) {
        return next(err);
    }
});

/* retrieves a statistics of candidates */
router.get('/getStatistics/:id', cors(), async (req, res, next) => {
    try {
        const id = req.params.id;
        const candidateCount = await candidateService.getCandidateTotalStatistics(id);
        const pendingCandidateCount = await candidateService.getPendingCandidateTotalStatistics(id);
        const todayStatistics = await candidateService.getCandidateTotalTodayStatistics(id);

        Promise.all([candidateCount, pendingCandidateCount, todayStatistics]).then(data => {
            const obj = {
                'total': data[0],
                'pending': data[1],
                'today': data[2]
            };
            return res.status(200).json({ 'candidateStatistics': obj });
        });
    } catch (err) {
        return next(err);
    }
});

/* retrieves a count of candidates */
router.get('/getCount', cors(), async (req, res, next) => {
    try {
        const candidateCount = await candidateService.getCount();

        Promise.all(candidateCount).then(data => {
            return res.status(200).json({ 'count': data[0]['count'] });
        });
    } catch (err) {
        return next(err);
    }
});

/* filter & search the candidate list */
router.post('/filter', cors(), async (req, res, next) => {
    const params = req.body;
    try {
        // filter param extract
        const skillsAnd = params['skillsAnd'];
        const skillsOr = params['skillsOr'];
        const authorization = params['authorizations'];
        const qualificationAnd = params['qualificationsAnd'];
        const qualificationOr = params['qualificationsOr'];
        const employer = params['employers'];
        const cities = params['cities'];
        const states = params['states'];
        const countries = params['countries'];
        const users = params['users'];
        const vendors = params['vendors'];
        const domains = params['domains'];
        const certifications = params['certifications'];
        const searchText = params['search'];
        // search history log
        if (searchText && searchText.length !== 0) {
            var countOffset = 0;
            for (var i = 0; i < searchText.length; i++) {
                if (searchText[i] === searchTextBackup[i]) {
                    countOffset++;
                }
            }
            if (countOffset !== searchText.length) {
                candidateUtilityService.setSearchData(searchText, params['userId']);
            }
            searchTextBackup = searchText;
        }
        const filterCandidateList = await candidateService.filter(skillsAnd, skillsOr, authorization, qualificationAnd, qualificationOr, employer, cities, states, countries, users, vendors, domains, certifications, searchText);
        Promise.all([filterCandidateList]).then(async data => {
            if (data && data[0] && data[0][0] && data[0][0].length !== 0) {
                const candidateDataList = data[0];
                const candidatesList = await candidateService.retrieve(null, candidateDataList);
                const candidateSkillMappingList = await skillMappingService.retrieve(null, candidateDataList);
                const skillsList = await skillService.retrieve();
                const candidateAuthorizationMappingList = await authorizationMappingService.retrieve(null, candidateDataList);
                const authorizationList = await authorizationService.retrieve();
                const candidateQualificationMappingList = await qualificationMappingService.retrieve(null, candidateDataList);
                const qualificationList = await qualificationService.retrieve();
                const candidateEmployerMappingList = await employerMappingService.retrieve(null, candidateDataList);
                const employerList = await employerService.retrieve();
                const domainList = await domainService.retrieve();
                const candidateDomainMappingList = await domainMappingService.retrieve(null, candidateDataList);
                const certificationList = await certificationService.retrieve();
                const candidateCertificationMappingList = await certificationMappingService.retrieve(null, candidateDataList);
                const cityList = await cityService.retrieve();
                const stateList = await stateService.retrieve();
                const countryList = await countryService.retrieve();
                const vendorList = await vendorService.retrieve();
                Promise.all([candidatesList,
                             candidateSkillMappingList,
                             skillsList,
                             candidateAuthorizationMappingList,
                             authorizationList,
                             candidateQualificationMappingList,
                             qualificationList,
                             candidateEmployerMappingList,
                             employerList,
                             domainList,
                             candidateDomainMappingList,
                             certificationList,
                             candidateCertificationMappingList,
                             cityList,
                             stateList,
                             countryList,
                             vendorList]).then(data => {
                    const candidateResultSet = candidateUtilityService.organize(data);
                    return res.status(200).json({ candidates: candidateResultSet });
                });
            } else {
                return res.status(200).json({ candidates: data[0] });
            }
        });
    } catch (err) {
        return next(err);
    }
});


module.exports = router;