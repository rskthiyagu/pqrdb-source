var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var vendorService = require('../services/vendor.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newVendor = await vendorService.create(body);

            Promise.all(newVendor).then(data => {
                return res.status(201).json({ "vendors": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const vendor = await vendorService.retrieve(req.params.id);

        Promise.all(vendor).then(data => {
            return res.status(200).json({ 'vendors': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const vendorList = await vendorService.retrieve();

        Promise.all(vendorList).then(data => {
            return res.status(200).json({ 'vendors': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves the best vendors */
router.get('/fetchBestVendors', cors(), async (req, res, next) => {
    try {
        const vendorList = await vendorService.retrieveBestVendors();

        Promise.all(vendorList).then(data => {
            return res.status(200).json({ 'vendors': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedVendor = await vendorService.update(req.params.id, req.body);

        Promise.all(updatedVendor).then(data => {
            return res.status(200).json({ 'vendors': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedVendor = await vendorService.delete(req.params.id);

        Promise.all(deletedVendor).then(data => {
            return res.status(200).json({ 'success': true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;