var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var userService = require('../services/user.service');

/* validate user */
router.get('/validateUser', cors(), async (req, res, next) => {
    try {
        const user = await userService.validate(req.query);

        Promise.all(user).then(async data => {
            if (data && data.length !== 0) {
                if (!data[0]['isActive']) {
                    return res.status(200).json({ users: [], status: 'Access Denied' });
                } else {
                    const userPasswordValidation = await userService.validatePasswordHash(data, req.query.password);
                    Promise.all(userPasswordValidation).then(statusData => {
                        if (statusData[0]['status'] === "pass") {
                            delete (data[0].password);
                            return res.status(200).json({ users: data });
                        } else if (statusData[0]['status'] === "fail") {
                            return res.status(200).json({ users: [] });
                        } else {
                            return res.status(200).json({ users: [] });
                        }
                    });
                }
            } else {
                return res.status(200).json({ users: data });
            }
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* validate username availability */
router.get('/validateUsernameAvailability', cors(), async (req, res, next) => {
    try {
        const user = await userService.validateUserNameAvailability(req.query);

        Promise.all(user).then(data => {
            if (data.length !== 0) {
                return res.status(200).json({ users: data, status: true });
            } else {
                return res.status(200).json({ users: data, status: false });
            }
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* validate email / mobile number availability */
router.get('/validateUserDetailAvailability', cors(), async (req, res, next) => {
    try {
        const user = await userService.validateUserDetailAvailability(req.query);

        Promise.all(user).then(data => {
            if (data.length !== 0) {
                return res.status(200).json({ users: data, status: true });
            } else {
                return res.status(200).json({ users: data, status: false });
            }
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
        var newUser = await userService.create(body);

        Promise.all(newUser).then(data => {
            return res.status(201).json({ users: data });
        });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* password change request post */
router.post('/submitForgotPassword', cors(), async (req, res, next) => {
    const body = req.body;

    try {
        var user = await userService.fetchUser(body);

        Promise.all(user).then(data => {
            if (data && data.length !== 0)
                return res.status(200).json({ 'status': 'success', 'data': data });
            else
                return res.status(200).json({ 'status': 'fail', 'data': data });
        });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* reset password forusers */
router.post('/resetPassword', cors(), async (req, res, next) => {
    var data = req.body;

    try {
        var validateUser = await userService.validateResetKey(data);

        Promise.all([validateUser]).then(async userData => {
            if (userData && userData.length !== 0) {
                var updatePassword = await userService.resetPassword(data);
                Promise.all(updatePassword).then( updatedData => {
                    return res.status(200).json({ status: 'success', user: updatedData });
                })
            } else {
                return res.status(200).json({ status: 'fail', user: userData });
            }
        });
    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* update the settings attribute for users */
router.post('/userDataTransfer/:id', cors(), async (req, res, next) => {
    var id = req.params.id;
    var updatedUserId = req.body.id;

    try {
        var userCreatedTransfer = await userService.userCandidateCreatedOnTransfer(id, updatedUserId);
        var userUpdatedTransfer = await userService.userCandidateUpdatedOnTransfer(id, updatedUserId);

        Promise.all([userCreatedTransfer, userUpdatedTransfer]).then(data => {
            return res.status(201).json({ users: 'success' });
        });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* update the settings attribute for users */
router.post('/updateUserAccess', cors(), async (req, res, next) => {
    const body = req.body;

    try {
        var newUser = await userService.updateUserAccess(body);

        Promise.all(newUser).then(data => {
            return res.status(201).json({ users: data });
        });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const user = await userService.retrieve(req.params.id);

        Promise.all(user).then(data => {
            return res.status(200).json({ user: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const userList = await userService.retrieve();

        Promise.all(userList).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchAccountManagerRecord', cors(), async (req, res, next) => {
    try {
        const userList = await userService.retrieveAccountManager();

        Promise.all(userList).then(data => {
            return res.status(200).json({ accountManager: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchTopRecord', cors(), async (req, res, next) => {
    try {
        const userList = await userService.retrieve(null, true);

        Promise.all(userList).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a pending users */
router.get('/fetchPendingRecord', cors(), async (req, res, next) => {
    try {
        const userList = await userService.retrievePendingUsers(null);

        Promise.all(userList).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a pending users */
router.get('/fetchRejectedRecord', cors(), async (req, res, next) => {
    try {
        const userList = await userService.retrieveRejectedUsers(null);

        Promise.all(userList).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});



/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedUser = await userService.update(req.params.id, req.body);

        Promise.all(updatedUser).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/updatePassword/:id', cors(), async (req, res, next) => {
    try {
        const user = await userService.validate(req.body);

        Promise.all(user).then(async data => {
            if (data && data.length !== 0) {
                if (!data[0]['isActive']) {
                    return res.status(200).json({ users: [], status: 'Access Denied' });
                } else {
                    const userPasswordValidation = await userService.validatePasswordHash(data, req.body.oldPasswordHash);
                    Promise.all(userPasswordValidation).then(async statusData => {
                        if (statusData[0]['status'] === "pass") {
                            const updatedUser = await userService.updatePassword(req.params.id, req.body);

                            Promise.all(updatedUser).then(data => {
                                return res.status(200).json({ users: data });
                            });
                        } else if (statusData[0]['status'] === "fail") {
                            return res.status(200).json({ users: [], status: 'Password not match' });
                        } else {
                            return res.status(200).json({ users: [] });
                        }
                    });
                }
            } else {
                return res.status(200).json({ users: data });
            }
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the user  by uid */
router.put('/updateCoverImage/:id', cors(), async (req, res, next) => {
    try {
        const updatedUser = await userService.updateCoverImage(req.params.id, req.body);

        Promise.all(updatedUser).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the user  by uid */
router.put('/updateProfileImage/:id', cors(), async (req, res, next) => {
    try {
        const updatedUser = await userService.updateProfileImage(req.params.id, req.body);

        Promise.all(updatedUser).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the user  by uid */
router.put('/deactivateUser/:id', cors(), async (req, res, next) => {
    try {
        const updatedUser = await userService.deactivateUser(req.params.id);

        Promise.all(updatedUser).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* activate the user by uid */
router.put('/activateUser/:id', cors(), async (req, res, next) => {
    try {
        const updatedUser = await userService.activateUser(req.params.id);

        Promise.all(updatedUser).then(data => {
            return res.status(200).json({ users: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedUser = await userService.delete(req.params.id);

        Promise.all(deletedUser).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a count of users */
router.get('/getCount', cors(), async (req, res, next) => {
    try {
        const userCount = await userService.getCount();

        Promise.all(userCount).then(data => {
            return res.status(200).json({ 'count': data[0] });
        });
    } catch (err) {
        return next(err);
    }
});

/* retrieves a search log of users */
router.get('/fetchSearchLogRecord/:id', cors(), async (req, res, next) => {
    try {
        const userSearchLog = await userService.fetchSearchLog(false, req.params.id);

        Promise.all(userSearchLog).then(data => {
            return res.status(200).json({ 'userSearchLog': data });
        });
    } catch (err) {
        return next(err);
    }
});

/* retrieves a top 5 search log of users */
router.get('/fetchRecentSearchLogRecord/:id', cors(), async (req, res, next) => {
    try {
        const userSearchLog = await userService.fetchSearchLog(true, req.params.id);

        Promise.all(userSearchLog).then(data => {
            return res.status(200).json({ 'userSearchLog': data });
        });
    } catch (err) {
        return next(err);
    }
});


module.exports = router;
