var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var submittalMappingService = require('../services/submittalMapping.service');
var historyService = require('../services/history.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newSubmittal = await submittalMappingService.create(body);
            var historyCreated = historyService.create(body);

            Promise.all(newSubmittal).then(data => {
                return res.status(201).json({ "submittals": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a submittals by id */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const submittal = await submittalMappingService.retrieve(req.params.id);

        Promise.all(submittal).then(data => {
            return res.status(200).json({ submittals: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a submittals by candidateid */
router.get('/fetchRecordByCandidates/:id', cors(), async (req, res, next) => {
    try {
        const submittal = await submittalMappingService.retrieve(null, req.params.id);

        Promise.all(submittal).then(data => {
            return res.status(200).json({ submittals: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a submittals by requirementid */
router.get('/fetchRecordByRequirements/:id', cors(), async (req, res, next) => {
    try {
        const submittal = await submittalMappingService.retrieve(null, null, req.params.id);

        Promise.all(submittal).then(data => {
            return res.status(200).json({ submittals: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a submittals by clientid */
router.get('/fetchRecordByClients/:id', cors(), async (req, res, next) => {
    try {
        const submittal = await submittalMappingService.retrieve(null, null, null, req.params.id);

        Promise.all(submittal).then(data => {
            return res.status(200).json({ submittals: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a submittals by submittedToid */
router.get('/fetchRecordBySubmittedTo/:id', cors(), async (req, res, next) => {
    try {
        const submittal = await submittalMappingService.retrieve(null, null, null, null, req.params.id);

        Promise.all(submittal).then(data => {
            return res.status(200).json({ submittals: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const submittalList = await submittalMappingService.retrieve();

        Promise.all(submittalList).then(data => {
            return res.status(200).json({ submittals: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedSubmittals = await submittalMappingService.update(req.params.id, req.body);

        Promise.all(updatedSubmittals).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

router.put('/updateStatus/:id', cors(), async (req, res, next) => {
    try {
        const updatedSubmittals = await submittalMappingService.updateStatus(req.params.id, req.body);

        Promise.all(updatedSubmittals).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedSubmittals = await submittalMappingService.delete(req.params.id);

        Promise.all(deletedSubmittals).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;