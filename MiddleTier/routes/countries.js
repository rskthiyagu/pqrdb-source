var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var countryService = require('../services/countries.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newCountry = await countryService.create(body);

            Promise.all(newCountry).then(data => {
                return res.status(201).json({ "countries": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const country = await countryService.retrieve(req.params.id);

        Promise.all(country).then(data => {
            return res.status(200).json({ 'countries': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const countryList = await countryService.retrieve();

        Promise.all(countryList).then(data => {
            return res.status(200).json({ 'countries': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedCountry = await countryService.update(req.params.id, req.body);

        Promise.all(updatedCountry).then(data => {
            return res.status(200).json({ 'countries': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedCountry = await countryService.delete(req.params.id);

        Promise.all(deletedCountry).then(data => {
            return res.status(200).json({ 'success': true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;