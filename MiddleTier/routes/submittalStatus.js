var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var submittalStatusService = require('../services/submittalStatus.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newStatus = await submittalStatusService.create(body);

            Promise.all(newStatus).then(data => {
                return res.status(201).json({ data });
            });
    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const status = await submittalStatusService.retrieve(req.params.id);

        Promise.all(status).then(data => {
            return res.status(200).json({ 'submittalStatus': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const statusList = await submittalStatusService.retrieve();

        Promise.all(statusList).then(data => {
            return res.status(200).json({ 'submittalStatus': data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedStatus = await submittalStatusService.update(req.params.id, req.body);

        Promise.all(updatedStatus).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedStatus = await submittalStatusService.delete(req.params.id);

        Promise.all(deletedStatus).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;