var express = require('express');
var router = express.Router();
var dbAccessService = require('../services/dbAccessService/dbAccess.service');

// dbAccessService.createConnection();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json({users: [{name: 'Timmy'}]});
});

module.exports = router;