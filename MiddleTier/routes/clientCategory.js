var express = require('express');
var router = express.Router();
var async = require('async');
var cors = require('cors');

var clientCategoryService = require('../services/clientCategory.service');

/* adds a new category to the list */
router.post('/create', cors(), async (req, res, next) => {
    const body = req.body;

    try {
            var newCategory = await clientCategoryService.create(body);

            Promise.all(newCategory).then(data => {
                return res.status(201).json({ "clientCategory": data });
            });

    }
    catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({ error: err.message });
        }

        // unexpected error
        return next(err);
    }
});

/* retrieves a category by categoryId */
router.get('/fetchRecord/:id', cors(), async (req, res, next) => {
    try {
        const clientCategory = await clientCategoryService.retrieve(req.params.id);

        Promise.all(clientCategory).then(data => {
            return res.status(200).json({ clientCategory: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* retrieves a vendor by vendorId */
router.get('/fetchRecord', cors(), async (req, res, next) => {
    try {
        const clientCategoryList = await clientCategoryService.retrieve();

        Promise.all(clientCategoryList).then(data => {
            return res.status(200).json({ clientCategory: data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* updates the vendor by uid */
router.put('/update/:id', cors(), async (req, res, next) => {
    try {
        const updatedClientCategory = await clientCategoryService.update(req.params.id, req.body);

        Promise.all(updatedClientCategory).then(data => {
            return res.status(200).json({ data });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

/* removes the customer from the customer list by uid */
router.delete('/delete/:id', cors(), async (req, res, next) => {
    try {
        const deletedClientCategory = await clientCategoryService.delete(req.params.id);

        Promise.all(deletedClientCategory).then(data => {
            return res.status(200).json({ success: true });
        });
    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

module.exports = router;