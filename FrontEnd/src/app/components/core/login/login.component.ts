import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';
import { HashingService } from '../../../services/hashing.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  loginFlag: boolean = true;
  userName: string;
  password: string;
  requiredFlag: boolean = false;
  errorFlag: boolean = false;
  userRegister: object;
  userCoverToUpload: File;
  userImageToUpload: File;
  rememberFlag: boolean = true;
  documentHeight: number;
  otpRequestId: string = '';
  otpFlag: boolean = false;
  otpInput: number;
  loggedUserBackup: object;
  accessFlag: boolean = false;
  userNameErrorFlag: boolean = false;
  userMobileEmailErrorFlag: boolean = false;
  emailValidFlag: boolean = false;
  confirmPasswordFlag: boolean = false;
  passwordViewFlag: boolean = false;
  confirmPasswordViewFlag: boolean = false;
  mobileNumberBackup: number;

  // forgot password fields
  forgotPasswordObject: object = {
    'userName': '',
    'password': '',
    'token': null,
    'email': ''
  };
  forgotPasswordFlag: boolean = false;
  forgotPasswordSubmit: boolean = false;
  resetPasswordMailFlag: boolean = true;

  constructor(private apiService: ApiService,
    private router: Router,
    private dataService: DataService,
    private toastrService: ToastrService,
    private utilityService: UtilityService,
    private hashingService: HashingService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    if (userData) {
      this.router.navigateByUrl('/dashboard');
    }
    this.resetRegisterFields();
    this.manipulateDocumentWidth();
  }

  // manipulate height
  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 141;
  }

  // navigate to registration page
  navigateModes(): void {
    this.loginFlag = !this.loginFlag;
    this.errorFlag = false;
    this.requiredFlag = false;
    this.resetRegisterFields();
  }

  // reset the register object
  resetRegisterFields(): void {
    const obj = {
      'firstName': '',
      'lastName': '',
      'userName': '',
      'password': '',
      'confirmPassword': '',
      'role': '',
      'userImage': '',
      'designation': '',
      'mobileNumber': '',
      'email': '',
      'fbLink': '',
      'twitterLink': '',
      'linkedinLink': '',
      'coverImage': ''
    };
    this.userRegister = obj;
    this.userName = '';
    this.password = '';
  }

  // validate required fields
  validateRequiredFields(): void {
    if (!this.userName || !this.password) {
      this.requiredFlag = true;
    } else {
      this.requiredFlag = false;
    }
  }

  // validate user
  validateUser(): void {
    this.validateRequiredFields();
    if (!this.requiredFlag) {
      this.utilityService.updateSpinnerFlag(true);
      this.hashingService.getHash(this.password).then(hashdata => {
        const passwordHash = hashdata;
        let param = {
          'userName': this.userName,
          'password': passwordHash
        };
        this.apiService.getData('/api/users/validateUser', param).subscribe(data => {
          if (data && data['users'] && data['users'].length != 0) {
            this.errorFlag = false;
            this.accessFlag = false;
            this.requiredFlag = false;
            this.loggedUserBackup = data['users'][0];
            // trigger otp for user
            // const url = '/api/users/generateOTP';
            // const param = {'mobileNumber': data['users'][0]['mobileNumber']};
            // this.apiService.postData(url, param).subscribe(data => {
            //   if (data && data.data) {
            //     this.otpRequestId = data.data.request_id;
            //     this.utilityService.updateSpinnerFlag(false);
            //     this.otpFlag = true;
            //   }
            // });
            // logged user setting
            this.dataService.setLoggedUserDetails(this.loggedUserBackup);
            if (this.rememberFlag) {
              localStorage.setItem('userDetails', JSON.stringify(this.loggedUserBackup));
            }
            this.errorFlag = false;
            this.toastrService.success('Login successful...', 'Success', { timeOut: 1000 });
            this.router.navigateByUrl('/dashboard');
          } else {
            if (data && data['status'] && data['status'] === 'Access Denied') {
              this.accessFlag = true;
              this.errorFlag = false;
              this.toastrService.error("Access Denied. Please contact administrator", "Access Denied", { timeOut: 2000 });
            } else {
              this.accessFlag = false;
              this.errorFlag = true;
              this.toastrService.error("Log in Failed...", "Error", { timeOut: 1000 });
            }
          }
          this.utilityService.updateSpinnerFlag(false);
        }, err => {
          this.errorFlag = true;
          this.utilityService.updateSpinnerFlag(false);
        });
      }, err => {
        this.toastrService.error("Something went wrong, Please try again...", "Error", { timeOut: 1000 });
        this.utilityService.updateSpinnerFlag(false);
      });
    }
  }

  // validate otp for user
  validateOTP(): void {
    const code = '' + this.otpInput;
    if (this.otpInput && code.length === 4) {
      // trigger otp for user
      this.utilityService.updateSpinnerFlag(true);
      const url = '/api/users/validateOTP';
      const param = { 'requestId': this.otpRequestId, 'otpCode': code };
      this.apiService.postData(url, param).subscribe(data => {
        console.log(data);
        if (data && data.data && data.data.status == 0) {
          this.utilityService.updateSpinnerFlag(false);
          this.otpFlag = false;
          // logged user setting
          this.dataService.setLoggedUserDetails(this.loggedUserBackup);
          if (this.rememberFlag) {
            localStorage.setItem('userDetails', JSON.stringify(this.loggedUserBackup));
          }
          this.errorFlag = false;
          this.toastrService.success('Login successful...', 'Success', { timeOut: 1000 });
          this.router.navigateByUrl('/dashboard');
        } else {
          this.errorFlag = true;
          this.toastrService.error("OTP not valid...", "Error", { timeOut: 1000 });
        }
      });
    }
  }

  // event handler for document selected
  documentSelected(e: any, event: any): void {
    if (e && e.item(0)) {
      this.userRegister['userImage'] = e.item(0);
      this.userImageToUpload = e.item(0);
    } else {
      this.userRegister['userImage'] = this.userImageToUpload['name'];
    }
      // event.target.files = this.userImageToUpload;
  }

  // event handler for document selected
  documentCoverSelected(e: any, event: any): void {
    if (e && e.item(0))
      this.userCoverToUpload = e.item(0);
    else
      event.target.files = this.userCoverToUpload;
  }

  // validate required fields for registration
  validateRegisterFields(): void {
    this.emailValidFlag = false;
    if (this.userRegister['userName'] === '' || this.userRegister['password'] === '' ||
      this.userRegister['confirmPassword'] === '' ||
      this.userRegister['firstName'] === '' || this.userRegister['lastName'] === '' ||
      this.userRegister['mobileNumber'] === '' || this.userRegister['email'] === '') {
      this.requiredFlag = true;
    } else {
      // email validation
      const emailPattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
      if ((this.userRegister['email']).match(emailPattern)) {
        this.requiredFlag = false;
        this.emailValidFlag = false;
      } else {
        this.requiredFlag = true;
        this.emailValidFlag = true;
      }
    }
  }

  // save image source for user
  saveImageSource(): void {
    const countUrl = '/api/users/getCount';
    this.apiService.getData(countUrl).subscribe(data => {
      const recordCount = parseInt(data['count']) + 1;
      if (this.userImageToUpload) {
        const userImageNameSplitArray = (this.userImageToUpload.name).split('.');
        const userImageFileType = userImageNameSplitArray[userImageNameSplitArray.length - 1];
        const userImageFileName = 'user_image_' + recordCount + '.' + userImageFileType;
        this.userRegister['fileName'] = userImageFileName;
        const formData: FormData = new FormData();
        formData.append('files', this.userImageToUpload, userImageFileName);
        this.uploadUserImage(formData);
      }
      // cover image
      if (this.userCoverToUpload) {
        const fileNameSplitArray = (this.userCoverToUpload.name).split('.');
        const fileType = fileNameSplitArray[fileNameSplitArray.length - 1];
        const fileName = 'user_cover_image_' + recordCount + '.' + fileType;
        this.userRegister['coverImage'] = fileName;
        const formData: FormData = new FormData();
        formData.append('files', this.userCoverToUpload, fileName);
        this.uploadCoverImage(formData);
      }
      // trigger create user call
      this.saveUserDetails();
    });
  }

  // upload userimage to server
  uploadUserImage(formData: FormData): void {
    this.apiService.postData('/api/users/uploadUserProfileImage', formData).subscribe(data => {
    });
  }

  // upload cover image to server
  uploadCoverImage(formData: FormData): void {
    this.apiService.postData('/api/users/uploadUserCoverImage', formData).subscribe(data => {
    });
  }

  // validate the password & confirmation is correct or not
  validatePassword(): void {
    if (this.userRegister && this.userRegister['password'] && this.userRegister['confirmPassword']) {
      if (this.userRegister['password'] === this.userRegister['confirmPassword']) {
        this.confirmPasswordFlag = true;
      } else {
        this.confirmPasswordFlag = false;
      }
    }
  }

  // register user
  registerUser(): void {
    this.validateRegisterFields();
    if (!this.requiredFlag) {
      this.validatePassword();
      if (this.confirmPasswordFlag) {
        this.utilityService.updateSpinnerFlag(true);
        // validate the username availability
        this.validateUserNameAvailability();
      } else {
        this.toastrService.error('Password & confirm password must be same', 'Password Mismatch', { timeOut: 3000 });
      }
    }
  }

  // register user fields
  registerationProcess(): void {
    this.userRegister['password'] = this.hashingService.getEncryptValue(this.userRegister['password']);
    if (this.userImageToUpload || this.userCoverToUpload) {
      this.saveImageSource();
    } else {
      this.saveUserDetails();
    }
  }

  // validate availability for username
  validateUserNameAvailability(): void {
    const url = '/api/users/validateUsernameAvailability';
    const param = { 'userName': this.userRegister['userName'] };
    this.apiService.getData(url, param).subscribe(data => {
      if (data && !data['status']) {
        // validate the user details availability ( mobile & email )
        this.userNameErrorFlag = false;
        this.validateUserDetailAvailability();
      } else {
        this.utilityService.updateSpinnerFlag(false);
        this.userNameErrorFlag = true;
        this.toastrService.error("Username is exists already...", "UserName Exists", { timeOut: 1000 });
      }
    });
  }

  // validate availability for mobile & email
  validateUserDetailAvailability(): void {
    const url = '/api/users/validateUserDetailAvailability';
    const param = { 'mobileNumber': this.userRegister['mobileNumber'], 'email': this.userRegister['email'] };
    this.apiService.getData(url, param).subscribe(data => {
      if (data && !data['status']) {
        this.userMobileEmailErrorFlag = false;
        this.registerationProcess();
      } else {
        this.userMobileEmailErrorFlag = true;
        this.utilityService.updateSpinnerFlag(false);
        this.toastrService.error("Mobile Number & Email exists already...", "User Details Exists", { timeOut: 1000 });
      }
    });
  }

  // save users details
  saveUserDetails(): void {
    this.apiService.postData('/api/users/create', this.userRegister).subscribe(data => {
      if (data && data['users'] && data['users'].length !== 0) {
        this.errorFlag = false;
        this.requiredFlag = false;
        this.resetRegisterFields();
        this.loginFlag = true;
        this.toastrService.success('Registration submitted for Admin Approval...', 'Success', { timeOut: 2000 });
      } else {
        this.errorFlag = true;
        this.toastrService.error("Registration Failed...", "Error", { timeOut: 1000 });
      }
      this.utilityService.updateSpinnerFlag(false);
    });
  }

  // enable forgot password flow
  enableForgotPassword(): void {
    this.forgotPasswordFlag = true;
  }

  // validate forgot password fields
  validateForgotPasswordFields(): boolean {
    if (this.forgotPasswordObject['userName'] && this.forgotPasswordObject['email']) {
      return true;
    } else {
      return false;
    }
  }

  // back to log in
  backtoLogin(): void {
    this.forgotPasswordFlag = false;
    this.forgotPasswordSubmit = false;
    this.resetPasswordMailFlag = true;
    this.forgotPasswordObject = {
      'userName': '',
      'email': '',
      'token': '',
      'password': ''
    };
  }

  // submit for forgot password
  submitForgotPassword(): void {
    this.forgotPasswordSubmit = true;
    const flag = this.validateForgotPasswordFields();
    if (flag) {
      // api call
      const url = '/api/users/submitForgotPassword';
      this.utilityService.updateSpinnerFlag(true);
      this.apiService.postData(url, this.forgotPasswordObject).subscribe(data => {
        if (data && data['status'] === 'success') {
          const param = data['data'][0];
          const url = '/api/users/passwordResetMail';
          this.forgotPasswordObject['id'] = data['id'];
          this.apiService.postData(url, param).subscribe(mailData => {
            this.utilityService.updateSpinnerFlag(false);
            this.forgotPasswordSubmit = false;
            this.resetPasswordMailFlag = false;
            this.toastrService.success('Password Reset Token sent to your mail successfully...', 'Success', { timeOut: 2500 });
          });
        } else {
          this.utilityService.updateSpinnerFlag(false);
          this.toastrService.error('No User registered / Inactive with given details. Please contact Administrator...', 'Error', { timeOut: 2500 });
        }
      });
    } else {
      this.toastrService.error('Please fill the details to proceed', 'Error', { timeOut: 2500 });
    }
  }

  // update the password
  updateForgotPassword(): void {
    this.forgotPasswordSubmit = true;
    if (this.forgotPasswordObject['password'] && this.forgotPasswordObject['token']) {
      // api call
      this.forgotPasswordObject['password'] = this.hashingService.getEncryptValue(this.forgotPasswordObject['password']);
      this.utilityService.updateSpinnerFlag(true);
      const url = '/api/users/resetPassword';
      this.apiService.postData(url, this.forgotPasswordObject).subscribe(data => {
        this.utilityService.updateSpinnerFlag(false);
        if (data && data['status'] === 'success') {
        this.forgotPasswordFlag = false;
        this.forgotPasswordSubmit = false;
        this.resetPasswordMailFlag = true;
        this.forgotPasswordObject = {
          'userName': '',
          'email': '',
          'token': '',
          'password': ''
        };
        this.toastrService.success('Password Reset Done successfully...', 'Success', { timeOut: 2500 });
        } else {
          this.toastrService.error('Reset token is not valid', 'Error', { timeOut: 2500 });
        }
      });
    } else {
      this.toastrService.error('Please fill the details to proceed', 'Error', { timeOut: 2500 });
    }
  }

  // field validation
  fieldValidate(event: any): void {
    const alphabetPattern = /^[A-Za-z]+$/;
    const typedText = event.target.value;
    if (typedText && typedText.length > 0 && !(typedText.match(alphabetPattern))) {
      event.target.value = typedText.substr(0, typedText.length-1);
    }
  }

  restrictNegativeValue(event: any): void {
    event.preventDefault();
    let data;
    if (event.key === 'e') {
      data = this.mobileNumberBackup;
    } else {
      data = event.target.value;
    }
    data = data.replace('-', '');
    if (data.length > 10)
      data = data.substr(0, 10);
    event.target.value = data;
    this.mobileNumberBackup = data;
  }

  // password view event handler
  changePasswordViewMode(flag: boolean): void {
    this.passwordViewFlag = flag;
  }

  // confirm password view event handler
  changeConfirmPasswordViewMode(flag: boolean): void {
    this.confirmPasswordViewFlag = flag;
  }

}
