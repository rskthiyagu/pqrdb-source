import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  frontFaceRotateValue: string = 'rotateY(0deg)';
  backFaceRotateValue: string = 'rotateY(-180deg)';
  userList: Array < string > = [];
  loggedUserData: object;
  documentHeight: number;

  constructor(private router: Router,
              private apiService: ApiService,
              private dataService: DataService,
              public utilityService: UtilityService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.manipulateDocumentWidth();
    this.getUserData();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 218;
  }

  // toggle card rotation
  rotateCard(offset): void {
    if (this.userList[offset]['rotateValue'] === 'rotateY(0deg)') {
      this.userList[offset]['rotateValue'] = 'rotateY(180deg)';
    } else {
      this.userList[offset]['rotateValue'] = 'rotateY(0deg)';
    }
  }

  // get user data
  getUserData(): void {
    const url = '/api/users/fetchRecord';
    this.apiService.getData(url).subscribe( data => {
      const userData = data.users;
      if (userData && userData.length !== 0) {
        for (var i = 0; i < userData.length; i++) {
          userData[i]['rotateValue'] = 'rotateY(0deg)';
          userData[i]['statisticsData'] = [];
          userData[i]['statisticsData'].push(userData[i]['userCompletedTotal']);
          userData[i]['statisticsData'].push(userData[i]['userPendingTotal']);
          userData[i]['statisticsData'].push(userData[i]['total']);
        }
        this.userList = userData;
      }
    })
  }

  // navigate to candidate dashboard page
  navigateDashboard(): void {
    this.router.navigateByUrl('/dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('dashboard');
  }

}
