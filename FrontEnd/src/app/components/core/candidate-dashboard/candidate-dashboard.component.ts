import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';
import { DataService } from '../../../services/data.service';


@Component({
  selector: 'app-candidate-dashboard',
  templateUrl: './candidate-dashboard.component.html',
  styleUrls: ['./candidate-dashboard.component.scss']
})
export class CandidateDashboardComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  filterDataSet: Array<object> = [];
  candidateRecord: Array<object> = [];
  searchInput: string;
  filterInput: object = {};
  documentHeight: number;
  // modal related declarations
  showPopUpFlag: boolean = false;
  modalConfig: object = {};
  selectedId: number;
  modalConfigSetUp: object = {
    'title': 'Candidate Availability Status',
    'proceedFlag': true,
    'cancelFlag': true,
    'warningFlag': true,
    'warningText': 'The candidate is contacted within 6 months',
    'fieldList': [
      {
        'id': 0,
        'title': 'Candidate Communication',
        'key': 'communication',
        'value': ''
      },
      {
        'id': 1,
        'title': 'Candidate Availability',
        'key': 'availability',
        'value': ''
      },
      {
        'id': 0,
        'title': 'Requirement',
        'key': 'requirement',
        'value': ''
      },
      {
        'id': 0,
        'title': 'Client',
        'key': 'client',
        'value': ''
      },
      {
        'id': 0,
        'title': 'Comments',
        'key': 'comment',
        'value': '',
        'textareaViewFlag': true
      }
    ]
  };

  filterDataArray: Array<object> = [{
    'title': 'Skills',
    'andOrFlag': true,
    'key': 'skills',
    'dataList': [{
      'title': 'Skills (AND)', 'dropdownData': [], 'dropdownSelected': [], 'subKey': 'skillsAnd', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Skills",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      }
    },
    {
      'title': 'Skills (OR)', 'dropdownData': [], 'dropdownSelected': [], 'subKey': 'skillsOr', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Skills",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      }
    }]
  },
  {
    'title': 'Authorizations', 'dropdownData': [], 'dropdownSelected': [], 'key': 'authorizations', 'dropdownSetting': {
      singleSelection: false,
      text: "Select Authorizations",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  },
  {
    'title': 'Qualifications',
    'andOrFlag': true,
    'key': 'qualifications',
    'dataList': [{
      'title': 'Qualifications (AND)', 'dropdownData': [], 'dropdownSelected': [], 'subKey': 'qualificationsAnd', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Qualifications",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      }
    },
    {
      'title': 'Qualifications (OR)', 'dropdownData': [], 'dropdownSelected': [], 'subKey': 'qualificationsOr', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Qualifications",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      }
    }]
  },
  {
    'title': 'Domains', 'dropdownData': [], 'dropdownSelected': [], 'key': 'domains', 'dropdownSetting': {
      singleSelection: false,
      text: "Select Domains",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  },
  {
    'title': 'Certifications', 'dropdownData': [], 'dropdownSelected': [], 'key': 'certifications', 'dropdownSetting': {
      singleSelection: false,
      text: "Select Certifications",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  },
  {
    'title': 'Cities', 'dropdownData': [], 'dropdownSelected': [], 'key': 'cities', 'dropdownSetting': {
      singleSelection: false,
      text: "Select Cities",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  },
  {
    'title': 'States', 'dropdownData': [], 'dropdownSelected': [], 'key': 'states', 'dropdownSetting': {
      singleSelection: false,
      text: "Select States",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  },
  {
    'title': 'Countries', 'dropdownData': [], 'dropdownSelected': [], 'key': 'countries', 'dropdownSetting': {
      singleSelection: false,
      text: "Select Countries",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  },
  {
    'title': 'Employers', 'dropdownData': [], 'dropdownSelected': [], 'key': 'employers', 'dropdownSetting': {
      singleSelection: false,
      text: "Select Employers",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  },
  {
    'title': 'Users', 'dropdownData': [], 'dropdownSelected': [], 'key': 'users', 'dropdownSetting': {
      singleSelection: false,
      text: "Select Users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  },
  {
    'title': 'Job Boards', 'dropdownData': [], 'dropdownSelected': [], 'key': 'vendors', 'dropdownSetting': {
      singleSelection: false,
      text: "Select job boards",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  }];

  filterUrlList: Array<string> = [
    '/api/skills/fetchRecord',
    '/api/authorizations/fetchRecord',
    '/api/qualifications/fetchRecord',
    '/api/users/fetchRecord',
    '/api/vendors/fetchRecord',
    '/api/cities/fetchRecord',
    '/api/states/fetchRecord',
    '/api/countries/fetchRecord',
    '/api/employers/fetchRecord',
    '/api/certifications/fetchRecord',
    '/api/domains/fetchRecord',
    '/api/durations/fetchRecord',
    '/api/taxTerms/fetchRecord'
  ];

  loggedUser: object;

  // candidate detail modal declarations
  candidateModalEditFlag: boolean = true;
  candidateModalDisplayFlag: boolean = false;
  candidateModalDetails: object = {};

  constructor(private router: Router,
    private apiService: ApiService,
    public utilityService: UtilityService,
    private dataService: DataService,
    private toastrService: ToastrService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (userData) {
      this.dataService.setLoggedUserDetails(userData);
      this.loggedUser = userData;
    } else if (!userData && !localUserData) {
      this.router.navigateByUrl('');
    } else {
      this.loggedUser = localUserData;
    }
    const flag = this.utilityService.getPendingFlag();
    if (flag) {
      this.router.navigateByUrl('pending-candidate-dashboard');
    }
    this.getDropdownItems();
    this.getCandidateList();
    this.manipulateDocumentWidth();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 203;
  }

  // filter input handler
  filterInputHandler(filterInput: object): void {
    this.filterInput = filterInput;
    let keyList = Object.keys(filterInput);
    this.utilityService.updateSpinnerFlag(true);
    if (keyList && keyList.length !== 0) {
      this.getFilteredCandidateList();
    } else {
      this.getCandidateList();
    }
  }

  // navigate to create candidate page
  navigateCreateCandidate(): void {
    this.router.navigateByUrl('candidate-create');
  }

  // get filtered candidate data
  getFilteredCandidateList(): void {
    // enable loader
    this.utilityService.updateSpinnerFlag(true);
    this.filterInput['userId'] = this.loggedUser['id'];
    const filterUrl = '/api/candidates/filter';
    this.apiService.postData(filterUrl, this.filterInput).subscribe(data => {
      this.candidateRecord = data['candidates'];
      this.utilityService.updateSpinnerFlag(false);
    });
  }

  // get candidate list
  getCandidateList(): void {
    // enable loader
    this.utilityService.updateSpinnerFlag(true);
    const url = '/api/candidates/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      this.candidateRecord = data['candidates'];
      this.utilityService.updateSpinnerFlag(false);
    });
  }

  // update the filter data into configuration
  updateFilterData(filterData: Array<object>): void {
    // skip inner loop by assuming the data is coming in same order given
    for (let i = 0; i < this.filterDataArray.length; i++) {
      for (let j = 0; j < filterData.length; j++) {
        const key = Object.keys(filterData[j])[0];
        if (this.filterDataArray[i]['andOrFlag'] && this.filterDataArray[i]['key'] === key) {
          for (let k = 0; k < this.filterDataArray[i]['dataList'].length; k++) {
            this.filterDataArray[i]['dataList'][k]['dropdownData'] = filterData[j][key];
          }
        } else {
          if (this.filterDataArray[i]['key'] === key)
            this.filterDataArray[i]['dropdownData'] = filterData[j][key];
        }
      }
    }
    this.filterDataSet = this.filterDataArray;
    this.utilityService.updateSpinnerFlag(false);
  }

  // get filter dropdown values
  getDropdownItems(): void {
    const filterDataBackup = this.dataService.getFieldDataList();
    if (filterDataBackup && filterDataBackup.length !== 0) {
      this.updateFilterData(filterDataBackup);
    } else {
      this.apiService.forkGetData(this.filterUrlList).subscribe(data => {
        this.dataService.setFieldDataList(data);
        this.updateFilterData(data);
      });
    }
  }

  // search input handler
  searchCandidate(): void {
    let searchList = [];
    let refinedSearchList = [];
    if (this.searchInput && this.searchInput !== '') {
      if ((this.searchInput).indexOf(',') !== -1) {
        searchList = (this.searchInput).split(",");
      } else {
        searchList.push(this.searchInput);
      }
      for (let i = 0; i < searchList.length; i++) {
        if ((searchList[i]).trim() !== '')
          refinedSearchList.push((searchList[i]).trim());
      }
    }

    // validate the filter search is equal or not
    let newSearchFlag = true;
    if (this.filterInput && this.filterInput['search'] && this.filterInput['search'].length !== 0 && refinedSearchList && refinedSearchList.length !== 0 && this.filterInput['search'].length === refinedSearchList.length) {
      let searchParamCount = 0;
      for (let i = 0; i < refinedSearchList.length; i++) {
        if (refinedSearchList[i] === this.filterInput['search'][i]) {
          searchParamCount++;
        }
      }
      if (searchParamCount === refinedSearchList.length) {
        newSearchFlag = false;
      }
    }

    if (newSearchFlag) {
      if ((this.filterInput['search'] && this.filterInput['search'].length === 0 && refinedSearchList && refinedSearchList.length === 0) || (refinedSearchList && refinedSearchList.length === 0 && !this.filterInput['search'])) {
        this.toastrService.error('Please type something to search...', 'Error', { timeOut: 1000 });
      } else {
        this.filterInput['search'] = refinedSearchList;
        this.utilityService.updateSpinnerFlag(true);
        const keyList = Object.keys(this.filterInput);
        if ((this.filterInput['search'] && this.filterInput['search'].length === 0) && keyList.length === 1) {
          this.getCandidateList();
        } else {
          this.getFilteredCandidateList();
        }
      }
    }
  }

  // clear search filter
  clearSearchCandidate(): void {
    this.searchInput = '';
    this.filterInput['search'] = [];
    this.getFilteredCandidateList();
  }

  // navigation to view page handler
  handleNavigationEvent(event): void {
    // back up of data
    this.dataService.setSelectedCandidateDetail(event['data']);
    const selectedData = event['data'];
    this.selectedId = event['id'];
    const updatedDate = new Date(selectedData['updatedOn']);
    updatedDate.setHours(0);
    updatedDate.setMinutes(0);
    updatedDate.setMilliseconds(0);
    const updatedRecordDateOffset = updatedDate.getTime();
    const currentDate = new Date();
    currentDate.setHours(0);
    currentDate.setMinutes(0);
    currentDate.setMilliseconds(0);
    const currentTime = currentDate.getTime();
    const differenceTime = currentTime - updatedRecordDateOffset;
    const differenceHour = differenceTime / 3600000;
    const differenceDays = differenceHour / 24;
    let currentMonth = new Date().getMonth();
    let currentYear = new Date().getFullYear();
    let daysCount = 0;
    for (let i = 0; i < 6; i++) {
      if (currentMonth >= 0) {
        daysCount += new Date(currentYear, currentMonth, 0).getDate();
        currentMonth--;
      } else {
        currentYear = currentYear - 1;
        currentMonth = 11;
        daysCount += new Date(currentYear, currentMonth, 0).getDate();
      }
    }
    if (differenceDays > daysCount) {           // need to change
      // get the candidate availability detail
      this.getAvailabilityData(this.selectedId);
    } else {
      this.handleProceedModal();
    }
  }

  // get availability data of candidate
  getAvailabilityData(selectedId: number): void {
    const url = '/api/candidates/fetchAvailableCandidates/' + selectedId;
    this.apiService.getData(url).subscribe(data => {
      const candidateData = data['candidates'][0];
      for (let i = 0; i < this.modalConfigSetUp['fieldList'].length; i++) {
        if (candidateData[this.modalConfigSetUp['fieldList'][i]['key']] === true) {
          this.modalConfigSetUp['fieldList'][i]['value'] = 'Yes';
        } else if (candidateData[this.modalConfigSetUp['fieldList'][i]['key']] === false) {
          this.modalConfigSetUp['fieldList'][i]['value'] = 'No';
        } else {
          this.modalConfigSetUp['fieldList'][i]['value'] = candidateData[this.modalConfigSetUp['fieldList'][i]['key']];
        }
      }
      this.modalConfig = this.modalConfigSetUp;
      this.openModal();
    });
  }

  // handle close modal
  handleCloseModal(flag): void {
    this.showPopUpFlag = flag;
  }

  // handle save modal
  handleProceedModal(): void {
    const url = 'candidate-view/' + this.selectedId;
    this.router.navigateByUrl(url);
  }

  // open modal
  openModal(): void {
    this.showPopUpFlag = true;
  }

  // navigate to dashboard
  navigateToDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // communication & interview modal methods

  // cancel the candidate detail modal
  candidateDetailCancel(): void {

  }

  // proceed the candidate detail modal
  candidateDetailProceed(): void {

  }

  // save the candidate detail
  candidateDetailSave(candidateDetails: object): void {

  }

  // search enter key press event
  searchInputTyped(event: any): void {
    if (event && event.keyCode === 13) {
      event.preventDefault();
      this.searchCandidate();
    }
  }

}
