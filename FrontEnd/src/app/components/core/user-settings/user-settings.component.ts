import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { UtilityService } from '../../../services/utility.service';
import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  userSettingTileListConfig: Array < object > = [
    {
      'id': 0,
      'title': 'Security',
      'description': 'Users uploads, Users Pending, Users Statistics, etc',
      'securityIcon': true,
      'activeFlag': 'isSecurity',
      'link': 'security'
    },
    {
      'id': 1,
      'title': 'Add / Update',
      'description': 'Add, Update, Delete, etc',
      'editIcon': true,
      'activeFlag': 'isCrud',
      'link': 'update-details'
    },
    {
      'id': 2,
      'title': 'Candidates Report',
      'description': 'Uploaded Candidates, Pending Candidates, Candidates Statistics, etc',
      'candidateReportIcon': true,
      'activeFlag': 'isCandidateReport',
      'link': 'candidate-report'
    },
    {
      'id': 3,
      'title': 'User Report',
      'description': 'Users uploads, Users Pending, Users Statistics, etc',
      'userReportIcon': true,
      'activeFlag': 'isUserReport',
      'link': 'user-report'
    },
    {
      'id': 4,
      'title': 'User Access',
      'description': 'enable users / revoke users access',
      'userAccessIcon': true,
      'activeFlag': 'isUserAccess',
      'link': 'user-access'
    }
  ];
  loggedUserData: any;
  userSettingTileList: Array < object > = [];
  userData: object = {};
  userList: Array < object > = [];
  documentHeight: number;
  // modal fields
  showPopUpFlag: boolean = false;
  modalConfig: object;
  modalAction: string;
  modalConfigSet: object = {
    'title': 'Grand / Revoke Access for users',
    'cancelFlag': true,
    'userFlag': true,
    'accessKey': '',
    'fieldList': []
  };

  constructor(private router: Router,
              private apiService: ApiService,
              public utilityService: UtilityService,
              private dataService: DataService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.getUserData();
    this.getUsersData();
    this.manipulateDocumentWidth();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 212;
  }

  // get user data for permission
  getUserData(): void {
    const url = '/api/users/fetchRecord/' + this.loggedUserData['id'];
    this.utilityService.updateSpinnerFlag(true); 
    this.apiService.getData(url).subscribe( data => {
      if (data) {
        this.userData = data['user'][0];
        const temp = [];
        for (let i = 0; i < this.userSettingTileListConfig.length; i++) {
          if (this.userData[this.userSettingTileListConfig[i]['activeFlag']]) {
            temp.push(this.userSettingTileListConfig[i]);
          }
        }
        this.userSettingTileList = temp;
        this.utilityService.updateSpinnerFlag(false);
      }
    });
  }

  // get all users
  getUsersData(): void {
    const url = '/api/users/fetchRecord';
    this.apiService.getData(url).subscribe( data => {
      if (data) {
        const temp = [];
        for (let i = 0; i < data['users'].length; i++) {
          if (data['users'][i]['id'] !== this.loggedUserData['id']) {
            temp.push(data['users'][i]);
          }
        }
        this.userList = temp;
      }
    })
  }

  // open user pop up for access
  openUserPopup(event: any, title: string): void {
    event.stopPropagation();
    let selectedObject;
    // validate the selected object
    for (let i = 0; i < this.userSettingTileListConfig.length; i++) {
      if (this.userSettingTileListConfig[i]['title'] === title) {
        selectedObject = this.userSettingTileListConfig[i];
      }
    }
    this.modalConfigSet['fieldList'] = this.userList;
    this.modalConfigSet['accessKey'] = selectedObject['activeFlag'];
    this.modalConfig = this.modalConfigSet;
    this.showPopUpFlag = true;
  }

  // handle close modal
  handleCloseModal(event): void {
    this.showPopUpFlag = event;
  }

  navigatePage(pageLink: string): void {
    this.router.navigateByUrl(pageLink);
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('dashboard');
  }

}
