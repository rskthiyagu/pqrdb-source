import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { UtilityService } from '../../../services/utility.service';
import { DataService } from '../../../services/data.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  documentHeight: number;
  loggedUserData: object;
  selectedKey: string;
  selectedRecordTitle: string;
  selectedRecordData: Array < object > = [];
  userDetail: Object;
  selectedIconFlag: boolean = false;

  detailConfig: Array < object > = [
    {
      'id': 0,
      'title': 'Protected details',
      'key': 'users',
      'bgColor': '#A1D066',
      'iconFlag': true,
      'url': ''
    },
    // {
    //   'id': 1,
    //   'title': 'Login History',
    //   'key': 'userSearchLog',
    //   'bgColor': '#FF6C02',
    //   'url': '/api/users/fetchSearchLogRecord'
    // },
    // {
    //   'id': 2,
    //   'title': 'Remember Devices',
    //   'key': 'users',
    //   'bgColor': '#FF6C02',
    //   'url': '/api/users/fetchSearchLogRecord'
    // },
    {
      'id': 1,
      'title': 'Search Log',
      'key': 'userSearchLog',
      'idFlag': true,
      'bgColor': '#FF6C02',
      'url': '/api/users/fetchSearchLogRecord'
    }
  ];

  userFieldList: Array < object > = [
    {
      'id': 0,
      'title': 'User Name',
      'activeFlag': 'IsUserNameVisible'
    },
    {
      'id': 1,
      'title': 'Mobile Number',
      'activeFlag': 'IsMobileNumberVisible'
    },
    {
      'id': 2,
      'title': 'Email Address',
      'activeFlag': 'IsEmailVisible'
    },
    {
      'id': 3,
      'title': 'Facebook Link',
      'activeFlag': 'IsFbLinkVisible'
    },
    {
      'id': 4,
      'title': 'Twitter Link',
      'activeFlag': 'IsTwitterLinkVisible'
    },
    {
      'id': 5,
      'title': 'Linkedin Link',
      'activeFlag': 'IsLinkedinLinkVisible'
    },
    {
      'id': 6,
      'title': 'Profile Image',
      'activeFlag': 'IsProfileImageVisible'
    },
    {
      'id': 7,
      'title': 'Cover Image',
      'activeFlag': 'IsCoverImageVisible'
    }
  ];

  constructor(private router: Router,
              public utilityService: UtilityService,
              private dataService: DataService,
              private apiService: ApiService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.manipulateDocumentWidth();
    this.getUserData();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 300;
  }

  // get the user data
  getUserData(): void {
    const url = '/api/users/fetchRecord/' + this.loggedUserData['id'];
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.getData(url).subscribe( data => {
      if (data) {
        this.userDetail = data.user[0];
        this.getSelectedOptionData(0);
        this.utilityService.updateSpinnerFlag(false);
      }
    });
  }

  // get the data of selected option
  getSelectedOptionData(id: number): void {
    // update the selected item
    for (let i = 0; i < this.detailConfig.length; i++) {
      if ( i === id) {
        this.detailConfig[i]['bgColor'] = '#A1D066';
      } else {
        this.detailConfig[i]['bgColor'] = '#FF6C02';
      }
    }
    this.selectedKey = this.detailConfig[id]['key'];
    this.selectedIconFlag = this.detailConfig[id]['iconFlag'];
    let url = this.detailConfig[id]['url'];
    this.selectedRecordTitle = this.detailConfig[id]['title'];
    if (url !== '') {
      if (this.detailConfig[id]['idFlag']) {
        url = url + '/' + this.loggedUserData['id'];
      }
      this.utilityService.updateSpinnerFlag(true);
      this.apiService.getData(url).subscribe(data => {
        this.utilityService.updateSpinnerFlag(false);
        if (data && data[this.detailConfig[id]['key']].length !== 0) {
          this.selectedRecordData = data[this.detailConfig[id]['key']];
        } else {
          this.selectedRecordData = [];
        }
      });
    } else {
      this.selectedRecordData = this.userFieldList;
    }
  }

  // show / hide user details
  toggleDetail(id: number, key: string, value: boolean): void {
    const url = '/api/users/updateUserAccess';
    const param = {};
    param['id'] = this.loggedUserData['id'];
    param['key'] = key;
    param['value'] = value;
    this.apiService.postData(url, param).subscribe( data => {
      this.getUserData();
    });
  }

  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('settings');
  }

}
