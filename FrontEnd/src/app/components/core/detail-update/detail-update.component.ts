import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-detail-update',
  templateUrl: './detail-update.component.html',
  styleUrls: ['./detail-update.component.scss']
})
export class DetailUpdateComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  loggedUserData: any;
  documentHeight: number;
  dataValue: string;
  selectedKey: string;
  selectedId: number;
  detailConfig: Array < object > = [
    {
      'id': 0,
      'title': 'Cities',
      'key': 'cities',
      'bgColor': '#A1D066'
    },
    {
      'id': 1,
      'title': 'States',
      'key': 'states',
      'bgColor': '#FF6C02'
    },
    {
      'id': 2,
      'title': 'Countries',
      'key': 'countries',
      'bgColor': '#FF6C02'
    },
    {
      'id': 3,
      'title': 'Skills',
      'key': 'skills',
      'bgColor': '#FF6C02'
    },
    {
      'id': 4,
      'title': 'Domains',
      'key': 'domains',
      'bgColor': '#FF6C02'
    },
    {
      'id': 5,
      'title': 'Certifications',
      'key': 'certifications',
      'bgColor': '#FF6C02'
    },
    {
      'id': 6,
      'title': 'Authorizations',
      'key': 'authorizations',
      'bgColor': '#FF6C02'
    },
    {
      'id': 7,
      'title': 'Employers',
      'key': 'employers',
      'bgColor': '#FF6C02'
    },
    {
      'id': 8,
      'title': 'Qualifications',
      'key': 'qualifications',
      'bgColor': '#FF6C02'
    },
    {
      'id': 9,
      'title': 'Job Boards',
      'key': 'vendors',
      'bgColor': '#FF6C02'
    }
  ];

  selectedRecordData: Array < object > = [];
  selectedRecordDataBackup: Array < object > = [];
  selectedRecordTitle: string = '';
  deleteSelectedItem: object;
  modalOperationFlag: string;
  searchText: string = '';

  // modal params
  showPopUpFlag: boolean = false;
  modalConfig: object;
  modalAction: string;
  modalConfigSet: object = {
    'title': '',
    'key': '',
    'saveFlag': true,
    'cancelFlag': true,
    'dataUpdateFlag': true,
    'fieldList': []
  };

  deleteModalConfigSet: object = {
    'title': 'Data Replacement',
    'key': '',
    'yesFlag': true,
    'noFlag': true,
    'dataDeleteFlag': true,
    'fieldList': [
      {
        'id': 0,
        'selectFlag': true,
        'selectConfig': {
          'title': '', 'dropdownData': [], 'dropdownSelected': [], 'key': '', 'data': '', 'dropdownSetting': {
            singleSelection: true,
            text: "Select",
            enableSearchFilter: false,
            classes: "myclass custom-class"
          }
        }
      }
    ]
  };

  alertModalSetUp: object = {
    'title': 'Alert',
    'okFlag': true,
    'cancelFlag': true,
    'contentFlag': true,
    'content': 'Do you want to delete the item?'
  };

  inputConfig: object = {
    'id': 0,
    'inputFlag': true,
    'inputConfig': {
      'title': '',
      'placeholder': '',
      'key': '',      
      'data': ''
    }
  };

  selectConfig: object = {
    'id': 1,
    'selectFlag': true,
    'selectConfig': {
      'title': '', 'dropdownData': [], 'dropdownSelected': [], 'key': '', 'data': '', 'dropdownSetting': {
        singleSelection: true,
        text: "Select",
        enableSearchFilter: false,
        classes: "myclass custom-class"
      }
    }
  };

  stateDataList: Array < object >;
  countryDataList: Array < object >;
  deleteFlag: boolean = false;
  

  constructor(private apiService: ApiService,
              public utilityService: UtilityService,
              private dataService: DataService,
              private router: Router,
              private toastrService: ToastrService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.manipulateDocumentWidth();
    this.getSelectedOptionData(0);
    this.getCountryData();
    this.getStateData();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 300;
  }

  // get country data
  getCountryData(): void {
    const url = '/api/countries/fetchRecord';
    this.apiService.getData(url).subscribe( data => {
      if (data && data['countries']) {
        this.countryDataList = data['countries'];
      }
    });
  }

  // get country data
  getStateData(): void {
    const url = '/api/states/fetchRecord';
    this.apiService.getData(url).subscribe( data => {
      if (data && data['states']) {
        this.stateDataList = data['states'];
      }
    });
  }

  // get the data of selected option
  getSelectedOptionData(id: number): void {
    this.selectedId = id;
    // update the selected item
    for (let i = 0; i < this.detailConfig.length; i++) {
      if ( i === id) {
        this.detailConfig[i]['bgColor'] = '#A1D066';
      } else {
        this.detailConfig[i]['bgColor'] = '#FF6C02';
      }
    }
    this.selectedKey = this.detailConfig[id]['key'];
    const url = '/api/' + this.detailConfig[id]['key'] + '/fetchRecord';
    this.utilityService.updateSpinnerFlag(true);
    this.selectedRecordTitle = this.detailConfig[id]['title'];
    this.apiService.getData(url).subscribe(data => {
      this.utilityService.updateSpinnerFlag(false);
      if (data && data[this.detailConfig[id]['key']].length !== 0) {
        this.selectedRecordData = data[this.detailConfig[id]['key']];
        this.selectedRecordDataBackup = data[this.detailConfig[id]['key']];
      } else {
        this.selectedRecordData = [];
        this.selectedRecordDataBackup = [];
      }
    });
  }

  // filter method
  filterUser(element, index, array) { 
    return (((array[index]['name']).toLowerCase()).indexOf((element['searchText']).toLowerCase()) !== -1);
 }

  // search user on key up event in input
  searchUser(event: any): void {
    this.searchText = '';
    const searchText = event.target.value;
    if (searchText !== '') {
      if (this.selectedRecordData && this.selectedRecordData.length !== 0) {
        for (let i = 0; i < this.selectedRecordDataBackup.length; i++) {
          this.selectedRecordDataBackup[i]['searchText'] = searchText;
        }
        const filteredResult = this.selectedRecordDataBackup.filter(this.filterUser);
        this.selectedRecordData = filteredResult;
      }
    } else {
      for (let i = 0; i < this.selectedRecordDataBackup.length; i++) {
        this.selectedRecordDataBackup[i]['searchText'] = '';
      }
      this.selectedRecordData = this.selectedRecordDataBackup;
    }
  }

  // delete confirmation
  deleteItemConfirmation(data: object): void {
    this.deleteSelectedItem = data;
    this.deleteFlag = true;
    this.openDeleteModal();
  }

  // delete item
  deleteItem(): void {
    const url = '/api/' + this.selectedKey + '/delete/' + this.deleteSelectedItem['id'];
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.deleteData(url).subscribe(data => {
      this.utilityService.updateSpinnerFlag(false);
      this.getSelectedOptionData(this.selectedId);
      this.handleCloseModal(false);
    });
  }

  // edit item
  editItem(data: object): void {
    for (let i = 0; i < this.selectedRecordData.length; i++) {
      if (this.selectedRecordData[i]['id'] === data['id']) {
        this.selectedRecordData[i]['editFlag'] = true;
        this.dataValue = data['name'];
      } else {
        this.selectedRecordData[i]['editFlag'] = false;
      }
    }
  }

  // save item
  saveItem(data: object): void {
    const url = '/api/' + this.selectedKey + '/update/' + data['id'];
    const param = {id: data['id'], name: this.dataValue};
    this.apiService.putData(url, param).subscribe(dataObject => {
      for (let i = 0; i < this.selectedRecordData.length; i++) {
        if (this.selectedRecordData[i]['id'] === data['id']) {
          this.selectedRecordData[i]['name'] = this.dataValue;
          this.selectedRecordData[i]['editFlag'] = false;
        }
      }
    });
  }

  // open delete modal
  openDeleteModal(): void {
    this.modalOperationFlag = 'delete';
    this.modalConfig = this.alertModalSetUp;
    this.showPopUpFlag = true;
  }

  // add new data
  openAddModal(selectedData: string): void {
    // selected data
    let selectedObject;
    for (let i = 0; i < this.detailConfig.length; i++) {
      if (this.detailConfig[i]['title'] === selectedData) {
        selectedObject = this.detailConfig[i];
      }
    }
    const modalConfigBackup = this.modalConfigSet;
    if (selectedObject['key'] === 'cities') {
      const config = [];
      this.inputConfig['inputConfig']['data'] = '';
      this.inputConfig['inputConfig']['title'] = selectedObject['title'];
      this.inputConfig['inputConfig']['placeholder'] = 'Please type the new ' + selectedObject['key'];
      this.inputConfig['inputConfig']['key'] = selectedObject['key'];
      config.push(this.inputConfig);
      const stateConfig = JSON.parse(JSON.stringify(this.selectConfig));
      stateConfig['selectConfig']['title'] = 'State';
      stateConfig['selectConfig']['key'] = 'states';
      stateConfig['selectConfig']['dropdownData'] = this.stateDataList;
      stateConfig['selectConfig']['dropdownSelected'] = [];
      config.push(stateConfig);
      const countryConfig = JSON.parse(JSON.stringify(this.selectConfig));
      countryConfig['selectConfig']['title'] = 'Country';
      countryConfig['selectConfig']['key'] = 'countries';
      countryConfig['selectConfig']['dropdownData'] = this.countryDataList;
      countryConfig['selectConfig']['dropdownSelected'] = [];
      config.push(countryConfig);
      modalConfigBackup['fieldList'] = config;
    } else if (selectedObject['key'] === 'states') {
      const config = [];
      this.inputConfig['inputConfig']['data'] = '';
      this.inputConfig['inputConfig']['title'] = selectedObject['title'];
      this.inputConfig['inputConfig']['placeholder'] = 'Please type the new ' + selectedObject['key'];
      this.inputConfig['inputConfig']['key'] = selectedObject['key'];
      config.push(this.inputConfig);
      const countryConfig = {};
      Object.assign(countryConfig, this.selectConfig);
      countryConfig['selectConfig']['title'] = 'Country';
      countryConfig['selectConfig']['key'] = 'countries';
      countryConfig['selectConfig']['dropdownData'] = this.countryDataList;
      countryConfig['selectConfig']['dropdownSelected'] = [];
      config.push(countryConfig);
      modalConfigBackup['fieldList'] = config;
    } else {
      const config = [];
      this.inputConfig['inputConfig']['data'] = '';
      this.inputConfig['inputConfig']['title'] = selectedObject['title'];
      this.inputConfig['inputConfig']['placeholder'] = 'Please type the new ' + selectedObject['key'];
      this.inputConfig['inputConfig']['key'] = selectedObject['key'];
      config.push(this.inputConfig);
      modalConfigBackup['fieldList'] = config;
    }
    modalConfigBackup['title'] = 'Add new ' + selectedObject['title'];
    modalConfigBackup['key'] = selectedObject['key'];
    this.modalConfig = modalConfigBackup;
    this.showPopUpFlag = true;
  }

  // save widget flow
  saveWidget(data: any): void {
    const url = '/api/' + this.modalConfig['key'] + '/create';
    const param = {};
	param['name'] = data[this.modalConfig['key']];
    this.apiService.postData(url, param).subscribe(data => {
      this.handleCloseModal(false);
      this.getSelectedOptionData(this.selectedId);
      this.toastrService.success('Operation successfully done...', 'Success', { timeOut: 1500});
    })
  }

  // handle close event
  handleCloseModal(event: boolean): void {
    this.showPopUpFlag = event;
  }

  // handle proceed emitter
  handleProceedEvent(event: any): void {
    this.handleCloseModal(false);
    if (this.deleteFlag) {
      const dataArray = [];
      for (let i = 0; i < this.selectedRecordDataBackup.length; i++) {
        if (this.deleteSelectedItem['id'] !== this.selectedRecordDataBackup[i]['id']) {
          dataArray.push(this.selectedRecordDataBackup[i]);
        }
      }
      this.deleteModalConfigSet['fieldList'][0]['selectConfig']['title'] = this.selectedKey;
      this.deleteModalConfigSet['fieldList'][0]['selectConfig']['key'] = this.selectedKey;
      this.deleteModalConfigSet['fieldList'][0]['selectConfig']['dropdownData'] = dataArray;
      this.deleteModalConfigSet['fieldList'][0]['selectConfig']['dropdwonSelected'] = [];
      this.modalConfig = this.deleteModalConfigSet;
      const scope = this;
      setTimeout(function() {
        scope.showPopUpFlag = true;
      }, 500);
      this.deleteFlag = false;
    }
  }

  // replace data for deleted data
  replaceData(data: object): void {
    // delete the record
    this.deleteItem();
    const url = '/api/candidates/updateDeleteRecord';
    const param = {};
    param['key'] = this.selectedKey;
    param['id'] = this.deleteSelectedItem['id'];
    param['value'] = data[this.selectedKey][0]['id'];
    this.apiService.postData(url, param).subscribe( data => {
      if (data) {
        this.handleCloseModal(false);
      }
    });
  }

  // yes handler
  yesModalHandler(data): void {
    this.replaceData(data);
  }

  // handle no wrapper handler
  noModalHandler(): void {
    this.deleteItem();
  }

  // cancel item
  cancelItem(data: object): void {
    for (let i = 0; i < this.selectedRecordData.length; i++) {
      if (this.selectedRecordData[i]['id'] === data['id']) {
        this.selectedRecordData[i]['editFlag'] = false;
      }
    }
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('settings');
  }

}
