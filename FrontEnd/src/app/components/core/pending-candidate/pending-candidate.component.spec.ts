import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingCandidateComponent } from './pending-candidate.component';

describe('PendingCandidateComponent', () => {
  let component: PendingCandidateComponent;
  let fixture: ComponentFixture<PendingCandidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingCandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
