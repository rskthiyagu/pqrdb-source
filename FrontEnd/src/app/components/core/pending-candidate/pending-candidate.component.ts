import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from 'src/app/services/data.service';
import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-pending-candidate',
  templateUrl: './pending-candidate.component.html',
  styleUrls: ['./pending-candidate.component.scss']
})
export class PendingCandidateComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  documentHeight: number;
  loggedUserData: object;
  candidateList: Array<object>;
  pendingFlag: boolean;
  userList: Array<object> = [];
  userSelectedList: Array<object> = [];
  documentUserHeight: number;
  userDropdownSetting: object = {
    singleSelection: false,
    text: "Select Users",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "myclass custom-class"
  };
  selectAllFlag: boolean = false;

  constructor(private dataService: DataService,
    private router: Router,
    private apiService: ApiService,
    public utilityService: UtilityService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.manipulateDocumentWidth();
    this.pendingFlag = this.utilityService.getPendingFlag();
    this.userSelectedList.push(this.loggedUserData['id']);
    this.getCandidateData();
    this.getUserData();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 202;
    this.documentUserHeight = this.documentHeight - 77;
  }

  // get pending candidates record for particular user
  getCandidateData(): void {
    const url = '/api/candidates/fetchPendingRecord';
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.postData(url, this.userSelectedList).subscribe(data => {
      this.utilityService.updateSpinnerFlag(false);
      this.candidateList = data['pendingCandidates'];
    });
  }

  // get user data
  getUserData(): void {
    const url = '/api/users/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data && data['users']) {
        const userData = data['users'];
        for (let i = 0; i < userData.length; i++) {
          if (userData[i]['id'] === this.loggedUserData['id']) {
            userData[i]['bgColor'] = '#A1D066';
          } else {
            userData[i]['bgColor'] = '#FF6C02';
          }
        }
        this.userList = userData;
      }
    });
  }

  // navigate to candidate dashboard page
  navigateDashboard(): void {
    this.router.navigateByUrl('/dashboard');
  }

  // handler for user selection
  handleSelectedData(event): void {
    for (let i = 0; i < this.userList.length; i++) {
      if (this.userList[i]['id'] === event['id']) {
        if (this.userList[i]['bgColor'] === '#FF6C02') {
          this.userList[i]['bgColor'] = '#A1D066';
          this.userSelectedList.push(event['id']);
          if (this.userList.length === this.userSelectedList.length) {
            this.selectAllFlag = true;
          }
        } else {
          this.userList[i]['bgColor'] = '#FF6C02';
          const updatedArray = [];
          for (let i = 0; i < this.userSelectedList.length; i++) {
            if (this.userSelectedList[i] !== event['id']) {
              updatedArray.push(this.userSelectedList[i]);
            }
          }
          this.userSelectedList = updatedArray;
        }
      }
    }
    // need to trigger the get data call
    this.getCandidateData();
  }

  // select / unselect all users
  selectUser(): void {
    for (let i = 0; i < this.userList.length; i++) {
      this.userList[i]['bgColor'] = '#A1D066';
      this.userSelectedList.push(this.userList[i]['id']);
    }
    this.selectAllFlag = true;
    this.getCandidateData();
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('dashboard');
  }

}
