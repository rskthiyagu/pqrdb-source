import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';
import { HashingService } from '../../../services/hashing.service';

@Component({
  selector: 'app-add-new-user',
  templateUrl: './add-new-user.component.html',
  styleUrls: ['./add-new-user.component.scss']
})
export class AddNewUserComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  userName: string;
  password: string;
  requiredFlag: boolean = false;
  errorFlag: boolean = false;
  userRegister: object;
  userCoverToUpload: File;
  userImageToUpload: File;
  rememberFlag: boolean = true;
  documentHeight: number;
  otpRequestId: string = '';
  otpFlag: boolean = false;
  otpInput: number;
  loggedUserBackup: object;
  accessFlag: boolean = false;
  userNameErrorFlag: boolean = false;
  userMobileEmailErrorFlag: boolean = false;

  constructor(private apiService: ApiService,
              private router: Router,
              private dataService: DataService,
              private toastrService: ToastrService,
              private utilityService: UtilityService,
              private hashingService: HashingService) { }

  ngOnInit() {
    this.resetRegisterFields();
    this.manipulateDocumentWidth();
  }

  // manipulate height
  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 141;
  }

  // reset the register object
  resetRegisterFields(): void {
    const obj = {
      'firstName': '',
      'lastName': '',
      'userName': '',
      'password': '',
      'role': '',
      'userImage': '',
      'designation': '',
      'mobileNumber': '',
      'email': '',
      'fbLink': '',
      'twitterLink': '',
      'linkedinLink': '',
      'coverImage': ''
    };
    this.userRegister = obj;
  }

  // event handler for document selected
  documentSelected(e): void {
    this.userImageToUpload = e.item(0);
  }

  // event handler for document selected
  documentCoverSelected(e): void {
    this.userCoverToUpload = e.item(0);
  }

  // validate required fields for registration
  validateRegisterFields(): void {
    if (this.userRegister['userName'] === '' || this.userRegister['password'] === '' ||
        this.userRegister['firstName'] === '' || this.userRegister['lastName'] === '' ||
        this.userRegister['mobileNumber'] === '' || this.userRegister['email'] === '') {
      this.requiredFlag = true;
    } else {
      this.requiredFlag = false;
    }
  }

  // save image source for user
  saveImageSource(): void {
      const countUrl = '/api/users/getCount';
      this.apiService.getData(countUrl).subscribe(data => {
        const recordCount = parseInt(data['count']) + 1;
        if (this.userImageToUpload) {
          const userImageNameSplitArray = (this.userImageToUpload.name).split('.');
          const userImageFileType = userImageNameSplitArray[userImageNameSplitArray.length - 1];
          const userImageFileName = 'user_image_' + recordCount + '.' + userImageFileType;
          this.userRegister['fileName'] = userImageFileName;
          const formData: FormData = new FormData();
          formData.append('files', this.userImageToUpload, userImageFileName);
          this.uploadUserImage(formData);
        }
        // cover image
        if (this.userCoverToUpload) {
          const fileNameSplitArray = (this.userCoverToUpload.name).split('.');
          const fileType = fileNameSplitArray[fileNameSplitArray.length - 1];
          const fileName = 'user_cover_image_' + recordCount + '.' + fileType;
          this.userRegister['coverImage'] = fileName;
          const formData: FormData = new FormData();
          formData.append('files', this.userCoverToUpload, fileName);
          this.uploadCoverImage(formData);
        }
        // trigger create user call
        this.saveUserDetails();
      });
  }

  // upload userimage to server
  uploadUserImage(formData: FormData): void {
    this.apiService.postData('/api/users/uploadUserProfileImage', formData).subscribe(data => {
    });
  }

  // upload cover image to server
  uploadCoverImage(formData: FormData): void {
    this.apiService.postData('/api/users/uploadUserCoverImage', formData).subscribe(data => {
    });
  }

  // register user
  registerUser(): void {
    this.validateRegisterFields();
    if (!this.requiredFlag) {
      this.utilityService.updateSpinnerFlag(true);
      // validate the username availability
      this.validateUserNameAvailability();
    }
  }
  
  // register user fields
  registerationProcess(): void {
    this.userRegister['password'] = this.hashingService.getEncryptValue(this.userRegister['password']);
      if (this.userImageToUpload || this.userCoverToUpload) {
        this.saveImageSource();
      } else {
        this.saveUserDetails();
      }
  }

  // validate availability for username
  validateUserNameAvailability(): void {
    const url = '/api/users/validateUsernameAvailability';
    const param = {'userName': this.userRegister['userName']};
    this.apiService.getData(url, param).subscribe( data => {
      if (data && !data['status']) {
        // validate the user details availability ( mobile & email )
        this.userNameErrorFlag = false;
        this.validateUserDetailAvailability();
      } else {
        this.utilityService.updateSpinnerFlag(false);
        this.userNameErrorFlag = true;
        this.toastrService.error("Username is exists already...", "UserName Exists", { timeOut: 1000});
      }
    });
  }

  // validate availability for mobile & email
  validateUserDetailAvailability(): void {
    const url = '/api/users/validateUserDetailAvailability';
    const param = {'mobileNumber': this.userRegister['mobileNumber'], 'email': this.userRegister['email']};
    this.apiService.getData(url, param).subscribe( data => {
      if (data && !data['status']) {
        this.userMobileEmailErrorFlag = false;
        this.registerationProcess();
      } else {
        this.userMobileEmailErrorFlag = true;
        this.utilityService.updateSpinnerFlag(false);
        this.toastrService.error("Mobile Number & Email exists already...", "User Details Exists", { timeOut: 1000});
      }
    });
  }

  // save users details
  saveUserDetails(): void {
    this.userRegister['isActive'] = 1;
    this.userRegister['isSubmitted'] = 0;
    this.apiService.postData('/api/users/create', this.userRegister).subscribe(data => {
      if (data && data['users'] && data['users'].length !== 0) {
        this.errorFlag = false;
        this.requiredFlag = false;
        this.resetRegisterFields();
        this.toastrService.success('User Created successfully...', 'Success', {timeOut: 2000});
		this.router.navigateByUrl('user-access');
      } else {
        this.errorFlag = true;
        this.toastrService.error("Registration Failed...", "Error", { timeOut: 1000});
      }
      this.utilityService.updateSpinnerFlag(false);
    });
  }

}
