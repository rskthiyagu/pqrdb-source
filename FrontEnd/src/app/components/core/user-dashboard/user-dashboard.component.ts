import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { Router } from '@angular/router';


import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  @HostListener("click", ['$event'])
  onClick(event) {
    // to hide the menu, if opened
    if (event && event.target && event.target.className !== 'fa-bars fas')
      this.viewMoreMenuFlag = false;
  }

  userDashboardConfig: Array<object> = [
    {
      'id': 0,
      'key': 'recentCandidates',
      'title': 'Recent Candidates',
      'url': '/api/candidates/fetchRecentRecord',
      'isActive': true,
      'tileFlag': true,
      'dbKey': 'IsRecentCandidateTileVisible'
    },
    {
      'id': 1,
      'key': 'pendingCandidates',
      'title': 'Pending Candidates',
      'url': '/api/candidates/fetchRecentPendingRecord',
      'isActive': true,
      'tileFlag': true,
      'dbKey': 'IsPendingCandidateTileVisible'
    },
    // {
    //   'id': 2,
    //   'key': 'recentSearches',
    //   'url': '/api/candidates/fetchRecentSearch'
    // },
    {
      'id': 3,
      'key': 'users',
      'title': 'Talent Managers',
      'url': '/api/users/fetchTopRecord',
      'isActive': true,
      'tileFlag': true,
      'dbKey': 'IsTalentManagerTileVisible'
    },
    // {
    //   'id': 4,
    //   'key': 'user',
    //   'title': '',
    //   'url': '/api/users/fetchRecord',
    //   'isActive': true
    // },
    {
      'id': 5,
      'key': 'candidateStatistics',
      'title': 'Progress Report',
      'url': '/api/candidates/getStatistics',
      'isActive': true,
      'tileFlag': true,
      'dbKey': 'IsProgressReportTileVisible'
    }
  ];

  // candidate table config
  candidateListConfig: Array<object> = [
    {
      'id': 0,
      'title': 'FirstName',
      'key': 'firstName'
    },
    {
      'id': 1,
      'title': 'LastName',
      'key': 'lastName'
    },
    {
      'id': 2,
      'title': 'Job Title',
      'key': 'jobTitle'
    },
    {
      'id': 3,
      'title': 'Email',
      'key': 'email'
    },
    {
      'id': 4,
      'title': 'Mobile Number',
      'key': 'mobileNumber'
    },
    {
      'id': 5,
      'title': 'Sourced User',
      'key': 'userName'
    },
    {
      'id': 6,
      'title': 'Created Date',
      'key': 'createdOn',
      'dateFlag': true
    }
  ];

  // pending candidate list config
  pendingCandidateListConfig: Array<object> = [
    {
      'id': 0,
      'title': 'FileName',
      'key': 'fileName',
      'linkFlag': true
    },
    {
      'id': 1,
      'title': 'Source User',
      'key': 'userName'
    },
    {
      'id': 2,
      'title': 'Created Date',
      'key': 'createdOn',
      'dateFlag': true
    }
  ];

  userConfig: Array<string> = [];
  userConfigUrlList: Array<string> = [];
  loggedUserDetails: object;

  recentCandidatesList: Array<object> = [];
  pendingCandidatesList: Array<object> = [];
  recentSearchesList: Array<object> = [];
  usersList: Array<object> = [];
  userList: Array<object> = [];
  candidateStatisticsList: object = {};
  documentHeight: number;
  intialCall: boolean = true;
  showNoResultFlag: boolean = false;
  viewMoreMenuFlag: boolean = false;
  pendingUserList: Array < object > = [];

  // modal params
  showPopUpFlag: boolean = false;
  modalConfig: object;
  modalAction: string;
  modalConfigSet: object = {
    'title': 'Add / Remove Widget',
    'applyFlag': true,
    'tileFlag': true,
    'fieldList': []
  };

  alertModalSetUp: object = {
    'title': 'Alert',
    'okFlag': true,
    'contentFlag': true,
    'content': 'There are more pending candidate records. Please fill those as priority'
  };

  constructor(private apiService: ApiService,
    private dataService: DataService,
    private router: Router,
    private utilityService: UtilityService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (userData) {
      this.dataService.setLoggedUserDetails(userData);
      this.loggedUserDetails = userData;
    } else if (!userData && !localUserData) {
      this.router.navigateByUrl('');
    } else {
      this.loggedUserDetails = localUserData;
    }
    this.getUserData();
    this.manipulateDocumentWidth();
  }

  // get user data
  getUserData(): void {
    const url = '/api/users/fetchRecord/' + this.loggedUserDetails['id'];
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.loggedUserDetails = data['user'][0];
        // set the active flag
        this.getActiveTiles();
        this.getUserConfiguration();
      }
    })
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 208;
  }

  // get user defined configuration
  getUserConfiguration(): void {
    let userConfig = [];
    let userConfigUrlList = [];
    for (let i = 0; i < this.userDashboardConfig.length; i++) {
      if ((this.userDashboardConfig[i]['key'] === 'pendingCandidates'
        || this.userDashboardConfig[i]['key'] === 'recentSearches'
        || this.userDashboardConfig[i]['key'] === 'user'
        || this.userDashboardConfig[i]['key'] === 'candidateStatistics') && this.intialCall) {
        this.userDashboardConfig[i]['url'] = this.userDashboardConfig[i]['url'] + '/' + this.loggedUserDetails['id'];
      }
      // if (this.userDashboardConfig[i]['isActive'] && this.loggedUserDetails[this.userDashboardConfig[i]['dbKey']]) {
        userConfig.push(this.userDashboardConfig[i]['key']);
        userConfigUrlList.push(this.userDashboardConfig[i]['url']);
      // }
    }
    this.intialCall = false;
    this.userConfig = userConfig;
    if (this.loggedUserDetails['isAdmin']) {
      userConfigUrlList.push('/api/users/fetchPendingRecord');
    }
    this.userConfigUrlList = userConfigUrlList;
    this.getUserDashboardData();
  }

  // set data for all widgets
  setWidgetData(data: Array<object>): void {
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < this.userConfig.length; j++) {
        const key = Object.keys(data[i])[0];
        if (key === this.userConfig[j]) {
          const name = key + 'List';
          if (key === 'user') {
            const statisticsData = [];
            statisticsData.push(data[i][this.userConfig[j]][0]['userCompletedTotal']);
            statisticsData.push(data[i][this.userConfig[j]][0]['userPendingTotal']);
            statisticsData.push(data[i][this.userConfig[j]][0]['total']);
            this[name] = statisticsData;
          } else {
            this[name] = data[i][this.userConfig[j]];
          }
        } else if (this.loggedUserDetails['isAdmin']) {
          this.pendingUserList = data[i]['users'];
        }
      }
    }
  }

  getUserDashboardData(): void {
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.forkGetData(this.userConfigUrlList).subscribe(data => {
      if (data && data.length !== 0) {
        const pendingData = data[1];
        if (pendingData && pendingData[this.userConfig[1]] && pendingData[this.userConfig[1]].length !== 0) {
          if (pendingData[this.userConfig[1]][0]['total'] > 10) {
            this.utilityService.setPendingFlag(true);
            this.utilityService.updateSpinnerFlag(false);
            this.modalConfig = this.alertModalSetUp;
            this.modalAction = 'navigation';
            this.showPopUpFlag = true;
          } else {
            this.setWidgetData(data);
            this.utilityService.updateSpinnerFlag(false);
          }
        } else {
            this.setWidgetData(data);
            this.utilityService.updateSpinnerFlag(false);
        }
      }
    });
  }

  // remove widget handler
  removeWidget(key: string): void {
    const userConfig = this.userConfig;
    const updatedConfig = [];
    let selectedConfig;
    for (var i = 0; i < userConfig.length; i++) {
      if (userConfig[i] !== key) {
        updatedConfig.push(userConfig[i]);
      }
    }
    // update db through api call
    // update inactive flag
    for (let i = 0; i < this.userDashboardConfig.length; i++) {
      if (key === this.userDashboardConfig[i]['key']) {
        selectedConfig = this.userDashboardConfig[i];
        this.userDashboardConfig[i]['isActive'] = false;
      }
    }
    const url = '/api/users/updateUserAccess';
    const requestData = {};
    requestData['id'] = this.loggedUserDetails['id'];
    requestData['key'] = selectedConfig['dbKey'];
    requestData['value'] = 0;
    this.apiService.postData(url, requestData).subscribe(data => {
      this.getUserData();
    });

    this.userConfig = updatedConfig;
  }

  // fetch the config data for modal
  getActiveTiles(): void {
    this.showNoResultFlag = true;
    for (let i = 0; i < this.userDashboardConfig.length; i++) {
      if (this.loggedUserDetails[this.userDashboardConfig[i]['dbKey']] && this.userDashboardConfig[i]['isActive']) {
        this.userDashboardConfig[i]['isActive'] = true;
        this.showNoResultFlag = false;
      } else {
        this.userDashboardConfig[i]['isActive'] = false;
      }
    }
  }

  // open modal for add /remove widget
  openModal(): void {
    // get the updated config list
    this.getActiveTiles();
    this.modalConfigSet['fieldList'] = this.userDashboardConfig;
    this.modalConfig = this.modalConfigSet;
    this.showPopUpFlag = true;
  }

  // handle close modal
  handleCloseModal(event): void {
    this.showPopUpFlag = event;
  }

  // handler proceed
  proceedModal(event): void {
    this.handleCloseModal(false);
    if (this.modalAction === 'navigation') {
      this.router.navigateByUrl('pending-candidate-dashboard');
    } else {
      this.getUserData();
    }
  }

  // save handler
  saveWidget(configData): void {
    this.userDashboardConfig = configData;
    const userConfig = [];
    for (let i = 0; i < this.userDashboardConfig.length; i++) {
      if (this.userDashboardConfig[i]['isActive']) {
        userConfig.push(this.userDashboardConfig[i]['key']);
      }
    }
    this.userConfig = userConfig;
    this.handleCloseModal(false);
  }

  // navigate to dashboard
  navigatePage(page: string): void {
    this.router.navigateByUrl(page);
  }

  // view more menu event handler
  showSubMenu(): void {
    this.viewMoreMenuFlag = !this.viewMoreMenuFlag;
  }

}
