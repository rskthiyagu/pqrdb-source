import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-user-access',
  templateUrl: './user-access.component.html',
  styleUrls: ['./user-access.component.scss']
})
export class UserAccessComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  loggedUserData: any;
  documentHeight: number;
  selectedKey: string;
  userCount: number = 0;
  selectedId: number;
  detailConfig: Array < object > = [
    {
      'id': 0,
      'title': 'Approved Users',
      'key': 'users',
      'bgColor': '#A1D066',
      'url': '/api/users/fetchRecord'
    },
    {
      'id': 1,
      'title': 'Pending Users',
      'key': 'users',
      'bgColor': '#FF6C02',
      'url': '/api/users/fetchPendingRecord'
    },
    {
      'id': 2,
      'title': 'Rejected Users',
      'key': 'users',
      'bgColor': '#FF6C02',
      'url': '/api/users/fetchRejectedRecord'
    }
  ];

  deleteModalConfigSet: object = {
    'title': 'Transfer Data to Users',
    'key': 'users',
    'saveFlag': true,
    'cancelFlag': true,
    'userDeleteFlag': true,
    'fieldList': [
      {
        'id': 0,
        'selectFlag': true,
        'selectConfig': {
          'title': 'User', 'dropdownData': [], 'dropdownSelected': [], 'key': 'userId', 'data': '', 'dropdownSetting': {
            singleSelection: true,
            text: "Select",
            requiredFlag: true,
            enableSearchFilter: false,
            classes: "myclass custom-class"
          }
        }
      }
    ]
  };

  alertModalSetUp: object = {
    'title': 'Alert',
    'okFlag': true,
    'cancelFlag': true,
    'contentFlag': true,
    'content': 'Do you want to delete the item?'
  };

  // modal related
  showPopUpFlag: boolean = false;
  modalConfig: object;

  selectedRecordData: Array < object > = [];
  selectedRecordDataBackup: Array < object > = [];
  selectedRecordTitle: string = '';
  searchText: string = '';
  deleteSelectedItem: number;

  constructor(private dataService: DataService,
              private apiService: ApiService,
              public utilityService: UtilityService,
              private router: Router,
              private toastrService: ToastrService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.manipulateDocumentWidth();
    this.getSelectedOptionData(0);
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 300;
  }

  // get the data of selected option
  getSelectedOptionData(id: number): void {
    // update the selected item
    this.selectedId = id;
    for (let i = 0; i < this.detailConfig.length; i++) {
      if ( i === id) {
        this.detailConfig[i]['bgColor'] = '#A1D066';
      } else {
        this.detailConfig[i]['bgColor'] = '#FF6C02';
      }
    }
    this.selectedKey = this.detailConfig[id]['key'];
    const url = this.detailConfig[id]['url'];
    this.utilityService.updateSpinnerFlag(true);
    this.selectedRecordTitle = this.detailConfig[id]['title'];
    this.apiService.getData(url).subscribe(data => {
      this.utilityService.updateSpinnerFlag(false);
      if (data && data[this.detailConfig[id]['key']].length !== 0) {
        const userList = data[this.detailConfig[id]['key']];
        const updatedUserList = [];
        for (let i = 0; i < userList.length; i++) {
          if (userList[i]['id'] !== this.loggedUserData['id']) {
            updatedUserList.push(userList[i]);
          }
        }
        this.selectedRecordData = updatedUserList;
        this.selectedRecordDataBackup = updatedUserList;
        this.userCount = this.selectedRecordData.length;
      } else {
        this.selectedRecordData = [];
        this.userCount = 0;
      }
    });
  }

  // approve user
  approveUser(id: number): void {
    const url = '/api/users/activateUser/' + id;
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.putData(url, null).subscribe( data => {
      this.utilityService.updateSpinnerFlag(false);
      this.getSelectedOptionData(this.selectedId);
      this.toastrService.success('User approved successfully...', 'Success', {timeOut: 2000});
    });
  }

  // reject user
  rejectUser(id: number): void {
    const url = '/api/users/deactivateUser/' + id;
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.putData(url, null).subscribe( data => {
      this.getSelectedOptionData(this.selectedId);
      this.utilityService.updateSpinnerFlag(false);
      this.toastrService.success('User rejected successfully...', 'Success', {timeOut: 2000});
    });
  }

  // view user details
  viewDetails(data: object): void {
    this.dataService.setUserData(data);
    const url = 'user-detail/' + data['id'];
    this.router.navigateByUrl(url);
  }

  // navigate to add user page
  navigateUserCreation(): void {
    this.router.navigateByUrl('add-new-user');
  }

  // filter method
  filterUser(element, index, array) { 
    return (((array[index]['name']).toLowerCase()).indexOf((element['searchText']).toLowerCase()) !== -1);
 }

  // search user on key up event in input
  searchUser(event: any): void {
    this.searchText = '';
    const searchText = event.target.value;
    if (searchText !== '') {
      if (this.selectedRecordData && this.selectedRecordData.length !== 0) {
        for (let i = 0; i < this.selectedRecordDataBackup.length; i++) {
          this.selectedRecordDataBackup[i]['searchText'] = searchText;
        }
        const filteredResult = this.selectedRecordDataBackup.filter(this.filterUser);
        this.selectedRecordData = filteredResult;
      }
    } else {
      for (let i = 0; i < this.selectedRecordDataBackup.length; i++) {
        this.selectedRecordDataBackup[i]['searchText'] = '';
      }
      this.selectedRecordData = this.selectedRecordDataBackup;
    }
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // get user list
  getUserList(): void {
    const url = '/api/users/fetchRecord';
    this.apiService.getData(url).subscribe( data => {
      if (data) {
        const userList = [];
        for (let i = 0; i < data['users'].length; i++) {
          if (data['users'][i]['id'] !== this.deleteSelectedItem) {
            userList.push(data['users'][i]);
          }
        }
        this.deleteModalConfigSet['fieldList'][0]['selectConfig']['dropdownData'] = userList;
        this.deleteModalConfigSet['fieldList'][0]['selectConfig']['dropdownSelected'] = [];
        this.modalConfig = this.deleteModalConfigSet;
        this.showPopUpFlag = true;
      }
    });
  }

  // handle proceed emitter
  handleProceedEvent(event: any): void {
      this.handleCloseModal(false);
      this.getUserList();
  }

  // save widget flow for update user data & delete user
  saveWidget(data: any): void {
    // delete user
    this.deleteItem();
    this.updateUserData(data);
  }

  // transfer user data on delete
  updateUserData(data): void {
    const url = '/api/' + this.modalConfig['key'] + '/userDataTransfer/' + this.deleteSelectedItem;
    const param = data;
    this.apiService.postData(url, param).subscribe(data => {
      this.handleCloseModal(false);
      this.getSelectedOptionData(this.selectedId);
      this.toastrService.success('Operation successfully done...', 'Success', { timeOut: 1500});
    });
  }

  // handle close event
  handleCloseModal(event: boolean): void {
    this.showPopUpFlag = event;
  }

  // open delete modal
  openDeleteModal(id: number): void {
    this.deleteSelectedItem = id;
    this.modalConfig = this.alertModalSetUp;
    this.showPopUpFlag = true;
  }

  // delete item
  deleteItem(): void {
    const url = '/api/' + this.selectedKey + '/delete/' + this.deleteSelectedItem;
    this.apiService.deleteData(url).subscribe(data => {
      const temp = [];
      for (let i = 0; i < this.selectedRecordData.length; i++) {
        if (this.selectedRecordData[i]['id'] !== data['id']) {
          this.selectedRecordData[i]['editFlag'] = false;
          temp.push(this.selectedRecordData[i]);
        }
      }
      this.selectedRecordData = temp;
      this.handleCloseModal(false);
    });
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('settings');
  }

}
