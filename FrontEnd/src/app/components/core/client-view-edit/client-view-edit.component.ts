import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';
import { ApiService } from '../../../services/api.service';

import * as moment from 'moment';

@Component({
  selector: 'app-client-view-edit',
  templateUrl: './client-view-edit.component.html',
  styleUrls: ['./client-view-edit.component.scss']
})
export class ClientViewEditComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  fieldDataList: Array<object> = [
    {
      'id': 0,
      'title': 'Client Full Name',
      'inputFlag': true,
      'key': 'name',
      'placeholder': 'Required',
      'requiredFlag': true,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false
    },
    {
      'id': 1,
      'title': 'Website',
      'inputFlag': true,
      'key': 'website',
      'placeholder': '',
      'requiredFlag': false,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false
    },
    {
      'id': 2,
      'title': 'Country',
      'errorFlag': false,
      'selectFlag': true,
      'key': 'countries',
      'requiredFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Country",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 3,
      'title': 'State',
      'errorFlag': false,
      'selectFlag': true,
      'key': 'states',
      'requiredFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select State",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 4,
      'title': 'City',
      'errorFlag': false,
      'key': 'cities',
      'requiredFlag': true,
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select City",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 5,
      'title': 'Zip Code',
      'inputFlag': true,
      'key': 'zipCode',
      'placeholder': 'Zip Code',
      'requiredFlag': false,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false
    },
    {
      'id': 6,
      'title': 'Category',
      'errorFlag': false,
      'key': 'clientCategory',
      'requiredFlag': false,
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Category",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 7,
      'title': 'Status',
      'errorFlag': false,
      'key': 'clientStatus',
      'requiredFlag': true,
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Status",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 8,
      'title': 'Client Account Manager',
      'errorFlag': false,
      'selectFlag': true,
      'requiredFlag': true,
      'key': 'accountManager',
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Account Manager",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    }
  ];

  fieldDataListConfig: Array<object> = [];

  // active requirements
  activeRequirementConfigObject: object = {
    'title': 'Active Requirements',
    'width': 15,
    'field': [
      { 'title': 'Job Id', 'key': 'count', 'linkFlag': true },
      { 'title': 'Job Position', 'key': 'title', 'linkFlag': true },
      { 'title': 'Location', 'multipleKeyFlag': true, 'keyList': ['citiesName', 'statesName', 'countriesName']  },
      { 'title': 'Account Manager', 'key': 'accountManagerName' },
      { 'title': 'Updated By', 'key': 'updateUserName' },
      { 'title': 'Updated On', 'key': 'updatedOn', 'dateFlag': true }
    ],
    'data': []
  }

  activeRequirementObject: object = {};

  // inactive requirement  
  inactiveRequirementConfigObject: object = {
    'title': 'InActive Requirements',
    'width': 15,
    'field': [
      { 'title': 'Job Id', 'key': 'count', 'linkFlag': true },
      { 'title': 'Job Position', 'key': 'candidateName', 'linkFlag': true },
      { 'title': 'Location', 'multipleKeyFlag': true, 'keyList': ['citiesName', 'statesName', 'countriesName']  },
      { 'title': 'Account Manager', 'key': 'accountManagerName' },
      { 'title': 'Updated By', 'key': 'updateUserName' },
      { 'title': 'Updated On', 'key': 'updatedOn', 'dateFlag': true }
    ],
    'data': []
  }

  inactiveRequirementObject: object = {};

  // hold requirement
  holdRequirementConfigObject: object = {
    'title': 'Hold Requirements',
    'width': 15,
    'field': [
      { 'title': 'Job Id', 'key': 'count', 'linkFlag': true },
      { 'title': 'Job Position', 'key': 'candidateName', 'linkFlag': true },
      { 'title': 'Location', 'multipleKeyFlag': true, 'keyList': ['citiesName', 'statesName', 'countriesName']  },
      { 'title': 'Account Manager', 'key': 'accountManagerName' },
      { 'title': 'Updated By', 'key': 'updateUserName' },
      { 'title': 'Updated On', 'key': 'updatedOn', 'dateFlag': true }
    ],
    'data': []
  }

  holdRequirementObject: object = {};

  // closed requirement
  closedRequirementConfigObject: object = {
    'title': 'Closed Requirements',
    'width': 15,
    'field': [
      { 'title': 'Job Id', 'key': 'count', 'linkFlag': true },
      { 'title': 'Job Position', 'key': 'candidateName', 'linkFlag': true },
      { 'title': 'Location', 'multipleKeyFlag': true, 'keyList': ['citiesName', 'statesName', 'countriesName']  },
      { 'title': 'Account Manager', 'key': 'accountManagerName' },
      { 'title': 'Updated By', 'key': 'updateUserName' },
      { 'title': 'Updated On', 'key': 'updatedOn', 'dateFlag': true }
    ],
    'data': []
  }

  closedRequirementObject: object = {};

  filledRequirementConfigObject: object = {
    'title': 'Filled Requirements',
    'width': 15,
    'field': [
      { 'title': 'Job Id', 'key': 'count', 'linkFlag': true },
      { 'title': 'Job Position', 'key': 'candidateName', 'linkFlag': true },
      { 'title': 'Location', 'multipleKeyFlag': true, 'keyList': ['citiesName', 'statesName', 'countriesName']  },
      { 'title': 'Account Manager', 'key': 'accountManagerName' },
      { 'title': 'Updated By', 'key': 'updateUserName' },
      { 'title': 'Updated On', 'key': 'updatedOn', 'dateFlag': true }
    ],
    'data': []
  }

  filledRequirementObject: object = {};

  // warning pop up
  warningModalSetUp: object = {
    'title': 'Warning',
    'proceedFlag': true,
    'cancelFlag': true,
    'contentFlag': true,
    'content': 'You may lose the unsaved changes. Do you want to continue?'
  };

  deleteModalSetUp: object = {
    'title': 'Delete Confirmation',
    'proceedFlag': true,
    'cancelFlag': true,
    'contentFlag': true,
    'content': 'Are you sure to delete the client?'
  };

  modalAction: string;
  modalConfig: object;

  filterUrlList: Array<string> = [
    '/api/cities/fetchRecord',
    '/api/states/fetchRecord',
    '/api/countries/fetchRecord',
    '/api/users/fetchAccountManagerRecord',
    '/api/clientStatus/fetchRecord',
    '/api/clientCategory/fetchRecord'
  ];

  viewFlag: boolean;
  editFlag: boolean;
  createFlag: boolean;
  loggedUserDetails: object;
  clientId: number;
  actionTitle: string = '';
  dataFlag: boolean = false;
  optionFlag: boolean = false;
  documentHeight: number;
  clientDetailObject: object = {};
  showPopUpFlag: boolean = false;

  constructor(private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private utilityService: UtilityService,
    private apiService: ApiService,
    private toastrService: ToastrService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (userData) {
      this.dataService.setLoggedUserDetails(userData);
      this.loggedUserDetails = userData;
    } else if (!userData && !localUserData) {
      this.router.navigateByUrl('');
    } else {
      this.loggedUserDetails = localUserData;
    }
    // to eradicate the ngIf data changing issue
    const scope = this;
    setTimeout(function () {
      scope.validateOperation();
    }, 0);
  }

  // event handler for finding the operation of page
  validateOperation(): void {
    const routePath = this.route.routeConfig.path;
    this.route.params.subscribe(params => {
      if (params) {
        this.clientId = +params['clientId'];
      }
      // page detector
      if (routePath.includes('client-view') && this.clientId) {    // candidate view page
        this.viewFlag = true;
        this.editFlag = false;
        this.createFlag = false;
        this.actionTitle = 'Client View';
        this.manipulateDocumentWidth();
        this.getClientData();
        this.fetchActiveRequirement();
        this.fetchInactiveRequirement();
        this.fetchHoldRequirement();
        this.fetchClosedRequirement();
        this.fetchFilledRequirement();
      } else if (routePath.includes('client-edit') && this.clientId) {   // candidate edit page
        this.viewFlag = false;
        this.editFlag = true;
        this.createFlag = false;
        this.actionTitle = 'Client Edit';
        this.manipulateDocumentWidth();
        this.getClientData();
        // do validate the data is already present in data service or not
        this.getDropdownItems();
      } else if (routePath.includes('client-create') && !this.clientId) {   // candidate create page
        this.viewFlag = false;
        this.editFlag = false;
        this.createFlag = true;
        this.actionTitle = 'Client Create';
        this.getDropdownItems();
        this.manipulateDocumentWidth();
      } else {      // redirect to home page
        this.router.navigateByUrl('/client-dashboard');
      }
    });
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 203;
  }

  // get filter dropdown values
  getDropdownItems(): void {
    // enable loader
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.forkGetData(this.filterUrlList).subscribe(data => {
      this.updateFilterData(data);
    });
  }

  // update the filter data into configuration
  updateFilterData(filterData: Array<object>): void {
    // skip inner loop by assuming the data is coming in same order given
    for (let i = 0; i < this.fieldDataList.length; i++) {
      for (let j = 0; j < filterData.length; j++) {
        const key = Object.keys(filterData[j])[0];
        if (this.fieldDataList[i]['selectFlag']) {
          if (key == this.fieldDataList[i]['key'])
            this.fieldDataList[i]['dropdownData'] = filterData[j][this.fieldDataList[i]['key']];
        }
      }
    }
    // trigger config setter method
    this.optionFlag = true;
    this.editDetailConfigSetter();
    this.utilityService.updateSpinnerFlag(false);
  }

  // fetch the requirement data
  getClientData(): void {
    this.utilityService.updateSpinnerFlag(true);
    const url = '/api/clients/fetchRecord/' + this.clientId;
    this.apiService.getData(url).subscribe(data => {
      if (data && data['clients'] && data['clients'].length > 0)
        this.clientDetailObject = data['clients'][0][0];
      else
        this.clientDetailObject = {};
      this.composeData();
      this.utilityService.updateSpinnerFlag(false);
    });
  }

  // compose data structure
  composeData(): void {
    for ( let i = 0; i < this.fieldDataList.length; i++) {
      if (this.fieldDataList[i]['selectFlag']) {
        const name = this.fieldDataList[i]['key'] + 'Name';
        const id = this.fieldDataList[i]['key'] + 'Id';
        const obj = {};
        obj['id'] = this.clientDetailObject[id];
        obj['name'] = this.clientDetailObject[name];
        this.clientDetailObject[this.fieldDataList[i]['key']] = [obj];
      }
    }
    this.setClientData();
  }

  // set the client data
  setClientData(): void {
    let editDetailList = this.fieldDataList;
    for (let i = 0; i < editDetailList.length; i++) {
      if (editDetailList[i]['selectFlag']) {     // select box data feeding
        editDetailList[i]['dropdownSelected'] = this.clientDetailObject[editDetailList[i]['key']];
      } else {                                          // text box data feeding
        editDetailList[i]['data'] = this.clientDetailObject[editDetailList[i]['key']];
      }
    }
    this.fieldDataList = editDetailList;
    // trigger config object setter method
    this.dataFlag = true;
    this.editDetailConfigSetter();
  }

  // set the configuration of detail tiles
  editDetailConfigSetter(): void {
    // if (this.dataFlag && this.optionFlag) {
    //   this.fieldDataListConfig = this.fieldDataList;
    //   this.dataFlag = false;
    //   this.optionFlag = false;
    // } else if (this.createFlag && this.optionFlag) {
    //   this.fieldDataListConfig = this.fieldDataList;
    //   this.optionFlag = false;
    // }
    this.fieldDataListConfig = this.fieldDataList;
  }

  // handle select event in select box
  handleSelectedItems(selectedData: any, offset: number, key: string): void {
    // update the object
    if (selectedData && selectedData.length !== 0) {
      switch (key) {
        case 'countries': {
          const stateUrl = '/api/states/fetchRecordByCountryId/' + selectedData[0]['id'];
          const cityUrl = '/api/cities/fetchRecordByCountryId/' + selectedData[0]['id'];
          const urlList = [stateUrl, cityUrl];
          this.apiService.forkGetData(urlList).subscribe(data => {
            this.fieldDataListConfig[offset + 1]['dropdownData'] = data[0]['states'];
            this.fieldDataListConfig[offset + 2]['dropdownData'] = data[1]['cities'];
            // selected values setter
            this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 1]['dropdownSelected'] = [];
            this.fieldDataListConfig[offset + 2]['dropdownSelected'] = [];
            // set the result set value
            this.clientDetailObject[key] = selectedData;
            this.clientDetailObject['states'] = [];
            this.clientDetailObject['cities'] = [];
          });
          break;
        }
        case 'states': {
          const cityUrl = '/api/cities/fetchRecordByStateId/' + selectedData[0]['id'];
          this.apiService.getData(cityUrl).subscribe(data => {
            // selected values for country
            this.fieldDataListConfig[offset - 1]['dropdownSelected'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
            this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 1]['dropdownSelected'] = [];
            // set the result set value
            this.clientDetailObject['countries'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
            this.clientDetailObject[key] = selectedData;
            this.clientDetailObject['cities'] = [];
          });
          break;
        }
        case 'cities': {
          // selected values for country
          this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
          this.fieldDataListConfig[offset - 1]['dropdownSelected'] = [{ 'id': selectedData[0]['stateId'], 'name': selectedData[0]['stateName'], 'countryId': selectedData[0]['countryId'], 'countryName': selectedData[0]['countryName'] }];
          this.fieldDataListConfig[offset - 2]['dropdownSelected'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
          // set the result set value
          this.clientDetailObject['countries'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
          this.clientDetailObject['states'] = [{ 'id': selectedData[0]['stateId'], 'name': selectedData[0]['stateName'], 'countryId': selectedData[0]['countryId'], 'countryName': selectedData[0]['countryName'] }];
          this.clientDetailObject['cities'] = selectedData;
          break;
        }
        default: {
          this.clientDetailObject[key] = selectedData;
        }
      }
    } else {
      switch (key) {
        case 'countries': {
          const stateUrl = '/api/states/fetchRecord';
          const cityUrl = '/api/cities/fetchRecord';
          const urlList = [stateUrl, cityUrl];
          this.apiService.forkGetData(urlList).subscribe(data => {
            this.fieldDataListConfig[offset + 1]['dropdownData'] = data[0]['states'];
            this.fieldDataListConfig[offset + 2]['dropdownData'] = data[1]['cities'];
            // selected value
            this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 1]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 2]['dropdownSelected'] = selectedData;

            // set the result values
            this.clientDetailObject['countries'] = selectedData;
            this.clientDetailObject['states'] = selectedData;
            this.clientDetailObject['cities'] = selectedData;
          });
          break;
        }
        case 'states': {
          const cityUrl = '/api/cities/fetchRecord';
          this.apiService.getData(cityUrl).subscribe(data => {
            // selected value
            this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 1]['dropdownSelected'] = selectedData;
            // set the result set values
            this.clientDetailObject['states'] = selectedData;
            this.clientDetailObject['cities'] = selectedData;
          });
          break;
        }
        case 'cities': {
          this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
          this.clientDetailObject['cities'] = selectedData;
          break;
        }
        default: {
          this.clientDetailObject[key] = this.fieldDataListConfig[offset]['dropdownSelected'];
        }
      }
    }
  }

  // handle add new item in select box
  addNewItemHandler(event: any, key: string): void {

  }

  // handle type event
  handleTypeEvent(event: any, key: string): void {
    // validation need to be done
    this.clientDetailObject[key] = event.target.value;
  }

  // save requirement
  saveRequirement(): void {
    let url;
    let apiCall;
    const errorFlag = this.validateClientDetails();
    if (errorFlag) {
      this.toastrService.error('Please fill the required fields...', 'Error', { timeOut: 1500 });
      return;
    } else {
      if (this.editFlag) {
        url = '/api/clients/update';
        apiCall = 'updateUrl';
        this.clientDetailObject['updattedBy'] = this.loggedUserDetails['id'];
        this.clientDetailObject['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      } else {
        url = '/api/clients/create';
        apiCall = 'postData'
        this.clientDetailObject['createdBy'] = this.loggedUserDetails['id'];
        this.clientDetailObject['updattedBy'] = this.loggedUserDetails['id'];
        this.clientDetailObject['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        this.clientDetailObject['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      }
      this.apiService[apiCall](url, this.clientDetailObject).subscribe(data => {
        if (this.clientId) {
          this.toastrService.success('Client Updated Successfully...', 'Success', { timeOut: 1000 });
          const url = '/client-view/' + this.clientId;
          this.router.navigateByUrl(url);
        } else {
          this.toastrService.success('Client Saved Successfully...', 'Success', { timeOut: 1000 });
          this.router.navigateByUrl('/client-dashboard');
        }
        // reset field
        this.clientDetailObject = {};
      });
    }
  }

  // delete confirmation
  deleteConfirmation(): void {
    this.modalConfig = this.deleteModalSetUp;
    this.showPopUpFlag = true;
    this.modalAction = 'delete';
  }

  // delete requirement
  deleteClient(): void {
    const url = '/api/clients/delete/' + this.clientId;
    this.apiService.deleteData(url).subscribe(data => {
      this.showPopUpFlag = false;
    });
  }

  // cancel confirmation
  cancelConfirmation(): void {
    this.modalConfig = this.warningModalSetUp;
    this.showPopUpFlag = true;
    this.modalAction = 'cancel';
  }

  // cancel changes
  cancelChanges(): void {
    if (this.clientId) {
      const url = 'client-view/' + this.clientId;
      this.router.navigateByUrl(url);
    } else {
      this.router.navigateByUrl('client-dashboard');
    }
    this.showPopUpFlag = false;
  }

  // close event
  handleCloseModal(event): void {
    this.showPopUpFlag = false;
  }

  // proceed event
  proceedModal(): void {

  }

  // skip modal
  skipModal(): void {
    
  }

  handleSaveModal(event: any): void {

  }

  // validation
  validateClientDetails(): boolean {
    let errorFlag = false;
    for (let i = 0; i < this.fieldDataListConfig.length; i++) {
      if (this.fieldDataListConfig[i]['requiredFlag']) {
        if (this.fieldDataListConfig[i]['selectFlag'] && (!this.clientDetailObject[this.fieldDataListConfig[i]['key']] || (this.clientDetailObject[this.fieldDataListConfig[i]['key']] && this.clientDetailObject[this.fieldDataListConfig[i]['key']].length === 0))) {
          this.fieldDataListConfig[i]['errorFlag'] = true;
          errorFlag = true;
        } else if (this.fieldDataListConfig[i]['inputFlag'] && !this.fieldDataListConfig[i]['data']) {
          this.fieldDataListConfig[i]['errorFlag'] = true;
          errorFlag = true;
        } else {
          this.fieldDataListConfig[i]['errorFlag'] = false;
        }
      }
    }
    if (errorFlag) {
      return true;
    } else {
      return false;
    }
  }

  // navigate to dashboard
  navigateToDashboard(): void {
    this.router.navigateByUrl('client-dashboard');
  }

  // previous route
  previousRoute(): void {

  }

  // modal open
  openModal(a, b): void {

  }

  // fetch clients based on status
  fetchActiveRequirement(): void {
    const url = '/api/requirements/fetchRecordByClientAndStatus/' + this.clientId + '/1';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.activeRequirementConfigObject['data'] = data['requirements'];
        this.activeRequirementObject = this.activeRequirementConfigObject;
      }
    });
  }

  // fetch clients based on status
  fetchInactiveRequirement(): void {
    const url = '/api/requirements/fetchRecordByClientAndStatus/' + this.clientId + '/2';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.inactiveRequirementConfigObject['data'] = data['requirements'];
        this.inactiveRequirementObject = this.inactiveRequirementConfigObject;
      }
    });
  }

  // fetch clients based on status
  fetchHoldRequirement(): void {
    const url = '/api/requirements/fetchRecordByClientAndStatus/' + this.clientId + '/3';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.holdRequirementConfigObject['data'] = data['requirements'];
        this.holdRequirementObject = this.holdRequirementConfigObject;
      }
    });
  }

  // fetch clients based on status
  fetchClosedRequirement(): void {
    const url = '/api/requirements/fetchRecordByClientAndStatus/' + this.clientId + '/4';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.closedRequirementConfigObject['data'] = data['requirements'];
        this.closedRequirementObject = this.closedRequirementConfigObject;
      }
    });
  }

  // fetch clients based on status
  fetchFilledRequirement(): void {
    const url = '/api/requirements/fetchRecordByClientAndStatus/' + this.clientId + '/5';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.filledRequirementConfigObject['data'] = data['requirements'];
        this.filledRequirementObject = this.filledRequirementConfigObject;
      }
    });
  }

  // handle link click event
  handleNavigation(event: any): void {
    const url = 'requirement-view/' + event['data']['id'];
    this.router.navigateByUrl(url);
  }

  // handle for table action event
  handleTableEvent(event: any): void {
    
  }
}
