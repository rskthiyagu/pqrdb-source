import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-client-dashboard',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.scss']
})
export class ClientDashboardComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  filterDataSetConfig: Array < object > = [
    {
      'title': 'Client Name', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Countries', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'States', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Cities', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Status', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Category', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Account Manager', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
  ];

  documentHeight: number;
  clientRecord: Array < object > = [];

  constructor(private router: Router,
              private apiService: ApiService,
              private utilityService: UtilityService) { }

  ngOnInit() {
    this.getClientData();
  }

  // get client data
  getClientData(): void {
    const url = '/api/clients/fetchRecord';
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.clientRecord = data['clients'][0];
      }
      this.utilityService.updateSpinnerFlag(false);
    });
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('dashboard');
  }

  // client create page
  navigateCreateClient(): void {
    this.router.navigateByUrl('client-create');
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 203;
  }

  // handle event
  handleNavigationEvent(event: any): void {
    this.router.navigateByUrl('client-view/' + event['id']);
  }

  // filter
  filterInputHandler(e): void {
    
  }

}
