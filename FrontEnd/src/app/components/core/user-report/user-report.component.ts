import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-user-report',
  templateUrl: './user-report.component.html',
  styleUrls: ['./user-report.component.scss']
})
export class UserReportComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  loggedUserData: any;
  userData: any;
  documentHeight: number;
  candidateStatisticsList: any;
  userStatisticsData: Array < any > = [];

  constructor(private apiService: ApiService,
              public utilityService: UtilityService,
              private dataService: DataService,
              private router: Router) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.getUserData();
    this.getUserStatisticsData(this.loggedUserData['id']);
    this.manipulateDocumentWidth();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 212;
  }

  // get user data
  getUserData(): void {
    const url = '/api/users/fetchRecord';
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.getData(url).subscribe( data => {
      if (data) {
        const userData = data['users'];
        for (let i = 0; i < userData.length; i++) {
          userData[i]['bgColor'] = '#FF6C02';
          userData[i]['statisticsData'] = [];
          userData[i]['statisticsData'].push(userData[i]['userCompletedTotal']);
          userData[i]['statisticsData'].push(userData[i]['userPendingTotal']);
          userData[i]['statisticsData'].push(userData[i]['total']);
          if (userData[i]['id'] === this.loggedUserData['id']) {
            userData[i]['bgColor'] = '#A1D066';
            this.userStatisticsData = userData[i]['statisticsData'];
          }
        }
        this.userData = userData;
      }
      this.utilityService.updateSpinnerFlag(false);
    });
  }

  // get user statistics data
  getUserStatisticsData(id: number): void {
    const url = '/api/candidates/getStatistics/' + id;
    this.apiService.getData(url).subscribe( data => {
      this.candidateStatisticsList = data['candidateStatistics'];
    });
  }

  // handle selected user
  handleSelectedData(user: object): void {
    // update color
    for (let i = 0; i < this.userData.length; i++) {
      if (this.userData[i]['id'] === user['id']) {
        this.userData[i]['bgColor'] = '#A1D066';
        this.getUserStatisticsData(user['id']);
        this.userStatisticsData = this.userData[i]['statisticsData'];
      } else {
        this.userData[i]['bgColor'] = '#FF6C02';
      }
    }
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('settings');
  }

}
