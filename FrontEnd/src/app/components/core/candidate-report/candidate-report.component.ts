import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-candidate-report',
  templateUrl: './candidate-report.component.html',
  styleUrls: ['./candidate-report.component.scss']
})
export class CandidateReportComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  loggedUserData: any;
  userData: any;
  documentHeight: number;
  candidateStatisticsList: any;
  userStatisticsData: Array < any > = [];

  constructor(private apiService: ApiService,
              public utilityService: UtilityService,
              private dataService: DataService,
              private router: Router) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.manipulateDocumentWidth();
    this.getUserStatisticsData(this.loggedUserData['id']);
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 212;
  }

  // get user statistics data
  getUserStatisticsData(id: number): void {
    const url = '/api/candidates/getStatistics/' + id;
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.getData(url).subscribe( data => {
      this.candidateStatisticsList = data['candidateStatistics'];
      // candidate pie chart statistics data
      const temp = [];
      temp.push(this.candidateStatisticsList['total'][0]['candidateTotal']);
      temp.push(this.candidateStatisticsList['pending'][0]['candidatePendingTotal']);
      temp.push(this.candidateStatisticsList['today'][0]['candidateTodayTotal']);
      this.userStatisticsData = temp;
      this.utilityService.updateSpinnerFlag(false);
    });
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }
  
  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('settings');
  }

}
