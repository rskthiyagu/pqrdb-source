import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateViewEditComponent } from './candidate-view-edit.component';

describe('CandidateViewEditComponent', () => {
  let component: CandidateViewEditComponent;
  let fixture: ComponentFixture<CandidateViewEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateViewEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateViewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
