import { Component, AfterViewInit, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';
import { DataService } from '../../../services/data.service';

import * as moment from 'moment';

@Component({
  selector: 'app-candidate-view-edit',
  templateUrl: './candidate-view-edit.component.html',
  styleUrls: ['./candidate-view-edit.component.scss']
})
export class CandidateViewEditComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  @ViewChild('singleUpload', { static: false }) singleUpload;
  @ViewChild('multiUpload', { static: false }) multiUpload;
  @ViewChild('iframeElement', { static: false }) iframeElement;

  files = [];
  iframeFlag: boolean = true;
  iframeDoc: any;
  rockStarFlag: boolean;

  doc;
  fileToUpload: File = null;
  documentUploadedFlag: boolean = false;
  viewFlag: boolean = false;
  editFlag: boolean = false;
  createFlag: boolean = false;
  fieldDataObject: Array<object> = [];
  actionTitle: string = '';
  uploadSkipFlag: boolean = false;
  candidateId: number;
  documentHeight: number;
  filterDataSet: Array<object> = [];
  candidateDetailObject: object = {};
  fieldDataArray: Array<object> = [];
  loggedUserDetails: object;
  dataFlag: boolean = false;
  optionFlag: boolean = false;
  infoEnableFlag: boolean;
  candidateSaveFlag: boolean = false;
  // bulk upload variables
  enableBulkUpload: boolean = false;
  filesToUpload: any;
  bulkFileCount: number;
  currentFileCount: number;
  bulkUploadProgressFlag: boolean = false;
  bulkUploadSingle: boolean = false;
  docSubmitFlag: boolean = false;
  // bulk upload resume only candidates or pending candidates
  pendingCandidateDataList: Array<object> = [];
  bulkUploadPendingCandidateFlag: boolean = false;
  pendingCandidateInitialFlag: boolean = false;
  pendingRecordCount: number;
  newlyAddedId: object;
  createCommunicationModalflag: boolean;
  candidateAdvancedDetail: object;

  fieldSetArray: Array<object> = [
    {
      'id': 1,
      'title': 'Candidate Name',
      'key': 'firstName',
      'data': '',
      'imgSrc': 'personal-detail.png',
      'listFlag': false,
      'iconFlag': true,
      'iconClass': 'user'
    }, {
      'id': 2,
      'title': 'Mobile Number',
      'key': 'mobileNumber',
      'data': '',
      'imgSrc': 'mobile.png',
      'listFlag': false,
      'iconFlag': true,
      'iconClass': 'mobile-alt'
    }, {
      'id': 3,
      'title': 'Email Address',
      'key': 'email',
      'data': '',
      'imgSrc': 'email.png',
      'listFlag': false
    }, {
      'id': 4,
      'title': 'LinkedIn Detail',
      'key': 'linkedin',
      'data': '',
      'imgSrc': 'linkedin.png',
      'listFlag': false
    },
    {
      'id': 5,
      'title': 'Skills',
      'key': 'skills',
      'data': '',
      'imgSrc': 'skills.png',
      'listFlag': true
    }, {
      'id': 6,
      'title': 'Authorizations',
      'key': 'authorizations',
      'data': '',
      'imgSrc': 'authorization.png',
      'listFlag': true,
      'iconFlag': true,
      'iconClass': 'passport'
    }, {
      'id': 7,
      'title': 'Education',
      'key': 'qualifications',
      'data': '',
      'imgSrc': 'qualification.png',
      'listFlag': true
    }, {
      'id': 8,
      'title': 'Previous Employers',
      'key': 'employers',
      'data': '',
      'imgSrc': 'employer.png',
      'listFlag': true
    }, {
      'id': 9,
      'title': 'Certifications',
      'key': 'certifications',
      'data': '',
      'imgSrc': 'employer.png',
      'listFlag': true
    }, {
      'id': 10,
      'title': 'Domains',
      'key': 'domains',
      'data': '',
      'imgSrc': 'employer.png',
      'listFlag': true
    }];

  fieldDataList: Array<object> = [
    {
      'id': 0,
      'title': 'Personal Details',
      'imgSrc': 'personal-detail.png',
      'progressText': 'next',
      'selectFlag': false,
      'requiredFlag': true,
      'optional': true,
      'noteFlag': false,
      'detailList': [
        { 'title': 'First Name', 'inputFlag': true, 'key': 'firstName', 'data': '', 'type': 'text', 'placeholder': 'required', 'pattern': '[a-zA-Z0-9\s]+', 'requiredFlag': true, 'errorFlag': false },
        { 'title': 'Last Name', 'inputFlag': true, 'key': 'lastName', 'data': '', 'type': 'text', 'placeholder': 'required', 'pattern': '[a-zA-Z0-9\s]+', 'requiredFlag': true, 'errorFlag': false },
        {
          'title': 'Gender',
          'imgSrc': 'gender.png',
          'progressText': 'next',
          'selectFlag': true,
          'key': 'gender',
          'errorFlag': false,
          'requiredFlag': true,
          'dropdownData': [{ 'id': 0, 'name': 'Male' }, { 'id': 1, 'name': 'Female' }],
          'dropdownSelected': [],
          'dropdownSetting': {
            singleSelection: true,
            text: "Select Gender (required)",
            // selectAllText: 'Select All',
            // unSelectAllText: 'UnSelect All',
            enableSearchFilter: false,
            classes: "myclass custom-class",
            addNewItemOnFilter: false
          }
        },
        { 'title': 'Job Title', 'inputFlag': true, 'key': 'jobTitle', 'placeholder': 'required', 'requiredFlag': true, 'data': '', 'pattern': '[a-zA-Z0-9\s]+', 'type': 'text', 'errorFlag': false },
        { 'title': 'Overall Experience', 'inputFlag': true, 'key': 'experience', 'placeholder': '', 'requiredFlag': false, 'data': '', 'pattern': '[a-zA-Z0-9\s]+', 'type': 'number', 'errorFlag': false }
      ]
    }, {
      'id': 1,
      'title': 'Contact Details',
      'imgSrc': 'contact-detail.png',
      'progressText': 'next',
      'selectFlag': false,
      'requiredFlag': true,
      'detailList': [
        { 'title': 'Mobile Number', 'key': 'mobileNumber', 'placeholder': 'required', 'requiredFlag': true, 'pattern': '[0-9]', 'data': '', 'type': 'text', 'errorFlag': false },
        { 'title': 'Email', 'key': 'email', 'data': '', 'type': 'text', 'placeholder': 'required', 'pattern': '[a-zA-Z0-9\s]+', 'requiredFlag': true, 'errorFlag': false },
        { 'title': 'LinkedIn Link', 'key': 'linkedin', 'placeholder': '', 'requiredFlag': false, 'pattern': '[a-zA-Z0-9\s]+', 'data': '', 'type': 'text', 'errorFlag': false }
      ]
    }, {
      'id': 2,
      'title': 'Location Details',
      'imgSrc': 'location.jpg',
      'progressText': 'next',
      'selectFlag': false,
      'multiSelect': true,
      'detailList': [
        {
          'title': 'Countries',
          'key': 'countries',
          'dropdownData': [],
          'dropdownSelected': [],
          'errorFlag': false,
          'selectFlag': true,
          'requiredFlag': true,
          'dropdownSetting': {
            singleSelection: true,
            text: "Select Country (required)",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            addNewItemOnFilter: true
          }
        }, {
          'title': 'States',
          'key': 'states',
          'dropdownData': [],
          'dropdownSelected': [],
          'errorFlag': false,
          'selectFlag': true,
          'dropdownSetting': {
            singleSelection: true,
            text: "Select State",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            addNewItemOnFilter: true
          }
        }, {
          'title': 'Cities',
          'key': 'cities',
          'dropdownData': [],
          'dropdownSelected': [],
          'errorFlag': false,
          'selectFlag': true,
          'dropdownSetting': {
            singleSelection: true,
            text: "Select City",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            addNewItemOnFilter: true
          }
        }
      ]
    }, {
      'id': 3,
      'title': 'Salary Details',
      'imgSrc': 'location.jpg',
      'progressText': 'next',
      'selectFlag': false,
      'multiSelect': false,
      'optional': true,
      'noteFlag': true,
      'noteText': "Use 'K' for thousands, 'L' for Lakhs as Metrics or Use numeric Represntation",
      'countryFlag': true,
      'detailList': [
        { 'title': 'Hourly Rate ($)', 'dollarFlag': true, 'noteFlag': true, 'inputFlag': true, 'selectedCountry': ['usa', 'canada'], 'key': 'hourlyRate', 'placeholder': 'required', 'requiredFlag': true, 'pattern': '[0-9]', 'data': '', 'type': 'text', 'errorFlag': false, 'noteText': 'Choose either Hourly Rate or Expected Salary as Manadatory' },
        { 'title': 'Expected Salary ($)', 'dollarFlag': true, 'inputFlag': true, 'selectedCountry': ['usa', 'canada'], 'key': 'expectedSalary', 'placeholder': 'required', 'requiredFlag': true, 'pattern': '[0-9]', 'data': '', 'type': 'text', 'errorFlag': false },
        {
          'title': 'Tax terms',
          'key': 'taxTerms',
          'selectedCountry': ['usa', 'canada'],
          'selectFlag': true,
          'dropdownData': [],
          'dropdownSelected': [],
          'errorFlag': false,
          'requiredFlag': true,
          'dropdownSetting': {
            singleSelection: true,
            text: "Select Tax terms (required)",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            addNewItemOnFilter: false
          }
        },
        { 'title': 'Current CTC (₹)', 'rupeeFlag': true, 'inputFlag': true, 'selectedCountry': ['india'], 'key': 'currentCTC', 'placeholder': 'required', 'requiredFlag': true, 'pattern': '[0-9]', 'data': '', 'type': 'text', 'errorFlag': false },
        { 'title': 'Expected CTC (₹)', 'rupeeFlag': true, 'inputFlag': true, 'selectedCountry': ['india'], 'key': 'expectedCTC', 'placeholder': 'required', 'requiredFlag': true, 'pattern': '[0-9]', 'data': '', 'type': 'text', 'errorFlag': false },
        { 'title': 'Notice Period (Days)', 'dayFlag': true, 'inputFlag': true, 'selectedCountry': ['india'], 'key': 'noticePeriod', 'placeholder': 'required', 'requiredFlag': true, 'pattern': '[0-9]', 'data': '', 'type': 'text', 'errorFlag': false },
        {
          'title': 'Duration',
          'key': 'duration',
          'selectedCountry': ['usa', 'canada', 'india'],
          'selectFlag': true,
          'dropdownData': [],
          'dropdownSelected': [],
          'errorFlag': false,
          'requiredFlag': true,
          'dropdownSetting': {
            singleSelection: true,
            text: "Select Duration (required)",
            enableSearchFilter: false,
            classes: "myclass custom-class",
            addNewItemOnFilter: false
          }
        },
        {
          'title': 'Relocation',
          'key': 'relocation',
          'selectedCountry': ['usa', 'canada', 'india'],
          'selectFlag': true,
          'dropdownData': [{ 'id': 0, 'name': 'No' }, { 'id': 1, 'name': 'Yes' }],
          'dropdownSelected': [],
          'errorFlag': false,
          'requiredFlag': true,
          'dropdownSetting': {
            singleSelection: true,
            text: "Select Relocation (required)",
            enableSearchFilter: false,
            classes: "myclass custom-class",
            addNewItemOnFilter: false
          }
        },
        {
          'title': 'Relocation States',
          'key': 'relocationStates',
          'selectedCountry': ['usa', 'canada'],
          'selectFlag': true,
          'dropdownData': [],
          'dropdownSelected': [],
          'errorFlag': false,
          'dropdownSetting': {
            singleSelection: false,
            text: "Select State",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            addNewItemOnFilter: false
          }
        }, {
          'title': 'Relocation Cities',
          'key': 'relocationCities',
          'selectedCountry': ['india'],
          'selectFlag': true,
          'dropdownData': [],
          'dropdownSelected': [],
          'errorFlag': false,
          'dropdownSetting': {
            singleSelection: false,
            text: "Select City",
            enableSearchFilter: true,
            classes: "myclass custom-class",
            addNewItemOnFilter: false
          }
        }
      ]
    }, {
      'id': 4,
      'title': 'Skills',
      'imgSrc': 'skills.png',
      'progressText': 'next',
      'key': 'skills',
      'errorFlag': false,
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'requiredFlag': true,
      'dropdownSetting': {
        singleSelection: false,
        text: "Select Skills (required)",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    }, {
      'id': 5,
      'title': 'Domains',
      'imgSrc': 'skills.png',
      'progressText': 'next',
      'key': 'domains',
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: false,
        text: "Select Domains",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    }, {
      'id': 6,
      'title': 'Certifications',
      'imgSrc': 'skills.png',
      'progressText': 'next',
      'key': 'certifications',
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: false,
        text: "Select Certifications",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    }, {
      'id': 7,
      'title': 'Work Authorizations',
      'imgSrc': 'authorization.png',
      'progressText': 'next',
      'key': 'authorizations',
      'selectFlag': true,
      'countryFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: false,
        text: "Select Authorizations",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    }, {
      'id': 8,
      'title': 'previous Companies',
      'imgSrc': 'employer.png',
      'progressText': 'next',
      'key': 'employers',
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: false,
        text: "Select Employers",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    }, {
      'id': 9,
      'title': 'Education',
      'imgSrc': 'qualification.png',
      'progressText': 'next',
      'key': 'qualifications',
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: false,
        text: "Select Qualifications",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    }, {
      'id': 10,
      'title': 'Job Boards',
      'imgSrc': 'jobboard.png',
      'progressText': 'Done',
      'key': 'vendors',
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Job Board",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    }];

  // for review fields
  candidateReviewFieldList: Array<object> = [
    {
      'id': 0,
      'title': 'Did you contact this candidate in the past (a month earlier) ?',
      'imgSrc': 'candidate-feedback.jpg',
      'progressText': 'Next',
      'key': 'knownCandidate',
      'selectFlag': true,
      'errorFlag': false,
      'dataKey': '',
      'dropdownData': [{ 'id': 0, 'name': 'No' }, { 'id': 1, 'name': 'Yes' }],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select",
        enableSearchFilter: false,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 1,
      'title': 'Candidate submitted to:',
      'imgSrc': 'candidate-feedback.jpg',
      'progressText': 'Next',
      'key': 'knownCandidateSubmitted',
      'dataKey': 'submittalStatus',
      'dependentKey': [{ 'key': 'knownCandidate', 'value': 'yes' }],
      'selectFlag': true,
      'errorFlag': false,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 2,
      'title': 'Did the candidate attend the Interview in the past?',
      'imgSrc': 'candidate-feedback.jpg',
      'progressText': 'Next',
      'key': 'knownCandidateInterviewed',
      'dataKey': '',
      'dependentKey': [{ 'key': 'knownCandidateSubmitted', 'value': 'not submitted to any' }],
      'selectFlag': true,
      'notFlag': true,
      'errorFlag': false,
      'dropdownData': [{ 'id': 0, 'name': 'No' }, { 'id': 1, 'name': 'Yes' }],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select",
        enableSearchFilter: false,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 3,
      'title': 'How many rounds of interview did the candidate attend?',
      'imgSrc': 'candidate-feedback.jpg',
      'progressText': 'Next',
      'key': 'knownCandidateInterviewRound',
      'dataKey': 'interviewRounds',
      'dependentKey': [{ 'key': 'knownCandidateInterviewed', 'value': 'yes' }],
      'selectFlag': true,
      'errorFlag': false,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 4,
      'title': 'Interview Status:',
      'imgSrc': 'candidate-feedback.jpg',
      'progressText': 'Next',
      'errorFlag': false,
      'key': 'knownCandidateInterviewResult',
      'dataKey': 'interviewStatus',
      'dependentKey': [{ 'key': 'knownCandidateInterviewed', 'value': 'yes' }],
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 5,
      'title': 'Communication Skills (Out of 10):',
      'imgSrc': 'candidate-feedback.jpg',
      'progressText': 'Next',
      'key': 'communicationSkill',
      'max': 10,
      'dataKey': '',
      'dependentKey': [{ 'key': 'knownCandidate', 'value': 'yes' }],
      'selectFlag': false,
      'inputFlag': true,
      'type': 'number',
      'placeholder': 'Rate here...',
      'data': '',
      'errorFlag': false
    },
    {
      'id': 6,
      'title': "Recruiter's Comments:",
      'imgSrc': 'candidate-feedback.jpg',
      'progressText': 'Done',
      'key': 'comments',
      'dataKey': '',
      'dependencyKey': [{ 'key': 'knownCandidate', 'value': 'yes' }],
      'selectFlag': false,
      'textareaFlag': true,
      'placeholder': 'comments...',
      'data': '',
      'errorFlag': false
    }
  ];

  candidateReviewFields: Array<object> = [];

  extraDataApi: Array<string> = [
    '/api/submittalStatus/fetchRecord',
    '/api/interviewStatus/fetchRecord',
    '/api/interviewRounds/fetchRecord'
  ];

  filterUrlList: Array<string> = [
    '/api/skills/fetchRecord',
    '/api/authorizations/fetchRecord',
    '/api/qualifications/fetchRecord',
    '/api/users/fetchRecord',
    '/api/vendors/fetchRecord',
    '/api/cities/fetchRecord',
    '/api/states/fetchRecord',
    '/api/countries/fetchRecord',
    '/api/employers/fetchRecord',
    '/api/certifications/fetchRecord',
    '/api/domains/fetchRecord',
    '/api/durations/fetchRecord',
    '/api/taxTerms/fetchRecord'
  ];

  // pop up related variables
  // modal related declarations
  showPopUpFlag: boolean = false;
  modalSubmitActionFlag: string = '';
  modalConfig: object = {};
  selectedModalOption: string;
  selectedCommunicationOption: object;
  // alert modal
  alertModalSetUp: object = {
    'title': 'Alert',
    'okFlag': true,
    'contentFlag': true,
    'content': 'There are more pending candidate records. Please fill those as priority'
  };
  modalAction: string;

  // warning pop up
  warningModalSetUp: object = {
    'title': 'Warning',
    'proceedFlag': true,
    'cancelFlag': true,
    'contentFlag': true,
    'content': 'You may lose the unsaved changes. Do you want to continue?'
  }

  // candidate detail modal declarations
  candidateModalEditFlag: boolean = true;
  candidateModalDisplayFlag: boolean = false;
  candidateModalDetails: object = {};
  communicationPopupFlag: boolean;

  // skills suggestion modal
  suggestedSkillModalDisplayFlag: boolean = false;
  suggestedSkillList: Array<string> = [];
  highlighiSkillFlag: boolean = false;
  newSkillList: Array<string> = [];
  acquiredSkillList: Array<object> = [];

  // history pop up
  historyDisplayFlag: boolean = false;
  candidateHistoryData: Array < object > = [];
  candidatePastHistoryData: Array < object > = [];

  constructor(private http: HttpClient,
    public utilityService: UtilityService,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private toastrService: ToastrService,
    private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (userData) {
      this.dataService.setLoggedUserDetails(userData);
      this.loggedUserDetails = userData;
    } else if (!userData && !localUserData) {
      this.router.navigateByUrl('');
    } else {
      this.loggedUserDetails = localUserData;
    }
    // to eradicate the ngIf data changing issue
    const scope = this;
    setTimeout(function () {
      scope.validateOperation();
    }, 0);
  }

  // event handler for finding the operation of page
  validateOperation(): void {
    const routePath = this.route.routeConfig.path;
    this.route.params.subscribe(params => {
      if (params) {
        this.candidateId = +params['candidateId'];
      }
      // page detector
      if (routePath.includes('candidate-view') && this.candidateId) {    // candidate view page
        this.viewFlag = true;
        this.editFlag = false;
        this.createFlag = false;
        this.actionTitle = 'Candidate View';
        this.documentUploadedFlag = true;
        this.manipulateDocumentWidth();
        this.getHistoryData();
        this.getPastHistoryData();
        this.historyDisplayFlag = true;
        this.getCandidateData();
      } else if (routePath.includes('candidate-edit') && this.candidateId) {   // candidate edit page
        this.viewFlag = false;
        this.editFlag = true;
        this.createFlag = false;
        this.actionTitle = 'Candidate Edit';
        this.documentUploadedFlag = true;
        this.manipulateDocumentWidth();
        this.getCandidateData();
        // do validate the data is already present in data service or not
        this.getDropdownItems();
      } else if (routePath.includes('candidate-create') && !this.candidateId) {   // candidate create page
        this.viewFlag = false;
        this.editFlag = false;
        this.createFlag = true;
        this.actionTitle = 'Candidate Create';
        this.documentUploadedFlag = false;
        this.getDropdownItems();
      } else {      // redirect to home page
        this.router.navigateByUrl('/candidate-dashboard');
      }
    });
  }

  // reset fields on submit
  resetFields(): void {
    this.candidateDetailObject = {};
    this.doc = null;
    this.iframeDoc = null;
    this.fileToUpload = null;
    this.documentUploadedFlag = false;
    this.fieldDataObject = [];
    this.actionTitle = '';
    this.uploadSkipFlag = false;
    this.candidateId = 0;
    this.documentHeight = 0;
    // this.filterDataSet = [];
    this.candidateDetailObject = {};
    // this.fieldDataArray = [];
    this.dataFlag = false;
    this.optionFlag = false;
    this.infoEnableFlag = true;
    // bulk upload variables
    if (!this.enableBulkUpload) {
      this.enableBulkUpload = false;
      this.filesToUpload = null;
      this.bulkFileCount = 0;
      this.currentFileCount = 0;
    }
  }

  // reset filter data on submit
  resetFilterData(): void {
    let editDetailList = this.filterDataSet;
    for (let i = 0; i < editDetailList.length; i++) {
      if (editDetailList[i]['multiSelect']) {           // location data feeding
        const detailList = editDetailList[i]['detailList'];
        for (let j = 0; j < detailList.length; j++) {
          detailList[j]['dropdownSelected'] = [];
          // if (!detailList[j]['dropdownSelected'])
          //   detailList[j]['dropdownSelected'] = [];
          // if (this.candidateDetailObject[detailList[j]['key']])
          //   detailList[j]['dropdownSelected'].push(this.candidateDetailObject[detailList[j]['key']]);
        }
        editDetailList[i]['detailList'] = detailList;
      } else if (editDetailList[i]['selectFlag']) {     // select box data feeding
        // if (this.candidateDetailObject[editDetailList[i]['key']])
        //   editDetailList[i]['dropdownSelected'] = this.candidateDetailObject[editDetailList[i]['key']];
        // else
          editDetailList[i]['dropdownSelected'] = [];
      } else {                                       // text box data feeding
        const detailList = editDetailList[i]['detailList'];
        for (let j = 0; j < detailList.length; j++) {
          // if (this.candidateDetailObject[detailList[j]['key']])
          //   detailList[j]['data'] = this.candidateDetailObject[detailList[j]['key']];
          // else
            detailList[j]['data'] = '';
        }
        editDetailList[i]['detailList'] = detailList;
      }
    }
    this.filterDataSet = editDetailList;
  }

  // submit trigger get call for data
  saveCandidate(): void {
    if (this.documentUploadedFlag) {
      this.utilityService.updateSpinnerFlag(true);
      this.candidateSaveFlag = true;
    } else {
      this.toastrService.error('Please upload the resume to save', 'Error', { timeOut: 2500 });
    }
  }

  getDataSet(dataObject: object): void {
    if (dataObject['flag']) {
      // validate suggested skill sync up
      // if (dataObject['data']['skills'] && this.fieldDataList[3]['dropdownSelected'] && dataObject['data']['skills'].length !== this.fieldDataList[3]['dropdownSelected'].length) {
      //   dataObject['data']['skills'] = this.fieldDataList[3]['dropdownSelected'];
      // }
      Object.assign(this.candidateDetailObject, dataObject['data']);
      // check the email & mobile
      if (this.createFlag) {
        this.validateCandidateDetails();
      } else {
        this.submitCandidate();
      }
    } else {
      this.candidateSaveFlag = false;
      this.utilityService.updateSpinnerFlag(false);
    }
  }

  // get the data for extra information of candidate
  getExtraInformation(): void {
    this.apiService.forkGetData(this.extraDataApi).subscribe(data => {
      // setting the data for fields
      for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < this.candidateReviewFieldList.length; j++) {
          const key = Object.keys(data[i])[0];
          if (key === this.candidateReviewFieldList[j]['dataKey']) {
            this.candidateReviewFieldList[j]['dropdownData'] = data[i][key];
          }
        }
      }
      this.candidateReviewFields = this.candidateReviewFieldList;
      this.utilityService.updateSpinnerFlag(false);
    });
    // this.candidateReviewFields = this.candidateReviewFieldList;
  }

  // validate candidate email & mobilenumber
  validateCandidateDetails(): void {
    const url = '/api/candidates/validateUserDetails';
    const param = {};
    param['mobileNumber'] = this.candidateDetailObject['mobileNumber'];
    param['email'] = this.candidateDetailObject['email'];
    this.apiService.postData(url, param).subscribe(data => {
      if (data) {
        if (data && data['status']) {
          // getting the extra information about user
          this.candidateReviewFields = [];
          this.getExtraInformation();
        } else {
          this.toastrService.error('Email is already exist', 'Error', { timeOut: 4000 });
          this.candidateSaveFlag = false;
          this.utilityService.updateSpinnerFlag(false);
        }
      }
    });
  }

  // submit the candidate
  submitCandidate(): void {
    let url: string;
    let apiCall: string;
    if (this.candidateDetailObject['id']) {
      url = '/api/candidates/update/' + this.candidateDetailObject['id'];
      apiCall = 'putData';
    } else {
      url = '/api/candidates/create';
      apiCall = 'postData';
    }
    this.candidateDetailObject['sourcedBy'] = this.loggedUserDetails['id'];
    this.candidateDetailObject['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
    this.candidateDetailObject['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
    this.apiService[apiCall](url, this.candidateDetailObject).subscribe(data => {
      this.candidateSaveFlag = false;
      this.utilityService.updateSpinnerFlag(false);
      let editFlag = false;
      let editId;
      if (this.candidateDetailObject['id']) {
        this.toastrService.success("Candidate Details Updated Successfully...", "Success", { timeOut: 1000 });
        editFlag = true;
        editId = this.candidateDetailObject['id'];
      } else {
        this.toastrService.success("Candidate Saved Successfully...", "Success", { timeOut: 1000 });
      }
      if (this.createFlag && this.createCommunicationModalflag === false && data['id']) {
        this.candidateId = data['id'];
        this.candidateDetailSave(null);
      }
      this.resetFields();
      this.resetFilterData();
      if (this.enableBulkUpload) {
        if (this.currentFileCount !== this.bulkFileCount - 1) {
          this.currentFileCount++;
          this.documentSelected(this.filesToUpload, true, true);
        } else {
          this.bulkUploadProgressFlag = false;
          this.docSubmitFlag = false;
          const url = '/api/candidates/getPendingCount/' + this.loggedUserDetails['id'];
          this.apiService.getData(url).subscribe(data => {
            if (data['count'] > 10) {
              this.utilityService.setPendingFlag(true);
              this.modalConfig = this.alertModalSetUp;
              this.showPopUpFlag = true;
              this.modalAction = 'pendingNavigation';
            } else {
              this.utilityService.setPendingFlag(false);
              if (editFlag) {
                const url = '/candidate-view/' + editId;
                editFlag = false;
                this.router.navigateByUrl(url);
              } else {
                this.router.navigateByUrl('/candidate-dashboard');
              }
            }
          });
        }
      } else {
        const url = '/api/candidates/getPendingCount/' + this.loggedUserDetails['id'];
        this.apiService.getData(url).subscribe(data => {
          if (data['count'] > 10) {
            this.utilityService.setPendingFlag(true);
            this.modalConfig = this.alertModalSetUp;
            this.showPopUpFlag = true;
            this.modalAction = 'pendingNavigation';
            this.router.navigateByUrl('/pending-candidate-dashboard');
          } else {
            this.utilityService.setPendingFlag(false);
            if (editFlag) {
              const url = '/candidate-view/' + editId;
              editFlag = false;
              this.router.navigateByUrl(url);
            } else {
              this.router.navigateByUrl('/candidate-dashboard');
            }
          }
        });
      }
    });
  }

  // submit bulk candidate upload
  bulkUploadCandidates(): void {
    this.bulkUploadPendingCandidateFlag = true;
    this.candidateDetailObject['fileName'] = null;
    // upload resumes for pending records
    this.pendingCandidateInitialFlag = true;
    this.pendingCandidateDataList = [];
    this.utilityService.updateSpinnerFlag(true);
    this.uploadPendingCandidateResume(this.filesToUpload);
  }

  // submit pending candidate with resumes
  submitPendingCandidates(): void {
    const url = '/api/candidates/createPendingRecord';
    for (let i = 0; i < this.pendingCandidateDataList.length; i++) {
      this.pendingCandidateDataList[i]['sourcedBy'] = this.loggedUserDetails['id'];
      this.pendingCandidateDataList[i]['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      this.pendingCandidateDataList[i]['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
    }
    this.apiService.postData(url, this.pendingCandidateDataList).subscribe(data => {
      const url = '/api/candidates/getPendingCount/' + this.loggedUserDetails['id'];
      this.apiService.getData(url).subscribe(data => {
        this.utilityService.updateSpinnerFlag(false);
        this.bulkUploadPendingCandidateFlag = false;
        this.pendingCandidateDataList = [];
        this.toastrService.success("Candidate Saved Successfully...", "Success", { timeOut: 1000 });
        if (data['count'] > 10) {
          this.utilityService.setPendingFlag(true);
          this.modalConfig = this.alertModalSetUp;
          this.showPopUpFlag = true;
          this.modalAction = 'pendingNavigation';
        } else {
          this.utilityService.setPendingFlag(false);
          this.router.navigateByUrl('/candidate-dashboard');
        }
      });
    });
  }

  // handle add new row in dropdown
  handleAddNewItem(event: object): void {
    const url = '/api/' + event['key'] + '/create';
    this.apiService.postData(url, event).subscribe(data => {
      if (data && data[event['key']] && data[event['key']].length !== 0) {
        this.newlyAddedId = null;
        const obj = {};
        obj['id'] = data[event['key']][0]['id'];
        obj['name'] = event['name'];
        obj['key'] = event['key'];
        this.newlyAddedId = obj;
      }
    });
  }

  // handler for detail tile done event
  handleDetailComplete(): void {
    // this.infoEnableFlag = true;
    // this.submitCandidate();
    this.candidateReviewFields = [];
    this.createCommunicationModalflag = true;
    this.candidateModalDetails = this.candidateDetailObject;
    this.candidateModalDisplayFlag = true;
  }

  // handle selected data in pop up
  handleselectedData(selectedData: object): void {
    this.candidateDetailObject[selectedData['key']] = selectedData['data'][selectedData['key']];
  }

  // set candidate data for view & edit page
  setCandidateData(): void {
    if (this.viewFlag) {
      let detailList = this.fieldSetArray;
      for (let i = 0; i < detailList.length; i++) {
        if (this.candidateDetailObject[this.fieldSetArray[i]['key']]) {
          if (this.fieldSetArray[i]['key'] === 'firstName' && this.viewFlag) {
            detailList[i]['data'] = this.candidateDetailObject['firstName'];
            if (this.candidateDetailObject['lastName'])
              detailList[i]['data'] += ', ' + this.candidateDetailObject['lastName'];
          } else {
            detailList[i]['data'] = this.candidateDetailObject[this.fieldSetArray[i]['key']];
          }
        }
      }
      this.fieldDataArray = detailList;
    } else {
      let editDetailList = this.fieldDataList;
      for (let i = 0; i < editDetailList.length; i++) {
        if (editDetailList[i]['multiSelect']) {           // location data feeding
          const detailList = editDetailList[i]['detailList'];
          for (let j = 0; j < detailList.length; j++) {
            detailList[j]['dropdownSelected'] = this.candidateDetailObject[detailList[j]['key']];
          }
          editDetailList[i]['detailList'] = detailList;
        } else if (editDetailList[i]['selectFlag']) {     // select box data feeding
          editDetailList[i]['dropdownSelected'] = this.candidateDetailObject[editDetailList[i]['key']];
        } else {                                          // text box data feeding
          const detailList = editDetailList[i]['detailList'];
          for (let j = 0; j < detailList.length; j++) {
            detailList[j]['data'] = this.candidateDetailObject[detailList[j]['key']];
          }
          editDetailList[i]['detailList'] = detailList;
        }
      }
      this.fieldDataList = editDetailList;
      // trigger config object setter method
      this.dataFlag = true;
      this.editDetailConfigSetter();
    }
    // assigning the document to local object
    if (this.candidateDetailObject['fileName'] && this.candidateDetailObject['fileName'] != 'null') {
      this.doc = 'http://pqrdb.geval6.com/api/resumes/' + this.candidateDetailObject['fileName']; // http://pqrdb.geval6.com/api/resumes/
      // this.doc = 'http://pqrdb.geval6.com/api/resumes/resume_candidate_13.doc';
      // const docViewerUrl = 'https://docs.google.com/gview?url=' + this.doc;
      // validate the viewer that support pdf or not
      const fileNameSplitter = (this.candidateDetailObject['fileName']).split('.');
      const fileType = fileNameSplitter[fileNameSplitter.length - 1];
      let docViewerUrl;
      if ((fileType).toLowerCase() == 'pdf') {
        docViewerUrl = 'https://docs.google.com/gview?url=' + this.doc + '&embedded=true';
        // docViewerUrl = 'https://docs.google.com/viewer?url=' + this.doc + '&embeded=true';
      } else {
        docViewerUrl = 'https://view.officeapps.live.com/op/embed.aspx?src=' + this.doc;
        // docViewerUrl = 'https://docs.google.com/viewer?url=' + this.doc + '&embeded=true';
      }
      this.iframeDoc = this.sanitizer.bypassSecurityTrustResourceUrl(docViewerUrl);
      if (this.editFlag) {
        const fileDetails = { 'fileName': './candidateResumes/' + this.candidateDetailObject['fileName'] };
        this.apiService.postData('/api/candidates/suggestedSkills', fileDetails).subscribe(data => {
          this.fetchSuggestedSkills(data);
          this.utilityService.updateSpinnerFlag(false);
        });
      }
    } else {
      this.doc = null;
      this.iframeDoc = null;
      this.utilityService.updateSpinnerFlag(false);
    }
  }

  // get candidate data
  getCandidateData(): void {
    this.utilityService.updateSpinnerFlag(true);
    // let candidateData = this.dataService.getSelectedCandidateDetail();
    // if (candidateData) {
    //   this.candidateDetailObject = candidateData;
    //   this.setCandidateData();
    // } else {
      const url = '/api/candidates/fetchRecord/' + this.candidateId;
      this.apiService.getData(url).subscribe(data => {
        this.dataService.setSelectedCandidateDetail(data['candidates'][0]);
        this.candidateDetailObject = data['candidates'][0];
        this.setCandidateData();
      });
    // }
  }

  // set the configuration of detail tiles
  editDetailConfigSetter(): void {
    if (this.dataFlag && this.optionFlag) {
      this.filterDataSet = this.fieldDataList;
      this.dataFlag = false;
      this.optionFlag = false;
    } else if (this.createFlag && this.optionFlag) {
      this.filterDataSet = this.fieldDataList;
      this.optionFlag = false;
    }
  }

  // update the filter data into configuration
  updateFilterData(filterData: Array<object>): void {
    // skip inner loop by assuming the data is coming in same order given
    for (let i = 0; i < this.fieldDataList.length; i++) {
      for (let j = 0; j < filterData.length; j++) {
        const key = Object.keys(filterData[j])[0];
        if (this.fieldDataList[i]['multiSelect'] || this.fieldDataList[i]['optional']) {           // location data feeding
          const detailList = this.fieldDataList[i]['detailList'];
          for (let k = 0; k < detailList.length; k++) {
            if (key == this.fieldDataList[i]['detailList'][k]['key'] && this.fieldDataList[i]['detailList'][k]['selectFlag']) {
              this.fieldDataList[i]['detailList'][k]['dropdownData'] = filterData[j][key];
            }
          }
        } else if (this.fieldDataList[i]['selectFlag']) {
          if (key == this.fieldDataList[i]['key'])
            this.fieldDataList[i]['dropdownData'] = filterData[j][this.fieldDataList[i]['key']];
        }
      }
    }
    // trigger config setter method
    this.optionFlag = true;
    this.editDetailConfigSetter();
    // this.utilityService.updateSpinnerFlag(false);
  }

  // enable detail tile flow
  enableInfoDetails(): void {
    this.documentUploadedFlag = true;
    const backupConfig = this.filterDataSet;
    this.filterDataSet = [];
    this.filterDataSet = backupConfig;
  }

  // get filter dropdown values
  getDropdownItems(): void {
    const filterDataBackup = this.dataService.getFieldDataList();
    // enable loader
    // this.utilityService.updateSpinnerFlag(true);
    if (filterDataBackup && filterDataBackup.length !== 0) {
      this.updateFilterData(filterDataBackup);
    } else {
      this.apiService.forkGetData(this.filterUrlList).subscribe(data => {
        //this.dataService.setFieldDataList(data);
        this.updateFilterData(data);
      });
    }
  }

  // bulk upload option enable handller
  uploadBulkResume(): void {
    this.enableBulkUpload = true;
  }

  // event handler for document selected
  documentSelected(e: any, flag?: boolean, bulkFlag?: boolean): void {
    this.enableBulkUpload = bulkFlag;
    // validate for bulk upload
    if (this.enableBulkUpload && flag && e && e.length !== 0) {
      if (this.files && this.files.length !== 0) {
        this.fileToUpload = e[this.currentFileCount];
      } else {
        this.fileToUpload = e[this.currentFileCount];
      }
      this.submitDocument();
    } else if (this.enableBulkUpload && e && e.length !== 0) {
      this.files = null;
      const updatedFileList = [];
      const fileArray = e;
      for (let i = 0; i < e.length; i++) {
        let flag = this.validateFileType(e[i]);
        if (flag) {
          updatedFileList.push(e.item(i));
        } else {
          fileArray.splice(i);
          this.toastrService.error('Please select proper file to proceed', 'Unsupported File type', { timeOut: 3000 });
        }
      }
      // this.multiUpload.nativeElement.value = fileArray;
      this.bulkFileCount = updatedFileList.length;
      this.filesToUpload = updatedFileList;
      this.currentFileCount = 0;
      // nullify other fields
      this.deselect();
      this.singleUpload.nativeElement.value = '';
      this.fileToUpload = updatedFileList[this.currentFileCount];
    } else {
      this.files = null;
      let flag = this.validateFileType(e.item(0));
      if (flag) {
        this.fileToUpload = e.item(0);
        this.filesToUpload = null;
      } else {
        this.singleUpload.nativeElement.value = '';
        this.toastrService.error('Please select proper file to proceed', 'Unsupported File type', { timeOut: 3000 });
      }
      // nullify other fields
      this.multiUpload.nativeElement.value = '';
      this.deselect();
    }
  }

  // resume upload for pending candidates
  uploadPendingCandidateResume(e: any): void {
    this.fileToUpload = e[this.currentFileCount];
    // get count for naming
    if (this.pendingCandidateInitialFlag) {
      const countUrl = '/api/candidates/getCount';
      this.apiService.getData(countUrl).subscribe(data => {
        this.pendingCandidateInitialFlag = false;
        this.pendingRecordCount = parseInt(data['count']) + 1;
        const fileNameSplitArray = (this.fileToUpload.name).split('.');
        const fileType = fileNameSplitArray[fileNameSplitArray.length - 1];
        const namePrefix = 'resume_candidate_' + this.pendingRecordCount;
        const currentDate = new Date();
        const dateFormat = currentDate.getDate() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getFullYear() + '_' + currentDate.getHours() + '_' + currentDate.getMinutes() + '_' + currentDate.getSeconds();
        const fileName = 'resume_candidate_' + this.pendingRecordCount + '_' + dateFormat + '.' + fileType;
        this.candidateDetailObject['fileName'] = fileName;
        const formData: FormData = new FormData();
        formData.append('files', this.fileToUpload, fileName);
        formData.append('fileName', namePrefix);
        let dataObject = {};
        Object.assign(dataObject, this.candidateDetailObject);
        this.pendingCandidateDataList.push(dataObject);
        this.uploadResume(formData);
      });
    } else {
      this.pendingRecordCount = this.pendingRecordCount + 1;
      const fileNameSplitArray = (this.fileToUpload.name).split('.');
      const fileType = fileNameSplitArray[fileNameSplitArray.length - 1];
      const namePrefix = 'resume_candidate_' + this.pendingRecordCount;
      const currentDate = new Date();
      const dateFormat = currentDate.getDate() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getFullYear() + '_' + currentDate.getHours() + '_' + currentDate.getMinutes() + '_' + currentDate.getSeconds();
      const fileName = 'resume_candidate_' + this.pendingRecordCount + '_' + dateFormat + '.' + fileType;
      this.candidateDetailObject['fileName'] = fileName;
      const formData: FormData = new FormData();
      formData.append('files', this.fileToUpload, fileName);
      formData.append('fileName', namePrefix);
      let dataObject = {};
      Object.assign(dataObject, this.candidateDetailObject);
      this.pendingCandidateDataList.push(dataObject);
      this.uploadResume(formData);
    }
  }

  // validate required field for file upload
  validateFileUploadField(): boolean {
    if (this.fileToUpload) {
      return true;
    } else {
      return false;
    }
  }

  // event handler for submit document
  submitDocument(): void {
    // validate required field
    const validateFlag = this.validateFileUploadField();
    if (!validateFlag) {
      this.toastrService.error('Please upload resume to proceed...', 'Error', { timeOut: 2000 });
      return;
    }
    // enable loader
    this.utilityService.updateSpinnerFlag(true);
    // validate bulk upload flag
    if (this.filesToUpload && this.filesToUpload.length > 1) {
      this.enableBulkUpload = true;
    }
    if (this.enableBulkUpload) {
      this.docSubmitFlag = true;
    }
    if (this.createFlag || !this.candidateDetailObject['fileName']) {
      const countUrl = '/api/candidates/getCount';
      this.apiService.getData(countUrl).subscribe(data => {
        const recordCount = parseInt(data['count']) + 1;
        const fileNameSplitArray = (this.fileToUpload.name).split('.');
        const fileType = fileNameSplitArray[fileNameSplitArray.length - 1];
        const namePrefix = 'resume_candidate_' + recordCount;
        const currentDate = new Date();
        const dateFormat = currentDate.getDate() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getFullYear() + '_' + currentDate.getHours() + '_' + currentDate.getMinutes() + '_' + currentDate.getSeconds();
        const fileName = 'resume_candidate_' + recordCount + '_' + dateFormat + '.' + fileType;
        this.candidateDetailObject['fileName'] = fileName;
        const formData: FormData = new FormData();
        formData.append('files', this.fileToUpload, fileName);
        formData.append('fileName', namePrefix);
        this.uploadResume(formData);
      });
    } else {
      const fileName = this.candidateDetailObject['fileName'];
      const formData: FormData = new FormData();
      formData.append('files', this.fileToUpload, fileName);
      this.uploadResume(formData);
    }
  }

  // upload resume of candidate
  uploadResume(formData): void {
    if (this.enableBulkUpload) {
      this.bulkUploadProgressFlag = true;
    }
    this.apiService.postData('/api/candidates/uploadResume', formData).subscribe(data => {
      // nullify the fields
      this.files = [];
      if (this.bulkUploadPendingCandidateFlag) {
        if (this.currentFileCount !== this.bulkFileCount - 1) {
          this.currentFileCount++;
          this.uploadPendingCandidateResume(this.filesToUpload);
        } else {
          this.submitPendingCandidates();
        }
      } else {
        const fileDetails = { 'fileName': data['fileName'] };
        this.apiService.postData('/api/candidates/suggestedSkills', fileDetails).subscribe(data => {
          this.fetchSuggestedSkills(data);
          this.utilityService.updateSpinnerFlag(false);
        });
        this.doc = 'http://pqrdb.geval6.com/api/resumes/' + this.candidateDetailObject['fileName']; // http://pqrdb.geval6.com/api/resumes/
        // this.doc = 'http://pqrdb.geval6.com/api/resumes/resume_candidate_13.doc';
        // const docViewerUrl = 'https://docs.google.com/gview?url=' + this.doc;
        const fileNameSplitter = (this.candidateDetailObject['fileName']).split('.');
        const fileType = fileNameSplitter[fileNameSplitter.length - 1];
        let docViewerUrl;
        if ((fileType).toLowerCase() == 'pdf') {
          docViewerUrl = 'https://docs.google.com/gview?url=' + this.doc + '&embedded=true';
          // docViewerUrl = 'https://docs.google.com/viewer?url=' + this.doc + '&embeded=true';
        } else {
          docViewerUrl = 'https://view.officeapps.live.com/op/embed.aspx?src=' + this.doc;
          // docViewerUrl = 'https://docs.google.com/viewer?url=' + this.doc + '&embeded=true';
        }
        this.iframeDoc = this.sanitizer.bypassSecurityTrustResourceUrl(docViewerUrl);
        this.documentUploadedFlag = true;
        this.manipulateDocumentWidth();
      }
    });
  }

  // fetch & display suggested skills
  fetchSuggestedSkills(data: any): void {
    // update skills selected
    if (data && data['acquiredSkills'] && data['acquiredSkills'].length !== 0) {
      /*const filterData = this.filterDataSet;
      for (let i = 0; i < filterData.length; i++) {
        if (filterData[i]['key'] === 'skills') {
          filterData[i]['dropdownSelected'] = data['acquiredSkills'];
        }
      } */
      this.acquiredSkillList = data['acquiredSkills'];
      //this.filterDataSet = filterData;
    } else {  // resetting empty for new uploads
      /*const filterData = this.filterDataSet;
      for (let i = 0; i < filterData.length; i++) {
        if (filterData[i]['key'] === 'skills') {
          filterData[i]['dropdownSelected'] = [];
        }
      } */
      this.acquiredSkillList = [];
      // this.filterDataSet = filterData;
    }
    // update suggestion skills in pop up & open it
    if (data && data['suggestSkills'] && data['suggestSkills'].length !== 0) {
      this.suggestedSkillList = data['suggestSkills'];
      if ((this.editFlag && this.candidateDetailObject['isPending'] === 1) || this.createFlag)
        this.suggestedSkillModalDisplayFlag = true;
    } else {
      this.suggestedSkillList = [];
      this.suggestedSkillModalDisplayFlag = false;
      this.toastrService.info('No Skill Suggestion Available', 'Skill Suggestions');
    }
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 203;
  }

  // disable the resume upload block
  skipUploadResume(): void {
    this.documentUploadedFlag = true;
    this.uploadSkipFlag = true;
    this.doc = null;
    this.iframeDoc = null;
    this.manipulateDocumentWidth();
  }

  // enable the resume upload block
  enableUploadResume(): void {
    this.documentUploadedFlag = false;
    this.uploadSkipFlag = false;
    this.enableBulkUpload = false;
    this.bulkUploadSingle = false;
    this.resetFields();
    this.resetFilterData();
    this.getDropdownItems();
  }

  // enable the resume upload during bulk upload
  enableUploadBulkResume(): void {
    this.documentUploadedFlag = false;
    this.uploadSkipFlag = false;
    this.bulkUploadSingle = true;
  }

  // download resume
  downloadResume(): void {
    window.open(this.doc, '_self');
    this.toastrService.success('Resume downloaded successfully...', 'Success', { timeOut: 2500 });
  }

  // edit candidate
  editCandidate(): void {
    const url = 'candidate-edit/' + this.candidateId;
    this.router.navigateByUrl(url);
  }

  // cancel confirmation
  cancelConfirmation(): void {
    if (this.fileToUpload) {
      this.modalConfig = this.warningModalSetUp;
      this.showPopUpFlag = true;
      this.modalAction = 'cancel';
    } else {
      this.cancelChanges();
    }
  }

  // cancel changes
  cancelChanges(): void {
    if (this.enableBulkUpload) {
      if (this.currentFileCount !== this.bulkFileCount - 1) {
        this.currentFileCount++;
        this.documentSelected(this.filesToUpload, true, true);
        this.doc = null;
        this.iframeDoc = null;
      } else {
        this.bulkUploadProgressFlag = false;
        this.docSubmitFlag = false;
        this.router.navigateByUrl('/candidate-dashboard');
      }
    } else {
      if (this.utilityService.previousUrl === 'pending-candidate-dashboard') {
        this.router.navigateByUrl('pending-candidate-dashboard');
      } else {
        this.router.navigateByUrl('candidate-dashboard');
      }
    }
    this.showPopUpFlag = false;
  }

  // handle sucess event for document loaded in viewer
  handleDocumentLoadSucess(event): void {
    // disable loader
    this.utilityService.updateSpinnerFlag(false);
    // accessibility mode setting
    //const scope = this;
    //setTimeout(function() {
    //const innerDoc = this.iframeElement.nativeElement.contentDocument;
    // document.domain = 'http://localhost:4200';
    // const iframe = document.getElementById('iframeElement');
    // const innerDoc = iframe.contentDocument || iframe.contentWindow.document;
    //const menuElement = innerDoc.getElementById('ControlMenu-Small14');
    //console.log("^%%^     ", menuElement);
    //alert(menuElement);
    //menuElement.click();
    this.fieldDataObject = this.fieldDataList;
  }

  // handle close modal
  handleCloseModal(flag): void {
    this.showPopUpFlag = flag;
    this.selectedModalOption = null;
  }

  // handle save modal
  handleSaveModal(data): void {
    if (this.selectedModalOption === 'communication') {
      if (data && data['communication'] && data['communication'].length !== 0) {
        if (data['communication'][0]['id'] === 1) {
          this.selectedCommunicationOption = data['communication'][0];
          this.selectedModalOption = 'communicationFull';
          this.handleCloseModal(false);
          this.openModal(null);
        } else {
          this.handleCloseModal(false);
          if (this.modalSubmitActionFlag === 'back') {
            this.router.navigateByUrl('candidate-dashboard');
          } else if (this.modalSubmitActionFlag === 'edit') {
            this.editCandidate();
          } else if (this.modalSubmitActionFlag === 'dashboard') {
            this.router.navigateByUrl('dashboard');
          }
        }
      }
    } else {
      // push candidate availability status of candidate
      const url = '/api/candidates/updateCandidateAvailability/' + this.candidateId;
      data['sourcedBy'] = this.loggedUserDetails['id'];
      data['communication'] = this.selectedCommunicationOption;
      this.apiService.putData(url, data).subscribe(data => {
        this.toastrService.success("Candidate Updated Successfully...", "Success", { timeOut: 1000 });
        this.handleCloseModal(false);
        if (this.modalSubmitActionFlag === 'back') {
          this.router.navigateByUrl('candidate-dashboard');
        } else if (this.modalSubmitActionFlag === 'edit') {
          this.editCandidate();
        } else if (this.modalSubmitActionFlag === 'dashboard') {
          this.router.navigateByUrl('dashboard');
        }
      });
    }
  }

  // handler proceed
  proceedModal(): void {
    if (this.selectedModalOption === 'communication') {
      this.selectedModalOption = 'communicationFull';
    } else {
      if (this.modalAction === 'pendingNavigation') {
        this.router.navigateByUrl('pending-candidate-dashboard');
      } else if (this.modalAction === 'cancel') {
        this.cancelChanges();
      }
    }
  }

  // skip handler
  skipModal(): void {
    this.handleCloseModal(false);
    if (this.modalSubmitActionFlag === 'back') {
      this.router.navigateByUrl('candidate-dashboard');
    } else if (this.modalSubmitActionFlag === 'edit') {
      this.editCandidate();
    } else if (this.modalSubmitActionFlag === 'dashboard') {
      this.router.navigateByUrl('dashboard');
    }
  }

  // open modal
  openModal(action?: any, key?: string, viewFlag?: boolean): void {
    // back up the action for later purpose
    if (action) {
      this.modalSubmitActionFlag = action;
    }
    if (!this.selectedModalOption)
      this.selectedModalOption = key;

    if (viewFlag)
      this.communicationPopupFlag = false;
    else
      this.communicationPopupFlag = true;
    // if (this.selectedModalOption === 'communication') {
    //   this.modalConfig = this.communicateModalSetUp;
    //   this.showPopUpFlag = true;
    //} else {
    // fetch availability detail of candidate
    const url = '/api/candidates/fetchAvailableCandidates/' + this.candidateId;
    this.apiService.getData(url).subscribe(data => {
      const candidateData = data['candidates'][0];
      // if (candidateData['userName']) {
      //   for (let i = 0; i < this.modalConfigSetUp['fieldList'].length; i++) {
      //     if (this.modalConfigSetUp['fieldList'][i]['selectFlag']) {
      //       if (candidateData[this.modalConfigSetUp['fieldList'][i]['selectConfig']['key']]) {
      //         this.modalConfigSetUp['fieldList'][i]['selectConfig']['dropdownSelected'] = [{ 'id': 1, 'name': 'Yes' }];
      //       } else {
      //         this.modalConfigSetUp['fieldList'][i]['selectConfig']['dropdownSelected'] = [{ 'id': 0, 'name': 'No' }];
      //       }
      //     } else if (this.modalConfigSetUp['fieldList'][i]['inputFlag']) {
      //       if (this.modalConfigSetUp['fieldList'][i]['inputConfig']['key'] === 'communication') {
      //         this.modalConfigSetUp['fieldList'][i]['inputConfig']['data'] = this.selectedCommunicationOption['name'];
      //       } else {
      //         this.modalConfigSetUp['fieldList'][i]['inputConfig']['data'] = candidateData[this.modalConfigSetUp['fieldList'][i]['inputConfig']['key']];
      //       }
      //     } else {
      //       this.modalConfigSetUp['fieldList'][i]['textareaConfig']['data'] = candidateData[this.modalConfigSetUp['fieldList'][i]['textareaConfig']['key']];
      //     }
      //   }
      // } else {
      //   this.modalConfigSetUp['fieldList'][0]['inputConfig']['data'] = this.selectedCommunicationOption['name'];
      // }
      // this.modalConfig = this.modalConfigSetUp;
      // this.showPopUpFlag = true;
      this.candidateModalDetails = candidateData;
      this.candidateModalDisplayFlag = true;
    });
    //}
  }

  // navigate to dashboard
  navigateToDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // communication & interview modal methods

  // cancel the candidate detail modal
  candidateDetailCancel(): void {
    this.candidateModalDisplayFlag = false;
  }

  // proceed the candidate detail modal
  candidateDetailProceed(): void {
    this.candidateModalDisplayFlag = false;
    if (this.modalSubmitActionFlag === 'back') {
      this.router.navigateByUrl('candidate-dashboard');
    } else if (this.modalSubmitActionFlag === 'edit') {
      this.editCandidate();
    } else if (this.modalSubmitActionFlag === 'dashboard') {
      this.router.navigateByUrl('dashboard');
    }
  }

  // save the candidate detail
  candidateDetailSave(candidateDetails: object): void {
    this.candidateModalDisplayFlag = false;
    if (candidateDetails)
      this.candidateAdvancedDetail = candidateDetails;
    if (this.createFlag && this.createCommunicationModalflag) {
      this.createCommunicationModalflag = false;
      this.submitCandidate();
    } else {
      // push candidate availability status of candidate
      const url = '/api/candidates/updateCandidateAvailability/' + this.candidateId;
      this.candidateAdvancedDetail['submissionDetails']['candidateId'] = this.candidateId;
      this.candidateAdvancedDetail['interviewDetails']['candidateId'] = this.candidateId;
      this.apiService.putData(url, this.candidateAdvancedDetail).subscribe(data => {
        if (!this.createFlag)
          this.toastrService.success("Candidate Updated Successfully...", "Success", { timeOut: 1000 });
        this.candidateDetailCancel();
        if (this.modalSubmitActionFlag === 'back') {
          this.router.navigateByUrl('candidate-dashboard');
        } else if (this.modalSubmitActionFlag === 'edit') {
          this.editCandidate();
        } else if (this.modalSubmitActionFlag === 'dashboard') {
          this.router.navigateByUrl('dashboard');
        }
      });
    }
  }

  // previous route change
  previousRoute(): void {
    if (this.createFlag) {
      this.router.navigateByUrl('candidate-dashboard');
    } else {
      const url = 'candidate-view/' + this.candidateDetailObject['id'];
      this.router.navigateByUrl(url);
    }
  }

  // open suggestion modal
  openSuggestionModal(): void {
    this.suggestedSkillModalDisplayFlag = true;
  }

  // close suggested skill modal
  closeSuggestedSkillModal(): void {
    this.suggestedSkillModalDisplayFlag = false;
  }

  // handle add skill
  handleAddSkill(event: string): void {
    this.newSkillList = [event];
  }

  // handle all skill event
  handleAddAllSkills(event: Array<string>): void {
    this.newSkillList = event;
  }

  // dropzone methods
  // onSingleSelect(event: any): void {
  //   const flag = this.validateFileType(event.addedFiles[0], true);
  //   if (flag) {
  //     this.files = [event.addedFiles[0]];
  //     this.fileToUpload = event.addedFiles[0];
  //   } else {
  //     this.toastrService.error('Please select proper file to proceed', 'Unsupported File type', { timeOut: 3000 });
  //   }
  // }

  onRemove(event: any): void {
    this.files.splice(this.files.indexOf(event), 1);
    this.validateFileSelection();
  }

  // deselect the selection on other mode in action
  deselect(flag?: boolean): void {
    this.files = [];
  }

  onMultiSelect(event: any): void {
    this.files.push(...event.addedFiles);
    this.singleUpload.nativeElement.value = '';
    this.multiUpload.nativeElement.value = '';
    this.filesToUpload = null;
    this.fileToUpload = null;
    this.validateFileSelection();
  }

  // selection validation
  validateFileSelection(): void {
    const formattedFileList = [];
    if (this.files && this.files.length > 1) {
      // validate the file format
      for (let i = 0; i < this.files.length; i++) {
        const flag = this.validateFileType(this.files[i]);
        if (flag) {
          formattedFileList.push(this.files[i]);
        } else {
          this.toastrService.error('Removing the file from multiple files', 'Unsupported File type', { timeOut: 3000 });
        }
      }
      this.files = formattedFileList;
      this.bulkFileCount = this.files.length;
      this.filesToUpload = this.files;
      this.currentFileCount = 0;
      this.fileToUpload = this.files[0];
    } else if (this.files && this.files.length === 1) {
      const flag = this.validateFileType(this.files[0]);
      if (flag) {
        this.fileToUpload = this.files[0];
        this.filesToUpload = null;
      } else {
        this.filesToUpload = null;
        this.fileToUpload = null;
        this.files = [];
        this.toastrService.error('Please select proper file to proceed', 'Unsupported File type', { timeOut: 3000 });
      }
    } else {
      this.fileToUpload = null;
      this.filesToUpload = null;
      // this.toastrService.error('Please select proper file to proceed', 'Unsupported File type', { timeOut: 3000 });
    }
  }

  // validate the file type to be allowed
  validateFileType(file: any): boolean {
    let fileType = '';
    if (file) {
      const fileName = file.name;
      const fileNameSplitArray = (fileName).split('.');
      fileType = fileNameSplitArray[fileNameSplitArray.length - 1];
    } else {
      return false;
    }
    if (fileType === 'doc' || fileType === 'docx' || fileType === 'pdf') {
      return true;
    } else {
      return false;
    }
  }

  // delete confirmation pop up
  deleteConfirmation(): void {
    
  }

  // delete candidate record - hard delete
  deleteCandidate(): void {
    const url = '/api/candidates/delete/' + this.candidateDetailObject['id'];
    this.apiService.deleteData(url).subscribe(data => {
      this.toastrService.success('Candidate Deleted Successfully...', 'Candidate Deletion', { timeOut: 3000 });
      this.router.navigateByUrl('candidate-dashboard');
    });
  }

  // block listing candidate
  blockListCandidatePopup(): void {

  }

  // blocklist candidate update
  blockListCandidate(): void {
    const url = '/api/candidates/blockCandidate/' + this.candidateId;
    this.apiService.putData(url, null).subscribe(data => {
      this.toastrService.success('Candidate Blocklisted Successfully...', 'Success', { timeOut: 1500 });
      this.router.navigateByUrl('candidate-dashboard');
    });
  }

  // communication pop up in view mode
  viewCommunicationPopup(): void {
    this.openModal(null, 'communication', true);
  }

  // rock star pop up
  enableRockStar(flag: boolean): void {
    this.rockStarFlag = flag;
  }

  // handle close event
  handleHistoryClose(): void {
    this.historyDisplayFlag = false;
  }

  // get history data
  getHistoryData(): void {
    const url = '/api/history/fetchRecordByCandidates/' + this.candidateId;
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.candidateHistoryData = data['history'];
      }
    })
  }

  // get past history data
  getPastHistoryData(): void {
    const url = '/api/history/fetchPastHistory/' + this.candidateId;
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.candidatePastHistoryData = data['history'];
      }
    })
  }

  // open history modal
  enableHistoryPopUp(): void {
    this.historyDisplayFlag = true;
  }

}
