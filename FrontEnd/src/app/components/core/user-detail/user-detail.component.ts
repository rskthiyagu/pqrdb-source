import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { DataService } from '../../../services/data.service';
import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  selectedUserData: object = {
    'firstName': '',
    'lastName': '',
    'userName': '',
    'designation': '',
    'mobileNumber': '',
    'email': '',
    'fbLink': '',
    'twitterLink': '',
    'linkedinLink': '',
    'coverImage': '',
    'profileImage': ''
  };
  userId: number;
  documentHeight: number;
  loggedUserDetails: object;

  constructor(private dataService: DataService,
              private apiService: ApiService,
              public utilityService: UtilityService,
              private route: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (userData) {
      this.dataService.setLoggedUserDetails(userData);
      this.loggedUserDetails = userData;
    } else if (!userData && !localUserData) {
      this.router.navigateByUrl('');
    } else {
      this.loggedUserDetails = localUserData;
    }
    this.getUserId();
    const userDataValue = this.dataService.getUserData();
    if (userDataValue) {
      this.selectedUserData = userDataValue;
    } else {
      this.getUserData();
    }
    this.manipulateDocumentWidth();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 231;
  }

  // get the user id
  getUserId(): void {
    this.route.params.subscribe(params => {
      if (params) {
        this.userId = +params['userId'];
      }
    });
  }


  // get user data
  getUserData(): void {
    const url = '/api/users/fetchRecord/' + this.userId;
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.getData(url).subscribe(data => {
      this.utilityService.updateSpinnerFlag(false);
      this.selectedUserData = data['user'][0];
    });
  }

  // approve user
  approveUser(): void {
    const url = '/api/users/activateUser/' + this.userId;
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.putData(url, null).subscribe( data => {
      this.utilityService.updateSpinnerFlag(false);
      this.toastrService.success('User approved successfully...', 'Success', {timeOut: 2000});
      this.cancel();
    });
  }

  // reject user
  rejectUser(): void {
    const url = '/api/users/deactivateUser/' + this.userId;
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.putData(url, null).subscribe( data => {
      this.utilityService.updateSpinnerFlag(false);
      this.toastrService.success('User rejected successfully...', 'Success', {timeOut: 2000});
      this.cancel();
    });
  }

  // cancel user
  cancel(): void {
    this.router.navigateByUrl('user-access');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('user-access');
  }

}
