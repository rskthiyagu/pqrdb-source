import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-interview-dashboard',
  templateUrl: './interview-dashboard.component.html',
  styleUrls: ['./interview-dashboard.component.scss']
})
export class InterviewDashboardComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  filterDataSet: Array < object > = [];
  interviewRecord: Array < object > = [];
  documentHeight: number;
  interviewModalDisplayFlag: boolean = false;
  filterDataSetConfig: Array < object > = [
    {
      'title': 'Clients', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      comingSoonFlag: true
    },
    {
      'title': 'Requirement', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      comingSoonFlag: true
    },
    {
      'title': 'Status', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      comingSoonFlag: true
    },
    {
      'title': 'Interview Rounds', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      comingSoonFlag: true
    },
    {
      'title': 'Account Manager', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      comingSoonFlag: true
    }
  ];

  selectedInterviewData: object;

  constructor(private router: Router,
              private apiService: ApiService) { }

  ngOnInit() {
    this.manipulateDocumentWidth();
    // filter config
    this.filterDataSet = this.filterDataSetConfig;
    this.fetchInterviewData();
  }

  // fetch requirement data
  fetchInterviewData(): void {
    const url = '/api/interviews/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.interviewRecord = data['interviews'];
      }
    });
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 205;
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('dashboard');
  }

  // filter input handler
  filterInputHandler(event: any): void {

  }

  // handle navigation from tile
  handleNavigationEvent(event: any): void {
    this.selectedInterviewData = {};
    this.selectedInterviewData['candidateId'] = event['candidateId'];
    this.selectedInterviewData['requirementId'] = event['requirementId'];
    this.interviewModalDisplayFlag = true;
  }

  // handle interview modal close
  handleCloseEmitter(): void {
    this.interviewModalDisplayFlag = false;;
  }

}
