import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../../../services/data.service';
import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  optionList: Array<object> = [
    {
      'id': 1,
      'title': 'Basic Details',
      'key': 'basicDetail',
      'bgColor': '#A1D066'
    },
    {
      'id': 2,
      'title': 'Personal Details',
      'key': 'personDetailUpdate',
      'bgColor': '#FF6C02'
    },
    {
      'id': 3,
      'title': 'Change Password',
      'key': 'changePassword',
      'bgColor': '#FF6C02'
    },
    {
      'id': 4,
      'title': 'Log Out',
      'key': 'logOut',
      'bgColor': '#FF6C02'
    }
  ];
  loggedUserData: any;
  userData: any;
  basicDetailFlag: boolean = true;
  personalDetailFlag: boolean = false;
  changePasswordFlag: boolean = false;
  documentHeight: number;

  constructor(private router: Router,
              private dataService: DataService,
              private apiService: ApiService,
              public utilityService: UtilityService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (localUserData) {
      this.loggedUserData = userData;
    } else if (userData) {
      this.loggedUserData = userData;
      this.dataService.setLoggedUserDetails(userData);
    } else {
      this.router.navigateByUrl('');
    }
    this.getUserData();
    this.manipulateDocumentWidth();
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 212;
  }

  // get user data
  getUserData(): void {
    const url = '/api/users/fetchRecord/' + this.loggedUserData['id'];
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.getData(url).subscribe( data => {
      this.userData = data['user'][0];
      this.utilityService.updateSpinnerFlag(false);
    })
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // handle selection
  handleSelectedData(option: any): void {
    switch(option['key']) {
      case 'basicDetail': {
        this.basicDetailFlag = true;
        this.personalDetailFlag = false;
        this.changePasswordFlag = false;
        break;
      }
      case 'personDetailUpdate': {
        this.basicDetailFlag = false;
        this.personalDetailFlag = true;
        this.changePasswordFlag = false;
        break;
      }
      case 'changePassword': {
        this.basicDetailFlag = false;
        this.personalDetailFlag = false;
        this.changePasswordFlag = true;
        break;
      }
      case 'logOut': {
        this.dataService.setLoggedUserDetails(null);
        localStorage.removeItem('userDetails');
        this.router.navigateByUrl('');
        break;
      }
    }

    // update color
    for (let i = 0; i < this.optionList.length; i++) {
      if (this.optionList[i]['key'] === option['key']) {
        this.optionList[i]['bgColor'] = '#A1D066';
      } else {
        this.optionList[i]['bgColor'] = '#FF6C02';
      }
    }
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('dashboard');
  }

}
