import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-requirement-dashboard',
  templateUrl: './requirement-dashboard.component.html',
  styleUrls: ['./requirement-dashboard.component.scss']
})
export class RequirementDashboardComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  filterDataSet: Array < object > = [];
  requirementRecord: Array < object > = [];
  documentHeight: number;
  filterDataSetConfig: Array < object > = [
    {
      'title': 'Client Name', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Countries', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'States', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Cities', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Status', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Account Manager', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
  ];

  constructor(private router: Router,
              private apiService: ApiService) { }

  ngOnInit() {
    this.manipulateDocumentWidth();
    // filter config
    this.filterDataSet = this.filterDataSetConfig;
    this.fetchRequirementData();
    this.requirementRecord = [];
  }

  // fetch requirement data
  fetchRequirementData(): void {
    const url = '/api/requirements/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.requirementRecord = data['requirements'];
      }
    });
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 205;
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('dashboard');
  }

  // filter input handler
  filterInputHandler(event: any): void {

  }

  // handle navigation from tile
  handleNavigationEvent(event: any): void {
    const url = 'requirement-view/' + event['id'];
    this.router.navigateByUrl(url);
  }

  // navigate to create page
  navigateCreateRequirement(): void {
    this.router.navigateByUrl('requirement-create');
  }

}
