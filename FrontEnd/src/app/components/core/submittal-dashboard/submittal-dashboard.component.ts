import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-submittal-dashboard',
  templateUrl: './submittal-dashboard.component.html',
  styleUrls: ['./submittal-dashboard.component.scss']
})
export class SubmittalDashboardComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  filterDataSet: Array < object > = [];
  submittalRecord: Array < object > = [];
  documentHeight: number;
  cancelPopUpDisplayFlag: boolean = false;
  filterDataSetConfig: Array < object > = [
    {
      'title': 'Clients', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Requirements', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    },
    {
      'title': 'Account Manager', 'dropdownData': [], 'dropdownSelected': [], 'key': 'clients', 'dropdownSetting': {
        singleSelection: false,
        text: "Select Clients",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class"
      },
      'comingSoonFlag': true
    }
  ];

  // modal related fields
  createFlag: boolean = false;
  submittionDetails: object;
  communicationPopupFlag: boolean = true;
  interviewModalDisplayFlag: boolean = false;
  submittalFlag: boolean = false;
  interviewFlag: boolean = false;

  constructor(private router: Router,
              private apiService: ApiService,
              private utilityService: UtilityService) { }

  ngOnInit() {
    this.manipulateDocumentWidth();
    // filter config
    this.filterDataSet = this.filterDataSetConfig;
    this.getSubmittalData();
  }

  // get submittal data
  getSubmittalData(): void {
    const url = '/api/submittals/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.submittalRecord = data['submittals'];
      }
    });
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 205;
  }

  // navigate to dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  // previous route
  previousRoute(): void {
    this.router.navigateByUrl('dashboard');
  }

  // filter input handler
  filterInputHandler(event: any): void {

  }

  // handle interview event
  handleInterviewSchedule(event: any): void {
    this.submittionDetails = event;
    this.interviewFlag = true;
    this.interviewModalDisplayFlag = true;
  }

  // handle cancel event
  handleCancel(event: any): void {
    this.cancelPopUpDisplayFlag = true;
  }

  // handle modal close
  handleCancelPopUpClose(): void {
    this.cancelPopUpDisplayFlag = false;
  }

  // handle view detail event
  handleViewDetail(event: any): void {

  }

  // cancel modal
  candidateDetailCancel(): void {
    this.submittionDetails = {};
    this.interviewFlag = false;
    this.interviewModalDisplayFlag = false;
  }

  // save modal data
  candidateDetailSave(event: any): void {
    this.scheduleInterview(event['interviewDetails']);
    this.updateSubmittionDetail(event['id'], event);
  }

  // schedule interview
  scheduleInterview(obj: any): void {
    const url = '/api/interviews/create';
    this.apiService.postData(url, obj).subscribe(data => {

    });
  }

  // update submittion flag
  updateSubmittionDetail(id: number, data: any): void {
    const url = '/api/submittals/updateStatus/' + id;
    let obj = {};
    obj['isInterviewScheduled'] = true;
    obj['isCancelled'] = false;
    obj['updatedBy'] = data['interviewDetails']['updatedBy'];
    obj['updatedOn'] = data['interviewDetails']['updatedOn'];
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.putData(url, obj).subscribe(data => {
      this.utilityService.updateSpinnerFlag(false);
      this.candidateDetailCancel();
      this.getSubmittalData();
    });
  }

  candidateDetailProceed(): void {

  }

}
