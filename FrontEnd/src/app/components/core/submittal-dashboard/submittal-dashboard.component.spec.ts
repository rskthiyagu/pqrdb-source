import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittalDashboardComponent } from './submittal-dashboard.component';

describe('SubmittalDashboardComponent', () => {
  let component: SubmittalDashboardComponent;
  let fixture: ComponentFixture<SubmittalDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmittalDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittalDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
