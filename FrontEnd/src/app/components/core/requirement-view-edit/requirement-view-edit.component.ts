import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { ToastrService } from 'ngx-toastr';

import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';
import { ApiService } from '../../../services/api.service';

import * as moment from 'moment';

@Component({
  selector: 'app-requirement-view-edit',
  templateUrl: './requirement-view-edit.component.html',
  styleUrls: ['./requirement-view-edit.component.scss']
})
export class RequirementViewEditComponent implements OnInit {
  fileToUpload: any;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.manipulateDocumentWidth();
  }

  @ViewChild('singleUpload', { static: false }) singleUpload;

  fieldDataList: Array<object> = [
    {
      'id': 0,
      'title': 'Job ID',
      'inputFlag': true,
      'key': 'count',
      'disableFlag': true,
      'placeholder': 'Required',
      'requiredFlag': true,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false
    },
    {
      'id': 1,
      'title': 'Title/Position',
      'inputFlag': true,
      'key': 'title',
      'disableFlag': false,
      'placeholder': 'Required',
      'requiredFlag': true,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false
    },
    {
      'id': 2,
      'title': 'Client',
      'errorFlag': false,
      'selectFlag': true,
      'requiredFlag': true,
      'key': 'clients',
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        'singleSelection': true,
        'text': "Select Client (Required)",
        'enableSearchFilter': true,
        'classes': "myclass custom-class",
        'addNewItemOnFilter': false
      }
    },
    {
      'id': 3,
      'title': 'End Client',
      'inputFlag': true,
      'key': 'endClients',
      'noteFlag': true,
      'disableFlag': false,
      'placeholder': '',
      'requiredFlag': false,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false,
      'noteText': 'Please enter the End Client Name, If you choose the Tier-1 Client'
    },
    {
      'id': 4,
      'title': 'Country',
      'errorFlag': false,
      'selectFlag': true,
      'requiredFlag': true,
      'key': 'countries',
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Country (Required)",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    },
    {
      'id': 5,
      'title': 'State',
      'errorFlag': false,
      'selectFlag': true,
      'requiredFlag': true,
      'key': 'states',
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select State (Required)",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    },
    {
      'id': 6,
      'title': 'City',
      'errorFlag': false,
      'key': 'cities',
      'selectFlag': true,
      'requiredFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select City (Required)",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    },
    {
      'id': 7,
      'title': 'Zip Code',
      'inputFlag': true,
      'disableFlag': false,
      'key': 'zipCode',
      'placeholder': '',
      'requiredFlag': false,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false
    },
    {
      'id': 8,
      'title': 'Duration',
      'errorFlag': false,
      'selectFlag': true,
      'requiredFlag': false,
      'key': 'duration',
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Duration",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 9,
      'title': 'Client BillRate',
      'inputFlag': true,
      'disableFlag': false,
      'key': 'clientBillRate',
      'placeholder': 'Required',
      'requiredFlag': true,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false
    },
    {
      'id': 10,
      'title': 'Candidate PayRate',
      'inputFlag': true,
      'disableFlag': false,
      'key': 'candidatePayRate',
      'placeholder': 'Required',
      'requiredFlag': true,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'text',
      'errorFlag': false
    },
    {
      'id': 11,
      'title': 'Job Status',
      'errorFlag': false,
      'key': 'jobStatus',
      'requiredFlag': true,
      'selectFlag': true,
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Job Status (Required)",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 12,
      'title': 'Overall Experience',
      'inputFlag': true,
      'disableFlag': false,
      'key': 'experience',
      'placeholder': '',
      'requiredFlag': false,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'number',
      'errorFlag': false
    },
    {
      'id': 13,
      'title': 'No. Of Positions',
      'disableFlag': false,
      'inputFlag': true,
      'key': 'positionNumber',
      'placeholder': '',
      'requiredFlag': false,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'number',
      'errorFlag': false
    },
    {
      'id': 14,
      'title': 'Work Authorizations',
      'errorFlag': false,
      'selectFlag': true,
      'requiredFlag': true,
      'key': 'authorizations',
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: false,
        text: "Select Authorizations (Required)",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    },
    {
      'id': 15,
      'title': 'Client Account Manager',
      'errorFlag': false,
      'selectFlag': true,
      'requiredFlag': true,
      'key': 'accountManager',
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: true,
        text: "Select Account Manager (Required)",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: false
      }
    },
    {
      'id': 16,
      'title': 'Skills',
      'errorFlag': false,
      'selectFlag': true,
      'requiredFlag': false,
      'key': 'skills',
      'dropdownData': [],
      'dropdownSelected': [],
      'dropdownSetting': {
        singleSelection: false,
        text: "Select Skills",
        enableSearchFilter: true,
        classes: "myclass custom-class",
        addNewItemOnFilter: true
      }
    },
    {
      'id': 17,
      'title': 'Detailed Job Description',
      'textareaFlag': true,
      'key': 'jobDescription',
      'placeholder': '',
      'requiredFlag': false,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'number',
      'errorFlag': false
    },
    {
      'id': 18,
      'title': 'Job Description File',
      'uploadFlag': true,
      'key': 'jobDescriptionFile',
      'placeholder': '',
      'requiredFlag': false,
      'data': '',
      'pattern': '[a-zA-Z0-9\s]+',
      'type': 'number',
      'errorFlag': false
    }
  ];

  fieldDataListConfig: Array<object> = [];

  // submission object
  submissionConfigObject: object = {
    'title': 'Submissions Details',
    'width': 15,
    'field': [
      { 'id': 0, 'title': 'Candidate Name', 'multipleKeyFlag': true, 'keyList': ['candidateFirstName', 'candidateLastName'], 'linkFlag': true },
      { 'id': 1, 'title': 'Client Name', 'key': 'clientName', 'linkFlag': true },
      { 'id': 2, 'title': 'Submitted To', 'key': 'submittedToName' },
      { 'id': 3, 'title': 'Submitted By', 'key': 'updateUserName' },
      { 'id': 4, 'title': 'Submitted On', 'key': 'updatedOn', 'dateFlag': true },
      { 'id': 5, 'title': '', 'key': null, 'actionFlag': true, 'actionList': [{'key': 'scheduleInterview', 'title': 'Schedule Interview'}]} // {'key': 'edit', 'title': 'Edit'}, {'key': 'scheduleInterview', 'title': 'Schedule Interview'}, {'key': 'cancelSubmission', 'title': 'Cancel Submission'}
    ],
    'data': []
  }

  submissionObject: object = {};

  // interview object
  interviewConfigObject: object = {
    'title': 'Interview Inprogress Details',
    'width': 13,
    'field': [
      { 'id': 0, 'title': 'Candidate Name', 'key': 'candidateName', 'linkFlag': true },
      { 'id': 1, 'title': 'Client Name', 'key': 'clientName', 'linkFlag': true },
      { 'id': 2, 'title': 'Round', 'key': 'interviewRoundName' },
      { 'id': 3, 'title': 'Status', 'key': 'status' },
      { 'id': 4, 'title': 'Updated By', 'key': 'updateUserName' },
      { 'id': 5, 'title': 'Updated On', 'key': 'updatedOn', 'dateFlag': true },
      { 'id': 6, 'title': '', 'key': null, 'actionFlag': true, 'actionList': [{'key': 'scheduleInterview', 'title': 'Schedule Interview'}]}
    ],
    'data': []
  }

  interviewObject: object = {};

  // selected object
  selectedConfigObject: object = {
    'title': 'Placed Candidates Details',
    'width': 23,
    'field': [
      { 'id': 0, 'title': 'Candidate Name', 'key': 'candidateName', 'linkFlag': true },
      { 'id': 1, 'title': 'Client Name', 'key': 'clientName', 'linkFlag': true },
      { 'id': 2, 'title': 'Updated By', 'key': 'updateUserName' },
      { 'id': 3, 'title': 'Updated On', 'key': 'updatedOn', 'dateFlag': true }
    ],
    'data': []
  }

  selectedObject: object = {};

  // rejected object
  rejectedConfigObject: object = {
    'title': 'Rejected Candidates Details',
    'width': 23,
    'field': [
      { 'id': 0, 'title': 'Candidate Name', 'key': 'candidateName', 'linkFlag': true },
      { 'id': 1, 'title': 'Client Name', 'key': 'clientName', 'linkFlag': true },
      { 'id': 2, 'title': 'Updated By', 'key': 'updateUserName' },
      { 'id': 3, 'title': 'Updated On', 'key': 'updatedOn', 'dateFlag': true }
    ],
    'data': []
  }

  rejectedObject: object = {};

  // warning pop up
  warningModalSetUp: object = {
    'title': 'Warning',
    'proceedFlag': true,
    'cancelFlag': true,
    'contentFlag': true,
    'content': 'You may lose the unsaved changes. Do you want to continue?'
  };

  deleteModalSetUp: object = {
    'title': 'Delete Confirmation',
    'proceedFlag': true,
    'cancelFlag': true,
    'contentFlag': true,
    'content': 'Are you sure to delete the requirement?'
  };

  modalAction: string;
  modalConfig: object;

  filterUrlList: Array<string> = [
    '/api/skills/fetchRecord',
    '/api/authorizations/fetchRecord',
    '/api/clients/fetchClientRecord',
    '/api/cities/fetchRecord',
    '/api/states/fetchRecord',
    '/api/countries/fetchRecord',
    '/api/durations/fetchRecord',
    '/api/users/fetchAccountManagerRecord',
    '/api/requirementStatus/fetchRecord'
  ];

  viewFlag: boolean;
  editFlag: boolean;
  createFlag: boolean;
  loggedUserDetails: object;
  requirementId: number;
  actionTitle: string = '';
  dataFlag: boolean = false;
  optionFlag: boolean = false;
  documentHeight: number;
  requirementDetailObject: object = {};
  showPopUpFlag: boolean = false;
  endClientFlag: boolean = false;

  countryErrorFlag: boolean = false;
  stateErrorFlag: boolean = false;

  jobDescriptionFileFlag: boolean = false;
  jobDescriptionModalDisplay: boolean = false;
  docUrl: any;

  // interview pop up
  interviewModalDisplayFlag: boolean = false;
  selectedInterviewData: object;

  constructor(private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private utilityService: UtilityService,
    private apiService: ApiService,
    private toastrService: ToastrService,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (userData) {
      this.dataService.setLoggedUserDetails(userData);
      this.loggedUserDetails = userData;
    } else if (!userData && !localUserData) {
      this.router.navigateByUrl('');
    } else {
      this.loggedUserDetails = localUserData;
    }
    // to eradicate the ngIf data changing issue
    const scope = this;
    setTimeout(function () {
      scope.validateOperation();
    }, 0);
  }

  // event handler for finding the operation of page
  validateOperation(): void {
    const routePath = this.route.routeConfig.path;
    this.route.params.subscribe(params => {
      if (params) {
        this.requirementId = +params['requirementId'];
      }
      // page detector
      if (routePath.includes('requirement-view') && this.requirementId) {    // candidate view page
        this.viewFlag = true;
        this.editFlag = false;
        this.createFlag = false;
        this.actionTitle = 'Requirement View';
        this.manipulateDocumentWidth();
        this.getRequirementData();
        this.getSubmittalData();
        this.getInterviewInprogressData();
      } else if (routePath.includes('requirement-edit') && this.requirementId) {   // candidate edit page
        this.viewFlag = false;
        this.editFlag = true;
        this.createFlag = false;
        this.actionTitle = 'Requirement Edit';
        this.manipulateDocumentWidth();
        this.getRequirementData();
        // do validate the data is already present in data service or not
        this.getDropdownItems();
      } else if (routePath.includes('requirement-create') && !this.requirementId) {   // candidate create page
        this.viewFlag = false;
        this.editFlag = false;
        this.createFlag = true;
        this.actionTitle = 'Requirement Create';
        this.getRequirementId();
        this.manipulateDocumentWidth();
      } else {      // redirect to home page
        this.router.navigateByUrl('/requirement-dashboard');
      }
    });
  }

  // get requirement id
  getRequirementId(): void {
    const url = '/api/requirements/getCount';
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.getData(url).subscribe(data => {
      if (data['count'])
        this.fieldDataList[0]['data'] = data['count'] + 1;
      else
        this.fieldDataList[0]['data'] = 1;
      this.utilityService.updateSpinnerFlag(false);
      this.getDropdownItems();
    });
  }

  // get submittal data
  getSubmittalData(): void {
    const url = '/api/submittals/fetchRecordByRequirements/' + this.requirementId;
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.submissionConfigObject['data'] = data['submittals'];
        this.submissionObject = this.submissionConfigObject;
      }
    });
  }

  // get interview inprogress data
  getInterviewInprogressData(): void {
    const url = '/api/interviews/fetchRecordByRequirements/' + this.requirementId;
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.fetchInterviewRelatedData(data['interviews']);
      }
    });
  }

  // fetch interview related data
  fetchInterviewRelatedData(interviewData: Array<object>): void {
    const inprogressInterviewList = [];
    let subInterviewList = [];
    const completedInterviewList = [];
    const selectedInterviewList = [];
    const failedInterviewList = [];
    for (let i = 0; i < interviewData.length; i++) {
      if (interviewData[i]['interviewRound'] == 6) {
        completedInterviewList.push(interviewData[i]);
      }
    }

    subInterviewList = interviewData.filter(function (el) {
      completedInterviewList.filter(function (e) {
        return e['candidateId'] !== el['candidateId'];
      });
    });

    // get inprogress interview
    let pendingList = [];
    for (let i = 0; i < subInterviewList.length; i++) {
      if (!pendingList.includes(subInterviewList[i])) {
        pendingList.push(subInterviewList[i]);
      }
    }

    // get selected & rejected list
    for (let i = 0; i < completedInterviewList.length; i++) {
      if (completedInterviewList[i]['interviewStatus'] == 1) {
        selectedInterviewList.push(completedInterviewList[i]);
      } else if (completedInterviewList[i]['interviewStatus'] == 4) {
        failedInterviewList.push(completedInterviewList[i]);
      }
    }

    // set the data to config
    this.selectedConfigObject['data'] = selectedInterviewList;
    this.rejectedConfigObject['data'] = failedInterviewList;

    this.selectedObject = this.selectedConfigObject;
    this.rejectedObject = this.rejectedConfigObject;
  }

  // document height calculation
  manipulateDocumentWidth(): void {
    const screenWidth = window.innerHeight;
    this.documentHeight = screenWidth - 203;
  }

  // get filter dropdown values
  getDropdownItems(): void {
    // enable loader
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.forkGetData(this.filterUrlList).subscribe(data => {
      this.updateFilterData(data);
    });
  }

  // update the filter data into configuration
  updateFilterData(filterData: Array<object>): void {
    // skip inner loop by assuming the data is coming in same order given
    for (let i = 0; i < this.fieldDataList.length; i++) {
      for (let j = 0; j < filterData.length; j++) {
        const key = Object.keys(filterData[j])[0];
        if (this.fieldDataList[i]['selectFlag']) {
          if (key == this.fieldDataList[i]['key'])
            this.fieldDataList[i]['dropdownData'] = filterData[j][this.fieldDataList[i]['key']];
        }
      }
    }
    // trigger config setter method
    this.optionFlag = true;
    this.editDetailConfigSetter();
    this.utilityService.updateSpinnerFlag(false);
  }

  // fetch the requirement data
  getRequirementData(): void {
    this.utilityService.updateSpinnerFlag(true);
    const url = '/api/requirements/fetchRecord/' + this.requirementId;
    this.apiService.getData(url).subscribe(data => {
      this.requirementDetailObject = data['requirements'][0];
      this.utilityService.updateSpinnerFlag(false);
      this.composeData();
    });
  }

  // compose data structure
  composeData(): void {
    for (let i = 0; i < this.fieldDataList.length; i++) {
      if (this.fieldDataList[i]['selectFlag']) {
        if (this.fieldDataList[i]['key'] === 'skills' || this.fieldDataList[i]['key'] === 'authorizations') {
          if (this.requirementDetailObject[this.fieldDataList[i]['key']] && this.requirementDetailObject[this.fieldDataList[i]['key']].length > 0) {
            const list = [];
            for (let j = 0; j < this.requirementDetailObject[this.fieldDataList[i]['key']].length; j++) {
              const name = this.fieldDataList[i]['key'] + 'Name';
              const id = this.fieldDataList[i]['key'] + 'Id';
              const obj = {};
              obj['id'] = this.requirementDetailObject[this.fieldDataList[i]['key']][j][id];
              obj['name'] = this.requirementDetailObject[this.fieldDataList[i]['key']][j][name];
              list.push(obj);
            }
            this.requirementDetailObject[this.fieldDataList[i]['key']] = list;
          } else {
            this.requirementDetailObject[this.fieldDataList[i]['key']] = [];
          }
        } else {
          const name = this.fieldDataList[i]['key'] + 'Name';
          const id = this.fieldDataList[i]['key'] + 'Id';
          const obj = {};
          obj['id'] = this.requirementDetailObject[id];
          obj['name'] = this.requirementDetailObject[name];
          this.requirementDetailObject[this.fieldDataList[i]['key']] = [obj];
        }
      }
    }
    this.setRequirementData();
  }

  // set the requirement data
  setRequirementData(): void {
    let editDetailList = this.fieldDataList;
    for (let i = 0; i < editDetailList.length; i++) {
      if (editDetailList[i]['selectFlag']) {     // select box data feeding
        editDetailList[i]['dropdownSelected'] = this.requirementDetailObject[editDetailList[i]['key']];
      } else {                                          // text box data feeding
        editDetailList[i]['data'] = this.requirementDetailObject[editDetailList[i]['key']];
      }
    }
    this.fieldDataList = editDetailList;
    // trigger config object setter method
    this.dataFlag = true;
    this.editDetailConfigSetter();
  }

  // set the configuration of detail tiles
  editDetailConfigSetter(): void {
    // if (this.dataFlag && this.optionFlag) {
    //   this.fieldDataListConfig = this.fieldDataList;
    //   this.dataFlag = false;
    //   this.optionFlag = false;
    // } else if (this.createFlag && this.optionFlag) {
    //   this.fieldDataListConfig = this.fieldDataList;
    //   this.optionFlag = false;
    // }
    this.fieldDataListConfig = this.fieldDataList;
  }

  // handle select event in select box
  handleSelectedItems(selectedData: any, offset: number, key: string): void {
    // update the object
    if (selectedData && selectedData.length !== 0) {
      switch (key) {
        case 'countries': {
          const stateUrl = '/api/states/fetchRecordByCountryId/' + selectedData[0]['id'];
          const cityUrl = '/api/cities/fetchRecordByCountryId/' + selectedData[0]['id'];
          const urlList = [stateUrl, cityUrl];
          this.apiService.forkGetData(urlList).subscribe(data => {
            this.fieldDataListConfig[offset + 1]['dropdownData'] = data[0]['states'];
            this.fieldDataListConfig[offset + 2]['dropdownData'] = data[1]['cities'];
            // selected values setter
            this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 1]['dropdownSelected'] = [];
            this.fieldDataListConfig[offset + 2]['dropdownSelected'] = [];
            // set the result set value
            this.requirementDetailObject[key] = selectedData;
            this.requirementDetailObject['states'] = [];
            this.requirementDetailObject['cities'] = [];
          });
          break;
        }
        case 'states': {
          const cityUrl = '/api/cities/fetchRecordByStateId/' + selectedData[0]['id'];
          this.apiService.getData(cityUrl).subscribe(data => {
            // selected values for country
            this.fieldDataListConfig[offset - 1]['dropdownSelected'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
            this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 1]['dropdownSelected'] = [];
            // set the result set value
            this.requirementDetailObject['countries'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
            this.requirementDetailObject[key] = selectedData;
            this.requirementDetailObject['cities'] = [];
          });
          break;
        }
        case 'cities': {
          // selected values for country
          this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
          this.fieldDataListConfig[offset - 1]['dropdownSelected'] = [{ 'id': selectedData[0]['stateId'], 'name': selectedData[0]['stateName'], 'countryId': selectedData[0]['countryId'], 'countryName': selectedData[0]['countryName'] }];
          this.fieldDataListConfig[offset - 2]['dropdownSelected'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
          // set the result set value
          this.requirementDetailObject['countries'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
          this.requirementDetailObject['states'] = [{ 'id': selectedData[0]['stateId'], 'name': selectedData[0]['stateName'], 'countryId': selectedData[0]['countryId'], 'countryName': selectedData[0]['countryName'] }];
          this.requirementDetailObject['cities'] = selectedData;
          break;
        }
        case 'clients': {
          this.requirementDetailObject[key] = selectedData;
          if (selectedData['categoryId'] === 2)
            this.endClientFlag = true;
          else
            this.endClientFlag = false;
          break;
        }
        default: {
          // if (this.requirementDetailObject[key] && this.requirementDetailObject[key].length > 0) {
          //   this.requirementDetailObject[key].push(selectedData[0]);
          // } else {
          this.requirementDetailObject[key] = selectedData;
          // }
        }
      }
    } else {
      switch (key) {
        case 'countries': {
          const stateUrl = '/api/states/fetchRecord';
          const cityUrl = '/api/cities/fetchRecord';
          const urlList = [stateUrl, cityUrl];
          this.apiService.forkGetData(urlList).subscribe(data => {
            this.fieldDataListConfig[offset + 1]['dropdownData'] = data[0]['states'];
            this.fieldDataListConfig[offset + 2]['dropdownData'] = data[1]['cities'];
            // selected value
            this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 1]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 2]['dropdownSelected'] = selectedData;

            // set the result values
            this.requirementDetailObject['countries'] = selectedData;
            this.requirementDetailObject['states'] = selectedData;
            this.requirementDetailObject['cities'] = selectedData;
          });
          break;
        }
        case 'states': {
          const cityUrl = '/api/cities/fetchRecord';
          this.apiService.getData(cityUrl).subscribe(data => {
            // selected value
            this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
            this.fieldDataListConfig[offset + 1]['dropdownSelected'] = selectedData;
            // set the result set values
            this.requirementDetailObject['states'] = selectedData;
            this.requirementDetailObject['cities'] = selectedData;
          });
          break;
        }
        case 'cities': {
          this.fieldDataListConfig[offset]['dropdownSelected'] = selectedData;
          this.requirementDetailObject['cities'] = selectedData;
          break;
        }
        case 'clients': {
          this.requirementDetailObject[key] = selectedData;
          if (selectedData['categoryId'] === 2)
            this.endClientFlag = true;
          else
            this.endClientFlag = false;
          break;
        }
        default: {
          this.requirementDetailObject[key] = this.fieldDataListConfig[offset]['dropdownSelected'];
        }
      }
    }
  }

  // handle the create record
  createRecord(key: string, dataObj: any): void {
    const url = '/api/' + key + '/create';
    this.apiService.postData(url, dataObj).subscribe(data => {
      let updatedObj = dataObj;
      updatedObj['id'] = data[0]['id'];
      this.requirementDetailObject[key].push(updatedObj);
    });
  }

  // handle add new item in select box
  addNewItemHandler(event: any, key: string): void {
    switch (key) {
      case 'cities': {
        this.stateErrorFlag = false;
        this.countryErrorFlag = false;
        if (!this.requirementDetailObject['states'] || (this.requirementDetailObject['states'] && this.requirementDetailObject['states'].length === 0)) {
          this.stateErrorFlag = true;
        } else if (!this.requirementDetailObject['countries'] || (this.requirementDetailObject['countries'] && this.requirementDetailObject['countries'].length === 0)) {
          this.countryErrorFlag = true;
        } else {
          this.createRecord('cities', event);
        }
        break;
      }
      case 'states': {
        this.countryErrorFlag = false;
        if (!this.requirementDetailObject['countries'] || (this.requirementDetailObject['countries'] && this.requirementDetailObject['countries'].length === 0)) {
          this.countryErrorFlag = true;
        } else {
          this.createRecord('states', event);
        }
        break;
      }
      case 'countries': {
        this.createRecord('countries', event);
        break;
      }
      default: {
        this.createRecord(key, event);
      }
    }
  }

  // upload job description file
  uploadFile(): void {
    const url = '/api/requirements/uploadFile';
    const formData: FormData = new FormData();
    const fileName = this.fileToUpload.name;
    formData.append('files', this.fileToUpload, fileName);
    this.apiService.postData(url, formData).subscribe(data => {
    });
  }

  // save requirement
  saveRequirement(): void {
    let url;
    let apiCall;
    const errorFlag = this.validateClientDetails();
    if (errorFlag) {
      this.toastrService.error('Please fill required fields...', 'Error', { timeOut: 1500 });
      return;
    } else {
      if (this.fileToUpload) {
        this.requirementDetailObject['fileName'] = this.fileToUpload.name;
        this.uploadFile();
      }
      if (this.editFlag) {
        url = '/api/requirements/update/' + this.requirementId;
        apiCall = 'putData';
      } else {
        url = '/api/requirements/create';
        apiCall = 'postData'
      }
      this.requirementDetailObject['sourcedBy'] = this.loggedUserDetails['id'];
      this.requirementDetailObject['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      this.requirementDetailObject['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      this.apiService[apiCall](url, this.requirementDetailObject).subscribe(data => {
        if (this.requirementId) {
          this.toastrService.success('Requirement Updated Successfully...', 'Success', { timeOut: 1000 });
          const url = '/requirement-view/' + this.requirementId;
          this.router.navigateByUrl(url);
        } else {
          this.toastrService.success('Requirement Saved Successfully...', 'Success', { timeOut: 1000 });
          this.router.navigateByUrl('/requirement-dashboard');
        }
        this.requirementDetailObject = {};
      });
    }
  }

  // delete confirmation
  deleteConfirmation(): void {
    this.modalConfig = this.deleteModalSetUp;
    this.showPopUpFlag = true;
    this.modalAction = 'delete';
  }

  // delete requirement
  deleteRequirement(): void {
    const url = '/api/requirements/delete/' + this.requirementId;
    this.apiService.deleteData(url).subscribe(data => {
      this.showPopUpFlag = false;
    });
  }

  // cancel confirmation
  cancelConfirmation(): void {
    this.modalConfig = this.warningModalSetUp;
    this.showPopUpFlag = true;
    this.modalAction = 'cancel';
  }

  // cancel changes
  cancelChanges(): void {
    if (this.requirementId) {
      const url = 'requirement-view/' + this.requirementId;
      this.router.navigateByUrl(url);
    } else {
      this.router.navigateByUrl('requirement-dashboard');
    }
    this.showPopUpFlag = false;
  }

  // validate the file type to be allowed
  validateFileType(file: any): boolean {
    let fileType = '';
    if (file) {
      const fileName = file.name;
      const fileNameSplitArray = (fileName).split('.');
      fileType = fileNameSplitArray[fileNameSplitArray.length - 1];
    } else {
      return false;
    }
    if (fileType === 'doc' || fileType === 'docx' || fileType === 'pdf') {
      return true;
    } else {
      return false;
    }
  }

  // upload file
  documentSelected(e: any): void {
    let flag = this.validateFileType(e.item(0));
    if (flag) {
      this.fileToUpload = e.item(0);
    } else {
      this.singleUpload.nativeElement.value = '';
      this.toastrService.error('Please select proper file to proceed', 'Unsupported File type', { timeOut: 3000 });
    }
  }

  // validation
  validateClientDetails(): boolean {
    let errorFlag = false;
    for (let i = 0; i < this.fieldDataListConfig.length; i++) {
      if (this.fieldDataListConfig[i]['requiredFlag']) {
        if (this.fieldDataListConfig[i]['selectFlag'] && (!this.requirementDetailObject[this.fieldDataListConfig[i]['key']] || (this.requirementDetailObject[this.fieldDataListConfig[i]['key']] && this.requirementDetailObject[this.fieldDataListConfig[i]['key']].length === 0))) {
          this.fieldDataListConfig[i]['errorFlag'] = true;
          errorFlag = true;
        } else if (this.fieldDataListConfig[i]['inputFlag'] && !this.fieldDataListConfig[i]['data']) {
          this.fieldDataListConfig[i]['errorFlag'] = true;
          errorFlag = true;
        } else {
          this.fieldDataListConfig[i]['errorFlag'] = false;
        }
      }
    }
    if (errorFlag) {
      return true;
    } else {
      return false;
    }
  }

  // navigate to dashboard
  navigateToDashboard(): void {
    this.router.navigateByUrl('requirement-dashboard');
  }

  // open modal
  openModal(a, b): void {

  }

  handleCloseModal(e): void {

  }

  skipModal(): void {

  }

  handleSaveModal(event): void {

  }

  proceedModal(): void {
    this.cancelChanges();
  }

  previousRoute(): void {
    if (this.createFlag || this.viewFlag) {
      this.router.navigateByUrl('requirement-dashboard');
    } else {
      const url = 'candidate-view/' + this.requirementId;
      this.router.navigateByUrl(url);
    }
  }

  navigateToEdit(): void {
    this.router.navigateByUrl('requirement-edit/' + this.requirementId);
  }

  // handling click event from table
  handleNavigation(event: any): void {
    let url = '';
    if (event.field.key === 'candidateName')
      url = 'candidate-view/' + event['data']['candidateId'];
    else if (event.field.key === 'clientName')
      url = 'client-view/' + event['data']['clientId'];
    this.router.navigateByUrl(url);
  }

  // open doc viewer for job description file
  openDocViewer(): void {
    const fileLink = 'http://pqrdb.geval6.com/api/requirementDocuments/' + this.requirementDetailObject['jobDescriptionFile'];
      // validate the viewer that support pdf or not
      const fileNameSplitter = (this.requirementDetailObject['jobDescriptionFile']).split('.');
      const fileType = fileNameSplitter[fileNameSplitter.length - 1];
      let docViewerUrl;
    if ((fileType).toLowerCase() == 'pdf') {
      docViewerUrl = 'https://docs.google.com/gview?url=' + fileLink + '&embedded=true';
      // docViewerUrl = 'https://docs.google.com/viewer?url=' + this.doc + '&embeded=true';
    } else {
      docViewerUrl = 'https://view.officeapps.live.com/op/embed.aspx?src=' + fileLink;
      // docViewerUrl = 'https://docs.google.com/viewer?url=' + this.doc + '&embeded=true';
    }
    this.docUrl = this.sanitizer.bypassSecurityTrustResourceUrl(docViewerUrl);
    this.jobDescriptionModalDisplay = true;
  }

  // close the pop up
  closeModal(): void {
    this.jobDescriptionModalDisplay = false;
    this.docUrl = '';
  }

  // handle typed event
  handleTypedEvent(event: any, key: string, data: string): void {
    // number field validation
    if (key === 'noticePeriod') {
      let flag = true;
      let updatedData = '';
      for (let i = 0; i < data.length; i++) {
        if ((parseInt(data[i]) >= 0 && parseInt(data[i]) <= 9) || data[i] == '.') {
          updatedData += data[i];
        } else {
          flag = false;
        }
      }
      if (!flag) {
        data = updatedData;
        event.target.value = data;
      }
    } else if (key === 'hourlyRate' || key === 'expectedSalary' || key === 'currentCTC' || key === 'expectedCTC') {
      // validate not number in mobile
      let flag = true;
      let updatedData = '';
      for (let i = 0; i < data.length; i++) {
        if ((parseInt(data[i]) >= 0 && parseInt(data[i]) <= 9) || data[i] == ',' || data[i] == '.' || data[i] == 'l' || data[i] == 'L' || data[i] == 'k' || data[i] == 'K') {
          updatedData += data[i];
        } else {
          flag = false;
        }
      }
      if (!flag) {
        data = updatedData;
        event.target.value = data;
      }
    }
    this.requirementDetailObject[key] = event.target.value;
  }

  // handle event from submital & interview
  handleTableEvent(event: any): void {
    this.selectedInterviewData = {};
    let obj = {};
    obj['candidateId'] = event['candidateId'];
    obj['requirementId'] = this.requirementId;
    this.selectedInterviewData = obj;
    this.interviewModalDisplayFlag = true;
  }

  // handle close event handler for interview pop up
  handleCloseEmitter(): void {
    this.interviewModalDisplayFlag = false;
  }

}
