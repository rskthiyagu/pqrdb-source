import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequirementViewEditComponent } from './requirement-view-edit.component';

describe('RequirementViewEditComponent', () => {
  let component: RequirementViewEditComponent;
  let fixture: ComponentFixture<RequirementViewEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequirementViewEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementViewEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
