import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingUserShortTileComponent } from './pending-user-short-tile.component';

describe('PendingUserShortTileComponent', () => {
  let component: PendingUserShortTileComponent;
  let fixture: ComponentFixture<PendingUserShortTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingUserShortTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingUserShortTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
