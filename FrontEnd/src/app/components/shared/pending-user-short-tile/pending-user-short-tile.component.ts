import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pending-user-short-tile',
  templateUrl: './pending-user-short-tile.component.html',
  styleUrls: ['./pending-user-short-tile.component.scss']
})
export class PendingUserShortTileComponent implements OnInit {

  @Input() set candidateList(data) {
    if (data && data.length !== 0) {
      for (let i = 0; i < data.length; i++) {
        let flag = false;
        data[i]['heightValue'] = 155;
        data[i]['fullName'] = '';
        if (data[i]['firstName']) {
          data[i]['fullName'] = data[i]['firstName'];
          flag = true;
        }
        if (data[i]['lastName']) {
          if (flag) {
            data[i]['fullName'] += ', ';
          }
          data[i]['fullName'] += data[i]['lastName'];
        }
        // date updation
        data[i]['createdOn'] = new Date(data[i]['createdOn']);
        data[i]['updatedOn'] = new Date(data[i]['updatedOn']);
      }
      this.candidateDataList = data;
    } else {
      this.candidateDataList = [];
    }
  }

  @Output() navigationEmitter = new EventEmitter();

  candidateDataList: Array < object >;

  constructor(private toastrService: ToastrService) { }

  ngOnInit() {
  }

  // toggle view more detail
  toggleViewDetail(event, offset, flag): void {
    event.stopPropagation();
    if (flag) {
      this.candidateDataList[offset]['viewDetailFlag'] = true;
      this.candidateDataList[offset]['heightValue'] = 341;
    } else {
      this.candidateDataList[offset]['viewDetailFlag'] = false;
      this.candidateDataList[offset]['heightValue'] = 155;
    }
  }

  // download resume
  downloadResume(event: any): void {

  }

  navigateViewPage(event: any): void {

  }

  // navigate candidate view page
  navigateCandidateView(id: number, data: object): void {
    this.navigationEmitter.emit({'id': id, 'data': data});
    // this.dataService.setSelectedCandidateDetail(data);
    // const url = 'candidate-view/' + id;
    // this.router.navigateByUrl(url);
  }

}
