import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-user-basic-detail',
  templateUrl: './user-basic-detail.component.html',
  styleUrls: ['./user-basic-detail.component.scss']
})
export class UserBasicDetailComponent implements OnInit {

  @Input() set userDetail(data) {
    this.userDetailData = data;
  }

  profileImagePickerFlag: boolean = false;
  coverImagePickerFlag: boolean = false;
  fileToUpload: any;
  userDetailData: any = {
    'firstName': '',
    'lastName': '',
    'userName': '',
    'designation': '',
    'mobileNumber': '',
    'email': '',
    'isActive': true
  };

  constructor(private apiService: ApiService,
              private utilityService: UtilityService,
              private toastrService: ToastrService,
              private router: Router) { }

  ngOnInit() {
  }

  // open file picker
  openFilePicker(source: string): void {
    if (source === 'cover') {
      if (this.coverImagePickerFlag) {
        this.profileImagePickerFlag = false;
        this.coverImagePickerFlag = false;
      } else {
        this.profileImagePickerFlag = false;
        this.coverImagePickerFlag = true;
      }
    } else {
      if (this.profileImagePickerFlag) {
        this.profileImagePickerFlag = false;
        this.coverImagePickerFlag = false;  
      } else {
        this.profileImagePickerFlag = true;
        this.coverImagePickerFlag = false;
      }
    }
  }

  // deactivate user
  deactivateUser(): void {
    const url = '/api/users/deactivateUser/' + this.userDetailData['id'];
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.putData(url, {}).subscribe(data => {
      this.utilityService.updateSpinnerFlag(false);
      this.toastrService.success("User Deactivated Successfully...", "Success", { timeOut: 1000 });
      // logging out the user
      localStorage.removeItem('userDetails');
      this.router.navigateByUrl('');
    });
  }

  // submit document
  submitDocument(flag: string): void {
    let fileName;
    let url;
    let actionKey;
    if (flag === 'cover') {
      url = '/api/users/uploadUserCoverImage';
      flag = 'coverImage';
      if (this.userDetailData['coverImage'])
        fileName = this.userDetailData['coverImage'];
      else {
        const imageTypeIndex = (this.fileToUpload.name).indexOf('.');
        const imageType = (this.fileToUpload.name).substr(imageTypeIndex, this.fileToUpload.name.length);
        fileName = 'user_cover_image_' + this.userDetailData['id'] + imageType;
        this.updateImageDetail('CoverImage', fileName);
      }
    } else {
      url = '/api/users/uploadUserProfileImage';
      flag = 'profileImage';
      if (this.userDetailData['profileImage'])
        fileName = this.userDetailData['profileImage'];
      else {
        const imageTypeIndex = (this.fileToUpload.name).indexOf('.');
        const imageType = (this.fileToUpload.name).substr(imageTypeIndex, this.fileToUpload.name.length);
        fileName = 'user_image_' + this.userDetailData['id'] + imageType;
        this.updateImageDetail('FileName', fileName);
      }
    }
      const formData: FormData = new FormData();
      formData.append('files', this.fileToUpload, fileName);
      this.uploadUserImage(url, formData, actionKey, fileName);
  }

  // update image detail in user, if not exist
  updateImageDetail(key, fileName): void {
    const url = '/api/users/updateUserAccess';
    const param = {};
    param['id'] = this.userDetailData['id'];
    param['key'] = key;
    param['value'] = fileName;
    this.apiService.postData(url, param).subscribe( data => {

    });
  }

  // upload userimage to server
  uploadUserImage(url: string, formData: FormData, action: string, fileName: string): void {
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.postData(url, formData).subscribe(data => {
        // window.location.reload();
        this.coverImagePickerFlag = false;
        this.profileImagePickerFlag = false;
        this.toastrService.success('Image updated successfully...It will be reflected by reloading.', 'Success', { timeOut: 6000 });
        this.utilityService.updateSpinnerFlag(false);
        // window.location.reload();
    });
  }

  // event handler for document selected
  documentSelected(e: any, flag?: string): void {
    // validate for bulk upload
    this.fileToUpload = e.item(0);
    if (flag === 'cover') {
      this.submitDocument(flag);
    } else {
      this.submitDocument(flag);
    }
  }

  // update the cover image of the user
  // updateCoverImage(): void {
  //   const imageSrc = this.getImageSource('cover');
  //   this.utilityService.updateSpinnerFlag(true);
  //   const url = '/api/users/updateCoverImage/' + this.userDetail['id'];
  //   this.apiService.putData(url, imageSrc).subscribe( data => {
  //     this.toastrService.success("User Cover Image Updated Successfully...", "Success", { timeOut: 1000 });
  //     this.utilityService.updateSpinnerFlag(false);
  //   });
  // }

  // // update the profile image of the user
  // updateProfileImage(): void {
  //   const imageSrc = this.getImageSource('profile');
  //   this.utilityService.updateSpinnerFlag(true);
  //   const url = '/api/users/updateProfileImage/' + this.userDetail['id'];
  //   this.apiService.putData(url, imageSrc).subscribe( data => {
  //     this.toastrService.success("User Profile Image Updated Successfully...", "Success", { timeOut: 1000 });
  //     this.utilityService.updateSpinnerFlag(false);
  //   });
  // }

}
