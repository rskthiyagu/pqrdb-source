import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Input() dropdownList: Array < object >;
  @Input() selectedItems: Array<object>;
  @Input() dropdownSettings: object;
  @Input() key: string;

  @Output() selectedItemEmitter = new EventEmitter();
  @Output() addNewEmitter = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }


  onItemSelect(item: any) {
    this.selectedItemEmitter.emit(this.selectedItems);
  }
  OnItemDeSelect(item: any) {
    this.selectedItemEmitter.emit(this.selectedItems);
  }
  onSelectAll(items: any) {
    this.selectedItemEmitter.emit(this.selectedItems);
  }
  onDeSelectAll(items: any) {
    this.selectedItems = [];
    this.selectedItemEmitter.emit(this.selectedItems);
  }
  onAddItem(items: any) {    
    if (this.key !== 'states' && this.key !== 'cities') {
      this.dropdownList.push({'id': this.dropdownList.length + 1, 'name': items});
      this.selectedItems.push({'id': this.dropdownList.length + 1, 'name': items});
      this.addNewEmitter.emit(items);
    } else {
      this.addNewEmitter.emit(items);
    }
  }

}
