import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  @Input() set userData(data) {
    if (data && data.length !== 0) {
      for (var i = 0; i < data.length; i++) {
        data[i]['rotateValue'] = 'rotateY(0deg)';
        data[i]['statisticsData'] = [];
        data[i]['statisticsData'].push(data[i]['userCompletedTotal']);
        data[i]['statisticsData'].push(data[i]['userPendingTotal']);
        data[i]['statisticsData'].push(data[i]['total']);
      }
      this.userList = data;
    }
  }

  userList: Array < string > = [];
  loggedUser: object = {};

  constructor(private router: Router,
              private dataService: DataService) { }

  ngOnInit() {
    this.loggedUser = this.dataService.getLoggedUserDetails();
  }

  // toggle card rotation
  rotateCard(offset): void {
    if (this.userList[offset]['rotateValue'] === 'rotateY(0deg)') {
      this.userList[offset]['rotateValue'] = 'rotateY(180deg)';
    } else {
      this.userList[offset]['rotateValue'] = 'rotateY(0deg)';
    }
  }

  // navigate to user page
  navigateUserList(): void {
    this.router.navigateByUrl('/users-dashboard')
  }

}
