import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingCandidateTileComponent } from './pending-candidate-tile.component';

describe('PendingCandidateTileComponent', () => {
  let component: PendingCandidateTileComponent;
  let fixture: ComponentFixture<PendingCandidateTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingCandidateTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingCandidateTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
