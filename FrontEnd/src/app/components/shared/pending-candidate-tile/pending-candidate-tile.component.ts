import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-pending-candidate-tile',
  templateUrl: './pending-candidate-tile.component.html',
  styleUrls: ['./pending-candidate-tile.component.scss']
})
export class PendingCandidateTileComponent implements OnInit {

  @Input() candidateList: Array < object >;

  constructor(private dataService: DataService,
              private router: Router,
              private toastrService: ToastrService) { }

  ngOnInit() {
  }

  // navigate candidate view page
  navigateCandidateEdit(id: number, data: object): void {
    this.dataService.setSelectedCandidateDetail(data);
    const url = 'candidate-edit/' + id;
    this.router.navigateByUrl(url);
  }

  // download resume
  downloadResume(event: any, fileName: string): void {
    event.stopPropagation();
    const docUrl = 'http://210.18.138.11:8081/api/temp/' + fileName;
    window.open(docUrl, '_self');
    this.toastrService.success('Resume downloaded successfully...', 'Success', { timeOut: 2500});
  }

}
