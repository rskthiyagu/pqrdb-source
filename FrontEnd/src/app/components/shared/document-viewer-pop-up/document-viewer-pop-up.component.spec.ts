import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentViewerPopUpComponent } from './document-viewer-pop-up.component';

describe('DocumentViewerPopUpComponent', () => {
  let component: DocumentViewerPopUpComponent;
  let fixture: ComponentFixture<DocumentViewerPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentViewerPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentViewerPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
