import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-document-viewer-pop-up',
  templateUrl: './document-viewer-pop-up.component.html',
  styleUrls: ['./document-viewer-pop-up.component.scss']
})
export class DocumentViewerPopUpComponent implements OnInit {

  @Input() displayFlag: boolean;
  @Input() documentUrl: string;
  @Input() title: string;

  @Output() loadDocument = new EventEmitter();
  @Output() closeEmitter = new EventEmitter();

  documentHeight: number;

  constructor() { }

  ngOnInit() {
  }

  // handle selection of files
  handleDocumentLoadSucess(event): void {
    this.loadDocument.emit(event);
  }

  // close modal
  closeModal(): void {
    this.closeEmitter.emit();
  }

}
