import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {

  @Input() set data(dataSet) {
    let temp = [];
    if (dataSet && dataSet.length !== 0) {
      const obj = {
        data: dataSet
      }
      temp.push(obj);
      this.chartDatasets = temp;
    }
  }

  chartDatasets: Array < object > = [];

  constructor() { }

  ngOnInit() {
  }

  public chartType: string = 'pie';

  // public chartDatasets: Array<any> = [
  //   { data: [300, 50, 100, 40, 120], label: 'My First dataset' }
  // ];

  public chartLabels: Array<any> = ['Uploaded Candidates', 'Pending Candidates', 'Total Candidates'];

  public chartColors: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C'], // , '#949FB1', '#4D5360'
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870'],  // , '#A8B3C5', '#616774'
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

}
