import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-detail-table',
  templateUrl: './detail-table.component.html',
  styleUrls: ['./detail-table.component.scss']
})
export class DetailTableComponent implements OnInit {

  @Input() set configData(data) {
    if (data) {
      this.tableTitle = data['title'];
      this.widthOffset = data['width'];
      this.tableFieldList = data['field'];
      this.tableValueList = data['data'];
    }
  }

  @Output() linkEmitter = new EventEmitter();
  @Output() actionEmitter = new EventEmitter();

  tableFieldList: Array < string > = [];
  tableValueList: Array < object > = [];
  tableTitle: String = '';
  widthOffset: number;

  constructor() { }

  ngOnInit() {
  }

  // handling click event
  linkClicked(data: object, field: object): void {
    const obj = {
      'field': field,
      'data': data
    };
    this.linkEmitter.emit(obj);
  }

  // action click handler
  actionClicked(offset: any, obj: object): void {
    const tempArray = this.tableValueList;
    const flagBackup = tempArray[offset]['actionDropdownFlag'];
    for (let i = 0; i < tempArray.length; i++) {
      tempArray[i]['actionDropdownFlag'] = false;
    }
    tempArray[offset]['actionDropdownFlag'] = !flagBackup;
    this.tableValueList = tempArray;
  }

  // sub menu click handler
  subMenuClicked(menu: object, obj: object): void {
    this.actionEmitter.emit(obj);
  }

}
