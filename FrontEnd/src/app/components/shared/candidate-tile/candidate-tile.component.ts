import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-candidate-tile',
  templateUrl: './candidate-tile.component.html',
  styleUrls: ['./candidate-tile.component.scss']
})
export class CandidateTileComponent implements OnInit {

  @Input() set candidateList(data) {
    if (data && data.length !== 0) {
      for (let i = 0; i < data.length; i++) {
        let flag = false;
        data[i]['heightValue'] = 155;
        data[i]['fullName'] = '';
        if (data[i]['firstName']) {
          data[i]['fullName'] = data[i]['firstName'];
          flag = true;
        }
        if (data[i]['lastName']) {
          if (flag) {
            data[i]['fullName'] += ', ';
          }
          data[i]['fullName'] += data[i]['lastName'];
        }
        // date updation
        data[i]['createdOn'] = new Date(data[i]['createdOn']);
        data[i]['updatedOn'] = new Date(data[i]['updatedOn']);
      }
      this.candidateDataList = data;
    } else {
      this.candidateDataList = [];
    }
  }

  @Output() navigationEmitter = new EventEmitter();

  candidateDataList: Array < object >;

  constructor(private toastrService: ToastrService) { }

  ngOnInit() {
  }

  // toggle view more detail
  toggleViewDetail(event, offset, flag): void {
    event.stopPropagation();
    if (flag) {
      this.candidateDataList[offset]['viewDetailFlag'] = true;
      this.candidateDataList[offset]['heightValue'] = 300;
    } else {
      this.candidateDataList[offset]['viewDetailFlag'] = false;
      this.candidateDataList[offset]['heightValue'] = 155;
    }
  }

  // navigate candidate view page
  navigateCandidateView(id: number, data: object): void {
    this.navigationEmitter.emit({'id': id, 'data': data});
    // this.dataService.setSelectedCandidateDetail(data);
    // const url = 'candidate-view/' + id;
    // this.router.navigateByUrl(url);
  }

  // download resume
  downloadResume(event: any, fileName: string): void {
    event.stopPropagation();
    const docUrl = 'http://pqrdb.geval6.com/api/resumes/' + fileName;
    window.open(docUrl, '_self');
    this.toastrService.success('Resume downloaded successfully...', 'Success', { timeOut: 2500});
  }

}
