import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-overview-tile',
  templateUrl: './overview-tile.component.html',
  styleUrls: ['./overview-tile.component.scss']
})
export class OverviewTileComponent implements OnInit {

  @Input() set candidateStatisticsData(data) {
    if (data) {
      const dataArray = [];
      const keyList = Object.keys(data);
      for (let i = 0; i < keyList.length; i++) {
        if (keyList[i] === 'total') {
          data[keyList[i]][0]['title'] = 'Successful Uploads';
        } else if (keyList[i] === 'pending') {
          data[keyList[i]][0]['title'] = 'Pending Uploads';
        } else {
          data[keyList[i]][0]['title'] = 'Today Uploads';
        }
        const subKeyList = Object.keys(data[keyList[i]][0]);
        data[keyList[i]][0]['progress'] = (data[keyList[i]][0][subKeyList[1]] / data[keyList[i]][0][subKeyList[0]]) * 100;
        data[keyList[i]][0]['count'] = data[keyList[i]][0][subKeyList[1]];
        data[keyList[i]][0]['total'] = data[keyList[i]][0][subKeyList[0]];
        dataArray.push(data[keyList[i]][0]);
      }
      console.log(dataArray);
      this.statisticsData = dataArray;
    }
  }

  @Input() totalFlag: boolean;

  statisticsData: Array < object > = [];

  constructor() { }

  ngOnInit() {
  }

}
