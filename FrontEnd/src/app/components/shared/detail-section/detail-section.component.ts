import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-detail-section',
  templateUrl: './detail-section.component.html',
  styleUrls: ['./detail-section.component.scss']
})
export class DetailSectionComponent implements OnInit {

  @Input() set setFieldData(dataObject) {
    // this.editMode = dataObject['editFlag'];
    this.fieldData = dataObject;
    // if (this.editMode) {     disable this for sync suggusted skills on all modes
    this.setDefaultValues();
    // }
  }

  @Input() set acquiredSkillList(skillData) {
    if (this.fieldData && this.fieldData.length !== 0) {
      const filterData = this.fieldData;
      for (let i = 0; i < filterData.length; i++) {
        if (filterData[i]['key'] === 'skills') {
          if (filterData[i]['dropdownSelected'] && filterData[i]['dropdownSelected'].length > 0) {
            for (let j = 0; j < skillData.length; j++) {
              (filterData[i]['dropdownSelected']).push(skillData[j]);
            }
          } else {
            filterData[i]['dropdownSelected'] = skillData;
          }
        }
      }
      this.fieldData = filterData;
      this.setDefaultValues();
    }
  }

  @Input() set resultSetGetter(data) {
    if (data) {
      if (this.validateCandidateFields()) {
        const obj = { 'flag': true, 'data': this.resultSetObject }
        this.resultSetEmitter.emit(obj);
      } else {
        const obj = { 'flag': false, 'data': null }
        this.resultSetEmitter.emit(obj);
        this.toastrService.error('Please fill mandatory fields to proceed...', 'Error', { timeOut: 1000 });
      }
    }
  }

  @Input() set newlyAddedId(data) {
    if (data) {
      this.setNewlyAddedItemId(data);
    }
  }

  @Input() set highlightSkill(data) {
    this.highlightFlag = data;
  }

  @Input() set addNewSkill(data) {
    if (data && data.length !== 0) {
      this.addSuggestSkillFlag = true;
      for (let i = 0; i < data.length; i++) {
        this.addNewItemHandler(data[i], 'skills', 4);
      }
    }
  }

  @Output() addnewItemEmitter = new EventEmitter();
  @Output() resultSetEmitter = new EventEmitter();

  fieldData: Array<object>;
  fieldObject: object;
  resultSetObject: object = {};
  editMode: boolean;
  highlightFlag: boolean;
  newSuggestedSkill: Array<string> = [];
  addSuggestSkillFlag: boolean = false;

  // reuired fields
  requiredFieldList: Array<object> = [
    {
      'key': 'firstName',
      'detailFlag': true,
      'blockId': 0,
      'detailId': 0
    },
    {
      'key': 'lastName',
      'detailFlag': true,
      'blockId': 0,
      'detailId': 1
    },
    {
      'key': 'gender',
      'detailFlag': true,
      'blockId': 0,
      'detailId': 2,
      'selectFlag': true
    },
    {
      'key': 'jobTitle',
      'detailFlag': true,
      'blockId': 0,
      'detailId': 3
    },
    {
      'key': 'mobileNumber',
      'detailFlag': true,
      'blockId': 1,
      'detailId': 0
    },
    {
      'key': 'email',
      'detailFlag': true,
      'blockId': 1,
      'detailId': 1,
      'patternFlag': true,
      'pattern': /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
    },
    {
      'key': 'countries',
      'detailFlag': true,
      'blockId': 2,
      'detailId': 0,
      'selectFlag': true
    },
    {
      'key': 'skills',
      'detailFlag': false,
      'blockId': 4,
      'detailId': null,
      'selectFlag': true
    },
    {
      'key': 'authorizations',
      'detailFlag': false,
      'blockId': 7,
      'detailId': null,
      'selectFlag': true,
      'mandatoryKey': 'countries',
      'mandatoryValue': ['usa', 'canada'],
      'mandatorySelectFlag': true
    },
    // {
    //   'key': 'hourlyRate',
    //   'detailFlag': true,
    //   'blockId': 3,
    //   'detailId': 0,
    //   'mandatoryKey': 'countries',
    //   'mandatoryValue': ['usa', 'canada'],
    //   'mandatorySelectFlag': true
    // },
    {
      'key': 'currentCTC',
      'detailFlag': true,
      'blockId': 3,
      'detailId': 3,
      'mandatoryKey': 'countries',
      'mandatoryValue': ['india'],
      'mandatorySelectFlag': true
    },
    {
      'key': 'expectedCTC',
      'detailFlag': true,
      'blockId': 3,
      'detailId': 4,
      'mandatoryKey': 'countries',
      'mandatoryValue': ['india'],
      'mandatorySelectFlag': true
    },
    {
      'key': 'noticePeriod',
      'detailFlag': true,
      'blockId': 3,
      'detailId': 5,
      'mandatoryKey': 'countries',
      'mandatoryValue': ['india'],
      'mandatorySelectFlag': true
    },
    {
      'key': 'relocation',
      'detailFlag': true,
      'blockId': 3,
      'detailId': 7,
      'mandatoryKey': 'countries',
      'mandatoryValue': ['usa', 'canada', 'india'],
      'mandatorySelectFlag': true
    },
    {
      'key': 'taxTerms',
      'detailFlag': true,
      'blockId': 3,
      'detailId': 2,
      'selectFlag': true,
      'mandatoryKey': 'countries',
      'mandatoryValue': ['usa', 'canada'],
      'mandatorySelectFlag': true
    },
    {
      'key': 'duration',
      'detailFlag': true,
      'blockId': 3,
      'detailId': 6,
      'selectFlag': true,
      'mandatoryKey': 'countries',
      'mandatoryValue': ['usa', 'canada', 'india'],
      'mandatorySelectFlag': true
    }
  ];

  constructor(private toastrService: ToastrService,
    private apiService: ApiService) { }

  ngOnInit() {
  }

  // set the error flg
  setErrorFlag(i: number, flag: boolean): void {
    if (this.requiredFieldList[i]['detailFlag']) {
      this.fieldData[this.requiredFieldList[i]['blockId']]['detailList'][this.requiredFieldList[i]['detailId']]['errorFlag'] = flag;
    } else {
      this.fieldData[this.requiredFieldList[i]['blockId']]['errorFlag'] = flag;
    }
  }

  // validate the mandatory fields are filled or not
  validateCandidateFields(): boolean {
    // validation of required fields
    if (this.resultSetObject) {
      let offset = 0;
      for (let i = 0; i < this.requiredFieldList.length; i++) {
        if ((this.resultSetObject[this.requiredFieldList[i]['key']] && !this.requiredFieldList[i]['selectFlag'] && !this.requiredFieldList[i]['mandatoryKey']) ||
          ((this.resultSetObject[this.requiredFieldList[i]['key']] && this.resultSetObject[this.requiredFieldList[i]['key']].length > 0) && this.requiredFieldList[i]['selectFlag'] && !this.requiredFieldList[i]['mandatoryKey'])
          || this.requiredFieldList[i]['mandatoryKey']) {
          // check dependent values
          if (this.requiredFieldList[i]['mandatoryKey'] && this.resultSetObject[this.requiredFieldList[i]['mandatoryKey']]) {
            if ((!this.requiredFieldList[i]['mandatorySelectFlag'] &&
              this.requiredFieldList[i]['mandatoryValue'].includes((this.resultSetObject[this.requiredFieldList[i]['mandatoryKey']]).toLowerCase())) ||
              (this.resultSetObject[this.requiredFieldList[i]['mandatoryKey']]
                && this.resultSetObject[this.requiredFieldList[i]['mandatoryKey']].length > 0
                && this.resultSetObject[this.requiredFieldList[i]['mandatoryKey']][0]
                && this.resultSetObject[this.requiredFieldList[i]['mandatoryKey']][0]['name']
                && this.requiredFieldList[i]['mandatoryValue'].includes((this.resultSetObject[this.requiredFieldList[i]['mandatoryKey']][0]['name']).toLowerCase())
                && this.requiredFieldList[i]['mandatorySelectFlag'])) {
              if ((this.resultSetObject[this.requiredFieldList[i]['key']] && !this.requiredFieldList[i]['selectFlag']) ||
                ((this.resultSetObject[this.requiredFieldList[i]['key']] && this.resultSetObject[this.requiredFieldList[i]['key']].length > 0) && this.requiredFieldList[i]['selectFlag'])) {
                // remove the error
                this.setErrorFlag(i, false);
              } else {
                // remove the error
                this.setErrorFlag(i, true);
                offset++;
              }
            } else {
              // set the error
              this.setErrorFlag(i, false);
            }
          } else if (this.requiredFieldList[i]['patternFlag']) {
            if ((this.resultSetObject[this.requiredFieldList[i]['key']]).match(this.requiredFieldList[i]['pattern'])) {
              this.setErrorFlag(i, false);
            } else {
              this.setErrorFlag(i, true);
              offset++;
            }
          } else if (this.requiredFieldList[i]['patternFlag'] && this.requiredFieldList[i]['mandatoryKey']) {
            if ((this.resultSetObject[this.requiredFieldList[i]['key']]).match(this.requiredFieldList[i]['pattern']) &&
              ((this.requiredFieldList[i]['mandatoryValue'])).includes((this.resultSetObject[this.requiredFieldList[i]['key']]).toLowerCase())) {
              this.setErrorFlag(i, false);
            } else {
              this.setErrorFlag(i, true);
              offset++;
            }
          } else {
            this.setErrorFlag(i, false);
          }
        } else {
          // set the error
          this.setErrorFlag(i, true);
          offset++;
        }
      }
      // check the result
      if (offset > 0) {
        return false;
      } else {
        // if (this.resultSetObject['relocation'][0]['id'] === 1) {
        //    if ((this.resultSetObject['countries'][0]['name']).toLowerCase() == 'india') {
        //       if (this.resultSetObject['relocationCities'] && this.resultSetObject['relocationCities'].length > 0) {
        //           this.fieldData[3]['detailList'][9]['errorFlag'] = false;
        //           return true;
        //       } else {
        //           this.fieldData[3]['detailList'][9]['errorFlag'] = true;
        //           return false;
        //       }
        //    } else {
        //     if (this.resultSetObject['relocationStates'] && this.resultSetObject['relocationStates'].length > 0) {
        //       this.fieldData[3]['detailList'][8]['errorFlag'] = false;
        //       return true;
        //     } else {
        //       this.fieldData[3]['detailList'][8]['errorFlag'] = true;
        //       return false;
        //     }
        //    }
        // } else {
        //   return true;
        // }
        if ((this.resultSetObject['countries'] && this.resultSetObject['countries'].length > 0 && (this.resultSetObject['countries'][0]['name']).toLowerCase() !== 'india') && !this.resultSetObject['hourlyRate'] && !this.resultSetObject['expectedSalary']) {
          this.fieldData[3]['detailList'][0]['errorFlag'] = true;
          this.fieldData[3]['detailList'][1]['errorFlag'] = true;
          return false;
        } else {
          this.fieldData[3]['detailList'][0]['errorFlag'] = false;
          this.fieldData[3]['detailList'][1]['errorFlag'] = false;
        }
        return true;
      }
    } else {
      for (let i = 0; i < this.requiredFieldList.length; i++) {
        // set the error
        this.setErrorFlag(i, true);
      }
      return false;
    }
  }

  addNewItemHandler(addedText: string, key: string, offset: number): void {
    this.fieldObject = this.fieldData[offset];
    if (this.fieldObject['multiSelect']) {
      switch (key) {
        case 'cities': {
          if ((this.fieldObject['detailList'][0]['dropdownSelected'] && this.fieldObject['detailList'][0]['dropdownSelected'].length === 0) && (this.fieldObject['detailList'][1]['dropdownSelected'] && this.fieldObject['detailList'][1]['dropdownSelected'].length === 0)) {
            this.fieldObject['detailList'][0]['errorFlag'] = true;
            this.fieldObject['detailList'][1]['errorFlag'] = true;
          } else if ((this.fieldObject['detailList'][0]['dropdownSelected'] && this.fieldObject['detailList'][0]['dropdownSelected'].length === 0)) {
            this.fieldObject['detailList'][0]['errorFlag'] = true;
          } else if ((this.fieldObject['detailList'][1]['dropdownSelected'] && this.fieldObject['detailList'][1]['dropdownSelected'].length === 0)) {
            this.fieldObject['detailList'][1]['errorFlag'] = true;
          } else if ((this.fieldObject['detailList'][0]['dropdownSelected'] && this.fieldObject['detailList'][0]['dropdownSelected'].length !== 0) && (this.fieldObject['detailList'][1]['dropdownSelected'] && this.fieldObject['detailList'][1]['dropdownSelected'].length !== 0)) {
            this.fieldObject['detailList'][0]['errorFlag'] = false;
            this.fieldObject['detailList'][1]['errorFlag'] = false;
            this.addnewItemEmitter.emit({ 'key': key, 'name': addedText, 'stateId': this.fieldObject['detailList'][1]['dropdownSelected'][0]['id'], 'countryId': this.fieldObject['detailList'][0]['dropdownSelected'][0]['id'] });
            this.fieldData[offset]['detailList'][2]['dropdownData'].push({ 'id': this.fieldData[offset]['detailList'][0]['dropdownData'].length, 'name': addedText });
            this.fieldData[offset]['detailList'][2]['dropdownSelected'] = [{ 'id': this.fieldData[offset]['detailList'][0]['dropdownData'].length, 'name': addedText }];
            this.resultSetObject[key] = [{ 'id': this.fieldData[offset]['detailList'][0]['dropdownData'].length, 'name': addedText, 'newFlag': true }];
          }
          break;
        }
        case 'states': {
          if (this.fieldObject['detailList'][0]['dropdownSelected'] && this.fieldObject['detailList'][0]['dropdownSelected'].length === 0) {
            this.fieldObject['detailList'][0]['errorFlag'] = true;
          }
          if (this.fieldObject['detailList'][0]['dropdownSelected'] && this.fieldObject['detailList'][0]['dropdownSelected'].length !== 0) {
            this.fieldObject['detailList'][0]['errorFlag'] = false;
            this.addnewItemEmitter.emit({ 'key': key, 'name': addedText, 'countryId': this.fieldObject['detailList'][0]['dropdownSelected'][0]['id'] });
            this.fieldData[offset]['detailList'][2]['dropdownSelected'] = [];
            this.fieldData[offset]['detailList'][1]['dropdownData'].push({ 'id': this.fieldData[offset]['detailList'][1]['dropdownData'].length, 'name': addedText });
            this.fieldData[offset]['detailList'][1]['dropdownSelected'] = [{ 'id': this.fieldData[offset]['detailList'][1]['dropdownData'].length, 'name': addedText }];
            this.resultSetObject[key] = [{ 'id': this.fieldData[offset]['detailList'][1]['dropdownData'].length, 'name': addedText, 'newFlag': true }];
            // trigger select event
          }
          break;
        }
        case 'countries': {
          this.addnewItemEmitter.emit({ 'key': key, 'name': addedText });
          this.fieldData[offset]['detailList'][1]['dropdownSelected'] = [];
          this.fieldData[offset]['detailList'][2]['dropdownSelected'] = [];
          this.fieldData[offset]['detailList'][0]['dropdownSelected'] = [{ 'id': this.fieldData[offset]['detailList'][0]['dropdownData'].length, 'name': addedText }];
          this.resultSetObject[key] = [{ 'id': this.fieldData[offset]['detailList'][0]['dropdownData'].length, 'name': addedText, 'newFlag': true }];
          // set the dropdown values for relocation
          this.fieldData[3]['detailList'][8]['dropdownData'] = [];
          this.fieldData[3]['detailList'][9]['dropdownData'] = [];
          this.resultSetObject['relocationStates'] = [];
          this.resultSetObject['relocationCities'] = [];
          break;
        }
      }
    } else {
      if (((this.resultSetObject[key] && this.resultSetObject[key].length !== 0) && (!this.fieldObject['dropdownSetting']['singleSelection']))
        || (this.fieldObject['dropdownSelected'] && this.fieldObject['dropdownSelected'].length !== 0)) {
        if ((this.fieldObject['dropdownSelected'] && this.fieldObject['dropdownSelected'].length !== 0) && (this.resultSetObject[key] && this.resultSetObject[key].length === 0))
          this.resultSetObject[key] = this.fieldObject['dropdownSelected'];
        if (this.addSuggestSkillFlag)
          this.resultSetObject[key].push({ 'id': this.fieldObject['dropdownData'].length, 'name': addedText });
        this.resultSetObject[key][this.resultSetObject[key].length - 1]['newFlag'] = true;
      } else {
        this.resultSetObject[key] = [{ 'id': this.fieldObject['dropdownData'].length, 'name': addedText, 'newFlag': true }];
      }
      this.addnewItemEmitter.emit({ 'key': key, 'name': addedText });
    }
  }

  // handle dropdown select data setter
  handleSelectedItems(selectedData: Array<object>, key: string, offset?: number): void {
    if (offset !== null && offset !== undefined) {
      this.fieldObject = this.fieldData[offset];
      // nullify the errors on action
      this.fieldObject['detailList'][0]['errorFlag'] = false;
      this.fieldObject['detailList'][1]['errorFlag'] = false;
      if (this.fieldObject['multiSelect']) {
        if (selectedData && selectedData.length !== 0) {
          switch (key) {
            case 'countries': {
              const stateUrl = '/api/states/fetchRecordByCountryId/' + selectedData[0]['id'];
              const cityUrl = '/api/cities/fetchRecordByCountryId/' + selectedData[0]['id'];
              const urlList = [stateUrl, cityUrl];
              this.apiService.forkGetData(urlList).subscribe(data => {
                this.fieldData[offset]['detailList'][1]['dropdownData'] = data[0]['states'];
                this.fieldData[offset]['detailList'][2]['dropdownData'] = data[1]['cities'];
                // selected values setter
                this.fieldData[offset]['detailList'][0]['dropdownSelected'] = selectedData;
                this.fieldData[offset]['detailList'][1]['dropdownSelected'] = [];
                this.fieldData[offset]['detailList'][2]['dropdownSelected'] = [];
                // set the dropdown values for relocation
                let dataList = [{'id': 'any', 'name': 'Any'}]
                if ((selectedData[0]['name']).toLowerCase() === 'india') {
                  const list = dataList.concat(data[1]['cities']);
                  this.fieldData[3]['detailList'][8]['dropdownData'] = [];
                  this.fieldData[3]['detailList'][9]['dropdownData'] = list;
                } else {
                  const list = dataList.concat(data[0]['states']);
                  this.fieldData[3]['detailList'][8]['dropdownData'] = list;
                  this.fieldData[3]['detailList'][9]['dropdownData'] = [];
                }
                this.resultSetObject['relocationStates'] = [];
                this.resultSetObject['relocationCities'] = [];
                // set the result set value
                this.resultSetObject[key] = selectedData;
                this.resultSetObject['states'] = [];
                this.resultSetObject['cities'] = [];
              });
              break;
            }
            case 'states': {
              const cityUrl = '/api/cities/fetchRecordByStateId/' + selectedData[0]['id'];
              this.apiService.getData(cityUrl).subscribe(data => {
                this.fieldObject['detailList'][2]['dropdownData'] = data['cities'];
                // selected values for country
                this.fieldData[offset]['detailList'][0]['dropdownSelected'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
                this.fieldData[offset]['detailList'][1]['dropdownSelected'] = selectedData;
                this.fieldData[offset]['detailList'][2]['dropdownSelected'] = [];
                // set the result set value
                this.resultSetObject['countries'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
                this.resultSetObject[key] = selectedData;
                this.resultSetObject['cities'] = [];
              });
              break;
            }
            case 'cities': {
              // selected values for country
              this.fieldData[offset]['detailList'][2]['dropdownSelected'] = selectedData;
              this.fieldData[offset]['detailList'][1]['dropdownSelected'] = [{ 'id': selectedData[0]['stateId'], 'name': selectedData[0]['stateName'], 'countryId': selectedData[0]['countryId'], 'countryName': selectedData[0]['countryName'] }];
              this.fieldData[offset]['detailList'][0]['dropdownSelected'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
              // set the result set value
              this.resultSetObject['countries'] = [{ 'id': selectedData[0]['countryId'], 'name': selectedData[0]['countryName'] }];
              this.resultSetObject['states'] = [{ 'id': selectedData[0]['stateId'], 'name': selectedData[0]['stateName'], 'countryId': selectedData[0]['countryId'], 'countryName': selectedData[0]['countryName'] }];
              this.resultSetObject['cities'] = selectedData;
              break;
            }
          }
        } else {
          switch (key) {
            case 'countries': {
              const stateUrl = '/api/states/fetchRecord';
              const cityUrl = '/api/cities/fetchRecord';
              const urlList = [stateUrl, cityUrl];
              this.apiService.forkGetData(urlList).subscribe(data => {
                this.fieldData[offset]['detailList'][1]['dropdownData'] = data[0]['states'];
                this.fieldData[offset]['detailList'][2]['dropdownData'] = data[1]['cities'];
                // selected value
                this.fieldData[offset]['detailList'][0]['dropdownSelected'] = selectedData;
                this.fieldData[offset]['detailList'][1]['dropdownSelected'] = selectedData;
                this.fieldData[offset]['detailList'][2]['dropdownSelected'] = selectedData;
                // set the dropdown values for relocation
                  this.fieldData[3]['detailList'][8]['dropdownData'] = [];
                  this.fieldData[3]['detailList'][9]['dropdownData'] = [];
                  this.resultSetObject['relocationStates'] = [];
                  this.resultSetObject['relocationCities'] = [];
                // set the result values
                this.resultSetObject['countries'] = selectedData;
                this.resultSetObject['states'] = selectedData;
                this.resultSetObject['cities'] = selectedData;
              });
              break;
            }
            case 'states': {
              const cityUrl = '/api/cities/fetchRecord';
              this.apiService.getData(cityUrl).subscribe(data => {
                this.fieldObject['detailList'][2]['dropdownData'] = data['cities'];
                // selected value
                this.fieldData[offset]['detailList'][1]['dropdownSelected'] = selectedData;
                this.fieldData[offset]['detailList'][2]['dropdownSelected'] = selectedData;
                // set the result set values
                this.resultSetObject['states'] = selectedData;
                this.resultSetObject['cities'] = selectedData;
              });
              break;
            }
            case 'cities': {
              this.fieldData[offset]['detailList'][2]['dropdownSelected'] = selectedData;
              this.resultSetObject['cities'] = selectedData;
              break;
            }
          }
        }
      }
    } else {
      this.resultSetObject[key] = selectedData;
    }
  }

  // handle input data setter
  setInputData(event: any, key: string, data: string, pattern: any, type: string): void {
    // number field validation
    if (key === 'mobileNumber') {
      // validate not number in mobile
      let flag = true;
      let updatedData = '';
      for (let i = 0; i < data.length; i++) {
        if ((parseInt(data[i]) >= 0 && parseInt(data[i]) <= 9)) {
          updatedData += data[i];
        } else {
          flag = false;
        }
      }
      if (!flag) {
        data = updatedData;
        event.target.value = data;
      }
    } else if (key === 'noticePeriod') {
      let flag = true;
      let updatedData = '';
      for (let i = 0; i < data.length; i++) {
        if ((parseInt(data[i]) >= 0 && parseInt(data[i]) <= 9) || data[i] == '.') {
          updatedData += data[i];
        } else {
          flag = false;
        }
      }
      if (!flag) {
        data = updatedData;
        event.target.value = data;
      }
    } else if (key === 'hourlyRate' || key === 'expectedSalary' || key === 'currentCTC' || key === 'expectedCTC') {
      // validate not number in mobile
      let flag = true;
      let updatedData = '';
      for (let i = 0; i < data.length; i++) {
        if ((parseInt(data[i]) >= 0 && parseInt(data[i]) <= 9) || data[i] == ',' || data[i] == '.' || data[i] == 'l' || data[i] == 'L' || data[i] == 'k' || data[i] == 'K') {
          updatedData += data[i];
        } else {
          flag = false;
        }
      }
      if (!flag) {
        data = updatedData;
        event.target.value = data;
      }
    }
    this.resultSetObject[key] = data;
  }

  // set default values in edit mode
  setDefaultValues(): void {
    for (let i = 0; i < this.fieldData.length; i++) {
      // select case
      if (this.fieldData[i]['selectFlag']) {
        this.resultSetObject[this.fieldData[i]['key']] = this.fieldData[i]['dropdownSelected'];
      } else if (this.fieldData[i]['multiSelect']) {
        for (let j = 0; j < this.fieldData[i]['detailList'].length; j++) {
          this.resultSetObject[this.fieldData[i]['detailList'][j]['key']] = this.fieldData[i]['detailList'][j]['dropdownSelected'];
        }
      } else {
        for (let j = 0; j < this.fieldData[i]['detailList'].length; j++) {
          this.resultSetObject[this.fieldData[i]['detailList'][j]['key']] = this.fieldData[i]['detailList'][j]['data'];
        }
      }
    }
  }

  // set the id for newly added item
  setNewlyAddedItemId(obj: object): void {
    // for skill suggestion data population
    if (this.addSuggestSkillFlag) {
      this.apiService.getData('/api/skills/fetchRecord').subscribe(skillData => {
        if (skillData && skillData['skills']) {
          const fieldConfigDataSet = this.fieldData;
          fieldConfigDataSet[3]['dropdownData'] = skillData['skills'];
          this.fieldData = fieldConfigDataSet;
        }
        this.addSuggestSkillFlag = false;
      });
    }
    // update actual id for newly created skills
    if (obj['id']) {
      for (let i = 0; i < this.resultSetObject[obj['key']].length; i++) {
        if (this.resultSetObject[obj['key']][i]['newFlag']) {
          this.resultSetObject[obj['key']][i]['id'] = obj['id'];
          this.resultSetObject[obj['key']][i]['newFlag'] = false;
        }
      }
    }
    //}
  }

}
