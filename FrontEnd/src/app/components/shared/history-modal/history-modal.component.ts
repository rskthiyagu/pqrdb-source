import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-history-modal',
  templateUrl: './history-modal.component.html',
  styleUrls: ['./history-modal.component.scss']
})
export class HistoryModalComponent implements OnInit {

  @Input() displayFlag: boolean;
  @Input() set historyData(data) {
    this.composeHistory(data);
  }
  @Input() pastHistoryData: Array < object >; 

  @Output() closeEmitter = new EventEmitter();

  submittedOpenFlag: boolean = false;
  notSubmittedOpenFlag: boolean = false;
  historyOpenFlag: boolean = false;
  pastHistoryOpenFlag: boolean = false;

  submittedDataList: Array < string >;
  notSubmittedDataList: Array < string >;
  historyDataList: Array < string >;
  

  constructor() { }

  ngOnInit() {
  }

  // compose submited & not subitted & history text
  composeHistory(historyData: Array < object >): void {
    const submittedRecordList = [];
    const notsubmittedRecordList = [];
    const historyRecordList = [];
    for (let i = 0; i < historyData.length; i++) {
      let actionText = '';
      // availability check
      if (historyData[i]['availability']) {
        actionText = 'Availability status update on ' + historyData[i]['updatedOn'] + ' by ' + historyData[i]['updateUserName'];
        historyRecordList.push(actionText);
      }
      // submitted
      if (historyData[i]['submission'] === 1) {
        actionText = 'Submitted ' + historyData[i]['submittedToName'] + ' for ' + historyData[i]['requirementsName'] + ', ' + historyData[i]['clientsName'] + ' on ' + historyData[i]['updatedOn'] + ' by ' + historyData[i]['updateUserName'];
        historyRecordList.push(actionText);
        submittedRecordList.push(actionText);
      }
      // not submitted
      if (historyData[i]['submission'] === 0) {
        actionText = 'Not submitted' + ' for ' + historyData[i]['requirementsName'] + ', ' + historyData[i]['clientsName'] + ' on ' + historyData[i]['updatedOn'] + ' by ' + historyData[i]['updateUserName'];
        historyRecordList.push(actionText);
        notsubmittedRecordList.push(actionText);
      }
      // interview
      if (historyData[i]['interview']) {
        actionText = historyData[i]['interviewStatusName'] + ' for ' + historyData[i]['interviewRoundName'] + ' of ' + historyData[i]['requirementsName'] + ', ' + historyData[i]['clientsName'] + ' on ' + historyData[i]['updatedOn'] + ' by ' + historyData[i]['updateUserName'];
        historyRecordList.push(actionText);
      }
    }
    this.submittedDataList = submittedRecordList;
    this.notSubmittedDataList = notsubmittedRecordList;
    this.historyDataList = historyRecordList;
  }

  // toggle header
  toggleHeader(flag: string): void {
    this.submittedOpenFlag = false;
    this.notSubmittedOpenFlag = false;
    this.historyOpenFlag = false;
    this.pastHistoryOpenFlag = false;
    this[flag] = true;
  }

  // close emitter
  closeModal(): void {
    this.closeEmitter.emit();
  }

}
