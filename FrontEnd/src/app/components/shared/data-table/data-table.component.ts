import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  @Input() config: Array < object >;
  @Input() data: Array < object >;
  @Input() link: Array < object >;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  // navigate to appropriate dashboard
  navigateDashboard(link): void {
    this.router.navigateByUrl(link);
  }

}
