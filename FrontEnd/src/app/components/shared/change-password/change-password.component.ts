import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';
import { DataService } from '../../../services/data.service';
import { HashingService } from '../../../services/hashing.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  userCredential: object = {
    'oldPassword': '',
    'password': ''
  };
  passwordViewFlag: boolean = false;
  loggedUserDetails: object;
  requiredFlag: boolean = false;
  passwordError: boolean = false;

  constructor(private apiService: ApiService,
    private utilityService: UtilityService,
    private dataService: DataService,
    private router: Router,
    private toastrService: ToastrService,
    private hashingService: HashingService) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (userData) {
      this.dataService.setLoggedUserDetails(userData);
      this.loggedUserDetails = userData;
    } else if (!userData && !localUserData) {
      this.router.navigateByUrl('');
    } else {
      this.loggedUserDetails = localUserData;
    }
  }

  // change password view mode
  changePasswordViewMode(): void {
    this.passwordViewFlag = !this.passwordViewFlag;
  }

  // validate user data
  validateData(): boolean {
    if (this.userCredential && this.userCredential['oldPassword'] && this.userCredential['oldPassword'] !== '' && this.userCredential['password'] && this.userCredential['password'] !== '') {
      if (this.userCredential['oldPassword'] !== this.userCredential['password']) {
        return true;
      } else {
        this.passwordError = true;
        return false;
      }
    } else {
      return false;
    }
  }

  // get password hashing
  getPasswordHash(): void {
    this.hashingService.getHash(this.userCredential['oldPassword']).then(hashData => {
      const credentialObject = {};
      credentialObject['oldPasswordHash'] = hashData;
      credentialObject['encryptNewPassword'] = this.hashingService.getEncryptValue(this.userCredential['password']);
      credentialObject['userName'] = this.loggedUserDetails['userName'];
      this.updatePassword(credentialObject);
    });
  }

  // update password call
  updatePassword(credentialObject: object): void {
    const url = '/api/users/updatePassword/' + this.loggedUserDetails['id'];
      this.utilityService.updateSpinnerFlag(true);
      this.apiService.putData(url, credentialObject).subscribe(data => {
        this.utilityService.updateSpinnerFlag(false);
        if (data) {
        if (data && data['status'] && data['status'] === 'Password not match') {
          this.toastrService.error('Current Password not match', 'Error', { timeOut: 1000});
        } else {
          this.userCredential['password'] = '';
          this.userCredential['oldPassword'] = '';
          this.passwordError = false;
          this.requiredFlag = false;
          this.toastrService.success('Updation successful...', 'Success', { timeOut: 1000 });
        }
      } else {
        this.toastrService.error('Updation failed', 'Error', { timeOut: 1000});
      }
      });
  }

  // update user details
  updateUserDetail(): void {
    const flag = this.validateData();
    if (flag) {
      this.getPasswordHash();
    } else {
      this.requiredFlag = true;
      if (this.passwordError) {
        this.toastrService.error("Both passwords are same.", "Error", { timeOut: 1000 });
      } else {
        this.toastrService.error("Please fill required field...", "Error", { timeOut: 1000 });
      }
      this.passwordError = false;
    }
  }

  // cancel changes
  cancelChanges(): void {
    this.userCredential['password'] = '';
    this.userCredential['oldPassword'] = '';
  }

}
