import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateShortTileComponent } from './candidate-short-tile.component';

describe('CandidateShortTileComponent', () => {
  let component: CandidateShortTileComponent;
  let fixture: ComponentFixture<CandidateShortTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateShortTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateShortTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
