import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-candidate-short-tile',
  templateUrl: './candidate-short-tile.component.html',
  styleUrls: ['./candidate-short-tile.component.scss']
})
export class CandidateShortTileComponent implements OnInit {

  @Input() set data(candidateData) {
    this.candidateDataSet = candidateData;
    this.startOffset = 0;
    this.endOffset = 4;
    const temp = [];
    for (let i = 0; i < this.candidateDataSet.length; i++) {
      // date updation
      this.candidateDataSet[i]['createdOn'] = new Date(this.candidateDataSet[i]['createdOn']);
      // this.candidateDataSet[i]['createdOn'].setHours(this.candidateDataSet[i]['createdOn'].getHours() + 5);
      // this.candidateDataSet[i]['createdOn'].setMinutes(this.candidateDataSet[i]['createdOn'].getMinutes() + 30);
      this.candidateDataSet[i]['updatedOn'] = new Date(this.candidateDataSet[i]['updatedOn']);
      // this.candidateDataSet[i]['updatedOn'].setHours(this.candidateDataSet[i]['updatedOn'].getHours() + 5);
      // this.candidateDataSet[i]['updatedOn'].setMinutes(this.candidateDataSet[i]['updatedOn'].getMinutes() + 30);
    }
    for (let i = 0; i < 4; i++) {
      if (i < this.candidateDataSet.length)
        temp.push(this.candidateDataSet[i]);
    }
    this.candidatePagingData = temp;
  }

  candidateDataSet: Array<object> = [];
  candidatePagingData: Array<object> = [];
  startOffset: number;
  endOffset: number;

  constructor(private router: Router,
              private toastrService: ToastrService) { }

  ngOnInit() {
  }

  // navigate next rows
  navigateNext(): void {
    if (this.endOffset < this.candidateDataSet.length) {
      this.startOffset = this.startOffset + 4;
      this.endOffset = this.endOffset + 4;
      this.candidatePagingData = [];
      const temp = [];
      for (let i = this.startOffset; i < this.endOffset; i++) {
        if (i < this.candidateDataSet.length) {
          temp.push(this.candidateDataSet[i]);
        }
      }
      this.candidatePagingData = temp;
    }
  }

  // navigate back
  navigateBack(): void {
    if (this.startOffset !== 0) {
      this.startOffset = this.startOffset - 4;
      this.endOffset = this.endOffset - 4;
      this.candidatePagingData = [];
      const temp = [];
      for (let i = this.startOffset; i < this.endOffset; i++) {
        if (i < this.candidateDataSet.length) {
          temp.push(this.candidateDataSet[i]);
        }
      }
      this.candidatePagingData = temp;
    }
  }

  // navigate to candidate dashboard
  navigateDashboard(): void {
    this.router.navigateByUrl('candidate-dashboard');
  }

  // navigate to view page
  navigateViewPage(candidateData: object): void {
    const url = 'candidate-view/' + candidateData['id'];
    this.router.navigateByUrl(url);
  }

  // download resume
  downloadResume(event: any): void {
    event.stopPropagation();
    this.toastrService.success('Resume downloaded successfully...', 'Success', { timeOut: 2500});
  }

}
