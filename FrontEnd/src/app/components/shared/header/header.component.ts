import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  subTextFlag: boolean = true;
  subMenuFlag: boolean = false;

  constructor(public dataService: DataService,
              private router: Router) { }

  ngOnInit() {
    this.setHeaderTitleFlag();
  }

  // log out the user
  logoutUser(event: any): void {
    event.stopPropagation();
    this.disableSubMenu();
    this.dataService.setLoggedUserDetails(null);
    localStorage.removeItem('userDetails');
    this.router.navigateByUrl('');
  }

  // navigate to home from logo
  navigateToHome(event: any): void {
    event.stopPropagation();
    this.disableSubMenu();
    this.router.navigateByUrl('/dashboard');
  }

  // set the flag for header sub text
  setHeaderTitleFlag(): void {
    const scope = this;
    setInterval(function() {
      scope.subTextFlag = !scope.subTextFlag;
    }, 3000);
  }

  // navigate to profile & settings page
  navigatePage(event: any, pageLink: string): void {
    event.stopPropagation();
    this.disableSubMenu();
    this.router.navigateByUrl(pageLink);
  }

  // enable sub menu
  enableSubMenu(): void {
    this.subMenuFlag = !this.subMenuFlag;
  }

  // disable sub menu
  disableSubMenu(): void {
    this.subMenuFlag = false;
  }

}
