import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';

import * as moment from 'moment';

@Component({
  selector: 'app-candidate-detail-modal',
  templateUrl: './candidate-detail-modal.component.html',
  styleUrls: ['./candidate-detail-modal.component.scss']
})
export class CandidateDetailModalComponent implements OnInit {

  @Input() set candidateInfo(data) {
    if (data) {
      this.setData(data);
    }
  }

  @Input() set EditOptionFlag(flag) {
    this.editFlag = flag;
  }

  @Input() set submittalMode(flag) {
    this.submittalFlag = flag;
  }

  @Input() set interviewMode(flag) {
    if (flag)
      this.isInterview = true;
    else
      this.isInterview = false;
    this.interviewFlag = flag;
  }

  @Input() set displayFlag(data) {
    this.modalDisplayFlag = data;
    if (data) {
      this.getAvailableStatusData();
      this.getInterviewStatusData();
      this.getDurationData();
      this.getTaxTermsData();
      this.getClientData();
      this.getRequirementData();
      this.getSubmittedToData();
      this.getSubmitReasonData();
      if (this.availability === 1) {
        this.candidateAvailability['dropdownSelected'] = [{ 'id': 1, 'name': 'Yes' }];
      } else if (this.availability === 0) {
        this.candidateAvailability['dropdownSelected'] = [{ 'id': 0, 'name': 'No' }];
      }
    }
  }

  @Input() set editableFlag(data) {
    this.editFlag = data;
  }

  @Input() set newCandidateFlag(data) {
    this.createCandidateFlag = data;
  }

  @Output() saveEmitter = new EventEmitter();
  @Output() cancelEmitter = new EventEmitter();
  @Output() proceedEmitter = new EventEmitter();

  // dropdown configuration data
  candidateAvailability: object = {
    'title': 'Candidate Availability',
    'dropdownData': [{ 'id': 0, 'name': 'No' }, { 'id': 1, 'name': 'Yes' }],
    'dropdownSelected': [],
    'key': 'candidateAvailability',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Availability (Required)",
      enableSearchFilter: false,
      classes: "myclass custom-class"
    }
  };

  candidateAvailabilityStatus: object = {
    'title': 'Candidate Availability Status',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'candidateAvailabilityStatus',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Available Period (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  candidateAvailabilityStatusConfig: object = {};

  candidateSubmittedStatus: object = {
    'title': 'Submitted To',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'submittedTo',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Submitted To (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  candidateSubmittedStatusConfig: object = {};

  interviewRoundConfig: object = {
    'title': 'Interview Round',
    'dropdownData': [],
    'dropdownSelected': [{ 'id': 0, 'name': 'First Round' }],
    'key': 'interviewRound',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Interview Round (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  submitReasonObject: object = {
    'title': 'Submittion Reason',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'submitReason',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Submittion Reason (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  submitReasonConfig: object = {};

  candidateInterviewStatus: object = {
    'title': 'Candidate Interview Status',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'interviewStatus',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Status (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  candidateInterviewStatusConfig: object = {};

  // availability related fields
  taxTermsObject: object = {
    'title': 'Tax Terms',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'taxTerms',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Tax Terms (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  durationObject: object = {
    'title': 'Duration',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'duration',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Duration (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  submissionClientObject: object = {
    'title': 'Client',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'clients',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: false,
      text: "Select Client (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  submissionRequirementObject: object = {
    'title': 'Requirement',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'requirements',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: false,
      text: "Select Requirement (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  interviewClientObject: object = {
    'title': 'Client',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'clients',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: false,
      text: "Select Client (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  interviewRequirementObject: object = {
    'title': 'Requirement',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'requirements',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: false,
      text: "Select Requirement (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  taxTermsConfig: object = {};
  durationConfig: object = {};
  submissionClientConfig: object = {};
  submissionRequirementConfig: object = {};
  interviewClientConfig: object = {};
  interviewRequirementConfig: object = {};

  editFlag: boolean;
  modalDisplayFlag: boolean;
  createCandidateFlag: boolean;

  countryName: string = '';
  candidateName: string;
  requirementId: number;
  requirementName: string;
  clientName: string;

  // availability related fields
  isCommunication: any;
  availability: any;
  availabilityComments: string = '';
  expectedSalary: any;
  currentCTC: any;
  expectedCTC: any;
  noticePeriod: any;
  availableMonth: any;
  hourlyRate: any;
  taxTerms: Array<object>;
  duration: Array<object>;
  candidateAvailabilityPeriod: Array<object>;

  // interview submission related fields
  isInterviewSubmitted: any;
  submittedTo: any;
  submitReason: any;
  interviewSubmissionRequirement: Array<object>;
  interviewSubmissionClient: Array<object>;
  interviewSubmissionComments: string = ''

  // interview related fields
  isInterview: any;
  interviewRound: Array<object>;
  interviewStatus: Array<object>;
  interviewRequirement: Array<object>;
  interviewClient: Array<object>;
  interviewComments: string = ''

  // option flag
  submittalFlag: boolean = false;
  interviewFlag: boolean = false;

  // error flags
  communicationErrorFlag: boolean = false;
  availabilityErrorFlag: boolean = false;
  interviewStatusErrorFlag: boolean = false;
  hourlyRateErrorFlag: boolean = false;
  taxTermsErrorFlag: boolean = false;
  durationErrorFlag: boolean = false;
  candidateAvailabilityPeriodErrorFlag: boolean = false;
  currentCTCErrorFlag: boolean = false;
  expectedCTCErrorFlag: boolean = false;
  noticePeriodErrorFlag: boolean = false;
  availableMonthErrorFlag: boolean = false;
  interviewSubmissionRequirementErrorFlag: boolean = false;
  interviewSubmissionClientErrorFlag: boolean = false;
  interviewRoundErrorFlag: boolean = false;
  interviewRequirementErrorFlag: boolean = false;
  interviewClientErrorFlag: boolean = false;
  submittedToErrorFlag: boolean = false;
  interviewSubmissionErrorFlag: boolean = false;
  interviewErrorFlag: boolean = false;
  submitReasonErrorFlag: boolean = false;

  loggedUserDetails: object;
  isFinalRound: boolean = false;

  isViewOnly: boolean;

  constructor(private toastrService: ToastrService,
    private apiService: ApiService) { }

  ngOnInit() {
    this.loggedUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  }

  // set data from input
  setData(data: object): void {
    if (data) {
      const keyList = Object.keys(data);
      for (let i = 0; i < keyList.length; i++) {
        this[keyList[i]] = data[keyList[i]];
      }
      // country upd
      if (this['countries'] && this['countries'].length != 0) {
        this.countryName = this['countries'][0]['name'];
      }
      if (data['countryName']) {
        this.countryName = data['countryName'];
      }
      // update availability Data
      // if (this.availability === 1) {
      //   this.candidateAvailability['dropdownSelected'] = [{'id': 1, 'name': 'Yes'}];
      // } else if (this.availability === 0) {
      //   this.candidateAvailability['dropdownSelected'] = [{'id': 0, 'name': 'No'}];
      // } else if (this.availability && this.availability.length > 0) {
      //   this.candidateAvailability['dropdownSelected'] = this.availability;
      // } else {
      //   this.candidateAvailability['dropdownSelected'] = [];
      // }
    }
  }

  // update candidate availability status value
  updateCandidateAvailability(event: any, flag: boolean): void {
    this.isCommunication = flag;
  }

  // update candidate interview submit value
  updateCandidateInterviewSubmitted(flag: boolean): void {
    this.isInterviewSubmitted = flag;
  }

  // updte the interview flg
  updateCandidateInterview(flag: boolean): void {
    this.isInterview = flag;
  }

  // handle dropdown selected value
  handleSelectedData(value: any, key: string): void {
    if (key === 'interviewSubmissionRequirement') {
      this.interviewSubmissionRequirement = value;
      this.interviewSubmissionClient = [];
      for (let i = 0; i < value.length; i++) {
        this.interviewSubmissionClient.push({ 'id': value[i]['clientId'], 'name': value[i]['clientName'] });
      }
      this.submissionClientConfig['dropdownSelected'] = this.interviewSubmissionClient;
    } else if (key === 'interviewRequirement') {
      this.interviewRequirement = value;
      this.interviewClient = [];
      for (let i = 0; i < value.length; i++) {
        this.interviewClient.push({ 'id': value[i]['clientId'], 'name': value[i]['clientName'] });
      }
      this.interviewClientConfig['dropdownSelected'] = this.interviewClient;
    } else if (key === 'interviewClient') {
      this.interviewClient = value;
      const interviewClientIdList = [];
      for (let i = 0; i < this.interviewClient.length; i++) {
        interviewClientIdList.push(this.interviewClient[i]['id']);
      }
      const updatedRequirement = [];
      for (let i = 0; i < this.interviewRequirement.length; i++) {
        if (interviewClientIdList.includes(this.interviewRequirement[i]['clientId'])) {
          updatedRequirement.push(this.interviewRequirement[i]);
        }
      }
      this.interviewRequirement = updatedRequirement;
      this.interviewRequirementConfig['dropdownSelected'] = updatedRequirement;
    } else if (key === 'interviewSubmissionClient') {
      this.interviewSubmissionClient = value;
      const interviewClientIdList = [];
      for (let i = 0; i < this.interviewSubmissionClient.length; i++) {
        interviewClientIdList.push(this.interviewSubmissionClient[i]['id']);
      }
      const updatedRequirement = [];
      for (let i = 0; i < this.interviewSubmissionRequirement.length; i++) {
        if (interviewClientIdList.includes(this.interviewSubmissionRequirement[i]['clientId'])) {
          updatedRequirement.push(this.interviewSubmissionRequirement[i]);
        }
      }
      this.interviewSubmissionRequirement = updatedRequirement;
      this.submissionRequirementConfig['dropdownSelected'] = updatedRequirement;
    } else {
      this[key] = value;
      if (key === 'availability' && this.availability && this.availability.length > 0 && this.availability[0] && this.availability[0] !== 0 && this.availability[0] !== 1) {
        this.candidateAvailability['dropdownSelected'] = this.availability;
      }
    }
  }

  // validate the required fields
  validateRequiredFields(): boolean {
    let failFlag = false;
    // nullify flags -- availability related
    this.communicationErrorFlag = false;
    this.availabilityErrorFlag = false;
    this.hourlyRateErrorFlag = false;
    this.taxTermsErrorFlag = false;
    this.durationErrorFlag = false;
    this.candidateAvailabilityPeriodErrorFlag = false;
    this.currentCTCErrorFlag = false;
    this.expectedCTCErrorFlag = false;
    this.noticePeriodErrorFlag = false;
    this.availableMonthErrorFlag = false;
    // submission related
    this.interviewSubmissionErrorFlag = false;
    this.submittedToErrorFlag = false;
    this.interviewSubmissionRequirementErrorFlag = false;
    this.interviewSubmissionClientErrorFlag = false;
    // interview related
    this.interviewErrorFlag = false;
    this.interviewStatusErrorFlag = false;
    this.interviewRoundErrorFlag = false;
    this.interviewRequirementErrorFlag = false;
    this.interviewClientErrorFlag = false;

    // validate communication
    if ((this.isCommunication === true || this.isCommunication === false || this.isCommunication === 1 || this.isCommunication === 0) && !this.interviewFlag) {
      // validate communication is true
      if (this.isCommunication || this.isCommunication === 1) {
        if (this.availability == 0 || !this.availability || (this.availability && this.availability.length > 0 && this.availability[0]['name'] === 'No')) {
          if (!this.availableMonth) {
            this.availableMonthErrorFlag = true;
            failFlag = true;
          }
        } else if (this.availability === 1 || this.availability || (this.availability && this.availability.length > 0 && this.availability[0]['name'] === 'Yes')) {
          if ((this.countryName).toLowerCase() === 'india') {
            if (!this.currentCTC) {
              this.currentCTCErrorFlag = true;
              failFlag = true;
            }
            if (!this.expectedCTC) {
              this.expectedCTCErrorFlag = true;
              failFlag = true;
            }
            if (!this.noticePeriod) {
              this.noticePeriodErrorFlag = true;
              failFlag = true;
            }
          } else {
            if (!this.hourlyRate && !this.expectedSalary) {
              this.hourlyRateErrorFlag = true;
              failFlag = true;
            }
            if (!this.taxTerms || (this.taxTerms && this.taxTerms.length === 0)) {
              this.taxTermsErrorFlag = true;
              failFlag = true;
            }
            if (!this.duration || (this.duration && this.duration.length === 0)) {
              this.durationErrorFlag = true;
              failFlag = true;
            }
            if (!this.candidateAvailabilityPeriod || (this.candidateAvailabilityPeriod && this.candidateAvailabilityPeriod.length === 0)) {
              this.candidateAvailabilityPeriodErrorFlag = true;
              failFlag = true;
            }
          }
        } else {
          this.availabilityErrorFlag = true;
          failFlag = true;
        }
      }
    } else {
      if (!this.interviewFlag) {
        this.communicationErrorFlag = true;
        failFlag = true;
      }
    }
    // submission related
    if ((this.isInterviewSubmitted === true || this.isInterviewSubmitted === false || this.isInterviewSubmitted === 1 || this.isInterviewSubmitted === 0)) {
      // validation interview is true
      if (this.isInterviewSubmitted || this.isInterviewSubmitted === 1) {
        if (!this.submittedTo || (this.submittedTo && this.submittedTo.length === 0)) {
          this.submittedToErrorFlag = true;
          failFlag = true;
        }
        // submittal requirement
        if (!this.interviewSubmissionRequirement || (this.interviewSubmissionRequirement && this.interviewSubmissionRequirement.length === 0)) {
          this.interviewSubmissionRequirementErrorFlag = true;
          failFlag = true;
        }
        // interview client
        if (!this.interviewSubmissionClient || (this.interviewSubmissionClient && this.interviewSubmissionClient.length === 0)) {
          this.interviewSubmissionClientErrorFlag = true;
          failFlag = true;
        }
      } else if (this.isInterviewSubmitted === false || this.isInterviewSubmitted === 0) {
        if (!this.submitReason || (this.submitReason && this.submitReason.length === 0)) {
          this.submitReasonErrorFlag = true;
          failFlag = true;
        }
        // submittal requirement
        if ((!this.submitReason || (this.submitReason && this.submitReason.length > 0 && this.submitReason[0]['id'] !== 6)) && !this.interviewSubmissionRequirement || (this.interviewSubmissionRequirement && this.interviewSubmissionRequirement.length === 0)) {
          this.interviewSubmissionRequirementErrorFlag = true;
          failFlag = true;
        }
        // interview client
        if ((!this.submitReason || (this.submitReason && this.submitReason.length > 0 && this.submitReason[0]['id'] !== 6)) && !this.interviewSubmissionClient || (this.interviewSubmissionClient && this.interviewSubmissionClient.length === 0)) {
          this.interviewSubmissionClientErrorFlag = true;
          failFlag = true;
        }
      }
    } else {
      if (!this.interviewFlag) {
        this.interviewSubmissionErrorFlag = true;
        failFlag = true;
      }
    }
    // interview related
    if ((this.isInterviewSubmitted === true || this.isInterviewSubmitted === 1) || this.interviewFlag) {
      if ((this.isInterview === true || this.isInterview === false || this.isInterview === 1 || this.isInterview === 0)) {
        if (this.isInterview || this.isInterview === 1) {
          // interview round
          // if (!this.interviewRound || (this.interviewRound && this.interviewRound.length === 0)) {
          //   this.interviewRoundErrorFlag = true;
          //   failFlag = true;
          // }
          // interview status
          if (!this.interviewStatus || (this.interviewStatus && this.interviewStatus.length === 0)) {
            this.interviewStatusErrorFlag = true;
            failFlag = true;
          }
          if (!this.interviewFlag) {
            // interview requirement
            if ((!this.interviewRequirement || (this.interviewRequirement && this.interviewRequirement.length === 0))) {
              this.interviewRequirementErrorFlag = true;
              failFlag = true;
            }
            // interview client
            if (!this.interviewClient || (this.interviewClient && this.interviewClient.length === 0)) {
              this.interviewClientErrorFlag = true;
              failFlag = true;
            }
          }
        }
      } else {
        this.interviewErrorFlag = true;
        failFlag = true;
      }
    }

    if (failFlag) {
      return false;
    } else {
      return true;
    }
  }

  // compose objects
  composeResultObject(): object {
    const resultObj = {};
    if (!this.interviewFlag) {
      resultObj['communicationDetails'] = {};

      resultObj['communicationDetails']['isCommunication'] = this.isCommunication;
      if (this.isCommunication || this.isCommunication === 1) {
        if (this.availability && this.availability.length > 0 && this.availability[0] && this.availability[0] !== 1 && this.availability[0] !== 0) {
          this.availability = this.availability[0]['id'];
        }
        resultObj['communicationDetails']['availability'] = this.availability;
        if (this.availability || this.availability === 1) {
          if ((this.countryName).toLowerCase() !== 'india') {
            resultObj['communicationDetails']['hourlyRate'] = this.hourlyRate;
            resultObj['communicationDetails']['salary'] = this.expectedSalary;
            resultObj['communicationDetails']['taxTerms'] = this.taxTerms[0]['id'];
          } else {
            resultObj['communicationDetails']['currentCTC'] = this.currentCTC;
            resultObj['communicationDetails']['expectedCTC'] = this.expectedCTC;
            resultObj['communicationDetails']['noticePeriod'] = this.noticePeriod;
          }
          resultObj['communicationDetails']['candidateAvailabilityPeriod'] = this.candidateAvailabilityPeriod[0]['id'];
          resultObj['communicationDetails']['duration'] = this.duration[0]['id'];
        } else {
          resultObj['communicationDetails']['availableMonth'] = this.availableMonth;
        }
        resultObj['communicationDetails']['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['communicationDetails']['createdBy'] = this.loggedUserDetails['id'];
        resultObj['communicationDetails']['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['communicationDetails']['updatedBy'] = this.loggedUserDetails['id'];
        resultObj['communicationDetails']['availabilityComments'] = this.availabilityComments;
      } else {
        resultObj['communicationDetails']['availability'] = null;
        resultObj['communicationDetails']['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['communicationDetails']['createdBy'] = this.loggedUserDetails['id'];
        resultObj['communicationDetails']['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['communicationDetails']['updatedBy'] = this.loggedUserDetails['id'];
      }

      resultObj['submissionDetails'] = {};

      resultObj['submissionDetails']['isInterviewSubmitted'] = this.isInterviewSubmitted;
      if (this.isInterviewSubmitted || this.isInterviewSubmitted === 1) {
        resultObj['submissionDetails']['submittedTo'] = this.submittedTo[0]['id'];
        if ((this.interviewSubmissionRequirement && this.interviewSubmissionRequirement.length > 0)) {
          for (let i = 0; i < this.interviewSubmissionRequirement.length; i++) {
            if ((this.interviewRequirement && this.interviewRequirement.length > 0)) {
              for (let j = 0; j < this.interviewRequirement.length; j++) {
                if (this.interviewSubmissionRequirement[i]['id'] === this.interviewRequirement[j]['id']) {
                  this.interviewSubmissionRequirement[i]['isInterviewScheduled'] = true;
                }
              }
              if (!this.interviewSubmissionRequirement[i]['isInterviewScheduled']) {
                this.interviewSubmissionRequirement[i]['isInterviewScheduled'] = false;
              }
            } else {
              this.interviewSubmissionRequirement[i]['isInterviewScheduled'] = false;
            }
          }
        }
        resultObj['submissionDetails']['requirementList'] = this.interviewSubmissionRequirement;
        resultObj['submissionDetails']['interviewComments'] = this.interviewSubmissionComments;
        resultObj['submissionDetails']['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['submissionDetails']['createdBy'] = this.loggedUserDetails['id'];
        resultObj['submissionDetails']['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['submissionDetails']['updatedBy'] = this.loggedUserDetails['id'];
        resultObj['submissionDetails']['isCancelled'] = false;
      } else if (this.isInterviewSubmitted === false || this.isInterviewSubmitted === 0) {
        resultObj['submissionDetails']['cancelReason'] = this.submitReason[0]['id'];
        if (this.submitReason[0]['id'] !== 6)
          resultObj['submissionDetails']['requirementList'] = this.interviewSubmissionRequirement;
        resultObj['submissionDetails']['interviewComments'] = this.interviewSubmissionComments;
        resultObj['submissionDetails']['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['submissionDetails']['createdBy'] = this.loggedUserDetails['id'];
        resultObj['submissionDetails']['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['submissionDetails']['updatedBy'] = this.loggedUserDetails['id'];
        resultObj['submissionDetails']['isCancelled'] = true;
      }


      resultObj['interviewDetails'] = {};
      resultObj['interviewDetails']['isInterview'] = this.isInterview;
      if (this.isInterview || this.isInterview === 1) {
        if (this.isFinalRound)
          resultObj['interviewDetails']['interviewRound'] = 6;
        else
          resultObj['interviewDetails']['interviewRound'] = 1;
        resultObj['interviewDetails']['interviewStatus'] = this.interviewStatus[0]['id'];
        resultObj['interviewDetails']['requirementList'] = this.interviewRequirement;
        // resultObj['interviewDetails']['requirementId'] = this.interviewRequirement[0]['id'];
        // resultObj['interviewDetails']['clientId'] = this.interviewClient[0]['id'];
        resultObj['interviewDetails']['comments'] = this.interviewComments;
        resultObj['interviewDetails']['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['interviewDetails']['createdBy'] = this.loggedUserDetails['id'];
        resultObj['interviewDetails']['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
        resultObj['interviewDetails']['updatedBy'] = this.loggedUserDetails['id'];
      }
    } else {
      const requirementObj = {};
      requirementObj['id'] = this['requirementId'];
      requirementObj['clientId'] = this['clientId'];
      resultObj['id'] = this['id'];
      resultObj['interviewDetails'] = {};
      resultObj['interviewDetails']['isInterview'] = this.isInterview;
      if (this.isFinalRound)
        resultObj['interviewDetails']['interviewRound'] = 6;
      else
        resultObj['interviewDetails']['interviewRound'] = 1;
      resultObj['interviewDetails']['interviewStatus'] = this.interviewStatus[0]['id'];
      resultObj['interviewDetails']['requirementList'] = [requirementObj];
      resultObj['interviewDetails']['candidateId'] = this['candidateId'];
      // resultObj['interviewDetails']['requirementId'] = this.interviewRequirement[0]['id'];
      // resultObj['interviewDetails']['clientId'] = this.interviewClient[0]['id'];
      resultObj['interviewDetails']['comments'] = this.interviewComments;
      resultObj['interviewDetails']['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      resultObj['interviewDetails']['createdBy'] = this.loggedUserDetails['id'];
      resultObj['interviewDetails']['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      resultObj['interviewDetails']['updatedBy'] = this.loggedUserDetails['id'];
    }

    return resultObj;
  }

  // save the values
  saveData(): void {
    // view only option
    if (this.isViewOnly) {
      this.proceedEmitter.emit();
    } else {
      const validateFlag = this.validateRequiredFields();
      if (validateFlag) {
        const obj = this.composeResultObject();
        this.saveEmitter.emit(obj);
      } else {
        this.toastrService.error('Please fill the mandatory fields...', 'Error', { timeOut: 2500 });
      }
    }
  }

  // proceed the modal
  proceedModal(): void {
    this.proceedEmitter.emit();
  }

  // cancel the modal
  cancelModal(): void {
    this.cancelEmitter.emit();
  }

  // get available status data
  getAvailableStatusData(): void {
    const url = '/api/availabilityStatus/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.candidateAvailabilityStatus['dropdownData'] = data['availabilityStatus'];
        if (this.candidateAvailabilityPeriod) {
          for (let i = 0; i < data['availabilityStatus'].length; i++) {
            if (this.candidateAvailabilityPeriod === data['availabilityStatus'][i]['id']) {
              this.candidateAvailabilityStatus['dropdownSelected'] = [data['availabilityStatus'][i]];
              this.candidateAvailabilityPeriod = [data['availabilityStatus'][i]];
              break;
            }
            if ((this.candidateAvailabilityPeriod && this.candidateAvailabilityPeriod.length > 0)) {
              for (let j = 0; j < this.candidateAvailabilityPeriod.length; j++) {
                if (this.candidateAvailabilityPeriod[j]['id'] === data['availabilityStatus'][i]['id']) {
                  this.candidateAvailabilityStatus['dropdownSelected'] = this.candidateAvailabilityPeriod;
                  break;
                }
              }
            }
          }
        }
        this.candidateAvailabilityStatusConfig = this.candidateAvailabilityStatus;
      }
    });
  }

  // get submittedTo data
  getSubmittedToData(): void {
    const url = '/api/submittalStatus/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        const dataArray = [];
        for (let i = 0; i < data['submittalStatus'].length; i++) {
          if (data['submittalStatus'][i]['name'] !== 'Not Submitted to Any') {
            dataArray.push(data['submittalStatus'][i]);
          }
        }
        this.candidateSubmittedStatus['dropdownData'] = dataArray;
        if (this.submittedTo && this.submittalFlag) {
          for (let i = 0; i < data['submittalStatus'].length; i++) {
            if (this.submittedTo === data['submittalStatus'][i]['id']) {
              this.candidateSubmittedStatus['dropdownSelected'] = [data['submittalStatus'][i]];
              this.submittedTo = [data['submittalStatus'][i]];
              break;
            }
          }
        }
        this.candidateSubmittedStatusConfig = this.candidateSubmittedStatus;
      }
    });
  }

  // get interview status data
  getInterviewStatusData(): void {
    const url = '/api/interviewStatus/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.candidateInterviewStatus['dropdownData'] = data['interviewStatus'];
        if (this.interviewStatus && this.interviewFlag) {
          for (let i = 0; i < data['interviewStatus'].length; i++) {
            if (this.interviewStatus === data['interviewStatus'][i]['id']) {
              this.candidateInterviewStatus['dropdownSelected'] = [data['interviewStatus'][i]];
              this.interviewStatus = [data['interviewStatus'][i]];
              break;
            }
          }
        }
        this.candidateInterviewStatusConfig = this.candidateInterviewStatus;
      }
    });
  }

  // fetch duration data
  getDurationData(): void {
    const url = '/api/durations/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.durationObject['dropdownData'] = data['duration'];
        if (this.duration) {
          for (let i = 0; i < data['duration'].length; i++) {
            if (this.duration === data['duration'][i]['id']) {
              this.durationObject['dropdownSelected'] = [data['duration'][i]];
              this.duration = [data['duration'][i]];
              break;
            }
            if ((this.duration && this.duration.length > 0)) {
              for (let j = 0; j < this.duration.length; j++) {
                if (this.duration[j]['id'] === data['duration'][i]['id']) {
                  this.durationObject['dropdownSelected'] = this.duration;
                  break;
                }
              }
            }
          }
        }
        this.durationConfig = this.durationObject;
      }
    });
  }

  // fetch taxTerms data
  getTaxTermsData(): void {
    const url = '/api/taxTerms/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.taxTermsObject['dropdownData'] = data['taxTerms'];
        if (this.taxTerms) {
          for (let i = 0; i < data['taxTerms'].length; i++) {
            if (this.taxTerms === data['taxTerms'][i]['id']) {
              this.taxTermsObject['dropdownSelected'] = [data['taxTerms'][i]];
              this.taxTerms = [data['taxTerms'][i]];
              break;
            }
            if ((this.taxTerms && this.taxTerms.length > 0)) {
              for (let j = 0; j < this.taxTerms.length; j++) {
                if (this.taxTerms[j]['id'] === data['taxTerms'][i]['id']) {
                  this.taxTermsObject['dropdownSelected'] = this.taxTerms;
                  break;
                }
              }
            }
          }
        }
        this.taxTermsConfig = this.taxTermsObject;
      }
    });
  }

  // get client data
  getClientData(): void {
    const url = '/api/clients/fetchClientRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.submissionClientObject['dropdownData'] = data['clients'];
        this.interviewClientObject['dropdownData'] = data['clients'];
        if (this.interviewSubmissionClient && this.submittalFlag) {
          for (let i = 0; i < data['clients'].length; i++) {
            if (this.interviewSubmissionClient === data['clients'][i]['id']) {
              this.submissionClientObject['dropdownSelected'] = [data['clients'][i]];
              this.interviewSubmissionClient = [data['clients'][i]];
              break;
            }
          }
        }
        if (this.interviewClient && this.interviewFlag) {
          for (let i = 0; i < data['clients'].length; i++) {
            if (this.interviewClient === data['clients'][i]['id']) {
              this.interviewClientObject['dropdownSelected'] = [data['clients'][i]];
              this.interviewClient = [data['clients'][i]];
              break;
            }
          }
        }
        this.submissionClientConfig = this.submissionClientObject;
        this.interviewClientConfig = this.interviewClientObject;
      }
    });
  }

  // get requirement data
  getRequirementData(): void {
    const url = '/api/requirements/fetchRequirementRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        const requirementListArray = [];
        for (let i = 0; i < data['requirements'].length; i++) {
          let obj = {};
          obj['id'] = data['requirements'][i]['id'];
          obj['name'] = data['requirements'][i]['id'] + '-' + data['requirements'][i]['name'] + ', ' + data['requirements'][i]['clientName'];
          obj['clientId'] = data['requirements'][i]['clientId'];
          obj['clientName'] = data['requirements'][i]['clientName'];
          requirementListArray.push(obj);
        }
        this.submissionRequirementObject['dropdownData'] = requirementListArray;
        this.interviewRequirementObject['dropdownData'] = requirementListArray;
        if (this.interviewSubmissionRequirement && this.submittalFlag) {
          for (let i = 0; i < requirementListArray.length; i++) {
            if (this.interviewSubmissionRequirement === requirementListArray[i]['id']) {
              this.submissionRequirementObject['dropdownSelected'] = [requirementListArray[i]];
              this.interviewSubmissionRequirement = [requirementListArray[i]];
              break;
            }
          }
        }
        if (this.interviewRequirement && this.interviewFlag) {
          for (let i = 0; i < requirementListArray.length; i++) {
            if (this.interviewRequirement === requirementListArray[i]['id']) {
              this.interviewRequirementObject['dropdownSelected'] = [requirementListArray[i]];
              this.interviewRequirement = [requirementListArray[i]];
              break;
            }
          }
        }
        this.submissionRequirementConfig = this.submissionRequirementObject;
        this.interviewRequirementConfig = this.interviewRequirementObject;
      }
    });
  }

  // fetch interview round data
  getSubmitReasonData(): void {
    const url = '/api/submitReason/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.submitReasonObject['dropdownData'] = data['submitReason'];
        if (this.submitReason && this.submitReason) {
          for (let i = 0; i < data['submitReason'].length; i++) {
            if (this.submitReason === data['submitReason'][i]['id']) {
              this.submitReasonObject['dropdownSelected'] = [data['submitReason'][i]];
              this.submitReason = [data['submitReason'][i]];
              break;
            }
          }
        }
        this.submitReasonConfig = this.submitReasonObject;
      }
    });
  }

  // handle typed event
  handleTypedEvent(event: any, key: string, data: string): void {
    // number field validation
    if (key === 'noticePeriod') {
      let flag = true;
      let updatedData = '';
      for (let i = 0; i < data.length; i++) {
        if ((parseInt(data[i]) >= 0 && parseInt(data[i]) <= 9) || data[i] == '.') {
          updatedData += data[i];
        } else {
          flag = false;
        }
      }
      if (!flag) {
        data = updatedData;
        event.target.value = data;
      }
    } else if (key === 'hourlyRate' || key === 'expectedSalary' || key === 'currentCTC' || key === 'expectedCTC') {
      // validate not number in mobile
      let flag = true;
      let updatedData = '';
      for (let i = 0; i < data.length; i++) {
        if ((parseInt(data[i]) >= 0 && parseInt(data[i]) <= 9) || data[i] == ',' || data[i] == '.' || data[i] == 'l' || data[i] == 'L' || data[i] == 'k' || data[i] == 'K') {
          updatedData += data[i];
        } else {
          flag = false;
        }
      }
      if (!flag) {
        data = updatedData;
        event.target.value = data;
      }
    }
  }

  // handle view only flag update
  updateViewOnly(flag: boolean): void {
    this.isViewOnly = flag;
  }

}
