import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter-section',
  templateUrl: './filter-section.component.html',
  styleUrls: ['./filter-section.component.scss']
})
export class FilterSectionComponent implements OnInit {

  @Input() set filterData(data) {
    if (data && data.length !== 0) {
      for ( let i = 0; i < data.length; i++) {
        this.filterFlag[i] = false;
        data[i]['openFlag'] = false;
      }
      this.filterDataSet = data;
    }
  }

  @Output() filterEmitter = new EventEmitter();

  filterFlag: Array < boolean > = [];
  filterDataSet: Array < object > = [];
  filterInputObject: object = {};
  filterGlobalOpenFlag: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  // toggle filter handler
  toggleFilter(offset: number): void {
    this.filterDataSet[offset]['openFlag'] = !this.filterDataSet[offset]['openFlag'];
    let count = 0;
    for (let i = 0; i < this.filterDataSet.length; i++) {
      if (this.filterDataSet[i]['openFlag']) {
        count++
      }
    }
    if (count === this.filterDataSet.length) {
      this.filterGlobalOpenFlag = true;
    } else {
      this.filterGlobalOpenFlag = false;
    }
  }

  // handle data selected from filter events
  handleSelectedData(selectedData, key): void {
    if (selectedData && selectedData.length !== 0) {
      this.filterInputObject[key] = selectedData;
    } else {
      delete this.filterInputObject[key];
    }
    this.filterEmitter.emit(this.filterInputObject);
  }

  // enable the filter
  enableFilter(offset: number): void {
    let dataArray = [];
    for (let i = 0; i < this.filterFlag.length; i++) {
      if (i === offset) {
        dataArray.push(true);
      } else {
        dataArray.push(false);
      }
    }
    this.filterFlag = dataArray;
  }

  disableFilter(offset: number): void {
    let dataArray = [];
    for (let i = 0; i < this.filterFlag.length; i++) {
      if (i === offset) {
        dataArray.push(false);
      } else {
        dataArray.push(false);
      }
    }
    this.filterFlag = dataArray;
  }

  // claer all filter
  clearAllFilter(): void {
    for (let i = 0; i < this.filterDataSet.length; i++) {
      if (this.filterDataSet[i]['andOrFlag']) {
        for (let j = 0; j < this.filterDataSet[i]['dataList'].length; j++) {
          this.filterDataSet[i]['dataList'][j]['dropdownSelected'] = [];
        }
      } else {
        this.filterDataSet[i]['dropdownSelected'] = [];
      }
    }
    this.filterInputObject = {};
    this.filterEmitter.emit(this.filterInputObject);
  }

  // collapse all
  collapseAll(): void {
    this.filterGlobalOpenFlag = !this.filterGlobalOpenFlag;
    for (let i = 0; i < this.filterDataSet.length; i++) {
      this.filterDataSet[i]['openFlag'] = this.filterGlobalOpenFlag
    }
  }

}
