import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-submit-cancel-modal',
  templateUrl: './submit-cancel-modal.component.html',
  styleUrls: ['./submit-cancel-modal.component.scss']
})
export class SubmitCancelModalComponent implements OnInit {

  @Input() displayFlag: boolean;

  @Output() closeEmitter = new EventEmitter();

  submitReasonConfig: object = {
    'title': 'Submittion Reason',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'submitReason',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Submittion Reason (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  comments: string;

  //submitReasonConfig: object = {};

  constructor() { }

  ngOnInit() {
  }

  closeModal(): void {
    this.closeEmitter.emit();
  }

  cancelSubmission(): void {

  }

  handleSelectedData(e, key): void {

  }

}
