import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitCancelModalComponent } from './submit-cancel-modal.component';

describe('SubmitCancelModalComponent', () => {
  let component: SubmitCancelModalComponent;
  let fixture: ComponentFixture<SubmitCancelModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitCancelModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitCancelModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
