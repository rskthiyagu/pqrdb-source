import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';
import { DataService } from '../../../services/data.service';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() set displayModal(data) {
    this.displayModalFlag = data;
  }

  @Input() set modalConfigSetter(data) {
    this.modalConfig = data;
    if (this.modalConfig && this.modalConfig['tileFlag']) {
      this.tileConfig = this.modalConfig['fieldList'];
    } else if (this.modalConfig && this.modalConfig['fieldList'] && this.modalConfig['fieldList'].length !== 0) {
      for (let i = 0; i < this.modalConfig['fieldList'].length; i++) {
        if (this.modalConfig['fieldList'][i]['selectFlag']) {
          this.selectedItems[this.modalConfig['fieldList'][i]['selectConfig']['key']] = this.modalConfig['fieldList'][i]['selectConfig']['dropdownSelected'];
        }
      }
    }
  }

  @Output() hideModalEmitter = new EventEmitter();
  @Output() proceedModalEmitter = new EventEmitter();
  @Output() skipEmitter = new EventEmitter();
  @Output() saveEmitter = new EventEmitter();
  @Output() noModalEmitter = new EventEmitter();
  @Output() yesModalEmitter = new EventEmitter();

  displayModalFlag: boolean = false;
  modalConfig: object;
  selectedItems: object = {};
  tileConfig: Array<object>;
  loggedUserDetails: object = {};
  selectedData: object = {};

  constructor(private apiService: ApiService,
    private utilityService: UtilityService,
    private dataService: DataService,
    private toastrService: ToastrService) { }

  ngOnInit() {
    this.loggedUserDetails = this.dataService.loggedUser;
  }

  // close modal handler
  closeModal(): void {
    this.displayModalFlag = false;
    this.hideModalEmitter.emit(false);
  }

  // get the filled data
  getData(): object {
    const obj = {
      'communication': null,
      'availability': null,
      'requirement': null,
      'client': null,
      'comment': null
    };
    for (let i = 0; i < this.modalConfig['fieldList'].length; i++) {
      if (this.modalConfig['fieldList'][i]['selectFlag']) {
        obj[this.modalConfig['fieldList'][i]['selectConfig']['key']] = this.selectedItems[this.modalConfig['fieldList'][i]['selectConfig']['key']];
      } else if (this.modalConfig['fieldList'][i]['inputConfig']) {
        obj[this.modalConfig['fieldList'][i]['inputConfig']['key']] = this.modalConfig['fieldList'][i]['inputConfig']['data'];
      } else {
        obj[this.modalConfig['fieldList'][i]['textareaConfig']['key']] = this.modalConfig['fieldList'][i]['textareaConfig']['data'];
      }
    }
    return obj;
  }

  // handle tile action
  toggleAction(key): void {
    for (let i = 0; i < this.tileConfig.length; i++) {
      if (key === this.tileConfig[i]['key']) {
        this.tileConfig[i]['isActive'] = !(this.tileConfig[i]['isActive']);
        // update db through api call
        const url = '/api/users/updateUserAccess';
        const requestData = {};
        requestData['id'] = this.loggedUserDetails['id'];
        requestData['key'] = this.tileConfig[i]['dbKey'];
        requestData['value'] = this.tileConfig[i]['isActive'];
        this.apiService.postData(url, requestData).subscribe(data => {

        });
      }
    }
    const config = this.modalConfig;
    config['fieldList'] = this.tileConfig;
    this.modalConfig = config
  }

  // toggle user access
  toggleUserAccess(id: number): void {
    const modalUserData = this.modalConfig['fieldList'];
    for (let i = 0; i < modalUserData.length; i++) {
      if (modalUserData[i]['id'] === id) {
        modalUserData[i][this.modalConfig['accessKey']] = !modalUserData[i][this.modalConfig['accessKey']];
        this.updateUserAccess(modalUserData[i], this.modalConfig['accessKey']);
      }
    }
    this.modalConfig['fieldList'] = modalUserData;
  }

  // update user access
  updateUserAccess(userData: object, key: string): void {
    const url = '/api/users/updateUserAccess';
    const param = {
      'id': userData['id'],
      'key': key,
      'value': userData[key]
    };
    this.apiService.postData(url, param).subscribe(data => {
      if (data) {

      }
    })
  }

  // handle select event on select component
  handleSelectedData(event: any, key: string, offset: number): void {
    this.selectedItems[key] = event;
    this.modalConfig['fieldList'][offset]['selectConfig']['dropdownSelected'] = event;
  }

  // yes modal handler
  yesModal(): void {
    const fieldList = this.modalConfig['fieldList'];
      this.selectedData = {};
      const flag = this.validateRequiredFields(fieldList);
      if (flag) {
        this.yesModalEmitter.emit(this.selectedData);
      } else {
        this.toastrService.error('Please select the field to proceed', 'Error', { timeOut: 2000 });
      }
  }

  // save modal handler
  saveModal(): void {
    if (this.modalConfig['tileFlag']) {
      this.displayModalFlag = false;
      this.saveEmitter.emit(this.tileConfig);
    } else if (this.modalConfig['userFlag']) {
      this.displayModalFlag = false;
      this.saveEmitter.emit(this.modalConfig['fieldList']);
    } else if (this.modalConfig['dataDeleteFlag']) {
      
    } else if (this.modalConfig['userDeleteFlag'] || this.modalConfig['dataUpdateFlag']) {
      const fieldList = this.modalConfig['fieldList'];
      this.selectedData = {};
      const flag = this.validateRequiredFields(fieldList);
      if (flag) {
        this.saveEmitter.emit(this.selectedData);
      } else {
        this.toastrService.error('Please select the field to proceed', 'Error', { timeOut: 2000 });
      }
    } else {
      const obj = this.getData();
      if (this.modalConfig['requiredFlag']) {
        if (obj['communication'] && obj['communication'].length !== 0) {
          this.displayModalFlag = false;
          this.saveEmitter.emit(obj);
        } else {
          this.toastrService.error('Please select the field to proceed', 'Error', { timeOut: 2000 });
        }
      } else {
        this.displayModalFlag = false;
        this.saveEmitter.emit(obj);
      }
    }
  }

  // validate the required field set
  validateRequiredFields(fieldList:Array < object >): boolean {
    let dataOffset = 0;
    for (let i = 0; i < fieldList.length; i++) {
      if (fieldList[i]['selectFlag']) {
        if (fieldList[i]['selectConfig']['dropdownSelected'] && fieldList[i]['selectConfig']['dropdownSelected'].length !== 0) {
          dataOffset++;
          this.selectedData[fieldList[i]['selectConfig']['key']] = fieldList[i]['selectConfig']['dropdownSelected']
        }
      } else if (fieldList[i]['inputFlag']) {
        if (fieldList[i]['inputConfig']['data']) {
          this.selectedData[fieldList[i]['inputConfig']['key']] = fieldList[i]['inputConfig']['data'];
          dataOffset++;
        }
      }
    }
    if (dataOffset === fieldList.length) {
      return true;
    } else {
      return false;
    }
  }

  // proceed modal handler
  proceedModal(): void {
    this.displayModalFlag = false;
    this.proceedModalEmitter.emit();
  }

  // no modal handler
  noActionModal(): void {
    this.displayModalFlag = false;
    this.noModalEmitter.emit();
  }

  // skip modal
  skipModal(): void {
    this.displayModalFlag = false;
    this.skipEmitter.emit();
  }

}
