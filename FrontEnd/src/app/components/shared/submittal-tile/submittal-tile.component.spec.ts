import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmittalTileComponent } from './submittal-tile.component';

describe('SubmittalTileComponent', () => {
  let component: SubmittalTileComponent;
  let fixture: ComponentFixture<SubmittalTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmittalTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittalTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
