import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-submittal-tile',
  templateUrl: './submittal-tile.component.html',
  styleUrls: ['./submittal-tile.component.scss']
})
export class SubmittalTileComponent implements OnInit {

  @Input() set submittalList(data) {
    if (data && data.length !== 0) {
      for (let i = 0; i < data.length; i++) {
        data[i]['heightValue'] = 185;
      }
      this.requirementDataList = data;
    } else {
      this.requirementDataList = [];
    }
  }

  @Output() interviewEmitter = new EventEmitter();
  @Output() cancelEmitter = new EventEmitter();
  @Output() viewDetailEmitter = new EventEmitter();

  requirementDataList: Array < object >;

  constructor() { }

  ngOnInit() {
  }

  handleInterviewSchedule(event: any, data: object): void {
    event.stopPropagation();
    this.interviewEmitter.emit(data);
  }

  handleCancelSubmition(event: any, data: object): void {
    event.stopPropagation();
    this.cancelEmitter.emit(data);
  }

  // navigate candidate view page
  navigateViewPage(data: object): void {
    this.viewDetailEmitter.emit(data);
  }


}
