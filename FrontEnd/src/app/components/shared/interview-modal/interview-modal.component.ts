import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { UtilityService } from '../../../services/utility.service';

import * as moment from 'moment';

@Component({
  selector: 'app-interview-modal',
  templateUrl: './interview-modal.component.html',
  styleUrls: ['./interview-modal.component.scss']
})
export class InterviewModalComponent implements OnInit {

  @Input() set displayFlag(flag) {
    this.modalDisplayFlag = flag;
  }

  @Input() set selectedObject(data) {
    if (data) {
      this.candidateId = data['candidateId'];
      this.requirementId = data['requirementId'];
      this.getInterviewData();
      // this.getInterviewStatusData();
    }
  }

  @Output() closeEmitter = new EventEmitter();

  addFlag: boolean = false;
  modalDisplayFlag: boolean = false;
  interviewRound: boolean = false;
  isFinalRound: boolean = false;
  candidateName: string;
  requirementName: string;
  clientName: string;
  interviewData: Array < object > = [];
  comments: string;
  interviewStatus: Array < object >;

  candidateInterviewStatus: object = {
    'title': 'Candidate Interview Status',
    'dropdownData': [],
    'dropdownSelected': [],
    'key': 'interviewStatus',
    'requiredFlag': true,
    'dropdownSetting': {
      singleSelection: true,
      text: "Select Status (Required)",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    }
  };

  candidateInterviewStatusConfig: object = {};
  candidateId: number;
  requirementId: number;
  loggedUserDetails: object;

  constructor(private apiService: ApiService,
              private utilityService: UtilityService,
              private toastrService: ToastrService) { }

  ngOnInit() {
    this.loggedUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  }

  // toggle header
  toggleHeader(flag: string): void {
    for (let i = 0; i < this.interviewData.length; i++) {
      const key = 'round' + i + 'OpenFlag';
      this[key] = false;
    }
    this[flag] = true;
  }

  // get interview data
  getInterviewData(): void {
    const url = '/api/interviews/fetchRecord';
    const obj = {};
    obj['candidateId'] = this.candidateId;
    obj['requirementId'] = this.requirementId;
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.postData(url, obj).subscribe( data => {
      if (data) {
        if (data['interviews'] && data['interviews'].length > 0) {
          this.candidateName = data['interviews'][0]['candidateName'];
          this.requirementName = data['interviews'][0]['requirementName'];
          this.clientName = data['interviews'][0]['clientName'];
          this.interviewData = data['interviews'];
          for (let i = 0; i < data['interviews'].length; i++) {
            const key = 'round' + i + 'OpenFlag';
            this[key] = false;
            if (i === data['interviews'].length - 1)
              this[key] = true;
          }
          this.getInterviewStatusData();
        }
      }
      this.utilityService.updateSpinnerFlag(false);
    })
  }

  // close emitter
  closeModal(): void {
    this.closeEmitter.emit();
  }

  // get interview status data
  getInterviewStatusData(): void {
    const url = '/api/interviewStatus/fetchRecord';
    this.apiService.getData(url).subscribe(data => {
      if (data) {
        this.candidateInterviewStatus['dropdownData'] = data['interviewStatus'];
        const interviewStatus = this.interviewData[this.interviewData.length - 1]['interviewStatus'];
        if (interviewStatus) {
          for (let i = 0; i < data['interviewStatus'].length; i++) {
            if (interviewStatus === data['interviewStatus'][i]['id']) {
              this.candidateInterviewStatus['dropdownSelected'] = [data['interviewStatus'][i]];
              this.interviewStatus = [data['interviewStatus'][i]];
              break;
            }
          }
        }
        this.candidateInterviewStatusConfig = this.candidateInterviewStatus;
      }
    });
  }

  // validate fields
  validateField(): boolean {
    if (this.interviewStatus && this.interviewStatus.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  // compose object
  composeObject(): object {
    const resultObj = {};
    const requirementObj = {};
      requirementObj['id'] = this.interviewData[0]['requirementId'];
      requirementObj['clientId'] = this.interviewData[0]['clientId'];
      resultObj['id'] = this['id'];
      resultObj['isInterview'] = true;
      if (this.isFinalRound)
        resultObj['interviewRound'] = 6;
      else
        resultObj['interviewRound'] = this.interviewData[this.interviewData.length - 1]['interviewRound'];
      resultObj['interviewStatus'] = this.interviewStatus[0]['id'];
      resultObj['requirementList'] = [requirementObj];
      resultObj['candidateId'] = this['candidateId'];
      // resultObj['interviewDetails']['requirementId'] = this.interviewRequirement[0]['id'];
      // resultObj['interviewDetails']['clientId'] = this.interviewClient[0]['id'];
      resultObj['comments'] = this.comments;
      resultObj['createdOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      resultObj['createdBy'] = this.loggedUserDetails['id'];
      resultObj['updatedOn'] = moment.utc().format('YYYY-MM-DD HH:mm');
      resultObj['updatedBy'] = this.loggedUserDetails['id'];

      return resultObj;
  }

  // handle select
  handleSelectedData(event, key): void {
    this[key] = event;
  }

  // typed event
  handleTypedEvent(event: any): void {
    this.comments = event.target.value;
  }

  // save interview
  saveInterview(): void {
    const flag = this.validateField();
    if (!flag) {
      this.toastrService.error('Please fill all required fields...', 'Error', { timeOut: 1500 });
      return;
    }
    const url = '/api/interviews/create';
    const obj = this.composeObject();
    this.utilityService.updateSpinnerFlag(true);
    this.apiService.postData(url, obj).subscribe( data => {
      this.closeEmitter.emit();
      this.utilityService.updateSpinnerFlag(false);
      this.toastrService.success('Interview Details Updated Successfully...', 'Success', { timeOut: 1500 });
    })
  }

  // add round
  addRound(): void {
    const roundId = this.interviewData.length + 1;
    let roundName;
    switch(roundId) {
      case 1: {
        roundName = 'First Round';
        break;
      }
      case 2: {
        roundName = 'Second Round';
        break;
      }
      case 3: {
        roundName = 'Third Round';
        break;
      }
      case 4: {
        roundName = 'Fourth Round';
        break;
      }
      case 5: {
        roundName = 'Fifth Round';
        break;
      }
      case 6: {
        roundName = 'Final Round';
        break;
      }
    }
    const list = this.interviewData;
    const obj = this.interviewData[0];
    obj['interviewRoundName'] = roundName;
    obj['interviewRound'] = roundId;
    obj['comments'] = '';
    obj['customCreated'] = true;
    obj['customId'] = 'Id' + this.interviewData.length + 1;
    list.push(obj);
    this.interviewData = list;
    for (let i = 0; i < this.interviewData.length; i++) {
      const key = 'round' + i + 'OpenFlag';
      this[key] = false;
      if (i === this.interviewData.length - 1)
        this[key] = true;
    }
    this.addFlag = true;
  }

  // remove round
  removeRound(id: string): void {
    const listArray = [];
    for (let i = 0; i < this.interviewData.length; i++) {
      if (id != this.interviewData[i]['customId']) {
        listArray.push(this.interviewData[i]);
      }
    }
    this.interviewData = listArray;
    this.addFlag = false;
  }

}
