import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-field-data-alert',
  templateUrl: './field-data-alert.component.html',
  styleUrls: ['./field-data-alert.component.scss']
})
export class FieldDataAlertComponent implements OnInit {

  @Input() set fieldData(data) {
    if (data && data.length !== 0) {
      this.fieldDataList = data;
      this.fieldObject = data[0];
      this.fieldDataOffset = 0;
      this.fieldOrderList.push(this.fieldObject);
    }
  }

  @Output() selectedDataEmitter = new EventEmitter();
  @Output() submitEmitter = new EventEmitter();
  @Output() addnewItemEmitter = new EventEmitter();

  fieldDataList: Array<object>;
  fieldObject: object;
  fieldDataOffset: number;
  fieldDataModel: Array<object>;
  multiFieldDataModel: object = {};
  dataObject: object = {};
  inputValue: string = null;
  fieldOrderList: Array < object > = [];

  constructor() { }

  ngOnInit() {
  }

  // handle filter select box events
  handleSelectedItems(selectedData: Array<object>, key?: string): void {
    if (key) {
      this.multiFieldDataModel[key] = selectedData;
    } else {
      this.fieldDataModel = selectedData;
    }
  }

  // handle add new item
  addNewItemHandler(addedText: string, key: string): void {
    if (this.fieldObject['multiSelect']) {
      switch (key) {
        case 'cities': {
          if ((this.fieldObject['detailList'][1]['dropdownSelected'] && this.fieldObject['detailList'][1]['dropdownSelected'].length === 0) && (this.fieldObject['detailList'][2]['dropdownSelected'] && this.fieldObject['detailList'][2]['dropdownSelected'].length === 0)) {
            this.fieldObject['detailList'][1]['errorFlag'] = true;
            this.fieldObject['detailList'][2]['errorFlag'] = true;
          } else if ((this.fieldObject['detailList'][1]['dropdownSelected'] && this.fieldObject['detailList'][1]['dropdownSelected'].length === 0)) {
            this.fieldObject['detailList'][1]['errorFlag'] = true;
          } else if ((this.fieldObject['detailList'][2]['dropdownSelected'] && this.fieldObject['detailList'][2]['dropdownSelected'].length === 0)) {
            this.fieldObject['detailList'][2]['errorFlag'] = true;
          } else if ((this.fieldObject['detailList'][1]['dropdownSelected'] && this.fieldObject['detailList'][1]['dropdownSelected'].length !== 0) && (this.fieldObject['detailList'][2]['dropdownSelected'] && this.fieldObject['detailList'][2]['dropdownSelected'].length !== 0)) {
            this.fieldObject['detailList'][1]['errorFlag'] = false;
            this.fieldObject['detailList'][2]['errorFlag'] = false;
            this.addnewItemEmitter.emit({ 'key': key, 'name': addedText, 'stateId': this.fieldObject['detailList'][1]['dropdownSelected'][0]['id'], 'countryId': this.fieldObject['detailList'][2]['dropdownSelected'][0]['id'] });
          }
          break;
        }
        case 'states': {
          if (this.fieldObject['detailList'][2]['dropdownSelected'] && this.fieldObject['detailList'][2]['dropdownSelected'].length === 0) {
            this.fieldObject['detailList'][2]['errorFlag'] = true;
          }
          if (this.fieldObject['detailList'][2]['dropdownSelected'] && this.fieldObject['detailList'][2]['dropdownSelected'].length !== 0) {
            this.fieldObject['detailList'][2]['errorFlag'] = false;
            this.addnewItemEmitter.emit({ 'key': key, 'name': addedText, 'countryId': this.fieldObject['detailList'][2]['dropdownSelected'][0]['id'] });
          }
          break;
        }
        case 'countries': {
          this.addnewItemEmitter.emit({ 'key': key, 'name': addedText });
          break;
        }
      }
    } else {
      this.addnewItemEmitter.emit({ 'key': key, 'name': addedText });
    }
  }

  // navigate to previous fieldset
  backHandler(): void {
    this.fieldDataOffset--;
    this.fieldObject = this.fieldOrderList[this.fieldDataOffset];
  }

  // set vlue for input
  updateValue(event): void {
     this.inputValue = event.target.value;
  }

  // validate the required fields
  validateField(): boolean {
    // select box validation
    if (this.fieldObject['selectFlag']) {
      if (this.fieldDataModel && this.fieldDataModel.length !== 0) {
        this.fieldObject['errorFlag'] = false;
        return true;
      } else {
        this.fieldObject['errorFlag'] = true;
        return false;
      }
    } else {
      if (this.inputValue) {
        this.fieldObject['errorFlag'] = false;
        return true;
      } else {
        this.fieldObject['errorFlag'] = true;
        return false;
      }
    }
  }

  // navigate to next tile
  proceedNext(dataKey: string, skipFlag?: boolean): void {
    // do validate the input
    const flag = this.validateField();
    // do save the details in array
    if (flag || skipFlag) {
      const obj = {};
      if (skipFlag) {
        obj[dataKey] = null;
        // this.selectedDataEmitter.emit(obj);
      } else {
        if (this.fieldObject['selectFlag']) {
          obj[dataKey] = this.fieldDataModel;
          // store vlues
          this.dataObject[dataKey] = (this.fieldDataModel[0]['name']).toLowerCase();
          this.fieldDataModel = [];
        } else if (this.fieldObject['inputFlag'] || this.fieldObject['textareaFlag']) {
          obj[dataKey] = this.fieldObject['data'];
          this.dataObject[dataKey] = this.fieldObject['data'];
        }
        this.selectedDataEmitter.emit({'key': dataKey, 'data': obj});
      }
      
      if (this.fieldObject['progressText'] === "Done" || (!this.dataObject['knownCandidate']) || (obj['knownCandidate'] && obj['knownCandidate'].length !== 0 && obj['knownCandidate'][0]['id'] === 0)) {
        this.fieldObject = null;
        this.submitEmitter.emit();
        return;
      }
      // dependency field check
      if (this.fieldDataOffset < this.fieldDataList.length) {
        this.fieldDataOffset++;
        let currentObject = this.fieldDataList[this.fieldDataOffset];
        // dependency check
        if (currentObject['dependentKey'] && currentObject['dependentKey'].length > 0) {
          const offset = this.fieldDataOffset;
          let flag = false;
          for (let j = offset; j < this.fieldDataList.length; j++) {
            if (!flag) {
              for (let i = 0; i < currentObject['dependentKey'].length; i++) {
                if ((this.dataObject[currentObject['dependentKey'][i]['key']] === currentObject['dependentKey'][i]['value'] && !currentObject['notFlag']) ||
                    (this.dataObject[currentObject['dependentKey'][i]['key']] !== currentObject['dependentKey'][i]['value']  && currentObject['notFlag'])) {
                     this.fieldObject = currentObject;
                     this.fieldOrderList.push(this.fieldObject);
                     flag = true;
                     break;
                } else {
                  this.fieldDataOffset++;
                  currentObject = this.fieldDataList[this.fieldDataOffset];
                }
             }
            }
          }
        } else {
          this.fieldObject = currentObject;
        }
      } else {
        this.fieldObject = null;
      }
    }
  }

  // skip current data
  skipCurrent(obj: object): void {
    this.proceedNext(obj['key'], true);
  }

  // skill all
  skipAll(obj: object): void {
    this.fieldObject = null;
    this.submitEmitter.emit();
  }

}
