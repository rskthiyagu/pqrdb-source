import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldDataAlertComponent } from './field-data-alert.component';

describe('FieldDataAlertComponent', () => {
  let component: FieldDataAlertComponent;
  let fixture: ComponentFixture<FieldDataAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldDataAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldDataAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
