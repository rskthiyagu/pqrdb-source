import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-client-tile',
  templateUrl: './client-tile.component.html',
  styleUrls: ['./client-tile.component.scss']
})
export class ClientTileComponent implements OnInit {

  @Input() set clientList(data) {
    if (data && data.length !== 0) {
      for (let i = 0; i < data.length; i++) {
        data[i]['heightValue'] = 155;
      }
      this.clientDataList = data;
    } else {
      this.clientDataList = [];
    }
  }

  @Output() navigationEmitter = new EventEmitter();

  clientDataList: Array < object >;

  constructor() { }

  ngOnInit() {
  }

  // toggle view more detail
  toggleViewDetail(event, offset, flag): void {
    event.stopPropagation();
    if (flag) {
      this.clientDataList[offset]['viewDetailFlag'] = true;
      this.clientDataList[offset]['heightValue'] = 341;
    } else {
      this.clientDataList[offset]['viewDetailFlag'] = false;
      this.clientDataList[offset]['heightValue'] = 155;
    }
  }

  // navigate candidate view page
  navigateCandidateView(id: number, data: object): void {
    this.navigationEmitter.emit({'id': id, 'data': data});
  }


}
