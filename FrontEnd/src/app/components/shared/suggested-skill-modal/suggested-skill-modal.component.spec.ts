import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestedSkillModalComponent } from './suggested-skill-modal.component';

describe('SuggestedSkillModalComponent', () => {
  let component: SuggestedSkillModalComponent;
  let fixture: ComponentFixture<SuggestedSkillModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuggestedSkillModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestedSkillModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
