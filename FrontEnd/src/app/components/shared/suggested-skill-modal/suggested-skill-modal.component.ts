import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-suggested-skill-modal',
  templateUrl: './suggested-skill-modal.component.html',
  styleUrls: ['./suggested-skill-modal.component.scss']
})
export class SuggestedSkillModalComponent implements OnInit {

  @Input() displayFlag: boolean;
  @Input() skillList: Array < string > = [];

  @Output() addSkill = new EventEmitter();
  @Output() addAllSkillsEmitter = new EventEmitter();
  @Output() closeModalEmitter = new EventEmitter();

  editFlag: boolean = false;
  editable: string = '';
  editSkillId: number;
  editableData: string = '';

  constructor() { }

  ngOnInit() {
  }

  // editing a skill
  editSkill(skillData: string, id: number): void {
    this.editFlag = true;
    this.editSkillId = id;
    this.editable = skillData;
    this.editableData = skillData;
  }

  // update skill data
  updateSkill(): void {
    const skillList = this.skillList;
    skillList[this.editSkillId] = this.editable;
    this.skillList = skillList;
    this.editable = '';
    this.editableData = '';
    this.editFlag = false;
  }

  // cancel edit mode
  cancelEdit(): void {
    this.editFlag = false;
    this.editable = '';
    this.editableData = '';
  }

  // adding a skill
  addSkills(skillData: string, id: number): void {
    this.skillList.splice(id, 1);
    this.addSkill.emit(skillData);
  }

  // adding all skills
  addAllSkills(): void {
    this.addAllSkillsEmitter.emit(this.skillList);
    this.skillList = [];
  }

  // close modal
  closeModal(): void {
    this.displayFlag = false;
    this.closeModalEmitter.emit();
  }

}
