import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { ApiService } from '../../../services/api.service';
import { DataService } from '../../../services/data.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-personal-detail',
  templateUrl: './personal-detail.component.html',
  styleUrls: ['./personal-detail.component.scss']
})
export class PersonalDetailComponent implements OnInit {

  @Input() set userDetails(data) {
    this.userDataBackup = data;
    this.userRegister = data;
  }

  viewFlag: boolean = true;
  userDataBackup: object;
  userRegister: any = {
    'firstName': '',
    'lastName': '',
    'userName': '',
    'designation': '',
    'fbLink': '',
    'twitterLink': '',
    'linkedinLink': ''
  };
  requiredFlag: boolean = false;
  loggedUserDetails: object;

  constructor(private toastrService: ToastrService,
              private apiService: ApiService,
              private dataService: DataService,
              private utilityService: UtilityService,
              private router: Router) { }

  ngOnInit() {
    const userData = JSON.parse(localStorage.getItem('userDetails'));
    const localUserData = this.dataService.getLoggedUserDetails();
    if (userData) {
      this.dataService.setLoggedUserDetails(userData);
      this.loggedUserDetails = userData;
    } else if (!userData && !localUserData) {
      this.router.navigateByUrl('');
    } else {
      this.loggedUserDetails = localUserData;
    }
  }

  // navigate the edit mode
  navigateEditMode(): void {
    this.viewFlag = false;
  }

  // validate mandatory fields
  validateMandatoryField(): boolean {
    if (this.userRegister &&
        (this.userRegister['firstName'] && this.userRegister['firstName'] !== '') &&
        (this.userRegister['lastName'] && this.userRegister['lastName'] !== '') &&
        (this.userRegister['userName'] && this.userRegister['userName'] !== '') &&
        (this.userRegister['mobileNumber'] && this.userRegister['mobileNumber'] !== '') &&
        (this.userRegister['email'] && this.userRegister['email'] !== '')) {
      return true;
    } else {
      this.requiredFlag = true;
      return false;
    }
  }

  // reset the fields
  resetField(): void {
    this.userRegister = {
      'firstName': '',
      'lastName': '',
      'userName': '',
      'designation': '',
      'fbLink': '',
      'twitterLink': '',
      'linkedinLink': ''
    }
  }

  // update the user info
  updateUserDetail(): void {
    const flag = this.validateMandatoryField();
    if (flag) {
      const url = '/api/users/update/' + this.loggedUserDetails['id'];
      this.utilityService.updateSpinnerFlag(true);
      this.apiService.putData(url, this.userRegister).subscribe( data => {
        this.utilityService.updateSpinnerFlag(false);
        // this.resetField();
        this.viewFlag = true;
        this.toastrService.success('User Information Updated successful...', 'Success', {timeOut: 1000});
      });
    } else {
      this.toastrService.error("Update Failed...", "Error", { timeOut: 1000});
    }
  }

  // cancel changes
  cancelChanges(): void {
    this.userRegister = this.userDataBackup;
    this.viewFlag = true;
  }

}
