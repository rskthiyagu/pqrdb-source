import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-detail-tile',
  templateUrl: './detail-tile.component.html',
  styleUrls: ['./detail-tile.component.scss']
})
export class DetailTileComponent implements OnInit {

  @Input() set fieldSet(data) {
    console.log(data);
    if (data) {
      let j = 0;
      let value = 10;
      let offset = 1;
      for (let i = 0; i < data.length; i++) {
        data[i]['topValue'] = value;
        if (j == 1) {
          j = 0;
          offset = offset + 2;
          value = offset * 10;
        } else {
          j++;
        }
      }
      this.fieldListArray = data;
      this.fieldSetFlagList.push(false);
    }
  }

  fieldListArray: Array < object >;
  fieldSetFlagList: Array < boolean > = [];

  constructor() { }

  ngOnInit() {
  }

  // enable the detail tile on hover the image
  enableDetailTile(event: any, offset: number): void {
    this.fieldSetFlagList[offset] = true;
  }

  // disable the detail tile on mouse leave
  disableDetailTile(event: any, offset: number): void {
    for (let i = 0; i < this.fieldSetFlagList.length; i++) {
      this.fieldSetFlagList[i] = false;
    }
  }

}
