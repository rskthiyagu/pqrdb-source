import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewTileComponent } from './interview-tile.component';

describe('InterviewTileComponent', () => {
  let component: InterviewTileComponent;
  let fixture: ComponentFixture<InterviewTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
