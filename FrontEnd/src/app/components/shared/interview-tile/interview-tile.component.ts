import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'app-interview-tile',
  templateUrl: './interview-tile.component.html',
  styleUrls: ['./interview-tile.component.scss']
})
export class InterviewTileComponent implements OnInit {

  @Input() set interviewList(data) {
    if (data && data.length !== 0) {
      for (let i = 0; i < data.length; i++) {
        data[i]['heightValue'] = 155;
      }
      this.requirementDataList = data;
    } else {
      this.requirementDataList = [];
    }
  }

  @Output() navigationEmitter = new EventEmitter();

  requirementDataList: Array < object >;

  constructor() { }

  ngOnInit() {
  }

  // toggle view more detail
  toggleViewDetail(event, offset, flag): void {
    event.stopPropagation();
    if (flag) {
      this.requirementDataList[offset]['viewDetailFlag'] = true;
      this.requirementDataList[offset]['heightValue'] = 341;
    } else {
      this.requirementDataList[offset]['viewDetailFlag'] = false;
      this.requirementDataList[offset]['heightValue'] = 155;
    }
  }

  // navigate candidate view page
  navigateCandidateView(id: number, data: object): void {
    this.navigationEmitter.emit({'candidateId': data['candidateId'], 'requirementId': data['requirementId']});
  }


}
