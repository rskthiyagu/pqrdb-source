import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/core/login/login.component';
import { UserDashboardComponent } from './components/core/user-dashboard/user-dashboard.component';
import { UserListComponent} from './components/core/user-list/user-list.component';
import { UserProfileComponent } from './components/core/user-profile/user-profile.component';
import { UserSettingsComponent } from './components/core/user-settings/user-settings.component';
import { UserReportComponent } from './components/core/user-report/user-report.component';
import { UserAccessComponent } from './components/core/user-access/user-access.component';
import { UserDetailComponent } from './components/core/user-detail/user-detail.component';
import { CandidateReportComponent } from './components/core/candidate-report/candidate-report.component';
import { SecurityComponent } from './components/core/security/security.component';
import { DetailUpdateComponent } from './components/core/detail-update/detail-update.component';
import { AddNewUserComponent } from './components/core/add-new-user/add-new-user.component';
import { ClientDashboardComponent } from './components/core/client-dashboard/client-dashboard.component';
import { RequirementDashboardComponent } from './components/core/requirement-dashboard/requirement-dashboard.component';
import { InterviewDashboardComponent } from './components/core/interview-dashboard/interview-dashboard.component';
import { CandidateDashboardComponent } from './components/core/candidate-dashboard/candidate-dashboard.component';
import { PendingCandidateComponent } from './components/core/pending-candidate/pending-candidate.component';
import { CandidateViewEditComponent } from './components/core/candidate-view-edit/candidate-view-edit.component';
import { ClientViewEditComponent } from './components/core/client-view-edit/client-view-edit.component';
import { RequirementViewEditComponent } from './components/core/requirement-view-edit/requirement-view-edit.component';
import { SubmittalDashboardComponent } from './components/core/submittal-dashboard/submittal-dashboard.component';


const routes: Routes = [
  {
    'path': '',
    'component': LoginComponent
  },
  {
    'path': 'dashboard',
    'component': UserDashboardComponent
  },
  {
    'path': 'users-dashboard',
    'component': UserListComponent
  },
  {
    'path': 'profile',
    'component': UserProfileComponent
  },
  {
    'path': 'settings',
    'component': UserSettingsComponent
  },
  {
    'path': 'user-report',
    'component': UserReportComponent
  },
  {
    'path': 'user-access',
    'component': UserAccessComponent
  },
  {
    'path': 'candidate-report',
    'component': CandidateReportComponent
  },
  {
    'path': 'security',
    'component': SecurityComponent
  },
  {
    'path': 'update-details',
    'component': DetailUpdateComponent
  },
  {
    'path': 'user-detail/:userId',
    'component': UserDetailComponent
  },
  {
    'path': 'add-new-user',
    'component': AddNewUserComponent
  },
  {
    'path': 'client-dashboard',
    'component': ClientDashboardComponent
  },
  {
    'path': 'client-view/:clientId',
    'component': ClientViewEditComponent
  },
  {
    'path': 'client-edit/:clientId',
    'component': ClientViewEditComponent
  },
  {
    'path': 'client-create',
    'component': ClientViewEditComponent
  },
  {
    'path': 'requirement-dashboard',
    'component': RequirementDashboardComponent
  },
  {
    'path': 'requirement-view/:requirementId',
    'component': RequirementViewEditComponent
  },
  {
    'path': 'requirement-edit/:requirementId',
    'component': RequirementViewEditComponent
  },
  {
    'path': 'requirement-create',
    'component': RequirementViewEditComponent
  },
  {
    'path': 'interview-dashboard',
    'component': InterviewDashboardComponent
  },
  {
    'path': 'candidate-dashboard',
    'component': CandidateDashboardComponent
  },
  {
    'path': 'pending-candidate-dashboard',
    'component': PendingCandidateComponent
  },
  {
    'path': 'candidate-view/:candidateId',
    'component': CandidateViewEditComponent
  },
  {
    'path': 'candidate-edit/:candidateId',
    'component': CandidateViewEditComponent
  },
  {
    'path': 'candidate-create',
    'component': CandidateViewEditComponent
  },
  {
    'path': 'submittal-dashboard',
    'component': SubmittalDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
