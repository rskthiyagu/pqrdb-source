import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  loaderEnableFlag: boolean = false;
  spinnerMethod: any;
  currentUrl: string;
  previousUrl: string;
  pendingCandidateFlag: boolean;

  // private makes it to update only within the service.
  private spinnerFlag = new Subject<boolean>();
  // public variable will be used to only observe the changes
  spinnerFlagObservable = this.spinnerFlag.asObservable();

  constructor(private router: Router) {
    this.currentUrl = this.router.url;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      }
    });
  }

  updateSpinnerFlag(flag: boolean): void {
    this.spinnerFlag.next(flag);
  }

  // method for handling the loader behaviour
  setLoaderMethod(method: any): void {
    this.spinnerMethod = method;
  }

  // navigate to previous route handler
  navigatePreviousRoute(): void {
    // handle candidate dashboard direction flow
    if (this.currentUrl === '/candidate-dashboard') {
      this.router.navigateByUrl('/dashboard');
    } else if (this.currentUrl === '/pending-candidate-dashboard') {
      this.router.navigateByUrl('/dashboard');
    }
    this.router.navigateByUrl(this.previousUrl);
  }

  // setter for pending candidates flag
  setPendingFlag(flag: boolean): void {
    this.pendingCandidateFlag = flag;
  }

  // getter for pending candidates flag
  getPendingFlag(): boolean {
    return this.pendingCandidateFlag;
  }

}
