import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  selectedCandidateDetailObject: object;
  fieldDataArray: Array < object >;
  loggedUser: object;
  userData: object;

  constructor() { }

  // set selected candidate data
  setSelectedCandidateDetail(data: object): void {
    this.selectedCandidateDetailObject = data;
  }

  // get selected candidate data
  getSelectedCandidateDetail(): object {
    return this.selectedCandidateDetailObject;
  }

  // set the field data from dashboard for back up
  setFieldDataList(data: Array < object >): void {
    this.fieldDataArray = data;
  }

  // get the field data from other pages
  getFieldDataList(): Array < object > {
    return this.fieldDataArray;
  }

  // set selected user data
  setUserData(data: object): void {
    this.userData = data;
  }

  // get selected user data
  getUserData(): object {
    return this.userData;
  }

  // set logged user details
  setLoggedUserDetails(userDetails: object): void {
    this.loggedUser = userDetails;
  }

  // get logged user details
  getLoggedUserDetails(): object {
    return this.loggedUser;
  }
}
