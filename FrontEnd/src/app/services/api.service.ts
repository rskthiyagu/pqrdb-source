import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, forkJoin, observable } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { throwError } from 'rxjs';

import { ToastrService } from 'ngx-toastr';

import { environment } from '../../environments/environment';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  constructor(private http: HttpClient,
              private utilityService: UtilityService,
              private toastrService: ToastrService) { }

  // update url for prod environment
  updateUrl(url: string): string {    
    const updatedUrl = environment['baseUrl'] + url;
    return updatedUrl;
  }

  // get call
  getData(url: string, param?: object): Observable < any > {
    let updatedUrl = this.updateUrl(url);
    if (param) {
      // query string generation
      let queryString = '?';
      const keyList = Object.keys(param);
      for (let i = 0; i < keyList.length; i++) {
        if (i != 0) {
          queryString += "&&";
        }
        queryString += keyList[i] + '=' + param[keyList[i]];
      }
      updatedUrl += queryString;
      return this.http.get(updatedUrl).pipe(
        catchError(this.handleHttpError)
        );;
    } else {
      return this.http.get(updatedUrl).pipe(
        catchError(this.handleHttpError)
        );;
    }
  }

  // fork join get call
  forkGetData(urlList: Array < string >): Observable < any > {
    let responseArray = [];
    for (let i = 0; i < urlList.length; i++) {
      const url = this.updateUrl(urlList[i]);
      responseArray.push(this.http.get(url));
    }
    return forkJoin(responseArray).pipe(
      catchError(this.handleHttpError)
      );;
  }

  // post call
  postData(url: string, param: any): Observable < any > {
    const updatedUrl = this.updateUrl(url);
    return this.http.post(updatedUrl, param).pipe(
      catchError(this.handleHttpError)
      );;
  }

  // post call
  putData(url: string, param: any): Observable < any > {
    const updatedUrl = this.updateUrl(url);
    return this.http.put(updatedUrl, param).pipe(
      catchError(this.handleHttpError)
      );;
  }
  
  // delete call
  deleteData(url: string): Observable < any > {
	const updatedUrl = this.updateUrl(url);
	return this.http.delete(updatedUrl).pipe(
    catchError(this.handleHttpError)
    );
  }

  handleHttpError = (error: HttpErrorResponse) => {
    this.utilityService.updateSpinnerFlag(false);
    this.toastrService.error('Something went Wrong. Please Reload & try again...', 'Server Error', { timeOut: 8000 });
    return Observable.throw(error || 'Server error')
  }

}
