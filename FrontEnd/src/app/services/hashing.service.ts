import { Injectable } from '@angular/core';
import * as bcrypt from 'bcryptjs';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class HashingService {

  salt: number = 10;
  hashData: string;
  encryptKey: string = '123456$#@$^@1ERF';

  constructor() {}

  // get the hash for password
  getHash(password: string): any {
    return bcrypt.hash(password, this.salt);
  }

  // encrypt the password
  getEncryptValue(password: string): string {
      var key = CryptoJS.enc.Utf8.parse(this.encryptKey);
      var iv = CryptoJS.enc.Utf8.parse(this.encryptKey);
      var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(password.toString()), key,
      {
          keySize: 128 / 8,
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
      });
  
      return encrypted.toString();
  }
}
