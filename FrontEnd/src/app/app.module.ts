import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ToastrModule } from 'ngx-toastr';
import { MomentModule } from 'ngx-moment';
import { DragDropModule } from '@angular/cdk/drag-drop';
import * as bcrypt from 'bcryptjs';
import { NgxDropzoneModule } from 'ngx-dropzone';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { LoginComponent } from './components/core/login/login.component';
import { CandidateDashboardComponent } from './components/core/candidate-dashboard/candidate-dashboard.component';
import { CandidateViewEditComponent } from './components/core/candidate-view-edit/candidate-view-edit.component';
import { FilterComponent } from './components/shared/filter/filter.component';
import { FieldDataAlertComponent } from './components/shared/field-data-alert/field-data-alert.component';
import { DetailTileComponent } from './components/shared/detail-tile/detail-tile.component';
import { CandidateTileComponent } from './components/shared/candidate-tile/candidate-tile.component';
import { FilterSectionComponent } from './components/shared/filter-section/filter-section.component';
import { DetailSectionComponent } from './components/shared/detail-section/detail-section.component';
import { DataTableComponent } from './components/shared/data-table/data-table.component';
import { UserDashboardComponent } from './components/core/user-dashboard/user-dashboard.component';
import { TeamComponent } from './components/shared/team/team.component';
import { PendingCandidateComponent } from './components/core/pending-candidate/pending-candidate.component';
import { PendingCandidateTileComponent } from './components/shared/pending-candidate-tile/pending-candidate-tile.component';
import { UserListComponent } from './components/core/user-list/user-list.component';
import { PieChartComponent } from './components/shared/pie-chart/pie-chart.component';
import { ModalComponent } from './components/shared/modal/modal.component';
import { CandidateShortTileComponent } from './components/shared/candidate-short-tile/candidate-short-tile.component';
import { OverviewTileComponent } from './components/shared/overview-tile/overview-tile.component';
import { UserProfileComponent } from './components/core/user-profile/user-profile.component';
import { PersonalDetailComponent } from './components/shared/personal-detail/personal-detail.component';
import { ChangePasswordComponent } from './components/shared/change-password/change-password.component';
import { UserBasicDetailComponent } from './components/shared/user-basic-detail/user-basic-detail.component';
import { UserSettingsComponent } from './components/core/user-settings/user-settings.component';
import { SecurityComponent } from './components/core/security/security.component';
import { CandidateReportComponent } from './components/core/candidate-report/candidate-report.component';
import { UserReportComponent } from './components/core/user-report/user-report.component';
import { DetailUpdateComponent } from './components/core/detail-update/detail-update.component';
import { UserAccessComponent } from './components/core/user-access/user-access.component';
import { UserDetailComponent } from './components/core/user-detail/user-detail.component';
import { AddNewUserComponent } from './components/core/add-new-user/add-new-user.component';
import { CandidateDetailModalComponent } from './components/shared/candidate-detail-modal/candidate-detail-modal.component';
import { ClientDashboardComponent } from './components/core/client-dashboard/client-dashboard.component';
import { RequirementDashboardComponent } from './components/core/requirement-dashboard/requirement-dashboard.component';
import { InterviewDashboardComponent } from './components/core/interview-dashboard/interview-dashboard.component';
import { RequirementTileComponent } from './components/shared/requirement-tile/requirement-tile.component';
import { ClientTileComponent } from './components/shared/client-tile/client-tile.component';
import { PendingUserShortTileComponent } from './components/shared/pending-user-short-tile/pending-user-short-tile.component';
import { SuggestedSkillModalComponent } from './components/shared/suggested-skill-modal/suggested-skill-modal.component';
import { ClientViewEditComponent } from './components/core/client-view-edit/client-view-edit.component';
import { RequirementViewEditComponent } from './components/core/requirement-view-edit/requirement-view-edit.component';
import { SubmittalDashboardComponent } from './components/core/submittal-dashboard/submittal-dashboard.component';
import { InterviewTileComponent } from './components/shared/interview-tile/interview-tile.component';
import { SubmittalTileComponent } from './components/shared/submittal-tile/submittal-tile.component';
import { InterviewModalComponent } from './components/shared/interview-modal/interview-modal.component';
import { HistoryModalComponent } from './components/shared/history-modal/history-modal.component';
import { SubmitCancelModalComponent } from './components/shared/submit-cancel-modal/submit-cancel-modal.component';
import { DetailTableComponent } from './components/shared/detail-table/detail-table.component';
import { DocumentViewerPopUpComponent } from './components/shared/document-viewer-pop-up/document-viewer-pop-up.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    CandidateDashboardComponent,
    CandidateViewEditComponent,
    FilterComponent,
    FieldDataAlertComponent,
    DetailTileComponent,
    CandidateTileComponent,
    FilterSectionComponent,
    DetailSectionComponent,
    DataTableComponent,
    UserDashboardComponent,
    TeamComponent,
    PendingCandidateComponent,
    PendingCandidateTileComponent,
    UserListComponent,
    PieChartComponent,
    ModalComponent,
    CandidateShortTileComponent,
    OverviewTileComponent,
    UserProfileComponent,
    PersonalDetailComponent,
    ChangePasswordComponent,
    UserBasicDetailComponent,
    UserSettingsComponent,
    SecurityComponent,
    CandidateReportComponent,
    UserReportComponent,
    DetailUpdateComponent,
    UserAccessComponent,
    UserDetailComponent,
    AddNewUserComponent,
    CandidateDetailModalComponent,
    ClientDashboardComponent,
    RequirementDashboardComponent,
    InterviewDashboardComponent,
    RequirementTileComponent,
    ClientTileComponent,
    PendingUserShortTileComponent,
    SuggestedSkillModalComponent,
    ClientViewEditComponent,
    RequirementViewEditComponent,
    SubmittalDashboardComponent,
    InterviewTileComponent,
    SubmittalTileComponent,
    InterviewModalComponent,
    HistoryModalComponent,
    SubmitCancelModalComponent,
    DetailTableComponent,
    DocumentViewerPopUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    NgxDocViewerModule,
    AngularMultiSelectModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MomentModule,
    DragDropModule,
    NgxDropzoneModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  schemas: [ NO_ERRORS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
