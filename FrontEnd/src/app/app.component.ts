import { Component } from '@angular/core';

import { UtilityService } from './services/utility.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  spinnerFlag: boolean = false;

  constructor(public utilityService: UtilityService) { }

  ngOnInit() {
    this.trackSpinnerOption();
    this.utilityService.setLoaderMethod(this.emulateLoader);
  }

  // tracking the spinner flag
  trackSpinnerOption(): void {
    this.utilityService.spinnerFlagObservable.subscribe((flag) => {
      this.spinnerFlag = flag;
    });
  }

  // enable / disable the loader icon
  emulateLoader(flag: boolean): void {
    this.spinnerFlag = flag;
  }

}
